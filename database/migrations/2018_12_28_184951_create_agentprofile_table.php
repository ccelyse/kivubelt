<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAgentprofileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agentprofile', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('companyprofileid');
            $table->foreign('companyprofileid')->references('id')->on('users')->onDelete('cascade');
            $table->longText('agent_about')->nullable();;
            $table->string('agent_profilepicture')->nullable();;
            $table->string('agent_coverpicture')->nullable();;
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agentprofile');
    }
}
