<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCountryprofileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('countryprofile', function (Blueprint $table) {
            $table->increments('id');
            $table->string('countryname');
            $table->longText('countrygeneralinformation');
            $table->longText('countrymap');
            $table->string('countrylanguage');
            $table->string('countrycurrency');
            $table->string('coverimage');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('countryprofile');
    }
}
