<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDestinationfieldsToAddDestination extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('AddDestination', function($table) {
//            $table->string('destinationphotocredit');
            $table->string('destinationcoverimage');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('AddDestination', function($table) {
//            $table->string('destinationphotocredit');
            $table->string('destinationcoverimage');
        });
    }
}
