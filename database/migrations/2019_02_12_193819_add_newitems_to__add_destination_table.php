<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewitemsToAddDestinationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('countryprofile', function($table) {
            $table->string('destinationcapital');
            $table->string('destinationpopulation');
            $table->string('destinationsurfacearea');
            $table->string('destinationflag');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('AddDestination', function($table) {
            $table->string('destinationcapital');
            $table->string('destinationpopulation');
            $table->string('destinationsurfacearea');
            $table->string('destinationflag');
            $table->string('destinationclimate');
        });
    }
}
