<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDiscoveractivityTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('discoveractivity', function (Blueprint $table) {
            $table->increments('id');
            $table->string('discoveractivity_title');
            $table->string('discoveractivity_coverimage');
            $table->string('discoveractivity_generalpicture');
            $table->string('discoveractivity_category');
            $table->longText('discoveractivity_text');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('discoveractivity');
    }
}
