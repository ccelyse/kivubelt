<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEatanddrinkTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('eatanddrink', function (Blueprint $table) {
            $table->increments('id');
            $table->string('placetitle');
            $table->longText('placegeneralinformation');
            $table->string('coverimage');
            $table->string('generalimage');
            $table->string('placelocation');
            $table->string('placecategory');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('eatanddrink');
    }
}
