<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOperatorProfileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('OperatorProfile', function (Blueprint $table) {
            $table->increments('id');
            $table->longText('agent_about');
            $table->string('agent_emailaddress');
            $table->string('agent_phoneaddress');
            $table->longText('agent_mapddress');
            $table->longText('user_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('OperatorProfile');
    }
}
