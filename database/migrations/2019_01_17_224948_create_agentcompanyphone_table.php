<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAgentcompanyphoneTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agentcompanyphone', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('companyprofileid');
            $table->foreign('companyprofileid')->references('id')->on('users')->onDelete('cascade');
            $table->string('companyphone');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agentcompanyphone');
    }
}
