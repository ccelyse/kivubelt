<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event', function (Blueprint $table) {
            $table->increments('id');
            $table->string('eventname');
            $table->longtext('eventdescription');
            $table->string('eventstartdate');
            $table->string('eventstarttime');
            $table->string('eventenddate');
            $table->string('eventendtime');
            $table->string('eventimagename');
            $table->string('eventlocation');
            $table->string('eventuserid');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('event');
    }
}
