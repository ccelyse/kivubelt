<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAgentcompanyaddressTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agentcompanyaddress', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('companyprofileid');
            $table->foreign('companyprofileid')->references('id')->on('users')->onDelete('cascade');
            $table->string('companystreetaddress');
            $table->string('companyofficebuiling');
            $table->string('companycity');
            $table->string('companystateprovince');
            $table->string('companyzipcode');
            $table->string('companycountry');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agentcompanyaddress');
    }
}
