<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDestinationthingstodoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('destinationthingstodo', function (Blueprint $table) {
            $table->increments('id');
            $table->string('destinationid');
            $table->string('thingstodo');
            $table->string('thingstodoimage');
            $table->string('thingstododescription');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('destinationthingstodo');
    }
}
