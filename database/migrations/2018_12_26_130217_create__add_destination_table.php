<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddDestinationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('AddDestination', function (Blueprint $table) {
            $table->increments('id');
            $table->string('destinationtitle');
            $table->longText('destinationgeneralinformation');
            $table->longText('destinationmap');
            $table->longText('destinationcountry');
            $table->string('coverimage');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('AddDestination');
    }
}
