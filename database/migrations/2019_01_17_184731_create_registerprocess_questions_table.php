<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegisterprocessQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('registerprocess_questions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('questions_title');
            $table->longText('questions');
            $table->unsignedInteger('category_name_id');
            $table->foreign('category_name_id')->references('id')->on('registerprocess_cat_name')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('registerprocess_questions');
    }
}
