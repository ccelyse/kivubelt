<!DOCTYPE html>
<html lang="en-US">
<head>

    <meta property="og:url" content="https://kivubelt.travel/"/>
    <meta property="og:type" content="article"/>
    <meta property="og:title" content="Home"/>
    <meta property="og:description" content="life-changing extraordinary adventurous Your Journey Begins A journey of a 1000 miles starts with a single step. Import the full demo content with 1 click and create a head-turning website for your travel agency. When? January February March April May June July August September October November December Travel Type Adventure Cultural Discovery Historical Seaside Self-Guided Wildlife var htmlDiv = document.getElementById(&quot;rs-plugin-settings-inline-css&quot;); var htmlDivCss=&quot;&quot;; if(htmlDiv) { htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss; }else{ var htmlDiv = document.createElement(&quot;div&quot;); htmlDiv.innerHTML = &quot;&quot; + htmlDivCss + &quot;&quot;; document.getElementsByTagName(&quot;head&quot;)[0].appendChild(htmlDiv.childNodes[0]); } var htmlDiv = document.getElementById(&quot;rs-plugin-settings-inline-css&quot;); var [...]"/>
    <meta property="og:image" content="https://kivubelt.travel/newfrontend/wp-content/uploads/2018/02/logo-light.png"/>


    <meta charset="UTF-8"/>
    <link rel="profile" href="http://gmpg.org/xfn/11"/>

    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=yes">
    <title>@yield('title') | The crest of adventure</title>
    <script type="application/javascript">var mkdfToursAjaxURL = "https://wanderers.mikado-themes.com/wp-admin/admin-ajax.php"</script>
    <!-- Google Tag Manager for WordPress by gtm4wp.com -->
    <script data-cfasync="false" type="text/javascript">//<![CDATA[
        var gtm4wp_datalayer_name = "dataLayer";
        var dataLayer = dataLayer || [];
        //]]>
    </script>
    <!-- End Google Tag Manager for WordPress by gtm4wp.com --><link rel='dns-prefetch' href='https://apis.google.com' />
    <link rel='dns-prefetch' href='https://toolbar.qodeinteractive.com' />
    <link rel='dns-prefetch' href='https://maps.googleapis.com' />
    <link rel='dns-prefetch' href='https://fonts.googleapis.com' />
    <link rel='dns-prefetch' href='https://s.w.org' />


    <style type="text/css">img.wp-smiley,
        img.emoji {
            display: inline !important;
            border: none !important;
            box-shadow: none !important;
            height: 1em !important;
            width: 1em !important;
            margin: 0 .07em !important;
            vertical-align: -0.1em !important;
            background: none !important;
            padding: 0 !important;
        }
    </style>
    <link rel='stylesheet' id='js_composer_front-css'  href='newfrontend/wp-content/plugins/js_composer/assets/css/js_composer.min.css' type='text/css' media='all' />
    <link rel='stylesheet' id='contact-form-7-css'  href='newfrontend/wp-content/plugins/contact-form-7/includes/css/styles.css' type='text/css' media='all' />
    <link rel='stylesheet' id='mkdf-membership-style-css'  href='newfrontend/wp-content/plugins/mkdf-membership/assets/css/membership.min.css' type='text/css' media='all' />
    <link rel='stylesheet' id='mkdf-membership-responsive-style-css'  href='newfrontend/wp-content/plugins/mkdf-membership/assets/css/membership-responsive.min.css' type='text/css' media='all' />
    <link rel='stylesheet' id='wanderers-mkdf-modules-css'  href='newfrontend/wp-content/themes/wanderers/assets/css/modules.min.css' type='text/css' media='all' />
    <link rel='stylesheet' id='mkdf-tours-style-css'  href='newfrontend/wp-content/plugins/mkdf-tours/assets/css/tours.min.css' type='text/css' media='all' />
    <link rel='stylesheet' id='wanderers-mkdf-modules-responsive-css'  href='newfrontend/wp-content/themes/wanderers/assets/css/modules-responsive.min.css' type='text/css' media='all' />
    <link rel='stylesheet' id='mkdf-tours-responsive-style-css'  href='newfrontend/wp-content/plugins/mkdf-tours/assets/css/tours-responsive.min.css' type='text/css' media='all' />
    <link rel='stylesheet' id='nouislider-css'  href='newfrontend/wp-content/plugins/mkdf-tours/assets/css/nouislider.min.css' type='text/css' media='all' />
    <link rel='stylesheet' id='rabbit_css-css'  href='https://toolbar.qodeinteractive.com/_toolbar/assets/css/rbt-modules.css?ver=4.9.10' type='text/css' media='all' />
    <link rel='stylesheet' id='rs-plugin-settings-css'  href='newfrontend/wp-content/plugins/revslider/public/assets/css/settings.css' type='text/css' media='all' />
    <style id='rs-plugin-settings-inline-css' type='text/css'>#rs-demo-id {}
    </style>
    <link rel='stylesheet' id='wanderer-mkdf-default-style-css'  href='newfrontend/wp-content/themes/wanderers/style.css' type='text/css' media='all' />
    <link rel='stylesheet' id='mkdf-font_awesome-css'  href='newfrontend/wp-content/themes/wanderers/assets/css/font-awesome/css/font-awesome.min.css' type='text/css' media='all' />
    <link rel='stylesheet' id='mkdf-font_elegant-css'  href='newfrontend/wp-content/themes/wanderers/assets/css/elegant-icons/style.min.css' type='text/css' media='all' />
    <link rel='stylesheet' id='mkdf-ion_icons-css'  href='newfrontend/wp-content/themes/wanderers/assets/css/ion-icons/css/ionicons.min.css' type='text/css' media='all' />
    <link rel='stylesheet' id='mkdf-linea_icons-css'  href='newfrontend/wp-content/themes/wanderers/assets/css/linea-icons/style.css' type='text/css' media='all' />
    <link rel='stylesheet' id='mkdf-linear_icons-css'  href='newfrontend/wp-content/themes/wanderers/assets/css/linear-icons/style.css' type='text/css' media='all' />
    <link rel='stylesheet' id='mkdf-simple_line_icons-css'  href='newfrontend/wp-content/themes/wanderers/assets/css/simple-line-icons/simple-line-icons.css' type='text/css' media='all' />
    <link rel='stylesheet' id='mkdf-dripicons-css'  href='newfrontend/wp-content/themes/wanderers/assets/css/dripicons/dripicons.css' type='text/css' media='all' />
    <link rel='stylesheet' id='mediaelement-css'  href='newfrontend/wp-includes/js/mediaelement/mediaelementplayer-legacy.min.css' type='text/css' media='all' />
    <link rel='stylesheet' id='wp-mediaelement-css'  href='newfrontend/wp-includes/js/mediaelement/wp-mediaelement.min.css' type='text/css' media='all' />
    <link rel='stylesheet' id='wanderers-mkdf-woo-css'  href='newfrontend/wp-content/themes/wanderers/assets/css/woocommerce.min.css' type='text/css' media='all' />
    <style id='wanderers-mkdf-woo-inline-css' type='text/css'>/* generated in /home/mikadothemes/wanderers/public_html/newfrontend/wp-content/themes/wanderers/framework/admin/options/general/map.php wanderers_mkdf_page_general_style function */
        .page-id-14.mkdf-boxed .mkdf-wrapper { background-attachment: fixed;}

        /* generated in /home/mikadothemes/wanderers/public_html/newfrontend/wp-content/themes/wanderers/functions.php wanderers_mkdf_content_padding_top function */
        .page-id-14 .mkdf-content .mkdf-content-inner > .mkdf-container > .mkdf-container-inner, .page-id-14 .mkdf-content .mkdf-content-inner > .mkdf-full-width > .mkdf-full-width-inner { padding-top: 0px !important;}

        /* generated in /home/mikadothemes/wanderers/public_html/newfrontend/wp-content/themes/wanderers/assets/custom-styles/general-custom-styles.php wanderers_mkdf_smooth_page_transition_styles function */
        .mkdf-smooth-transition-loader { background-color: #ffffff;}


    </style>
    <link rel='stylesheet' id='wanderers-mkdf-woo-responsive-css'  href='newfrontend/wp-content/themes/wanderers/assets/css/woocommerce-responsive.min.css' type='text/css' media='all' />
    <link rel='stylesheet' id='wanderers-mkdf-style-dynamic-css'  href='newfrontend/wp-content/themes/wanderers/assets/css/style_dynamic.css' type='text/css' media='all' />
    <link rel='stylesheet' id='wanderers-mkdf-style-dynamic-responsive-css'  href='newfrontend/wp-content/themes/wanderers/assets/css/style_dynamic_responsive.css' type='text/css' media='all' />
    <link rel='stylesheet' id='wanderers-mkdf-google-fonts-css'  href='https://fonts.googleapis.com/css?family=Cabin%3A300%2C400%2C500%2C600%2C700%2C900%7CPlayfair+Display%3A300%2C400%2C500%2C600%2C700%2C900%7CMontserrat%3A300%2C400%2C500%2C600%2C700%2C900%7CKristi%3A300%2C400%2C500%2C600%2C700%2C900&amp;subset=latin-ext&amp;ver=1.0.0' type='text/css' media='all' />
    <script type='text/javascript' src='newfrontend/wp-includes/js/jquery/jquery.js'></script>
    <script type='text/javascript' src='newfrontend/wp-includes/js/jquery/jquery-migrate.min.js'></script>
    <script type='text/javascript' src='https://apis.google.com/js/platform.js'></script>
    <script type='text/javascript' src='newfrontend/wp-content/plugins/revslider/public/assets/js/jquery.themepunch.tools.min.js'></script>
    <script type='text/javascript' src='newfrontend/wp-content/plugins/revslider/public/assets/js/jquery.themepunch.revolution.min.js'></script>
    <script type='text/javascript'>
        /* <![CDATA[ */
        var wc_add_to_cart_params = {"ajax_url":"\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/?wc-ajax=%%endpoint%%","i18n_view_cart":"View cart","cart_url":"https:\/\/wanderers.mikado-themes.com\/cart\/","is_cart":"","cart_redirect_after_add":"no"};
        /* ]]> */
    </script>
    <script type='text/javascript' src='newfrontend/wp-content/plugins/woocommerce/assets/js/frontend/add-to-cart.min.js'></script>
    <script type='text/javascript' src='newfrontend/wp-content/plugins/duracelltomi-google-tag-manager/js/gtm4wp-form-move-tracker.js'></script>
    <script type='text/javascript' src='newfrontend/wp-content/plugins/js_composer/assets/js/vendors/woocommerce-add-to-cart.js'></script>
    <script type='text/javascript'>
        var mejsL10n = {"language":"en","strings":{"mejs.install-flash":"You are using a browser that does not have Flash player enabled or installed. Please turn on your Flash player plugin or download the latest version from https:\/\/get.adobe.com\/flashplayer\/","mejs.fullscreen-off":"Turn off Fullscreen","mejs.fullscreen-on":"Go Fullscreen","mejs.download-video":"Download Video","mejs.fullscreen":"Fullscreen","mejs.time-jump-forward":["Jump forward 1 second","Jump forward %1 seconds"],"mejs.loop":"Toggle Loop","mejs.play":"Play","mejs.pause":"Pause","mejs.close":"Close","mejs.time-slider":"Time Slider","mejs.time-help-text":"Use Left\/Right Arrow keys to advance one second, Up\/Down arrows to advance ten seconds.","mejs.time-skip-back":["Skip back 1 second","Skip back %1 seconds"],"mejs.captions-subtitles":"Captions\/Subtitles","mejs.captions-chapters":"Chapters","mejs.none":"None","mejs.mute-toggle":"Mute Toggle","mejs.volume-help-text":"Use Up\/Down Arrow keys to increase or decrease volume.","mejs.unmute":"Unmute","mejs.mute":"Mute","mejs.volume-slider":"Volume Slider","mejs.video-player":"Video Player","mejs.audio-player":"Audio Player","mejs.ad-skip":"Skip ad","mejs.ad-skip-info":["Skip in 1 second","Skip in %1 seconds"],"mejs.source-chooser":"Source Chooser","mejs.stop":"Stop","mejs.speed-rate":"Speed Rate","mejs.live-broadcast":"Live Broadcast","mejs.afrikaans":"Afrikaans","mejs.albanian":"Albanian","mejs.arabic":"Arabic","mejs.belarusian":"Belarusian","mejs.bulgarian":"Bulgarian","mejs.catalan":"Catalan","mejs.chinese":"Chinese","mejs.chinese-simplified":"Chinese (Simplified)","mejs.chinese-traditional":"Chinese (Traditional)","mejs.croatian":"Croatian","mejs.czech":"Czech","mejs.danish":"Danish","mejs.dutch":"Dutch","mejs.english":"English","mejs.estonian":"Estonian","mejs.filipino":"Filipino","mejs.finnish":"Finnish","mejs.french":"French","mejs.galician":"Galician","mejs.german":"German","mejs.greek":"Greek","mejs.haitian-creole":"Haitian Creole","mejs.hebrew":"Hebrew","mejs.hindi":"Hindi","mejs.hungarian":"Hungarian","mejs.icelandic":"Icelandic","mejs.indonesian":"Indonesian","mejs.irish":"Irish","mejs.italian":"Italian","mejs.japanese":"Japanese","mejs.korean":"Korean","mejs.latvian":"Latvian","mejs.lithuanian":"Lithuanian","mejs.macedonian":"Macedonian","mejs.malay":"Malay","mejs.maltese":"Maltese","mejs.norwegian":"Norwegian","mejs.persian":"Persian","mejs.polish":"Polish","mejs.portuguese":"Portuguese","mejs.romanian":"Romanian","mejs.russian":"Russian","mejs.serbian":"Serbian","mejs.slovak":"Slovak","mejs.slovenian":"Slovenian","mejs.spanish":"Spanish","mejs.swahili":"Swahili","mejs.swedish":"Swedish","mejs.tagalog":"Tagalog","mejs.thai":"Thai","mejs.turkish":"Turkish","mejs.ukrainian":"Ukrainian","mejs.vietnamese":"Vietnamese","mejs.welsh":"Welsh","mejs.yiddish":"Yiddish"}};
    </script>
    <script type='text/javascript' src='newfrontend/wp-includes/js/mediaelement/mediaelement-and-player.min.js'></script>
    <script type='text/javascript' src='newfrontend/wp-includes/js/mediaelement/mediaelement-migrate.min.js'></script>
    <script type='text/javascript'>
        /* <![CDATA[ */
        var _wpmejsSettings = {"pluginPath":"\/newfrontend/wp-includes\/js\/mediaelement\/","classPrefix":"mejs-","stretching":"responsive"};
        /* ]]> */
    </script>
    <link rel='https://api.w.org/' href='wp-json/index.json' />
    <link rel="EditURI" type="application/rsd+xml" title="RSD" href="xmlrpc.php-rsd.xml" />
    <link rel="wlwmanifest" type="application/wlwmanifest+xml" href="newfrontend/wp-includes/wlwmanifest.xml" />
    <meta name="generator" content="WordPress 4.9.10" />
    <meta name="generator" content="WooCommerce 3.3.4" />
    <link rel="canonical" href="index.html" />
    <link rel='shortlink' href='index.html' />

    <!-- Google Tag Manager for WordPress by gtm4wp.com -->
    <script data-cfasync="false" type="text/javascript">//<![CDATA[
        dataLayer.push({"pagePostType":"frontpage","pagePostType2":"single-page","pagePostAuthor":"admin"});//]]>
    </script>
    <script data-cfasync="false">//<![CDATA[
        (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            '//www.googletagmanager.com/gtm.'+'js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-KTQ2BTD');//]]>
    </script>
    <!-- End Google Tag Manager -->
    <!-- End Google Tag Manager for WordPress by gtm4wp.com -->	<noscript><style>.woocommerce-product-gallery{ opacity: 1 !important; }</style></noscript>
    <meta name="generator" content="Powered by WPBakery Page Builder - drag and drop page builder for WordPress."/>
    <!--[if lte IE 9]><link rel="stylesheet" type="text/css" href="https://wanderers.mikado-themes.com/newfrontend/wp-content/plugins/js_composer/assets/css/vc_lte_ie9.min.css" media="screen"><![endif]--><meta name="generator" content="Powered by Slider Revolution 5.4.7.2 - responsive, Mobile-Friendly Slider Plugin for WordPress with comfortable drag and drop interface." />
    <link rel="icon" href="frontend/assets/images/logok.png" sizes="32x32" />
    <link rel="icon" href="frontend/assets/images/logok.png" sizes="192x192" />
    <link rel="apple-touch-icon-precomposed" href="frontend/assets/images/logok.png" />
    <meta name="msapplication-TileImage" content="frontend/assets/images/logok.png" />
    <script type="text/javascript">function setREVStartSize(e){
            try{ e.c=jQuery(e.c);var i=jQuery(window).width(),t=9999,r=0,n=0,l=0,f=0,s=0,h=0;
                if(e.responsiveLevels&&(jQuery.each(e.responsiveLevels,function(e,f){f>i&&(t=r=f,l=e),i>f&&f>r&&(r=f,n=e)}),t>r&&(l=n)),f=e.gridheight[l]||e.gridheight[0]||e.gridheight,s=e.gridwidth[l]||e.gridwidth[0]||e.gridwidth,h=i/s,h=h>1?1:h,f=Math.round(h*f),"fullscreen"==e.sliderLayout){var u=(e.c.width(),jQuery(window).height());if(void 0!=e.fullScreenOffsetContainer){var c=e.fullScreenOffsetContainer.split(",");if (c) jQuery.each(c,function(e,i){u=jQuery(i).length>0?u-jQuery(i).outerHeight(!0):u}),e.fullScreenOffset.split("%").length>1&&void 0!=e.fullScreenOffset&&e.fullScreenOffset.length>0?u-=jQuery(window).height()*parseInt(e.fullScreenOffset,0)/100:void 0!=e.fullScreenOffset&&e.fullScreenOffset.length>0&&(u-=parseInt(e.fullScreenOffset,0))}f=u}else void 0!=e.minHeight&&f<e.minHeight&&(f=e.minHeight);e.c.closest(".rev_slider_wrapper").css({height:f})
            }catch(d){console.log("Failure at Presize of Slider:"+d)}
        };</script>
    <style type="text/css" id="wp-custom-css">.widget.widget_categories ul li.cat-item-51{
            display: none;
        }

        .mkdf-woo-single-page .star-rating::after {
            content: '';
            background-color: #fff;
            width: 1px;
            height: 100%;
            position: absolute;
            right: 0;
        }
        .mkdf-tour-gallery-item{
            position: relative;
            float: left;
            width: 20%;
            padding: 15px;
            box-sizing: border-box;
        }
        @media only screen and (max-width: 768px) {
            .mkdf-tour-gallery-item{
                position: relative;
                float: left;
                width: 50%;
                padding: 15px;
                box-sizing: border-box;
            }
        }
    </style>
    <link rel="stylesheet" href="newfrontend/assets/css/custom.css">
    <link rel="stylesheet" href="fontawesome/all.css">
    <script src="fontawesome/all.min.js" type="text/javascript"></script>
    <style type="text/css" data-type="vc_shortcodes-custom-css">.vc_custom_1521446912670{background-position: center !important;background-repeat: no-repeat !important;background-size: cover !important;}.vc_custom_1521545714459{padding-top: 108px !important;padding-bottom: 130px !important;}.vc_custom_1521216565077{padding-top: 108px !important;padding-bottom: 100px !important;background-image: url("newfrontend/wp-content/uploads/2018/02/h1-img-6-id=187.jpg") !important;background-position: center !important;background-repeat: no-repeat !important;background-size: cover !important;}.vc_custom_1522239544884{padding-top: 108px !important;padding-bottom: 86px !important;background-position: center !important;background-repeat: no-repeat !important;background-size: cover !important;}.vc_custom_1521449273073{padding-top: 108px !important;padding-bottom: 140px !important;}.vc_custom_1518432717043{margin-top: -59px !important;}.vc_custom_1521556287933{padding-top: 0px !important;padding-bottom: 0px !important;background-position: 0 0 !important;background-repeat: no-repeat !important;}.vc_custom_1521446892225{padding-top: 0px !important;background-image: url("newfrontend/wp-content/uploads/2018/02/h1-background-1-id=125.png") !important;background-position: center !important;background-repeat: no-repeat !important;background-size: cover !important;}.vc_custom_1518168566331{padding-top: 130px !important;padding-bottom: 100px !important;}.vc_custom_1521446919727{padding-bottom: 120px !important;}.vc_custom_1518169901755{padding-top: 0px !important;}.vc_custom_1518169907323{padding-top: 0px !important;}.vc_custom_1518169893412{padding-top: 0px !important;}.vc_custom_1518534876780{background-image: url("newfrontend/wp-content/uploads/2018/02/h1-img-2-id=127.jpg") !important;background-position: center !important;background-repeat: no-repeat !important;background-size: cover !important;}.vc_custom_1521805536861{padding-top: 0px !important;background-image: url("newfrontend/wp-content/uploads/2018/02/h1-img-3-id=128.jpg") !important;background-position: center !important;background-repeat: no-repeat !important;background-size: cover !important;}.vc_custom_1521806228644{padding-top: 0px !important;background-image: url("newfrontend/wp-content/uploads/2018/02/h1-img-4-id=129.jpg") !important;}.vc_custom_1518534932573{background-image: url("newfrontend/wp-content/uploads/2018/02/h1-img-5-id=170.jpg") !important;background-position: center !important;background-repeat: no-repeat !important;background-size: cover !important;}.vc_custom_1522058628416{margin-top: -8px !important;}.vc_custom_1518173740059{padding-top: 0px !important;}.vc_custom_1518432408753{padding-top: 0px !important;}.vc_custom_1518432732162{margin-bottom: -59px !important;}.vc_custom_1518548596679{padding-right: 25px !important;padding-left: 25px !important;background-image: url("newfrontend/wp-content/uploads/2018/02/h1-img-7-id=222.jpg") !important;}.vc_custom_1518178148845{padding-top: 0px !important;}.vc_custom_1518178162203{padding-top: 0px !important;}.vc_custom_1518432670305{padding-top: 60px !important;background-image: url("newfrontend/wp-content/uploads/2018/02/h1-img-8-id=251.jpg") !important;}.vc_custom_1521556270901{padding-top: 0px !important;background: #f7f7f7 url("newfrontend/assets/images/topo-bg-3-black.png") !important;}</style><noscript><style type="text/css">.wpb_animate_when_almost_visible { opacity: 1; }</style></noscript>
</head>
<body class="home page-template page-template-full-width page-template-full-width-php page page-id-14 mkdf-core-1.0 mkdf-social-login-1.0 mkdf-tours-1.0 wanderers-ver-1.0 mkdf-smooth-page-transitions mkdf-smooth-page-transitions-preloader mkdf-grid-1300 mkdf-disable-global-padding-bottom mkdf-light-header mkdf-sticky-header-on-scroll-down-up mkdf-dropdown-animate-height mkdf-header-standard mkdf-menu-area-shadow-disable mkdf-menu-area-in-grid-shadow-disable mkdf-menu-area-border-disable mkdf-menu-area-in-grid-border-disable mkdf-logo-area-border-disable mkdf-header-vertical-shadow-disable mkdf-header-vertical-border-disable mkdf-side-menu-slide-from-right mkdf-woocommerce-columns-3 mkdf-woo-small-space mkdf-woo-pl-info-below-image mkdf-woo-single-thumb-on-left-side mkdf-woo-single-has-pretty-photo mkdf-default-mobile-header mkdf-sticky-up-mobile-header mkdf-header-top-enabled wpb-js-composer js-comp-ver-5.4.7 vc_responsive mkdf-search-covers-header" itemscope itemtype="http://schema.org/WebPage">
@yield('content')
