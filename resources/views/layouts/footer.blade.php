
<footer id="colophone" class="site-footer">
    <div class="t-center site-footer__primary">
        <div class="container">
            <div class="site-footer__logo">
                <a href="index.html">
                    <h1 class="screen-reader-text">Kivu Belt</h1>
                    <img src="frontend/assets/images/logok.png" alt="Listiry" style="width: 100px;">
                </a>
            </div>
            <p class="t-small">Kivu Belt is making finding destination faster, easier, and customized for you.</p>
            <ul class="min-list inline-list site-footer__links site-footer__social">
                <li>
                    <a href="#">
                        {{--<i class="fa fa-facebook-f"></i>--}}
                        {{--<i class="fas fa-facebook"></i>--}}
                        <i class="fab fa-facebook"></i>
                    </a>
                </li>
                <li>
                    <a href="#">
                        {{--<i class="fa fa-twitter"></i>--}}
                        <i class="fab fa-twitter"></i>
                    </a>
                </li>
                <li>
                    <a href="#">
                        {{--<i class="fa fa-instagram"></i>--}}
                        <i class="fab fa-instagram"></i>
                    </a>
                </li>
                <li>
                    <a href="#">
                        {{--<i class="fa fa-youtube"></i>--}}
                        <i class="fab fa-youtube"></i>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <!-- .site-footer__primary -->

    <div class="site-footer__secondary">
        <div class="container">
            <div class="site-footer__secondary-wrapper">
                <p class="site-footer__copyright">&copy; 2019
                    <span class="c-secondary"></span> by Kivu Belt. All Rights Reserved.</p>
                <ul class="min-list inline-list site-footer__links site-footer__details">
                    <li>
                        <a href="tel:+0987654321">Tel: +250 783 384 772</a>
                    </li>
                    <!--<li>-->
                    <!--<a href="index-4.html#">Get this theme</a>-->
                    <!--</li>-->
                    <li>
                        <a href="index-4.html#">About</a>
                    </li>
                    <li>
                        <a href="index-4.html#">Contact</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- .site-footer__secondary -->
</footer><!-- #colophone -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBDyCxHyc8z9gMA5IlipXpt0c33Ajzqix4"></script>
<script src="https://cdn.rawgit.com/googlemaps/v3-utility-library/master/infobox/src/infobox.js"></script>
<script src="frontend/assets/scripts/app.js"></script>
</body>
</html>