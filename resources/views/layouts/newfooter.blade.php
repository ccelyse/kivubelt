
<footer class="mkdf-page-footer">
    <div class="mkdf-footer-bottom-holder">
        <div class="mkdf-footer-bottom-inner mkdf-grid">
            <div class="mkdf-grid-row ">
                <div class="mkdf-grid-col-12">
                    <div class="widget mkdf-separator-widget"><div class="mkdf-separator-holder clearfix  mkdf-separator-center mkdf-separator-normal">
                            <div class="mkdf-separator" style="border-style: solid;margin-top: 0px;margin-bottom: 15px"></div>
                        </div>
                    </div>
                    <div id="text-6" class="widget mkdf-footer-bottom-column-1 widget_text">
                        <div class="textwidget">
                            <h6 style="color: #603b12 !important;">© Copyright 2019 by Kivu Belt. All Rights Reserved.</h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KTQ2BTD"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script type="text/javascript">
    function revslider_showDoubleJqueryError(sliderID) {
        var errorMessage = "Revolution Slider Error: You have some jquery.js library include that comes after the revolution files js include.";
        errorMessage += "<br> This includes make eliminates the revolution slider libraries, and make it not work.";
        errorMessage += "<br><br> To fix it you can:<br>&nbsp;&nbsp;&nbsp; 1. In the Slider Settings -> Troubleshooting set option:  <strong><b>Put JS Includes To Body</b></strong> option to true.";
        errorMessage += "<br>&nbsp;&nbsp;&nbsp; 2. Find the double jquery.js include and remove it.";
        errorMessage = "<span style='font-size:16px;color:#BC0C06;'>" + errorMessage + "</span>";
        jQuery(sliderID).show().html(errorMessage);
    }
</script>
<script type='text/javascript' src='newfrontend/wp-content/plugins/js_composer/assets/js/dist/js_composer_front.min.js'></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var wpcf7 = {"apiSettings":{"root":"https:\/\/wanderers.mikado-themes.com\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"},"recaptcha":{"messages":{"empty":"Please verify that you are not a robot."}},"cached":"1"};
    /* ]]> */
</script>
<script type='text/javascript' src='newfrontend/wp-content/plugins/contact-form-7/includes/js/scripts.js'></script>
<script type='text/javascript' src='newfrontend/wp-includes/js/underscore.min.js'></script>
<script type='text/javascript' src='newfrontend/wp-includes/js/jquery/ui/core.min.js'></script>
<script type='text/javascript' src='newfrontend/wp-includes/js/jquery/ui/widget.min.js'></script>
<script type='text/javascript' src='newfrontend/wp-includes/js/jquery/ui/tabs.min.js'></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var mkdfGlobalVars = {"vars":{"mkdfAddForAdminBar":0,"mkdfElementAppearAmount":-100,"mkdfAjaxUrl":"https:\/\/wanderers.mikado-themes.com\/wp-admin\/admin-ajax.php","mkdfStickyHeaderHeight":70,"mkdfStickyHeaderTransparencyHeight":70,"mkdfTopBarHeight":46,"mkdfLogoAreaHeight":0,"mkdfMenuAreaHeight":126,"mkdfMobileHeaderHeight":70}};
    var mkdfPerPageVars = {"vars":{"mkdfStickyScrollAmount":0,"mkdfHeaderTransparencyHeight":0,"mkdfHeaderVerticalWidth":0}};
    /* ]]> */
</script>
<script type='text/javascript' src='newfrontend/wp-content/themes/wanderers/assets/js/modules.min.js'></script>
<script type='text/javascript' src='newfrontend/wp-content/plugins/mkdf-membership/assets/js/membership.min.js'></script>
<script type='text/javascript' src='newfrontend/wp-content/plugins/mkdf-tours/assets/js/modules/plugins/nouislider.min.js'></script>
<script type='text/javascript' src='newfrontend/wp-content/plugins/mkdf-tours/assets/js/modules/plugins/typeahead.bundle.min.js'></script>
<script type='text/javascript' src='newfrontend/wp-content/plugins/mkdf-tours/assets/js/modules/plugins/bloodhound.min.js'></script>
<script type='text/javascript' src='newfrontend/wp-content/plugins/mkdf-tours/assets/js/modules/plugins/select2.min.js'></script>
<script type='text/javascript' src='newfrontend/wp-includes/js/jquery/ui/datepicker.min.js'></script>
<script type='text/javascript'>
    jQuery(document).ready(function(jQuery){jQuery.datepicker.setDefaults({"closeText":"Close","currentText":"Today","monthNames":["January","February","March","April","May","June","July","August","September","October","November","December"],"monthNamesShort":["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],"nextText":"Next","prevText":"Previous","dayNames":["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"],"dayNamesShort":["Sun","Mon","Tue","Wed","Thu","Fri","Sat"],"dayNamesMin":["S","M","T","W","T","F","S"],"dateFormat":"MM d, yy","firstDay":1,"isRTL":false});});
</script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var mkdfToursSearchData = {"tours":["Ultimate Thailand","Discover India","Forest Adventure","China Tour","Safari Tour","Canada Tour","Pyramids Tour","Mexico Explore","Europe Tour","Berlin Tour","Budapest Tour","Highlights Rome","Diving Adventure","Reindeer Lake","Slovenia Wildlife","European History","Wild Lakes","Seaside Adventure","Discover Rome","Hungary Tour","Germany Tour","City Tour","Explore Mexico","Egypt Tour","Discover Thailand","Exotic Adventure","India Tour","Eastern Tour","Desert Tour","Wildlife Tour","Antique Europe","Awesome Mexico","Ancient Pyramids","Adventure Alaska","Action Safari","Asian Dicovery"],"destinations":["Canada","Mexico","Europe","Japan","China","Thailand","India","Africa"]};
    /* ]]> */
</script>
<script type='text/javascript' src='newfrontend/wp-content/plugins/mkdf-tours/assets/js/tours.min.js'></script>
<script type='text/javascript' src='https://toolbar.qodeinteractive.com/_toolbar/assets/js/jquery.lazy.min.js?ver=4.9.10'></script>
<script type='text/javascript' src='https://toolbar.qodeinteractive.com/_toolbar/assets/js/rbt-modules.js?ver=4.9.10'></script>
<script type='text/javascript' src='newfrontend/wp-content/plugins/woocommerce/assets/js/jquery-blockui/jquery.blockUI.min.js'></script>
<script type='text/javascript' src='newfrontend/wp-content/plugins/woocommerce/assets/js/js-cookie/js.cookie.min.js'></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var woocommerce_params = {"ajax_url":"\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/?wc-ajax=%%endpoint%%"};
    /* ]]> */
</script>
<script type='text/javascript' src='newfrontend/wp-content/plugins/woocommerce/assets/js/frontend/woocommerce.min.js'></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var wc_cart_fragments_params = {"ajax_url":"\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/?wc-ajax=%%endpoint%%","cart_hash_key":"wc_cart_hash_a2e1041dbd20ba843985c2c0042c04ae","fragment_name":"wc_fragments_a2e1041dbd20ba843985c2c0042c04ae"};
    /* ]]> */
</script>
<script type='text/javascript' src='newfrontend/wp-content/plugins/woocommerce/assets/js/frontend/cart-fragments.min.js'></script>
<script type='text/javascript' src='newfrontend/wp-includes/js/jquery/ui/accordion.min.js'></script>
<script type='text/javascript' src='newfrontend/wp-includes/js/mediaelement/wp-mediaelement.min.js'></script>
<script type='text/javascript' src='newfrontend/wp-content/themes/wanderers/assets/js/modules/plugins/jquery.appear.js'></script>
<script type='text/javascript' src='newfrontend/wp-content/themes/wanderers/assets/js/modules/plugins/modernizr.min.js'></script>
<script type='text/javascript' src='newfrontend/wp-includes/js/hoverIntent.min.js'></script>
<script type='text/javascript' src='newfrontend/wp-content/themes/wanderers/assets/js/modules/plugins/jquery.plugin.js'></script>
<script type='text/javascript' src='newfrontend/wp-content/themes/wanderers/assets/js/modules/plugins/owl.carousel.min.js'></script>
<script type='text/javascript' src='newfrontend/wp-content/plugins/js_composer/assets/lib/waypoints/waypoints.min.js'></script>
<script type='text/javascript' src='newfrontend/wp-content/themes/wanderers/assets/js/modules/plugins/fluidvids.min.js'></script>
<script type='text/javascript' src='newfrontend/wp-content/plugins/js_composer/assets/lib/prettyphoto/js/jquery.prettyPhoto.min.js'></script>
<script type='text/javascript' src='newfrontend/wp-content/themes/wanderers/assets/js/modules/plugins/perfect-scrollbar.jquery.min.js'></script>
<script type='text/javascript' src='newfrontend/wp-content/themes/wanderers/assets/js/modules/plugins/ScrollToPlugin.min.js'></script>
<script type='text/javascript' src='newfrontend/wp-content/themes/wanderers/assets/js/modules/plugins/parallax.min.js'></script>
<script type='text/javascript' src='newfrontend/wp-content/themes/wanderers/assets/js/modules/plugins/jquery.waitforimages.js'></script>
<script type='text/javascript' src='newfrontend/wp-content/themes/wanderers/assets/js/modules/plugins/jquery.easing.1.3.js'></script>
<script type='text/javascript' src='newfrontend/wp-content/plugins/js_composer/assets/lib/bower/isotope/dist/isotope.pkgd.min.js'></script>
<script type='text/javascript' src='newfrontend/wp-content/themes/wanderers/assets/js/modules/plugins/packery-mode.pkgd.min.js'></script>
<script type='text/javascript' src='newfrontend/wp-content/plugins/mkdf-core/shortcodes/countdown/assets/js/plugins/jquery.countdown.min.js'></script>
<script type='text/javascript' src='newfrontend/wp-content/plugins/mkdf-core/shortcodes/counter/assets/js/plugins/counter.js'></script>
<script type='text/javascript' src='newfrontend/wp-content/plugins/mkdf-core/shortcodes/counter/assets/js/plugins/absoluteCounter.min.js'></script>
<script type='text/javascript' src='newfrontend/wp-content/plugins/mkdf-core/shortcodes/custom-font/assets/js/plugins/typed.js'></script>
<script type='text/javascript' src='https://maps.googleapis.com/maps/api/js?key=AIzaSyADhFz3a11s8_leQorrsxUXHTQ3IYo-sYM&amp;ver=4.9.10'></script>
<script type='text/javascript' src='newfrontend/wp-includes/js/wp-embed.min.js'></script>



</body>
</html>