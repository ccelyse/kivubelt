<div class="mkdf-top-bar">

    <div class="mkdf-grid">
        <div class="mkdf-vertical-align-containers">
            <div class="mkdf-position-left">
                <div class="mkdf-position-left-inner">
                    <div id="text-4" class="widget widget_text mkdf-top-bar-widget">
                        <div class="textwidget"><p>Follow Us:&nbsp;&nbsp;</p>
                        </div>
                    </div>
                    <a class="mkdf-social-icon-widget-holder mkdf-icon-has-hover" data-hover-color="#ffcc05" style="color: #fff;;font-size: 25px;margin: 0px 8px 0px 1px;" href="https://www.instagram.com/kivubelt_/" target="_blank">
                        <span class="mkdf-social-icon-widget  social_instagram    "></span>		</a>

                    <a class="mkdf-social-icon-widget-holder mkdf-icon-has-hover" data-hover-color="#ffcc05" style="color: #fff;;font-size: 25px;margin: 0px 6px 0px 4px;" href="https://www.facebook.com/destinationkivubelt/" target="_blank">
                        <span class="mkdf-social-icon-widget  social_facebook    "></span>		</a>

                    <a class="mkdf-social-icon-widget-holder mkdf-icon-has-hover" data-hover-color="#ffcc05" style="color: #fff;;font-size: 25px;margin: 0px 4px;" href="https://twitter.com/kivu_belt?lang=de" target="_blank">
                        <span class="mkdf-social-icon-widget  social_twitter    "></span>		</a>

                    <a class="mkdf-social-icon-widget-holder mkdf-icon-has-hover" data-hover-color="#ffcc05" style="color: #fff;;font-size: 25px;margin: 0px 4px;" href="#" target="_blank">
                        <span class="mkdf-social-icon-widget  social_whatsapp"></span>		</a>

                    <div id="text-5" class="widget widget_text mkdf-top-bar-widget">
                        <div class="textwidget">
                            <div class="mkdf-iwt clearfix  mkdf-iwt-icon-left mkdf-iwt-icon-tiny">
                                <a itemprop="url" href="tel:167712444227" target="_blank">
                                    <div class="mkdf-iwt-content" style="padding-left: 14px">
                                        <p class="mkdf-iwt-title" style="color: #fff;font-size: 17px;margin-top: 3px">
                                            <span class="mkdf-iwt-title-text">+250 785257998</span>
                                        </p>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="mkdf-position-right">
                <div class="mkdf-position-right-inner">
                    <div class="widget mkdf-login-register-widget mkdf-user-not-logged-in"><a href="{{"login"}}">
                            <span class="mkdf-login-text">Login</span>
                        </a></div>											</div>
            </div>
        </div>

    </div>

</div>


<header class="mkdf-page-header">

    <div class="mkdf-menu-area mkdf-menu-right">
        <div class="mkdf-grid">
            <div class="mkdf-vertical-align-containers">
                <div class="mkdf-position-left">
                    <div class="mkdf-position-left-inner">
                        <div class="mkdf-logo-wrapper">
                            <a itemprop="url" href="{{url('/')}}" style="height: 70px;">
                                <img itemprop="image" class="mkdf-normal-logo" src="frontend/assets/images/logok.png" width="256" height="82"  alt="logo"/>
                                <img itemprop="image" class="mkdf-dark-logo" src="frontend/assets/images/logok.png" width="256" height="82"  alt="dark logo"/>
                                <img itemprop="image" class="mkdf-light-logo" src="frontend/assets/images/logok.png" width="256" height="82"  alt="light logo"/>    </a>
                        </div>
                    </div>
                </div>
                <div class="mkdf-position-right">
                    <div class="mkdf-position-right-inner">

                        <nav class="mkdf-main-menu mkdf-drop-down mkdf-default-nav">
                            <ul id="menu-main-menu" class="clearfix">

                                <li id="nav-menu-item-34" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-14 current_page_item mkdf-active-item narrow">
                                    <a href="{{url('/')}}" class=" current "><span class="item_outer"><span class="item_text">Home</span></span></a>
                                </li>
                                <li id="nav-menu-item-34" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-14 current_page_item mkdf-active-item narrow">
                                    <a href="{{url('/AboutRwanda')}}" class=" current "><span class="item_outer"><span class="item_text">About Rwanda</span></span></a>
                                </li>
                                <li id="nav-menu-item-34" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-14 current_page_item mkdf-active-item narrow">
                                    <a href="{{url('/AboutRegion')}}" class=" current "><span class="item_outer"><span class="item_text">About the Region</span></span></a>
                                </li>
                                <li id="nav-menu-item-34" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-14 current_page_item mkdf-active-item narrow">
                                    <a href="{{url('DiscoverK')}}" class=" current "><span class="item_outer"><span class="item_text">Discover</span></span></a>
                                </li>
                                <li id="nav-menu-item-34" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-14 current_page_item mkdf-active-item narrow">
                                    <a href="{{url('FStay')}}" class=" current "><span class="item_outer"><span class="item_text">Stay</span></span></a>
                                </li>
                                <li id="nav-menu-item-34" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-14 current_page_item mkdf-active-item narrow">
                                    <a href="" class=" current "><span class="item_outer"><span class="item_text">Eat & Drink</span></span></a>
                                </li>
                                <li id="nav-menu-item-34" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-14 current_page_item mkdf-active-item narrow">
                                    <a href="" class=" current "><span class="item_outer"><span class="item_text">Contact us</span></span></a>
                                </li>

                            </ul>
                        </nav>

                    </div>
                </div>
            </div>

        </div>
    </div>



    <div class="mkdf-sticky-header">
        <div class="mkdf-sticky-holder mkdf-menu-right">
            <div class="mkdf-grid">
                <div class="mkdf-vertical-align-containers">
                    <div class="mkdf-position-left">
                        <div class="mkdf-position-left-inner">
                            <div class="mkdf-logo-wrapper">
                                <a itemprop="url" href="{{url('')}}" style="height: 70px;">
                                    <img itemprop="image" class="mkdf-normal-logo" src="frontend/assets/images/logok.png" width="256" height="82"  alt="logo"/>
                                    <img itemprop="image" class="mkdf-dark-logo" src="frontend/assets/images/logok.png" width="256" height="82"  alt="dark logo"/>
                                    <img itemprop="image" class="mkdf-light-logo" src="newfrontend/wp-content/uploads/2018/02/logo-light.png" width="256" height="82"  alt="light logo"/>    </a>
                            </div>
                        </div>
                    </div>
                    <div class="mkdf-position-right">
                        <div class="mkdf-position-right-inner">

                            <nav class="mkdf-main-menu mkdf-drop-down mkdf-sticky-nav">

                                <ul id="menu-main-menu-1" class="clearfix">
                                    <li id="sticky-nav-menu-item-34" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-14 current_page_item mkdf-active-item narrow">
                                        <a href="{{('/')}}" class=" current "><span class="item_outer"><span class="item_text">Home</span><span class="plus"></span></span></a>
                                    </li>
                                    <li id="sticky-nav-menu-item-34" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-14 current_page_item mkdf-active-item narrow">
                                        <a href="{{url('/AboutRwanda')}}" class=" current "><span class="item_outer"><span class="item_text">About Rwanda</span><span class="plus"></span></span></a>
                                    </li>
                                    <li id="sticky-nav-menu-item-34" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-14 current_page_item mkdf-active-item narrow">
                                        <a href="{{url('/AboutRegion')}}" class=" current "><span class="item_outer"><span class="item_text">About the Region</span><span class="plus"></span></span></a>
                                    </li>
                                    <li id="sticky-nav-menu-item-34" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-14 current_page_item mkdf-active-item narrow">
                                        <a href="{{url('DiscoverK')}}" class=" current "><span class="item_outer"><span class="item_text">Discover</span><span class="plus"></span></span></a>
                                    </li>
                                    <li id="sticky-nav-menu-item-34" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-14 current_page_item mkdf-active-item narrow">
                                        <a href="{{url('FStay')}}" class=" current "><span class="item_outer"><span class="item_text">Stay</span><span class="plus"></span></span></a>
                                    </li>

                                    <li id="sticky-nav-menu-item-34" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-14 current_page_item mkdf-active-item narrow">
                                        <a href="" class=" current "><span class="item_outer"><span class="item_text">Eat & Drink</span><span class="plus"></span></span></a>
                                    </li>

                                    <li id="sticky-nav-menu-item-34" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-14 current_page_item mkdf-active-item narrow">
                                        <a href="" class=" current "><span class="item_outer"><span class="item_text">Contact us</span><span class="plus"></span></span></a>
                                    </li>

                                </ul>
                            </nav>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



</header>


<header class="mkdf-mobile-header">

    <div class="mkdf-mobile-header-inner">
        <div class="mkdf-mobile-header-holder">
            <div class="mkdf-grid">
                <div class="mkdf-vertical-align-containers">
                    <div class="mkdf-vertical-align-containers">
                        <div class="mkdf-mobile-menu-opener">
                            <a href="javascript:void(0)">
									<span class="mkdf-mobile-menu-icon">
										<span aria-hidden="true" class="mkdf-icon-font-elegant icon_menu " ></span>									</span>
                            </a>
                        </div>
                        <div class="mkdf-position-center">
                            <div class="mkdf-position-center-inner">


                                <div class="mkdf-mobile-logo-wrapper">
                                    <a itemprop="url" href="{{url('/')}}" style="height: 70px">
                                        <img itemprop="image" src="frontend/assets/images/logok.png" width="256" height="82"  alt="Mobile Logo"/>
                                    </a>
                                </div>

                            </div>
                        </div>
                        <div class="mkdf-position-right">
                            <div class="mkdf-position-right-inner">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <nav class="mkdf-mobile-nav">
            <div class="mkdf-grid">
                <ul id="menu-main-menu-2" class="">

                    <li id="mobile-menu-item-1195" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-14 current_page_item mkdf-active-item narrow">
                        <a href="{{('/')}}" class=" current "><span class="item_outer"><span class="item_text">Home</span><span class="plus"></span></span></a>
                    </li>
                    <li id="mobile-menu-item-1195" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-14 current_page_item mkdf-active-item narrow">
                        <a href="{{url('/AboutRwanda')}}" class=" current "><span class="item_outer"><span class="item_text">About Rwanda</span><span class="plus"></span></span></a>
                    </li>
                    <li id="mobile-menu-item-1195" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-14 current_page_item mkdf-active-item narrow">
                        <a href="{{url('/AboutRegion')}}" class=" current "><span class="item_outer"><span class="item_text">About the Region</span><span class="plus"></span></span></a>
                    </li>
                    <li id="mobile-menu-item-1195" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-14 current_page_item mkdf-active-item narrow">
                        <a href="{{url('Discover')}}" class=" current "><span class="item_outer"><span class="item_text">Discover</span><span class="plus"></span></span></a>
                    </li>
                    <li id="mobile-menu-item-1195" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-14 current_page_item mkdf-active-item narrow">
                        <a href="{{url('FStay')}}" class=" current "><span class="item_outer"><span class="item_text">Stay</span><span class="plus"></span></span></a>
                    </li>

                    <li id="mobile-menu-item-1195" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-14 current_page_item mkdf-active-item narrow">
                        <a href="" class=" current "><span class="item_outer"><span class="item_text">Eat & Drink</span><span class="plus"></span></span></a>
                    </li>

                    <li id="mobile-menu-item-1195" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-14 current_page_item mkdf-active-item narrow">
                        <a href="" class=" current "><span class="item_outer"><span class="item_text">Contact us</span><span class="plus"></span></span></a>
                    </li>

                </ul>
            </div>
        </nav>

    </div>

</header>

<a id='mkdf-back-to-top' href='index.html#'>
                <span class="mkdf-icon-stack">
                    <i class="mkdf-icon-font-awesome fa fa-angle-up "></i>                </span>
    <span class="mkdf-icon-stack mkdf-btt-text">
                    Top                </span>
</a>