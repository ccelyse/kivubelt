<header id="masthead" class="site-header site-header--layout-3 site-header--absolute">
    <div class="container">
        <div class="d-lg-flex justify-content-lg-between align-items-lg-center site-header__container">
            <div class="site-header__logo">
                <a href="{{url('/')}}">
                    <h1 class="screen-reader-text">Kivu belt</h1>
                    <img src="frontend/assets/images/logok.png" alt="Listiry" style="width: 100px;">
                </a>
            </div><!-- .site-header__logo -->

            <div class="d-lg-flex align-items-lg-center">
                <ul class="min-list main-navigation main-navigation--white">
                    <!--<li><a href="index-4.html#">Home</a></li>-->
                    <li><a href="{{url('AboutRwanda')}}">About Rwanda</a></li>
                    <li><a href="{{url('AboutRegion')}}">About the Region</a></li>
                    <li><a href="{{url('Discover')}}">Discover</a></li>
                    <li><a href="{{url('FStay')}}">Stay</a></li>
                    <li><a href="{{url('EatAndDrink')}}">Eat & Drink</a></li>
                    <li><a href="#">Trip ideas</a></li>
                    {{--<li><a href="#">Gallery</a></li>--}}
                    {{--<li><a href="#">Useful information</a></li>--}}
                    <li><a href="{{url('ContactUs')}}">Contact us</a></li>
                </ul><!-- .main-navigation -->

            </div>

            <div class="d-lg-none nav-mobile">
                <a href="index-4.html#" class="nav-toggle js-nav-toggle nav-toggle--white">
                    <span></span>
                </a><!-- .nav-toggle -->
            </div><!-- .nav-mobile -->
        </div><!-- .site-header__container -->
    </div><!-- .container -->
</header><!-- #masthead -->