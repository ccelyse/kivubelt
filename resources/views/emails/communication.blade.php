<section class="mkdf-side-menu">
    <div class="mkdf-close-side-menu-holder">
        <a class="mkdf-close-side-menu" href="#" target="_self">
            <span aria-hidden="true" class="mkdf-icon-font-elegant icon_close " ></span>		</a>
    </div>
    <div id="media_image-2" class="widget mkdf-sidearea widget_media_image"><a href="https://wanderers.mikado-themes.com"><img width="162" height="52" src="https://wanderers.mikado-themes.com/wp-content/uploads/2018/02/logo-sidearea.png" class="image wp-image-1467  attachment-full size-full" alt="logo" style="max-width: 100%; height: auto;" /></a></div>
    <div class="widget mkdf-custom-font-widget">
        <p class="mkdf-custom-font-holder  mkdf-cf-6946  " style="font-size: 18px;line-height: 30px;color: #b4b4b4;margin: 20px 0px 35px" data-item-class="mkdf-cf-6946">
            Proin gravida nibh vel velit auctor aliquet. Aenean sollic itudin, lorem quis bibendum auctornisi elit consequat ipsum, nec nibh id elit. Lorem ipsum dolor.
        </p>
    </div>
    <div id="text-12" class="widget mkdf-sidearea widget_text">
        <div class="mkdf-widget-title-holder">
            <h5 class="mkdf-widget-title">Latest Posts</h5>
        </div>
        <div class="textwidget"></div>
    </div>
    <div class="widget mkdf-separator-widget">
        <div class="mkdf-separator-holder clearfix  mkdf-separator-center mkdf-separator-normal">
            <div class="mkdf-separator" style="border-style: solid;margin-top: 0px;margin-bottom: 2px"></div>
        </div>
    </div>
    <div class="widget mkdf-blog-list-widget">
        <div class="mkdf-blog-list-holder mkdf-bl-simple mkdf-bl-one-column mkdf-small-space mkdf-bl-pag-no-pagination"  data-type=simple data-number-of-posts=2 data-number-of-columns=1 data-space-between-items=small data-orderby=date data-order=DESC data-image-size=thumbnail data-title-tag=h5 data-excerpt-length=40 data-post-info-section=yes data-post-info-image=yes data-post-info-author=yes data-post-info-date=yes data-post-info-category=yes data-post-info-comments=no data-post-info-like=no data-post-info-share=no data-pagination-type=no-pagination data-max-num-pages=20 data-next-page=2>
            <div class="mkdf-bl-wrapper mkdf-outer-space">
                <ul class="mkdf-blog-list">
                    <li class="mkdf-bl-item mkdf-item-space clearfix">
                        <div class="mkdf-bli-inner">
                            <div class="mkdf-post-image">
                                <a itemprop="url" href="https://wanderers.mikado-themes.com/solo-traveler-learns-on-the-road/" title="Solo Traveler Learns On The Road">
                                    <img width="150" height="150" src="https://wanderers.mikado-themes.com/wp-content/uploads/2018/02/blog-post-1-150x150.jpg" class="attachment-thumbnail size-thumbnail wp-post-image" alt="a" srcset="https://wanderers.mikado-themes.com/wp-content/uploads/2018/02/blog-post-1-150x150.jpg 150w, https://wanderers.mikado-themes.com/wp-content/uploads/2018/02/blog-post-1-550x550.jpg 550w, https://wanderers.mikado-themes.com/wp-content/uploads/2018/02/blog-post-1-100x100.jpg 100w" sizes="(max-width: 150px) 100vw, 150px" />					</a>
                            </div>
                            <div class="mkdf-bli-content">
                                <h5 itemprop="name" class="entry-title mkdf-post-title" >
                                    <a itemprop="url" href="https://wanderers.mikado-themes.com/solo-traveler-learns-on-the-road/" title="Solo Traveler Learns On The Road">
                                        Solo Traveler Learns On The Road            </a>
                                </h5>
                                <div itemprop="dateCreated" class="mkdf-post-info-date entry-date published updated">
                                    <a itemprop="url" href="https://wanderers.mikado-themes.com/2018/03/">
                                        <span class="icon_calendar"></span> March 8, 2018        </a>
                                    <meta itemprop="interactionCount" content="UserComments: 2"/>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="mkdf-bl-item mkdf-item-space clearfix">
                        <div class="mkdf-bli-inner">
                            <div class="mkdf-post-image">
                                <a itemprop="url" href="https://wanderers.mikado-themes.com/use-your-social-network-to-travel/" title="Use Your Social Network To Travel">
                                    <img width="150" height="150" src="https://wanderers.mikado-themes.com/wp-content/uploads/2018/02/blog-post-4-150x150.jpg" class="attachment-thumbnail size-thumbnail wp-post-image" alt="a" srcset="https://wanderers.mikado-themes.com/wp-content/uploads/2018/02/blog-post-4-150x150.jpg 150w, https://wanderers.mikado-themes.com/wp-content/uploads/2018/02/blog-post-4-550x550.jpg 550w, https://wanderers.mikado-themes.com/wp-content/uploads/2018/02/blog-post-4-100x100.jpg 100w" sizes="(max-width: 150px) 100vw, 150px" />					</a>
                            </div>
                            <div class="mkdf-bli-content">
                                <h5 itemprop="name" class="entry-title mkdf-post-title" >
                                    <a itemprop="url" href="https://wanderers.mikado-themes.com/use-your-social-network-to-travel/" title="Use Your Social Network To Travel">
                                        Use Your Social Network To Travel            </a>
                                </h5>
                                <div itemprop="dateCreated" class="mkdf-post-info-date entry-date published updated">
                                    <a itemprop="url" href="https://wanderers.mikado-themes.com/2018/03/">
                                        <span class="icon_calendar"></span> March 7, 2018        </a>
                                    <meta itemprop="interactionCount" content="UserComments: 2"/>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div id="text-13" class="widget mkdf-sidearea widget_text">
        <div class="textwidget">
            <p>
            <div class="mkdf-iwt clearfix  mkdf-iwt-icon-left mkdf-iwt-icon-tiny">
                <a itemprop="url" href="tel:167712444227" target="_blank">
                    <div class="mkdf-iwt-icon">
                  <span class="mkdf-icon-shortcode mkdf-normal   mkdf-icon-tiny"  data-hover-color="#ffcc05" data-color="#9b9b9b">
                  <span aria-hidden="true" class="mkdf-icon-font-elegant icon_phone mkdf-icon-element" style="color: #9b9b9b;font-size:14px" ></span>            </span>
                    </div>
                    <div class="mkdf-iwt-content" style="padding-left: 14px">
                        <h6 class="mkdf-iwt-title" style="color: #9b9b9b;margin-top: 10px">
                            <span class="mkdf-iwt-title-text">1-677-124-44227</span>
                        </h6>
                    </div>
                </a>
            </div>
            <div class="vc_empty_space"   style="height: 4px" ><span class="vc_empty_space_inner"></span></div>
            <div class="mkdf-iwt clearfix  mkdf-iwt-icon-left mkdf-iwt-icon-tiny">
                <a itemprop="url" href="mailto:wanderers.wp.theme@gmail.com" target="_self">
                    <div class="mkdf-iwt-icon">
                  <span class="mkdf-icon-shortcode mkdf-normal   mkdf-icon-tiny"  data-hover-color="#ffcc05" data-color="#9b9b9b">
                  <span aria-hidden="true" class="mkdf-icon-font-elegant icon_mail mkdf-icon-element" style="color: #9b9b9b;font-size:14px" ></span>            </span>
                    </div>
                    <div class="mkdf-iwt-content" style="padding-left: 14px">
                        <h6 class="mkdf-iwt-title" style="color: #9b9b9b;margin-top: 10px">
                            <span class="mkdf-iwt-title-text">wanderers@mikado-themes.com</span>
                        </h6>
                    </div>
                </a>
            </div>
            </p>
        </div>
    </div>
    <div class="widget mkdf-separator-widget">
        <div class="mkdf-separator-holder clearfix  mkdf-separator-center mkdf-separator-normal">
            <div class="mkdf-separator" style="border-style: solid;margin-top: 0px;margin-bottom: 2px"></div>
        </div>
    </div>
    <a class="mkdf-social-icon-widget-holder mkdf-icon-has-hover" data-hover-color="#ffcc05" style="color: #808285;;font-size: 13px;margin: 0px 8px 0px 0px;" href="https://twitter.com/MikadoThemes?lang=en" target="_blank">
        <span class="mkdf-social-icon-widget  social_twitter    "></span>		</a>
    <a class="mkdf-social-icon-widget-holder mkdf-icon-has-hover" data-hover-color="#ffcc05" style="color: #808285;;font-size: 13px;margin: 0px 8px;" href="https://www.facebook.com/Mikado-Themes-884182241781117" target="_blank">
        <span class="mkdf-social-icon-widget  social_facebook    "></span>		</a>
    <a class="mkdf-social-icon-widget-holder mkdf-icon-has-hover" data-hover-color="#ffcc05" style="color: #808285;;font-size: 13px;margin: 0px 8px;" href="https://www.instagram.com/mikadothemes/" target="_blank">
        <span class="mkdf-social-icon-widget  social_instagram    "></span>		</a>
    <a class="mkdf-social-icon-widget-holder mkdf-icon-has-hover" data-hover-color="#ffcc05" style="color: #808285;;font-size: 13px;margin: 0px 8px;" href="http://www.tumblr.com" target="_blank">
        <span class="mkdf-social-icon-widget  social_tumblr    "></span>		</a>
    <a class="mkdf-social-icon-widget-holder mkdf-icon-has-hover" data-hover-color="#ffcc05" style="color: #808285;;font-size: 13px;margin: 0px 8px;" href="http://www.linkedin.com" target="_blank">
        <span class="mkdf-social-icon-widget  social_linkedin    "></span>		</a>
    <a class="mkdf-social-icon-widget-holder mkdf-icon-has-hover" data-hover-color="#ffcc05" style="color: #808285;;font-size: 13px;margin: 0px 8px;" href="https://www.pinterest.com/mikadothemes/" target="_blank">
        <span class="mkdf-social-icon-widget  social_pinterest    "></span>		</a>
</section>
<div class="mkdf-wrapper">
    <div class="mkdf-wrapper-inner">
        <div class="mkdf-top-bar">
            <div class="mkdf-grid">
                <div class="mkdf-vertical-align-containers">
                    <div class="mkdf-position-left">
                        <div class="mkdf-position-left-inner">
                            <div id="text-4" class="widget widget_text mkdf-top-bar-widget">
                                <div class="textwidget">
                                    <p>Follow Us:&nbsp;&nbsp;</p>
                                </div>
                            </div>
                            <a class="mkdf-social-icon-widget-holder mkdf-icon-has-hover" data-hover-color="#ffcc05" style="color: #ffffff;;font-size: 12px;margin: 0px 8px 0px 1px;" href="https://www.instagram.com/mikadothemes/" target="_blank">
                                <span class="mkdf-social-icon-widget  social_instagram    "></span>		</a>
                            <a class="mkdf-social-icon-widget-holder mkdf-icon-has-hover" data-hover-color="#ffcc05" style="color: #ffffff;;font-size: 12px;margin: 0px 6px 0px 4px;" href="https://www.facebook.com/Mikado-Themes-884182241781117" target="_blank">
                                <span class="mkdf-social-icon-widget  social_facebook    "></span>		</a>
                            <a class="mkdf-social-icon-widget-holder mkdf-icon-has-hover" data-hover-color="#ffcc05" style="color: #ffffff;;font-size: 12px;margin: 0px 4px;" href="https://twitter.com/MikadoThemes?lang=en" target="_blank">
                                <span class="mkdf-social-icon-widget  social_twitter    "></span>		</a>
                            <a class="mkdf-social-icon-widget-holder mkdf-icon-has-hover" data-hover-color="#ffcc05" style="color: #ffffff;;font-size: 12px;margin: 0px 41px 0px 8px;" href="http://www.tumblr.com" target="_blank">
                                <span class="mkdf-social-icon-widget  social_tumblr    "></span>		</a>
                            <div id="text-5" class="widget widget_text mkdf-top-bar-widget">
                                <div class="textwidget">
                                    <div class="mkdf-iwt clearfix  mkdf-iwt-icon-left mkdf-iwt-icon-tiny">
                                        <a itemprop="url" href="tel:167712444227" target="_blank">
                                            <div class="mkdf-iwt-icon">
                              <span class="mkdf-icon-shortcode mkdf-normal   mkdf-icon-tiny"  data-hover-color="#f0cd2f" data-color="#fff">
                              <span aria-hidden="true" class="mkdf-icon-font-elegant icon_phone mkdf-icon-element" style="color: #fff;font-size:14px" ></span>            </span>
                                            </div>
                                            <div class="mkdf-iwt-content" style="padding-left: 14px">
                                                <p class="mkdf-iwt-title" style="color: #fff;margin-top: 3px">
                                                    <span class="mkdf-iwt-title-text">1-677-124-44227</span>
                                                </p>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="mkdf-position-right">
                        <div class="mkdf-position-right-inner">
                            <div class="widget mkdf-login-register-widget mkdf-user-not-logged-in"><a href="#" class="mkdf-login-opener">
                                    <span class="mkdf-login-text">Login</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <header class="mkdf-page-header">
            <div class="mkdf-menu-area mkdf-menu-right">
                <div class="mkdf-grid">
                    <div class="mkdf-vertical-align-containers">
                        <div class="mkdf-position-left">
                            <div class="mkdf-position-left-inner">
                                <div class="mkdf-logo-wrapper">
                                    <a itemprop="url" href="https://wanderers.mikado-themes.com/" style="height: 41px;">
                                        <img itemprop="image" class="mkdf-normal-logo" src="https://wanderers.mikado-themes.com/wp-content/uploads/2018/02/logo-dark.png" width="256" height="82"  alt="logo"/>
                                        <img itemprop="image" class="mkdf-dark-logo" src="https://wanderers.mikado-themes.com/wp-content/uploads/2018/02/logo-dark.png" width="256" height="82"  alt="dark logo"/>        <img itemprop="image" class="mkdf-light-logo" src="https://wanderers.mikado-themes.com/wp-content/uploads/2018/02/logo-light.png" width="256" height="82"  alt="light logo"/>    </a>
                                </div>
                            </div>
                        </div>
                        <div class="mkdf-position-right">
                            <div class="mkdf-position-right-inner">
                                <nav class="mkdf-main-menu mkdf-drop-down mkdf-default-nav">
                                    <ul id="menu-main-menu" class="clearfix">
                                        <li id="nav-menu-item-34" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home  narrow"><a href="https://wanderers.mikado-themes.com/" class=""><span class="item_outer"><span class="item_text">Home</span></span></a></li>
                                        <li id="nav-menu-item-40" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children  has_sub narrow">
                                            <a href="#" class=" no_link" onclick="JavaScript: return false;"><span class="item_outer"><span class="item_text">Pages</span><i class="mkdf-menu-arrow fa fa-angle-down"></i></span></a>
                                            <div class="second">
                                                <div class="inner">
                                                    <ul>
                                                        <li id="nav-menu-item-637" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/about-us/" class=""><span class="item_outer"><span class="item_text">About Us</span></span></a></li>
                                                        <li id="nav-menu-item-914" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/our-team/" class=""><span class="item_outer"><span class="item_text">Our Team</span></span></a></li>
                                                        <li id="nav-menu-item-815" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/contact/" class=""><span class="item_outer"><span class="item_text">Contact</span></span></a></li>
                                                        <li id="nav-menu-item-915" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/faq/" class=""><span class="item_outer"><span class="item_text">FAQ</span></span></a></li>
                                                        <li id="nav-menu-item-816" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/coming-soon/" class=""><span class="item_outer"><span class="item_text">Coming Soon</span></span></a></li>
                                                        <li id="nav-menu-item-633" class="menu-item menu-item-type-custom menu-item-object-custom "><a href="https://wanderers.mikado-themes.com/404" class=""><span class="item_outer"><span class="item_text">404</span></span></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </li>
                                        <li id="nav-menu-item-41" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children  has_sub narrow">
                                            <a href="#" class=" no_link" onclick="JavaScript: return false;"><span class="item_outer"><span class="item_text">Destinations</span><i class="mkdf-menu-arrow fa fa-angle-down"></i></span></a>
                                            <div class="second">
                                                <div class="inner">
                                                    <ul>
                                                        <li id="nav-menu-item-312" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/destination-list/" class=""><span class="item_outer"><span class="item_text">Destination List</span></span></a></li>
                                                        <li id="nav-menu-item-42" class="menu-item menu-item-type-post_type menu-item-object-destinations "><a href="https://wanderers.mikado-themes.com/destinations/thailand/" class=""><span class="item_outer"><span class="item_text">Destination Item</span></span></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </li>
                                        <li id="nav-menu-item-43" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-ancestor current-menu-parent menu-item-has-children mkdf-active-item has_sub narrow">
                                            <a href="#" class=" current  no_link" onclick="JavaScript: return false;"><span class="item_outer"><span class="item_text">Tours</span><i class="mkdf-menu-arrow fa fa-angle-down"></i></span></a>
                                            <div class="second">
                                                <div class="inner">
                                                    <ul>
                                                        <li id="nav-menu-item-86" class="menu-item menu-item-type-post_type menu-item-object-tour-item current-menu-item "><a href="https://wanderers.mikado-themes.com/tour-item/ultimate-thailand/" class=""><span class="item_outer"><span class="item_text">Tour Single</span></span></a></li>
                                                        <li id="nav-menu-item-319" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children sub">
                                                            <a href="#" class=""><span class="item_outer"><span class="item_text">Tour Lists</span></span></a>
                                                            <ul>
                                                                <li id="nav-menu-item-2273" class="menu-item menu-item-type-custom menu-item-object-custom "><a href="https://wanderers.mikado-themes.com/tour-search-page/?view_type=standard" class=""><span class="item_outer"><span class="item_text">Standard</span></span></a></li>
                                                                <li id="nav-menu-item-2274" class="menu-item menu-item-type-custom menu-item-object-custom "><a href="https://wanderers.mikado-themes.com/tour-search-page/?view_type=gallery" class=""><span class="item_outer"><span class="item_text">Gallery</span></span></a></li>
                                                                <li id="nav-menu-item-333" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/tour-list-carousel/" class=""><span class="item_outer"><span class="item_text">Carousel</span></span></a></li>
                                                                <li id="nav-menu-item-2275" class="menu-item menu-item-type-custom menu-item-object-custom "><a href="https://wanderers.mikado-themes.com/tour-search-page/?view_type=list" class=""><span class="item_outer"><span class="item_text">Search Page</span></span></a></li>
                                                            </ul>
                                                        </li>
                                                        <li id="nav-menu-item-1281" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children sub">
                                                            <a href="#" class=""><span class="item_outer"><span class="item_text">Dashboard</span></span></a>
                                                            <ul>
                                                                <li id="nav-menu-item-1284" class="menu-item menu-item-type-custom menu-item-object-custom "><a href="https://wanderers.mikado-themes.com/dashboard/?user-action=profile" class=""><span class="item_outer"><span class="item_text">My Profile</span></span></a></li>
                                                                <li id="nav-menu-item-1282" class="menu-item menu-item-type-custom menu-item-object-custom "><a href="https://wanderers.mikado-themes.com/dashboard/?user-action=edit-profile" class=""><span class="item_outer"><span class="item_text">Edit Profile</span></span></a></li>
                                                                <li id="nav-menu-item-1283" class="menu-item menu-item-type-custom menu-item-object-custom "><a href="https://wanderers.mikado-themes.com/dashboard/?user-action=my-bookings" class=""><span class="item_outer"><span class="item_text">My Bookings</span></span></a></li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </li>
                                        <li id="nav-menu-item-45" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children  has_sub narrow">
                                            <a href="#" class=" no_link" onclick="JavaScript: return false;"><span class="item_outer"><span class="item_text">Blog</span><i class="mkdf-menu-arrow fa fa-angle-down"></i></span></a>
                                            <div class="second">
                                                <div class="inner">
                                                    <ul>
                                                        <li id="nav-menu-item-525" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/blog-list/" class=""><span class="item_outer"><span class="item_text">Standard List</span></span></a></li>
                                                        <li id="nav-menu-item-625" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/blog-masonry/" class=""><span class="item_outer"><span class="item_text">Masonry List</span></span></a></li>
                                                        <li id="nav-menu-item-516" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children sub">
                                                            <a href="#" class=""><span class="item_outer"><span class="item_text">Single</span></span></a>
                                                            <ul>
                                                                <li id="nav-menu-item-515" class="menu-item menu-item-type-post_type menu-item-object-post "><a href="https://wanderers.mikado-themes.com/solo-traveler-learns-on-the-road/" class=""><span class="item_outer"><span class="item_text">Standard Post</span></span></a></li>
                                                                <li id="nav-menu-item-547" class="menu-item menu-item-type-post_type menu-item-object-post "><a href="https://wanderers.mikado-themes.com/new-ways-to-become-a-better-travel-blogger/" class=""><span class="item_outer"><span class="item_text">Gallery Post</span></span></a></li>
                                                                <li id="nav-menu-item-546" class="menu-item menu-item-type-post_type menu-item-object-post "><a href="https://wanderers.mikado-themes.com/use-your-social-network-to-travel/" class=""><span class="item_outer"><span class="item_text">Audio Post</span></span></a></li>
                                                                <li id="nav-menu-item-543" class="menu-item menu-item-type-post_type menu-item-object-post "><a href="https://wanderers.mikado-themes.com/travel-the-world-in-only-20-days/" class=""><span class="item_outer"><span class="item_text">Video Post</span></span></a></li>
                                                                <li id="nav-menu-item-544" class="menu-item menu-item-type-post_type menu-item-object-post "><a href="https://wanderers.mikado-themes.com/best-designed-wordpress-themes/" class=""><span class="item_outer"><span class="item_text">Link Post</span></span></a></li>
                                                                <li id="nav-menu-item-545" class="menu-item menu-item-type-post_type menu-item-object-post "><a href="https://wanderers.mikado-themes.com/marc-nicolaus-richmond/" class=""><span class="item_outer"><span class="item_text">Quote Post</span></span></a></li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </li>
                                        <li id="nav-menu-item-46" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children  has_sub narrow">
                                            <a href="#" class=" no_link" onclick="JavaScript: return false;"><span class="item_outer"><span class="item_text">Shop</span><i class="mkdf-menu-arrow fa fa-angle-down"></i></span></a>
                                            <div class="second">
                                                <div class="inner">
                                                    <ul>
                                                        <li id="nav-menu-item-39" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/shop/" class=""><span class="item_outer"><span class="item_text">List</span></span></a></li>
                                                        <li id="nav-menu-item-362" class="menu-item menu-item-type-post_type menu-item-object-product "><a href="https://wanderers.mikado-themes.com/shop/biking-helmet/" class=""><span class="item_outer"><span class="item_text">Single</span></span></a></li>
                                                        <li id="nav-menu-item-47" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children sub">
                                                            <a href="#" class=""><span class="item_outer"><span class="item_text">Shop Pages</span></span></a>
                                                            <ul>
                                                                <li id="nav-menu-item-36" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/my-account/" class=""><span class="item_outer"><span class="item_text">My account</span></span></a></li>
                                                                <li id="nav-menu-item-37" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/checkout/" class=""><span class="item_outer"><span class="item_text">Checkout</span></span></a></li>
                                                                <li id="nav-menu-item-38" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/cart/" class=""><span class="item_outer"><span class="item_text">Cart</span></span></a></li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </li>
                                        <li id="nav-menu-item-44" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children  has_sub wide">
                                            <a href="#" class=" no_link" onclick="JavaScript: return false;"><span class="item_outer"><span class="item_text">Elements</span><i class="mkdf-menu-arrow fa fa-angle-down"></i></span></a>
                                            <div class="second">
                                                <div class="inner">
                                                    <ul>
                                                        <li id="nav-menu-item-1195" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children sub">
                                                            <a href="#" class=""><span class="item_outer"><span class="item_text">Featured</span></span></a>
                                                            <ul>
                                                                <li id="nav-menu-item-1194" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/tour-list-standard/" class=""><span class="item_outer"><span class="item_text">Tour List Standard</span></span></a></li>
                                                                <li id="nav-menu-item-1409" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/tour-list-gallery/" class=""><span class="item_outer"><span class="item_text">Tour List Gallery</span></span></a></li>
                                                                <li id="nav-menu-item-1408" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/tour-list-masonry/" class=""><span class="item_outer"><span class="item_text">Tour List Masonry</span></span></a></li>
                                                                <li id="nav-menu-item-1419" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/tour-carousel/" class=""><span class="item_outer"><span class="item_text">Tour Carousel</span></span></a></li>
                                                                <li id="nav-menu-item-1539" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/tour-filter/" class=""><span class="item_outer"><span class="item_text">Tour Filter</span></span></a></li>
                                                                <li id="nav-menu-item-1499" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/destination-list-shortcode-2/" class=""><span class="item_outer"><span class="item_text">Destination List</span></span></a></li>
                                                                <li id="nav-menu-item-1190" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/parallax-sections/" class=""><span class="item_outer"><span class="item_text">Parallax Sections</span></span></a></li>
                                                                <li id="nav-menu-item-1189" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/video-button/" class=""><span class="item_outer"><span class="item_text">Video Button</span></span></a></li>
                                                            </ul>
                                                        </li>
                                                        <li id="nav-menu-item-1196" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children sub">
                                                            <a href="#" class=""><span class="item_outer"><span class="item_text">Classic</span></span></a>
                                                            <ul>
                                                                <li id="nav-menu-item-1217" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/accordions-and-toogles/" class=""><span class="item_outer"><span class="item_text">Accordions &#038; Toogles</span></span></a></li>
                                                                <li id="nav-menu-item-1229" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/tabs/" class=""><span class="item_outer"><span class="item_text">Tabs</span></span></a></li>
                                                                <li id="nav-menu-item-1228" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/buttons/" class=""><span class="item_outer"><span class="item_text">Buttons</span></span></a></li>
                                                                <li id="nav-menu-item-1216" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/icon-with-text/" class=""><span class="item_outer"><span class="item_text">Icon With Text</span></span></a></li>
                                                                <li id="nav-menu-item-1839" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/icon-list/" class=""><span class="item_outer"><span class="item_text">Icon List</span></span></a></li>
                                                                <li id="nav-menu-item-1226" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/google-maps/" class=""><span class="item_outer"><span class="item_text">Google Maps</span></span></a></li>
                                                                <li id="nav-menu-item-1215" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/contact-form/" class=""><span class="item_outer"><span class="item_text">Contact Form</span></span></a></li>
                                                                <li id="nav-menu-item-1213" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/clients/" class=""><span class="item_outer"><span class="item_text">Clients</span></span></a></li>
                                                            </ul>
                                                        </li>
                                                        <li id="nav-menu-item-1197" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children sub">
                                                            <a href="#" class=""><span class="item_outer"><span class="item_text">Infographic</span></span></a>
                                                            <ul>
                                                                <li id="nav-menu-item-1255" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/counters/" class=""><span class="item_outer"><span class="item_text">Counters</span></span></a></li>
                                                                <li id="nav-menu-item-1254" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/countdown/" class=""><span class="item_outer"><span class="item_text">Countdown</span></span></a></li>
                                                                <li id="nav-menu-item-1248" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/progress-bar/" class=""><span class="item_outer"><span class="item_text">Progress Bar</span></span></a></li>
                                                                <li id="nav-menu-item-1246" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/blog-list/" class=""><span class="item_outer"><span class="item_text">Blog List</span></span></a></li>
                                                                <li id="nav-menu-item-1192" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/team/" class=""><span class="item_outer"><span class="item_text">Team</span></span></a></li>
                                                                <li id="nav-menu-item-1214" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/top-reviews/" class=""><span class="item_outer"><span class="item_text">Top Reviews</span></span></a></li>
                                                                <li id="nav-menu-item-1971" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/call-to-action/" class=""><span class="item_outer"><span class="item_text">Call To Action</span></span></a></li>
                                                            </ul>
                                                        </li>
                                                        <li id="nav-menu-item-1198" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children sub">
                                                            <a href="#" class=""><span class="item_outer"><span class="item_text">Typography</span></span></a>
                                                            <ul>
                                                                <li id="nav-menu-item-1279" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/headings/" class=""><span class="item_outer"><span class="item_text">Headings</span></span></a></li>
                                                                <li id="nav-menu-item-1278" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/columns/" class=""><span class="item_outer"><span class="item_text">Columns</span></span></a></li>
                                                                <li id="nav-menu-item-1277" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/blockquote/" class=""><span class="item_outer"><span class="item_text">Blockquote</span></span></a></li>
                                                                <li id="nav-menu-item-1276" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/dropcaps/" class=""><span class="item_outer"><span class="item_text">Dropcaps</span></span></a></li>
                                                                <li id="nav-menu-item-1275" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/highlights/" class=""><span class="item_outer"><span class="item_text">Highlights</span></span></a></li>
                                                                <li id="nav-menu-item-1274" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/separators/" class=""><span class="item_outer"><span class="item_text">Separators</span></span></a></li>
                                                                <li id="nav-menu-item-1273" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/title-subtitle/" class=""><span class="item_outer"><span class="item_text">Title &#038; Subtitle</span></span></a></li>
                                                                <li id="nav-menu-item-1272" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/custom-font/" class=""><span class="item_outer"><span class="item_text">Custom Font</span></span></a></li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </nav>
                                <div class="mkdf-shopping-cart-holder" style="padding: 0 13px 0px">
                                    <div class="mkdf-shopping-cart-inner">
                                        <a itemprop="url" class="mkdf-header-cart" href="https://wanderers.mikado-themes.com/cart/">
                                            <span class="mkdf-cart-icon icon_bag_alt"></span>
                                            <span class="mkdf-cart-number">0</span>
                                        </a>
                                        <div class="mkdf-shopping-cart-dropdown">
                                            <ul>
                                                <li class="mkdf-empty-cart">No products in the cart.</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <a  style="margin: 0 21px 0px 14px;" class="mkdf-search-opener mkdf-icon-has-hover" href="javascript:void(0)">
                  <span class="mkdf-search-opener-wrapper">
                  <span class="mkdf-icon-font-elegant icon_search "></span>	                        </span>
                                </a>
                                <a class="mkdf-side-menu-button-opener mkdf-icon-has-hover"  href="javascript:void(0)" >
                  <span class="mkdf-side-menu-icon">
                  <span aria-hidden="true" class="mkdf-icon-font-elegant icon_menu " ></span>        	</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="mkdf-sticky-header">
                <div class="mkdf-sticky-holder mkdf-menu-right">
                    <div class="mkdf-grid">
                        <div class="mkdf-vertical-align-containers">
                            <div class="mkdf-position-left">
                                <div class="mkdf-position-left-inner">
                                    <div class="mkdf-logo-wrapper">
                                        <a itemprop="url" href="https://wanderers.mikado-themes.com/" style="height: 41px;">
                                            <img itemprop="image" class="mkdf-normal-logo" src="https://wanderers.mikado-themes.com/wp-content/uploads/2018/02/logo-dark.png" width="256" height="82"  alt="logo"/>
                                            <img itemprop="image" class="mkdf-dark-logo" src="https://wanderers.mikado-themes.com/wp-content/uploads/2018/02/logo-dark.png" width="256" height="82"  alt="dark logo"/>        <img itemprop="image" class="mkdf-light-logo" src="https://wanderers.mikado-themes.com/wp-content/uploads/2018/02/logo-light.png" width="256" height="82"  alt="light logo"/>    </a>
                                    </div>
                                </div>
                            </div>
                            <div class="mkdf-position-right">
                                <div class="mkdf-position-right-inner">
                                    <nav class="mkdf-main-menu mkdf-drop-down mkdf-sticky-nav">
                                        <ul id="menu-main-menu-1" class="clearfix">
                                            <li id="sticky-nav-menu-item-34" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home  narrow"><a href="https://wanderers.mikado-themes.com/" class=""><span class="item_outer"><span class="item_text">Home</span><span class="plus"></span></span></a></li>
                                            <li id="sticky-nav-menu-item-40" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children  has_sub narrow">
                                                <a href="#" class=" no_link" onclick="JavaScript: return false;"><span class="item_outer"><span class="item_text">Pages</span><span class="plus"></span><i class="mkdf-menu-arrow fa fa-angle-down"></i></span></a>
                                                <div class="second">
                                                    <div class="inner">
                                                        <ul>
                                                            <li id="sticky-nav-menu-item-637" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/about-us/" class=""><span class="item_outer"><span class="item_text">About Us</span><span class="plus"></span></span></a></li>
                                                            <li id="sticky-nav-menu-item-914" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/our-team/" class=""><span class="item_outer"><span class="item_text">Our Team</span><span class="plus"></span></span></a></li>
                                                            <li id="sticky-nav-menu-item-815" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/contact/" class=""><span class="item_outer"><span class="item_text">Contact</span><span class="plus"></span></span></a></li>
                                                            <li id="sticky-nav-menu-item-915" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/faq/" class=""><span class="item_outer"><span class="item_text">FAQ</span><span class="plus"></span></span></a></li>
                                                            <li id="sticky-nav-menu-item-816" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/coming-soon/" class=""><span class="item_outer"><span class="item_text">Coming Soon</span><span class="plus"></span></span></a></li>
                                                            <li id="sticky-nav-menu-item-633" class="menu-item menu-item-type-custom menu-item-object-custom "><a href="https://wanderers.mikado-themes.com/404" class=""><span class="item_outer"><span class="item_text">404</span><span class="plus"></span></span></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </li>
                                            <li id="sticky-nav-menu-item-41" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children  has_sub narrow">
                                                <a href="#" class=" no_link" onclick="JavaScript: return false;"><span class="item_outer"><span class="item_text">Destinations</span><span class="plus"></span><i class="mkdf-menu-arrow fa fa-angle-down"></i></span></a>
                                                <div class="second">
                                                    <div class="inner">
                                                        <ul>
                                                            <li id="sticky-nav-menu-item-312" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/destination-list/" class=""><span class="item_outer"><span class="item_text">Destination List</span><span class="plus"></span></span></a></li>
                                                            <li id="sticky-nav-menu-item-42" class="menu-item menu-item-type-post_type menu-item-object-destinations "><a href="https://wanderers.mikado-themes.com/destinations/thailand/" class=""><span class="item_outer"><span class="item_text">Destination Item</span><span class="plus"></span></span></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </li>
                                            <li id="sticky-nav-menu-item-43" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-ancestor current-menu-parent menu-item-has-children mkdf-active-item has_sub narrow">
                                                <a href="#" class=" current  no_link" onclick="JavaScript: return false;"><span class="item_outer"><span class="item_text">Tours</span><span class="plus"></span><i class="mkdf-menu-arrow fa fa-angle-down"></i></span></a>
                                                <div class="second">
                                                    <div class="inner">
                                                        <ul>
                                                            <li id="sticky-nav-menu-item-86" class="menu-item menu-item-type-post_type menu-item-object-tour-item current-menu-item "><a href="https://wanderers.mikado-themes.com/tour-item/ultimate-thailand/" class=""><span class="item_outer"><span class="item_text">Tour Single</span><span class="plus"></span></span></a></li>
                                                            <li id="sticky-nav-menu-item-319" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children sub">
                                                                <a href="#" class=""><span class="item_outer"><span class="item_text">Tour Lists</span><span class="plus"></span></span></a>
                                                                <ul>
                                                                    <li id="sticky-nav-menu-item-2273" class="menu-item menu-item-type-custom menu-item-object-custom "><a href="https://wanderers.mikado-themes.com/tour-search-page/?view_type=standard" class=""><span class="item_outer"><span class="item_text">Standard</span><span class="plus"></span></span></a></li>
                                                                    <li id="sticky-nav-menu-item-2274" class="menu-item menu-item-type-custom menu-item-object-custom "><a href="https://wanderers.mikado-themes.com/tour-search-page/?view_type=gallery" class=""><span class="item_outer"><span class="item_text">Gallery</span><span class="plus"></span></span></a></li>
                                                                    <li id="sticky-nav-menu-item-333" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/tour-list-carousel/" class=""><span class="item_outer"><span class="item_text">Carousel</span><span class="plus"></span></span></a></li>
                                                                    <li id="sticky-nav-menu-item-2275" class="menu-item menu-item-type-custom menu-item-object-custom "><a href="https://wanderers.mikado-themes.com/tour-search-page/?view_type=list" class=""><span class="item_outer"><span class="item_text">Search Page</span><span class="plus"></span></span></a></li>
                                                                </ul>
                                                            </li>
                                                            <li id="sticky-nav-menu-item-1281" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children sub">
                                                                <a href="#" class=""><span class="item_outer"><span class="item_text">Dashboard</span><span class="plus"></span></span></a>
                                                                <ul>
                                                                    <li id="sticky-nav-menu-item-1284" class="menu-item menu-item-type-custom menu-item-object-custom "><a href="https://wanderers.mikado-themes.com/dashboard/?user-action=profile" class=""><span class="item_outer"><span class="item_text">My Profile</span><span class="plus"></span></span></a></li>
                                                                    <li id="sticky-nav-menu-item-1282" class="menu-item menu-item-type-custom menu-item-object-custom "><a href="https://wanderers.mikado-themes.com/dashboard/?user-action=edit-profile" class=""><span class="item_outer"><span class="item_text">Edit Profile</span><span class="plus"></span></span></a></li>
                                                                    <li id="sticky-nav-menu-item-1283" class="menu-item menu-item-type-custom menu-item-object-custom "><a href="https://wanderers.mikado-themes.com/dashboard/?user-action=my-bookings" class=""><span class="item_outer"><span class="item_text">My Bookings</span><span class="plus"></span></span></a></li>
                                                                </ul>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </li>
                                            <li id="sticky-nav-menu-item-45" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children  has_sub narrow">
                                                <a href="#" class=" no_link" onclick="JavaScript: return false;"><span class="item_outer"><span class="item_text">Blog</span><span class="plus"></span><i class="mkdf-menu-arrow fa fa-angle-down"></i></span></a>
                                                <div class="second">
                                                    <div class="inner">
                                                        <ul>
                                                            <li id="sticky-nav-menu-item-525" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/blog-list/" class=""><span class="item_outer"><span class="item_text">Standard List</span><span class="plus"></span></span></a></li>
                                                            <li id="sticky-nav-menu-item-625" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/blog-masonry/" class=""><span class="item_outer"><span class="item_text">Masonry List</span><span class="plus"></span></span></a></li>
                                                            <li id="sticky-nav-menu-item-516" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children sub">
                                                                <a href="#" class=""><span class="item_outer"><span class="item_text">Single</span><span class="plus"></span></span></a>
                                                                <ul>
                                                                    <li id="sticky-nav-menu-item-515" class="menu-item menu-item-type-post_type menu-item-object-post "><a href="https://wanderers.mikado-themes.com/solo-traveler-learns-on-the-road/" class=""><span class="item_outer"><span class="item_text">Standard Post</span><span class="plus"></span></span></a></li>
                                                                    <li id="sticky-nav-menu-item-547" class="menu-item menu-item-type-post_type menu-item-object-post "><a href="https://wanderers.mikado-themes.com/new-ways-to-become-a-better-travel-blogger/" class=""><span class="item_outer"><span class="item_text">Gallery Post</span><span class="plus"></span></span></a></li>
                                                                    <li id="sticky-nav-menu-item-546" class="menu-item menu-item-type-post_type menu-item-object-post "><a href="https://wanderers.mikado-themes.com/use-your-social-network-to-travel/" class=""><span class="item_outer"><span class="item_text">Audio Post</span><span class="plus"></span></span></a></li>
                                                                    <li id="sticky-nav-menu-item-543" class="menu-item menu-item-type-post_type menu-item-object-post "><a href="https://wanderers.mikado-themes.com/travel-the-world-in-only-20-days/" class=""><span class="item_outer"><span class="item_text">Video Post</span><span class="plus"></span></span></a></li>
                                                                    <li id="sticky-nav-menu-item-544" class="menu-item menu-item-type-post_type menu-item-object-post "><a href="https://wanderers.mikado-themes.com/best-designed-wordpress-themes/" class=""><span class="item_outer"><span class="item_text">Link Post</span><span class="plus"></span></span></a></li>
                                                                    <li id="sticky-nav-menu-item-545" class="menu-item menu-item-type-post_type menu-item-object-post "><a href="https://wanderers.mikado-themes.com/marc-nicolaus-richmond/" class=""><span class="item_outer"><span class="item_text">Quote Post</span><span class="plus"></span></span></a></li>
                                                                </ul>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </li>
                                            <li id="sticky-nav-menu-item-46" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children  has_sub narrow">
                                                <a href="#" class=" no_link" onclick="JavaScript: return false;"><span class="item_outer"><span class="item_text">Shop</span><span class="plus"></span><i class="mkdf-menu-arrow fa fa-angle-down"></i></span></a>
                                                <div class="second">
                                                    <div class="inner">
                                                        <ul>
                                                            <li id="sticky-nav-menu-item-39" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/shop/" class=""><span class="item_outer"><span class="item_text">List</span><span class="plus"></span></span></a></li>
                                                            <li id="sticky-nav-menu-item-362" class="menu-item menu-item-type-post_type menu-item-object-product "><a href="https://wanderers.mikado-themes.com/shop/biking-helmet/" class=""><span class="item_outer"><span class="item_text">Single</span><span class="plus"></span></span></a></li>
                                                            <li id="sticky-nav-menu-item-47" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children sub">
                                                                <a href="#" class=""><span class="item_outer"><span class="item_text">Shop Pages</span><span class="plus"></span></span></a>
                                                                <ul>
                                                                    <li id="sticky-nav-menu-item-36" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/my-account/" class=""><span class="item_outer"><span class="item_text">My account</span><span class="plus"></span></span></a></li>
                                                                    <li id="sticky-nav-menu-item-37" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/checkout/" class=""><span class="item_outer"><span class="item_text">Checkout</span><span class="plus"></span></span></a></li>
                                                                    <li id="sticky-nav-menu-item-38" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/cart/" class=""><span class="item_outer"><span class="item_text">Cart</span><span class="plus"></span></span></a></li>
                                                                </ul>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </li>
                                            <li id="sticky-nav-menu-item-44" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children  has_sub wide">
                                                <a href="#" class=" no_link" onclick="JavaScript: return false;"><span class="item_outer"><span class="item_text">Elements</span><span class="plus"></span><i class="mkdf-menu-arrow fa fa-angle-down"></i></span></a>
                                                <div class="second">
                                                    <div class="inner">
                                                        <ul>
                                                            <li id="sticky-nav-menu-item-1195" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children sub">
                                                                <a href="#" class=""><span class="item_outer"><span class="item_text">Featured</span><span class="plus"></span></span></a>
                                                                <ul>
                                                                    <li id="sticky-nav-menu-item-1194" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/tour-list-standard/" class=""><span class="item_outer"><span class="item_text">Tour List Standard</span><span class="plus"></span></span></a></li>
                                                                    <li id="sticky-nav-menu-item-1409" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/tour-list-gallery/" class=""><span class="item_outer"><span class="item_text">Tour List Gallery</span><span class="plus"></span></span></a></li>
                                                                    <li id="sticky-nav-menu-item-1408" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/tour-list-masonry/" class=""><span class="item_outer"><span class="item_text">Tour List Masonry</span><span class="plus"></span></span></a></li>
                                                                    <li id="sticky-nav-menu-item-1419" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/tour-carousel/" class=""><span class="item_outer"><span class="item_text">Tour Carousel</span><span class="plus"></span></span></a></li>
                                                                    <li id="sticky-nav-menu-item-1539" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/tour-filter/" class=""><span class="item_outer"><span class="item_text">Tour Filter</span><span class="plus"></span></span></a></li>
                                                                    <li id="sticky-nav-menu-item-1499" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/destination-list-shortcode-2/" class=""><span class="item_outer"><span class="item_text">Destination List</span><span class="plus"></span></span></a></li>
                                                                    <li id="sticky-nav-menu-item-1190" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/parallax-sections/" class=""><span class="item_outer"><span class="item_text">Parallax Sections</span><span class="plus"></span></span></a></li>
                                                                    <li id="sticky-nav-menu-item-1189" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/video-button/" class=""><span class="item_outer"><span class="item_text">Video Button</span><span class="plus"></span></span></a></li>
                                                                </ul>
                                                            </li>
                                                            <li id="sticky-nav-menu-item-1196" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children sub">
                                                                <a href="#" class=""><span class="item_outer"><span class="item_text">Classic</span><span class="plus"></span></span></a>
                                                                <ul>
                                                                    <li id="sticky-nav-menu-item-1217" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/accordions-and-toogles/" class=""><span class="item_outer"><span class="item_text">Accordions &#038; Toogles</span><span class="plus"></span></span></a></li>
                                                                    <li id="sticky-nav-menu-item-1229" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/tabs/" class=""><span class="item_outer"><span class="item_text">Tabs</span><span class="plus"></span></span></a></li>
                                                                    <li id="sticky-nav-menu-item-1228" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/buttons/" class=""><span class="item_outer"><span class="item_text">Buttons</span><span class="plus"></span></span></a></li>
                                                                    <li id="sticky-nav-menu-item-1216" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/icon-with-text/" class=""><span class="item_outer"><span class="item_text">Icon With Text</span><span class="plus"></span></span></a></li>
                                                                    <li id="sticky-nav-menu-item-1839" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/icon-list/" class=""><span class="item_outer"><span class="item_text">Icon List</span><span class="plus"></span></span></a></li>
                                                                    <li id="sticky-nav-menu-item-1226" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/google-maps/" class=""><span class="item_outer"><span class="item_text">Google Maps</span><span class="plus"></span></span></a></li>
                                                                    <li id="sticky-nav-menu-item-1215" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/contact-form/" class=""><span class="item_outer"><span class="item_text">Contact Form</span><span class="plus"></span></span></a></li>
                                                                    <li id="sticky-nav-menu-item-1213" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/clients/" class=""><span class="item_outer"><span class="item_text">Clients</span><span class="plus"></span></span></a></li>
                                                                </ul>
                                                            </li>
                                                            <li id="sticky-nav-menu-item-1197" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children sub">
                                                                <a href="#" class=""><span class="item_outer"><span class="item_text">Infographic</span><span class="plus"></span></span></a>
                                                                <ul>
                                                                    <li id="sticky-nav-menu-item-1255" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/counters/" class=""><span class="item_outer"><span class="item_text">Counters</span><span class="plus"></span></span></a></li>
                                                                    <li id="sticky-nav-menu-item-1254" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/countdown/" class=""><span class="item_outer"><span class="item_text">Countdown</span><span class="plus"></span></span></a></li>
                                                                    <li id="sticky-nav-menu-item-1248" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/progress-bar/" class=""><span class="item_outer"><span class="item_text">Progress Bar</span><span class="plus"></span></span></a></li>
                                                                    <li id="sticky-nav-menu-item-1246" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/blog-list/" class=""><span class="item_outer"><span class="item_text">Blog List</span><span class="plus"></span></span></a></li>
                                                                    <li id="sticky-nav-menu-item-1192" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/team/" class=""><span class="item_outer"><span class="item_text">Team</span><span class="plus"></span></span></a></li>
                                                                    <li id="sticky-nav-menu-item-1214" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/top-reviews/" class=""><span class="item_outer"><span class="item_text">Top Reviews</span><span class="plus"></span></span></a></li>
                                                                    <li id="sticky-nav-menu-item-1971" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/call-to-action/" class=""><span class="item_outer"><span class="item_text">Call To Action</span><span class="plus"></span></span></a></li>
                                                                </ul>
                                                            </li>
                                                            <li id="sticky-nav-menu-item-1198" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children sub">
                                                                <a href="#" class=""><span class="item_outer"><span class="item_text">Typography</span><span class="plus"></span></span></a>
                                                                <ul>
                                                                    <li id="sticky-nav-menu-item-1279" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/headings/" class=""><span class="item_outer"><span class="item_text">Headings</span><span class="plus"></span></span></a></li>
                                                                    <li id="sticky-nav-menu-item-1278" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/columns/" class=""><span class="item_outer"><span class="item_text">Columns</span><span class="plus"></span></span></a></li>
                                                                    <li id="sticky-nav-menu-item-1277" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/blockquote/" class=""><span class="item_outer"><span class="item_text">Blockquote</span><span class="plus"></span></span></a></li>
                                                                    <li id="sticky-nav-menu-item-1276" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/dropcaps/" class=""><span class="item_outer"><span class="item_text">Dropcaps</span><span class="plus"></span></span></a></li>
                                                                    <li id="sticky-nav-menu-item-1275" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/highlights/" class=""><span class="item_outer"><span class="item_text">Highlights</span><span class="plus"></span></span></a></li>
                                                                    <li id="sticky-nav-menu-item-1274" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/separators/" class=""><span class="item_outer"><span class="item_text">Separators</span><span class="plus"></span></span></a></li>
                                                                    <li id="sticky-nav-menu-item-1273" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/title-subtitle/" class=""><span class="item_outer"><span class="item_text">Title &#038; Subtitle</span><span class="plus"></span></span></a></li>
                                                                    <li id="sticky-nav-menu-item-1272" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/custom-font/" class=""><span class="item_outer"><span class="item_text">Custom Font</span><span class="plus"></span></span></a></li>
                                                                </ul>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </nav>
                                    <div class="mkdf-shopping-cart-holder" style="padding: 0 13px 0px">
                                        <div class="mkdf-shopping-cart-inner">
                                            <a itemprop="url" class="mkdf-header-cart" href="https://wanderers.mikado-themes.com/cart/">
                                                <span class="mkdf-cart-icon icon_bag_alt"></span>
                                                <span class="mkdf-cart-number">0</span>
                                            </a>
                                            <div class="mkdf-shopping-cart-dropdown">
                                                <ul>
                                                    <li class="mkdf-empty-cart">No products in the cart.</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <a  style="margin: 0 21px 0px 14px;" class="mkdf-search-opener mkdf-icon-has-hover" href="javascript:void(0)">
                     <span class="mkdf-search-opener-wrapper">
                     <span class="mkdf-icon-font-elegant icon_search "></span>	                        </span>
                                    </a>
                                    <a class="mkdf-side-menu-button-opener mkdf-icon-has-hover"  href="javascript:void(0)" >
                     <span class="mkdf-side-menu-icon">
                     <span aria-hidden="true" class="mkdf-icon-font-elegant icon_menu " ></span>        	</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <form action="https://wanderers.mikado-themes.com/" class="mkdf-search-cover" method="get">
                <div class="mkdf-container">
                    <div class="mkdf-container-inner clearfix">
                        <div class="mkdf-form-holder-outer">
                            <div class="mkdf-form-holder">
                                <div class="mkdf-form-holder-inner">
                                    <span class="icon_search"></span>
                                    <input type="text" placeholder="Search on site..." name="s" class="mkdf_search_field" autocomplete="off" />
                                    <a class="mkdf-search-close" href="#">
                                        <span aria-hidden="true" class="mkdf-icon-font-elegant icon_close "></span>						</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </header>
        <header class="mkdf-mobile-header">
            <div class="mkdf-mobile-header-inner">
                <div class="mkdf-mobile-header-holder">
                    <div class="mkdf-grid">
                        <div class="mkdf-vertical-align-containers">
                            <div class="mkdf-vertical-align-containers">
                                <div class="mkdf-mobile-menu-opener">
                                    <a href="javascript:void(0)">
                     <span class="mkdf-mobile-menu-icon">
                     <span aria-hidden="true" class="mkdf-icon-font-elegant icon_menu " ></span>									</span>
                                    </a>
                                </div>
                                <div class="mkdf-position-center">
                                    <div class="mkdf-position-center-inner">
                                        <div class="mkdf-mobile-logo-wrapper">
                                            <a itemprop="url" href="https://wanderers.mikado-themes.com/" style="height: 41px">
                                                <img itemprop="image" src="https://wanderers.mikado-themes.com/wp-content/uploads/2018/02/logo-dark.png" width="256" height="82"  alt="Mobile Logo"/>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="mkdf-position-right">
                                    <div class="mkdf-position-right-inner">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <nav class="mkdf-mobile-nav">
                    <div class="mkdf-grid">
                        <ul id="menu-main-menu-2" class="">
                            <li id="mobile-menu-item-34" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home "><a href="https://wanderers.mikado-themes.com/" class=""><span>Home</span></a></li>
                            <li id="mobile-menu-item-40" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children  has_sub">
                                <h6><span>Pages</span></h6>
                                <span class="mobile_arrow"><i class="mkdf-sub-arrow fa fa-angle-right"></i><i class="fa fa-angle-down"></i></span>
                                <ul class="sub_menu">
                                    <li id="mobile-menu-item-637" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/about-us/" class=""><span>About Us</span></a></li>
                                    <li id="mobile-menu-item-914" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/our-team/" class=""><span>Our Team</span></a></li>
                                    <li id="mobile-menu-item-815" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/contact/" class=""><span>Contact</span></a></li>
                                    <li id="mobile-menu-item-915" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/faq/" class=""><span>FAQ</span></a></li>
                                    <li id="mobile-menu-item-816" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/coming-soon/" class=""><span>Coming Soon</span></a></li>
                                    <li id="mobile-menu-item-633" class="menu-item menu-item-type-custom menu-item-object-custom "><a href="https://wanderers.mikado-themes.com/404" class=""><span>404</span></a></li>
                                </ul>
                            </li>
                            <li id="mobile-menu-item-41" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children  has_sub">
                                <h6><span>Destinations</span></h6>
                                <span class="mobile_arrow"><i class="mkdf-sub-arrow fa fa-angle-right"></i><i class="fa fa-angle-down"></i></span>
                                <ul class="sub_menu">
                                    <li id="mobile-menu-item-312" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/destination-list/" class=""><span>Destination List</span></a></li>
                                    <li id="mobile-menu-item-42" class="menu-item menu-item-type-post_type menu-item-object-destinations "><a href="https://wanderers.mikado-themes.com/destinations/thailand/" class=""><span>Destination Item</span></a></li>
                                </ul>
                            </li>
                            <li id="mobile-menu-item-43" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-ancestor current-menu-parent menu-item-has-children mkdf-active-item has_sub">
                                <h6><span>Tours</span></h6>
                                <span class="mobile_arrow"><i class="mkdf-sub-arrow fa fa-angle-right"></i><i class="fa fa-angle-down"></i></span>
                                <ul class="sub_menu">
                                    <li id="mobile-menu-item-86" class="menu-item menu-item-type-post_type menu-item-object-tour-item current-menu-item "><a href="https://wanderers.mikado-themes.com/tour-item/ultimate-thailand/" class=""><span>Tour Single</span></a></li>
                                    <li id="mobile-menu-item-319" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children  has_sub">
                                        <a href="#" class=" mkdf-mobile-no-link"><span>Tour Lists</span></a><span class="mobile_arrow"><i class="mkdf-sub-arrow fa fa-angle-right"></i><i class="fa fa-angle-down"></i></span>
                                        <ul class="sub_menu">
                                            <li id="mobile-menu-item-2273" class="menu-item menu-item-type-custom menu-item-object-custom "><a href="https://wanderers.mikado-themes.com/tour-search-page/?view_type=standard" class=""><span>Standard</span></a></li>
                                            <li id="mobile-menu-item-2274" class="menu-item menu-item-type-custom menu-item-object-custom "><a href="https://wanderers.mikado-themes.com/tour-search-page/?view_type=gallery" class=""><span>Gallery</span></a></li>
                                            <li id="mobile-menu-item-333" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/tour-list-carousel/" class=""><span>Carousel</span></a></li>
                                            <li id="mobile-menu-item-2275" class="menu-item menu-item-type-custom menu-item-object-custom "><a href="https://wanderers.mikado-themes.com/tour-search-page/?view_type=list" class=""><span>Search Page</span></a></li>
                                        </ul>
                                    </li>
                                    <li id="mobile-menu-item-1281" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children  has_sub">
                                        <a href="#" class=" mkdf-mobile-no-link"><span>Dashboard</span></a><span class="mobile_arrow"><i class="mkdf-sub-arrow fa fa-angle-right"></i><i class="fa fa-angle-down"></i></span>
                                        <ul class="sub_menu">
                                            <li id="mobile-menu-item-1284" class="menu-item menu-item-type-custom menu-item-object-custom "><a href="https://wanderers.mikado-themes.com/dashboard/?user-action=profile" class=""><span>My Profile</span></a></li>
                                            <li id="mobile-menu-item-1282" class="menu-item menu-item-type-custom menu-item-object-custom "><a href="https://wanderers.mikado-themes.com/dashboard/?user-action=edit-profile" class=""><span>Edit Profile</span></a></li>
                                            <li id="mobile-menu-item-1283" class="menu-item menu-item-type-custom menu-item-object-custom "><a href="https://wanderers.mikado-themes.com/dashboard/?user-action=my-bookings" class=""><span>My Bookings</span></a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <li id="mobile-menu-item-45" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children  has_sub">
                                <h6><span>Blog</span></h6>
                                <span class="mobile_arrow"><i class="mkdf-sub-arrow fa fa-angle-right"></i><i class="fa fa-angle-down"></i></span>
                                <ul class="sub_menu">
                                    <li id="mobile-menu-item-525" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/blog-list/" class=""><span>Standard List</span></a></li>
                                    <li id="mobile-menu-item-625" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/blog-masonry/" class=""><span>Masonry List</span></a></li>
                                    <li id="mobile-menu-item-516" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children  has_sub">
                                        <a href="#" class=" mkdf-mobile-no-link"><span>Single</span></a><span class="mobile_arrow"><i class="mkdf-sub-arrow fa fa-angle-right"></i><i class="fa fa-angle-down"></i></span>
                                        <ul class="sub_menu">
                                            <li id="mobile-menu-item-515" class="menu-item menu-item-type-post_type menu-item-object-post "><a href="https://wanderers.mikado-themes.com/solo-traveler-learns-on-the-road/" class=""><span>Standard Post</span></a></li>
                                            <li id="mobile-menu-item-547" class="menu-item menu-item-type-post_type menu-item-object-post "><a href="https://wanderers.mikado-themes.com/new-ways-to-become-a-better-travel-blogger/" class=""><span>Gallery Post</span></a></li>
                                            <li id="mobile-menu-item-546" class="menu-item menu-item-type-post_type menu-item-object-post "><a href="https://wanderers.mikado-themes.com/use-your-social-network-to-travel/" class=""><span>Audio Post</span></a></li>
                                            <li id="mobile-menu-item-543" class="menu-item menu-item-type-post_type menu-item-object-post "><a href="https://wanderers.mikado-themes.com/travel-the-world-in-only-20-days/" class=""><span>Video Post</span></a></li>
                                            <li id="mobile-menu-item-544" class="menu-item menu-item-type-post_type menu-item-object-post "><a href="https://wanderers.mikado-themes.com/best-designed-wordpress-themes/" class=""><span>Link Post</span></a></li>
                                            <li id="mobile-menu-item-545" class="menu-item menu-item-type-post_type menu-item-object-post "><a href="https://wanderers.mikado-themes.com/marc-nicolaus-richmond/" class=""><span>Quote Post</span></a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <li id="mobile-menu-item-46" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children  has_sub">
                                <h6><span>Shop</span></h6>
                                <span class="mobile_arrow"><i class="mkdf-sub-arrow fa fa-angle-right"></i><i class="fa fa-angle-down"></i></span>
                                <ul class="sub_menu">
                                    <li id="mobile-menu-item-39" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/shop/" class=""><span>List</span></a></li>
                                    <li id="mobile-menu-item-362" class="menu-item menu-item-type-post_type menu-item-object-product "><a href="https://wanderers.mikado-themes.com/shop/biking-helmet/" class=""><span>Single</span></a></li>
                                    <li id="mobile-menu-item-47" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children  has_sub">
                                        <a href="#" class=" mkdf-mobile-no-link"><span>Shop Pages</span></a><span class="mobile_arrow"><i class="mkdf-sub-arrow fa fa-angle-right"></i><i class="fa fa-angle-down"></i></span>
                                        <ul class="sub_menu">
                                            <li id="mobile-menu-item-36" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/my-account/" class=""><span>My account</span></a></li>
                                            <li id="mobile-menu-item-37" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/checkout/" class=""><span>Checkout</span></a></li>
                                            <li id="mobile-menu-item-38" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/cart/" class=""><span>Cart</span></a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <li id="mobile-menu-item-44" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children  has_sub">
                                <h6><span>Elements</span></h6>
                                <span class="mobile_arrow"><i class="mkdf-sub-arrow fa fa-angle-right"></i><i class="fa fa-angle-down"></i></span>
                                <ul class="sub_menu">
                                    <li id="mobile-menu-item-1195" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children  has_sub">
                                        <a href="#" class=" mkdf-mobile-no-link"><span>Featured</span></a><span class="mobile_arrow"><i class="mkdf-sub-arrow fa fa-angle-right"></i><i class="fa fa-angle-down"></i></span>
                                        <ul class="sub_menu">
                                            <li id="mobile-menu-item-1194" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/tour-list-standard/" class=""><span>Tour List Standard</span></a></li>
                                            <li id="mobile-menu-item-1409" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/tour-list-gallery/" class=""><span>Tour List Gallery</span></a></li>
                                            <li id="mobile-menu-item-1408" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/tour-list-masonry/" class=""><span>Tour List Masonry</span></a></li>
                                            <li id="mobile-menu-item-1419" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/tour-carousel/" class=""><span>Tour Carousel</span></a></li>
                                            <li id="mobile-menu-item-1539" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/tour-filter/" class=""><span>Tour Filter</span></a></li>
                                            <li id="mobile-menu-item-1499" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/destination-list-shortcode-2/" class=""><span>Destination List</span></a></li>
                                            <li id="mobile-menu-item-1190" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/parallax-sections/" class=""><span>Parallax Sections</span></a></li>
                                            <li id="mobile-menu-item-1189" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/video-button/" class=""><span>Video Button</span></a></li>
                                        </ul>
                                    </li>
                                    <li id="mobile-menu-item-1196" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children  has_sub">
                                        <a href="#" class=" mkdf-mobile-no-link"><span>Classic</span></a><span class="mobile_arrow"><i class="mkdf-sub-arrow fa fa-angle-right"></i><i class="fa fa-angle-down"></i></span>
                                        <ul class="sub_menu">
                                            <li id="mobile-menu-item-1217" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/accordions-and-toogles/" class=""><span>Accordions &#038; Toogles</span></a></li>
                                            <li id="mobile-menu-item-1229" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/tabs/" class=""><span>Tabs</span></a></li>
                                            <li id="mobile-menu-item-1228" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/buttons/" class=""><span>Buttons</span></a></li>
                                            <li id="mobile-menu-item-1216" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/icon-with-text/" class=""><span>Icon With Text</span></a></li>
                                            <li id="mobile-menu-item-1839" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/icon-list/" class=""><span>Icon List</span></a></li>
                                            <li id="mobile-menu-item-1226" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/google-maps/" class=""><span>Google Maps</span></a></li>
                                            <li id="mobile-menu-item-1215" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/contact-form/" class=""><span>Contact Form</span></a></li>
                                            <li id="mobile-menu-item-1213" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/clients/" class=""><span>Clients</span></a></li>
                                        </ul>
                                    </li>
                                    <li id="mobile-menu-item-1197" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children  has_sub">
                                        <a href="#" class=" mkdf-mobile-no-link"><span>Infographic</span></a><span class="mobile_arrow"><i class="mkdf-sub-arrow fa fa-angle-right"></i><i class="fa fa-angle-down"></i></span>
                                        <ul class="sub_menu">
                                            <li id="mobile-menu-item-1255" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/counters/" class=""><span>Counters</span></a></li>
                                            <li id="mobile-menu-item-1254" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/countdown/" class=""><span>Countdown</span></a></li>
                                            <li id="mobile-menu-item-1248" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/progress-bar/" class=""><span>Progress Bar</span></a></li>
                                            <li id="mobile-menu-item-1246" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/blog-list/" class=""><span>Blog List</span></a></li>
                                            <li id="mobile-menu-item-1192" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/team/" class=""><span>Team</span></a></li>
                                            <li id="mobile-menu-item-1214" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/top-reviews/" class=""><span>Top Reviews</span></a></li>
                                            <li id="mobile-menu-item-1971" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/call-to-action/" class=""><span>Call To Action</span></a></li>
                                        </ul>
                                    </li>
                                    <li id="mobile-menu-item-1198" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children  has_sub">
                                        <a href="#" class=" mkdf-mobile-no-link"><span>Typography</span></a><span class="mobile_arrow"><i class="mkdf-sub-arrow fa fa-angle-right"></i><i class="fa fa-angle-down"></i></span>
                                        <ul class="sub_menu">
                                            <li id="mobile-menu-item-1279" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/headings/" class=""><span>Headings</span></a></li>
                                            <li id="mobile-menu-item-1278" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/columns/" class=""><span>Columns</span></a></li>
                                            <li id="mobile-menu-item-1277" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/blockquote/" class=""><span>Blockquote</span></a></li>
                                            <li id="mobile-menu-item-1276" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/dropcaps/" class=""><span>Dropcaps</span></a></li>
                                            <li id="mobile-menu-item-1275" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/highlights/" class=""><span>Highlights</span></a></li>
                                            <li id="mobile-menu-item-1274" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/separators/" class=""><span>Separators</span></a></li>
                                            <li id="mobile-menu-item-1273" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/title-subtitle/" class=""><span>Title &#038; Subtitle</span></a></li>
                                            <li id="mobile-menu-item-1272" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/custom-font/" class=""><span>Custom Font</span></a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
            <form action="https://wanderers.mikado-themes.com/" class="mkdf-search-cover" method="get">
                <div class="mkdf-container">
                    <div class="mkdf-container-inner clearfix">
                        <div class="mkdf-form-holder-outer">
                            <div class="mkdf-form-holder">
                                <div class="mkdf-form-holder-inner">
                                    <span class="icon_search"></span>
                                    <input type="text" placeholder="Search on site..." name="s" class="mkdf_search_field" autocomplete="off" />
                                    <a class="mkdf-search-close" href="#">
                                        <span aria-hidden="true" class="mkdf-icon-font-elegant icon_close "></span>						</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </header>
        <a id='mkdf-back-to-top' href='#'>
<span class="mkdf-icon-stack">
<i class="mkdf-icon-font-awesome fa fa-angle-up "></i>                </span>
            <span class="mkdf-icon-stack mkdf-btt-text">
Top                </span>
        </a>
        <div class="mkdf-content" style="margin-top: -80px">
            <div class="mkdf-content-inner">
                <div class="mkdf-title-holder mkdf-centered-type mkdf-title-va-window-top mkdf-preload-background mkdf-has-bg-image mkdf-bg-parallax" style="height: 515px;background-color: #303030;background-image:url(https://wanderers.mikado-themes.com/wp-content/uploads/2018/02/tour-1-title-img.jpg);" data-height="435">
                    <div class="mkdf-title-image">
                        <img itemprop="image" src="https://wanderers.mikado-themes.com/wp-content/uploads/2018/02/tour-1-title-img.jpg" alt="a" />
                    </div>
                    <div class="mkdf-title-wrapper" >
                        <div class="mkdf-title-inner">
                            <div class="mkdf-grid">
                                <h1 class="mkdf-page-title entry-title" >Ultimate Thailand</h1>
                                <h6 class="mkdf-page-subtitle" style="padding: 0 20%">Show travel details, galleries, allow the users to search &amp; more. And our simple booking form allows visitors to easily book their next tour. </h6>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mkdf-grid mkdf-tour-item-single-holder">
                    <div class="mkdf-tour-tabs mkdf-horizontal mkdf-tab-text">
                        <ul class="mkdf-tabs-nav clearfix">
                            <li class="mkdf-tour-nav-item">
                                <a href="tour-item-info-id">
                                    <span class="mkdf-tour-nav-section-icon ion-information-circled"></span>
                                    <span class="mkdf-tour-nav-section-title">
         INFORMATION						</span>
                                </a>
                            </li>
                            <li class="mkdf-tour-nav-item">
                                <a href="tour-item-plan-id">
                                    <span class="mkdf-tour-nav-section-icon ion-ios-book"></span>
                                    <span class="mkdf-tour-nav-section-title">
         TOUR PLAN						</span>
                                </a>
                            </li>
                            <li class="mkdf-tour-nav-item">
                                <a href="tour-item-location-id">
                                    <span class="mkdf-tour-nav-section-icon ion-ios-location"></span>
                                    <span class="mkdf-tour-nav-section-title">
         LOCATION						</span>
                                </a>
                            </li>
                            <li class="mkdf-tour-nav-item">
                                <a href="tour-item-gallery-id">
                                    <span class="mkdf-tour-nav-section-icon ion-camera"></span>
                                    <span class="mkdf-tour-nav-section-title">
         GALLERY						</span>
                                </a>
                            </li>
                            <li class="mkdf-tour-nav-item">
                                <a href="tour-item-review-id">
                                    <span class="mkdf-tour-nav-section-icon ion-android-people"></span>
                                    <span class="mkdf-tour-nav-section-title">
         REVIEWS						</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="mkdf-grid-row-medium-gutter">
                        <div class="mkdf-grid-col-9">
                            <article class="mkdf-tour-item-wrapper">
                                <div class="mkdf-tour-item-section mkdf-tab-container" id="tour-item-info-id">
                                    <div class="mkdf-tour-gallery-item-holder">
                                        <h3 class="mkdf-gallery-title">
                                            From our gallery
                                        </h3>
                                        <p class="mkdf-tour-gallery-item-excerpt">
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris a rutrum arcu. Donec ut lobortis ante, non imperdiet est. Praesent vulputate at enim sit amet mattis.
                                        </p>
                                        <div class="mkdf-tour-gallery clearfix">
                                            <div class="mkdf-tour-gallery-item">
                                                <a href="https://wanderers.mikado-themes.com/wp-content/uploads/2018/02/tour-1-img-1.jpg" data-rel="prettyPhoto[gallery_excerpt_pretty_photo]">
                                                    <img width="550" height="550" src="https://wanderers.mikado-themes.com/wp-content/uploads/2018/02/tour-1-img-1-550x550.jpg" class="attachment-wanderers_mkdf_square size-wanderers_mkdf_square" alt="a" srcset="https://wanderers.mikado-themes.com/wp-content/uploads/2018/02/tour-1-img-1-550x550.jpg 550w, https://wanderers.mikado-themes.com/wp-content/uploads/2018/02/tour-1-img-1-150x150.jpg 150w, https://wanderers.mikado-themes.com/wp-content/uploads/2018/02/tour-1-img-1-300x300.jpg 300w, https://wanderers.mikado-themes.com/wp-content/uploads/2018/02/tour-1-img-1-768x768.jpg 768w, https://wanderers.mikado-themes.com/wp-content/uploads/2018/02/tour-1-img-1-450x450.jpg 450w, https://wanderers.mikado-themes.com/wp-content/uploads/2018/02/tour-1-img-1.jpg 800w, https://wanderers.mikado-themes.com/wp-content/uploads/2018/02/tour-1-img-1-100x100.jpg 100w" sizes="(max-width: 550px) 100vw, 550px" />                            </a>
                                            </div>
                                            <div class="mkdf-tour-gallery-item">
                                                <a href="https://wanderers.mikado-themes.com/wp-content/uploads/2018/02/tour-1-img-2.jpg" data-rel="prettyPhoto[gallery_excerpt_pretty_photo]">
                                                    <img width="550" height="550" src="https://wanderers.mikado-themes.com/wp-content/uploads/2018/02/tour-1-img-2-550x550.jpg" class="attachment-wanderers_mkdf_square size-wanderers_mkdf_square" alt="a" srcset="https://wanderers.mikado-themes.com/wp-content/uploads/2018/02/tour-1-img-2-550x550.jpg 550w, https://wanderers.mikado-themes.com/wp-content/uploads/2018/02/tour-1-img-2-150x150.jpg 150w, https://wanderers.mikado-themes.com/wp-content/uploads/2018/02/tour-1-img-2-300x300.jpg 300w, https://wanderers.mikado-themes.com/wp-content/uploads/2018/02/tour-1-img-2-768x768.jpg 768w, https://wanderers.mikado-themes.com/wp-content/uploads/2018/02/tour-1-img-2-450x450.jpg 450w, https://wanderers.mikado-themes.com/wp-content/uploads/2018/02/tour-1-img-2.jpg 800w, https://wanderers.mikado-themes.com/wp-content/uploads/2018/02/tour-1-img-2-100x100.jpg 100w" sizes="(max-width: 550px) 100vw, 550px" />                            </a>
                                            </div>
                                            <div class="mkdf-tour-gallery-item">
                                                <a href="https://wanderers.mikado-themes.com/wp-content/uploads/2018/02/tour-1-img-3.jpg" data-rel="prettyPhoto[gallery_excerpt_pretty_photo]">
                                                    <img width="550" height="550" src="https://wanderers.mikado-themes.com/wp-content/uploads/2018/02/tour-1-img-3-550x550.jpg" class="attachment-wanderers_mkdf_square size-wanderers_mkdf_square" alt="a" srcset="https://wanderers.mikado-themes.com/wp-content/uploads/2018/02/tour-1-img-3-550x550.jpg 550w, https://wanderers.mikado-themes.com/wp-content/uploads/2018/02/tour-1-img-3-150x150.jpg 150w, https://wanderers.mikado-themes.com/wp-content/uploads/2018/02/tour-1-img-3-100x100.jpg 100w" sizes="(max-width: 550px) 100vw, 550px" />                            </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </article>
                        </div>