@extends('layouts.master')

@section('title', 'Kivu Belt')

@section('content')

@include('layouts.topmenu')
<link rel="stylesheet" type="text/css" href="frontend/engine1/style.css" />
<script type="text/javascript" src="frontend/engine1/jquery.js"></script>
<style>
    .page-banner--layout-1 {
        background-image: url(frontend/assets/images/lakekivu2.jpg);
        background-size:cover ;
    }
    .main-navigation--white > li > a {
        color: #603b12 !important;
        font-weight: bold;
        font-size: 15px;
    }
    .category__title {
        font-size: 15px;
        font-size: 0.9375rem;
        margin-bottom: 0;
        color: #fff !important;
    }
    .c-white {
        color: #fff !important;
    }
    #wowslider-container1 {
        width: 100%;
        max-width: 100% !important;
        max-height: 600px !important;
    }
    #wowslider-container1 .ws_images{
        max-width:100% !important;
        max-height: 600px !important;
    }
    #wowslider-container1 .ws_pause {
        display: none;
    }
    .site-header--absolute {
        position: relative;
    }
</style>
<div id="wowslider-container1">
    <div class="ws_images"><ul>
            <li><img src="frontend/assets/images/image4.jpg"   id="wows1_0"/></li>
            <li><img src="frontend/assets/images/lakekivu2.jpg" id="wows1_1"/></li>
            <li><img src="frontend/assets/images/visitone.jpg"  id="wows1_2"/></li>
            {{--<li><img src="frontend/assets/images/1.jpg" alt="NH_OO_Lifestyle_Arrival_1784_MASTER" title="NH_OO_Lifestyle_Arrival_1784_MASTER" id="wows1_3"/></li>--}}
            {{--<li><img src="frontend/assets/images/5.jpg" alt="paradise_kivu_fisherman_evening" title="paradise_kivu_fisherman_evening" id="wows1_4"/></li>--}}
            {{--<li><img src="frontend/assets/images/6.jpeg" alt="WhatsApp Image 2019-03-01 at 09.41.18 (1)" title="WhatsApp Image 2019-03-01 at 09.41.18 (1)" id="wows1_5"/></li>--}}
            {{--<li><img src="frontend/assets/images/7.jpeg" alt="WhatsApp Image 2019-03-01 at 09.41.18 (2)" title="WhatsApp Image 2019-03-01 at 09.41.18 (2)" id="wows1_6"/></li>--}}
            {{--<li><img src="frontend/assets/images/8.jpeg" alt="WhatsApp Image 2019-03-01 at 09.41.18 (3)" title="WhatsApp Image 2019-03-01 at 09.41.18 (3)" id="wows1_7"/></li>--}}

        </ul></div>

    <div class="ws_shadow"></div>
</div>
{{--<section class="page-banner page-banner--layout-1 parallax">--}}
    {{--<div class="container">--}}
        {{--<!-- Start WOWSlider.com BODY section -->--}}


        {{--<!-- End WOWSlider.com BODY section -->--}}
        {{--<div class="page-banner__container animated fadeInUp">--}}
            {{--<div class="page-banner__textcontent t-center">--}}
                {{--<h2 class="page-banner__title c-white">Explore Kivu Belt</h2>--}}
                {{--<p class="page-banner__subtitle c-white">Find the best places to visit , eat, drink, stay .</p>--}}
            {{--</div><!-- .page-banner__textcontent -->--}}

            {{--<div class="main-search-container">--}}
                {{--<form class="main-search main-search--layout-1 bg-mirage">--}}
                    {{--<div class="main-search__group main-search__group--primary">--}}
                        {{--<label for="main-search-name" class="cc-white">What?</label>--}}
                        {{--<input type="text" name="name" id="main-search-name" class="form-input" placeholder="restaurant, hotel, club...">--}}
                    {{--</div><!-- .main-search__group -->--}}

                    {{--<div class="main-search__group main-search__group--secondary">--}}
                        {{--<label for="main-search-location" class="cc-white">Where?</label>--}}
                        {{--<input type="text" name="location" id="main-search-location" class="form-input" placeholder="Nyungwe">--}}
                    {{--</div><!-- .main-search__group -->--}}
                    {{--<div class="main-search__group main-search__group--tertiary">--}}
                        {{--<button type="submit" class="button button--medium button--square button--primary">--}}
                            {{--<i class="fa fa-search"></i> Search--}}
                        {{--</button>--}}
                    {{--</div>--}}
                {{--</form>--}}
            {{--</div><!-- .main-search-container -->--}}

            {{--<div class="locations-container t-center">--}}
                {{--<ul class="min-list inline-list locations locations--layout-1">--}}
                    {{--<li class="location">--}}
                        {{--<a href="index.html#" class="c-white"><i class="fa fa-cutlery"></i>Eat & Drink</a>--}}
                    {{--</li>--}}
                    {{--<li class="location">--}}
                        {{--<a href="index.html#" class="c-white"><i class="fa fa-glass"></i>Things to do</a>--}}
                    {{--</li>--}}
                    {{--<li class="location">--}}
                        {{--<a href="index.html#" class="c-white"><i class="fa fa-hotel"></i>Place to Stay</a>--}}
                    {{--</li>--}}
                    {{--<li class="location">--}}
                        {{--<a href="index.html#" class="c-white"><i class="fa fa-plane"></i>Trips Ideas</a>--}}
                    {{--</li>--}}
                {{--</ul>--}}
            {{--</div>--}}
        {{--</div><!-- .page-banner__container -->--}}
    {{--</div><!-- .container -->--}}
{{--</section><!-- .page-banner -->--}}
<section class="category-container page-section category--layout-2">
    <div class="container">
        <h2 class="page-section__title t-center">Your Journey Begins</h2>
        <div class="row">

            <div class="col-sm-6 col-md-6">
                <div class="category t-center" style="background-image: url('frontend/assets/images/food-001.jpg')">
                    <a href="{{url('EatAndDrink')}}" class="c-white">
                        <div class="category__image">
                            <img src="frontend/assets/images/restaurant.png" alt="Restaurants">
                        </div>
                        <h3 class="category__title">Eat & Drink</h3>
                    </a>
                </div>
            </div><!-- .col -->

            <div class="col-sm-6 col-md-6">
                <div class="category t-center" style="background-image: url('frontend/assets/images/6944648490_9f68bf04a8_b.jpg')">
                    <a href="{{url('Discover')}}" class="c-white">
                        <div class="category__image">
                            <img src="frontend/assets/images/night-life.png" alt="Coffee">
                        </div>
                        <h3 class="category__title">Things to do</h3>
                    </a>
                </div>
            </div><!-- .col -->
            <div class="col-sm-6 col-md-6">
                <div class="category t-center" style="background-image: url('frontend/assets/images/modern-sea-view-bedroom-3d-260nw-756956185.jpg')">
                    <a href="{{url('FStay')}}" class="c-white">
                        <div class="category__image">
                            <img src="frontend/assets/images/hotel.png" alt="Hotel">
                        </div>
                        <h3 class="category__title">Stay</h3>
                    </a>
                </div>
            </div><!-- .col -->


            <div class="col-sm-6 col-md-6">
                <div class="category t-center" style="background-image: url('frontend/assets/images/DSC_0152.jpg')">
                    <a href="#" class="c-white">
                        <div class="category__image">
                            <img src="frontend/assets/images/shopping.png" alt="Shopping">
                        </div>
                        <h3 class="category__title">Trip ideas</h3>
                    </a>
                </div>
            </div><!-- .col -->
        </div><!-- .row -->
    </div><!-- .container -->
</section><!-- .category -->

<section class="instructions-container page-section instruction--layout-2">
    <div class="container">
        <h2 class="page-section__title t-center">Easy to Get Services</h2>
        <div class="instructions">
            <div class="instruction-container">
                <div class="instruction t-center">
                    <div class="instruction__image">
                        <img src="frontend/assets/images/maps-and-flags.svg" alt="Maps and Flags">
                    </div>
                    <h3 class="instruction__title">Choose What to Do</h3>
                    <p class="instruction__detail c-boulder">
                        Looking for a cozy hotel to stay, a restaurant to eat, a museum to visit or a mall to do some shopping?
                    </p>
                </div><!-- .instruction -->
            </div><!-- .col -->

            <div class="instruction-container">
                <div class="instruction t-center">
                    <div class="instruction__image">
                        <img src="frontend/assets/images/binoculars.svg" alt="Binoclulars">
                    </div>
                    <h3 class="instruction__title">Find What You Want</h3>
                    <p class="instruction__detail c-boulder">
                        Search and filter hundreds of listings, read reviews, explore photos and find the perfect spot.
                    </p>
                </div><!-- .instruction -->
            </div><!-- .col -->

            <div class="instruction-container">
                <div class="instruction t-center">
                    <div class="instruction__image">
                        <img src="frontend/assets/images/landscape.svg" alt="Landscape">
                    </div>
                    <h3 class="instruction__title">Explore Amazing Places</h3>
                    <p class="instruction__detail c-boulder">
                        Go and have a good time or even make a booking directly from the listing page. All of those, thanks!
                    </p>
                </div><!-- .instruction -->
            </div><!-- .col -->
        </div><!-- .instructions -->
    </div><!-- .container -->
</section><!-- .instruction-container -->

<section class="ads ads--layout-2 parallax">
    <div class="t-center ads__container">
        <div class="container">
            <h2 class="ads__title">Explore exciting destinations with Kivu Belt!</h2>
        </div>
    </div>
</section><!-- .ads -->
@include('layouts.footer')
<script type="text/javascript" src="frontend/engine1/wowslider.js"></script>
<script type="text/javascript" src="frontend/engine1/script.js"></script>
@endsection
