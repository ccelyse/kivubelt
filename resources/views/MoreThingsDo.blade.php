@extends('layouts.newmaster')

@section('title', 'Kivu Belt')

@section('content')
    <style>
        .mkdf-tours-destination-list-item{
            height: auto !important;
        }
        p{
            font-family: Cabin,sans-serif !important;
            line-height: 2 !important;
        }
    </style>
    <div class="mkdf-wrapper">
        <div class="mkdf-wrapper-inner">
            @include('layouts.newtopmenu')
            <div class="mkdf-content" style="margin-top: -80px">
                <div class="mkdf-content-inner">
                    @foreach($getmore as $moreinfo)
                        <div class="mkdf-full-width">
                            <div class="mkdf-full-width-inner">
                                <div class="mkdf-grid-row">
                                    <div class="mkdf-page-content-holder mkdf-grid-col-12">
                                        <div class="mkdf-title-holder mkdf-centered-type mkdf-title-va-window-top mkdf-preload-background mkdf-has-bg-image mkdf-bg-parallax" style="height: 515px;background-color: #303030;background-image:url('Placecoverimage/{{$moreinfo->generalimage}}'); background-size: cover;" data-height="435">
                                            <div class="mkdf-title-image">
                                                <img itemprop="image" src="Placecoverimage/{{$moreinfo->generalimage}}" alt="a" />
                                            </div>
                                            <div class="mkdf-title-wrapper" >
                                                <div class="mkdf-title-inner">
                                                    <div class="mkdf-grid">
                                                        <h1 class="mkdf-page-title entry-title" >{{$moreinfo->placetitle}}</h1>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="mkdf-container mkdf-default-page-template">
                            <div class="mkdf-container-inner clearfix">
                                <div class="mkdf-grid-row">
                                    <div class="mkdf-page-content-holder mkdf-grid-col-9">
                                        <div class="vc_row wpb_row vc_row-fluid vc_custom_1520853432029" >
                                            <div class="wpb_column vc_column_container vc_col-sm-12">
                                                <div class="vc_column-inner ">
                                                    <div class="wpb_wrapper">
                                                        <div class="wpb_text_column wpb_content_element  vc_custom_1519311743473" >
                                                            <div class="wpb_wrapper">
                                                                <h3>{{$moreinfo->placetitle}}</h3>
                                                            </div>
                                                        </div>
                                                        <div class="vc_empty_space"   style="height: 2px" ><span class="vc_empty_space_inner"></span></div>
                                                        <div class="wpb_text_column wpb_content_element " >
                                                            <div class="wpb_wrapper">
                                                                <?php
                                                                $text = $moreinfo->placegeneralinformation;
                                                                ?>
                                                                <p><?php echo $text; ?><p>
                                                            </div>
                                                        </div>


                                                        <div class="vc_empty_space"   style="height: 50px" ><span class="vc_empty_space_inner"></span></div>
                                                        <div class="mkdf-tours-carousel-holder mkdf-carousel-pagination">
                                                            <div class="mkdf-tours-row mkdf-normal-space">
                                                                <div class="mkdf-st-inner">
                                                                    <h2 class="mkdf-st-title" style="color:#fff;text-align: center;padding-bottom: 15px"> Discover Activities</h2>
                                                                </div>
                                                                <div class="mkdf-container mkdf-default-page-template">
                                                                    <div class="mkdf-container-inner clearfix">
                                                                        <div class="mkdf-grid-row">
                                                                            <div class="mkdf-page-content-holder mkdf-grid-col-12">
                                                                                <div class="vc_row wpb_row vc_row-fluid vc_custom_1520853822055" >
                                                                                    <div class="wpb_column vc_column_container vc_col-sm-12">
                                                                                        <div class="vc_column-inner ">
                                                                                            <div class="wpb_wrapper">
                                                                                                <div class="mkdf-tours-destination-list mkdf-destinations-masonry">
                                                                                                    <div class="mkdf-tours-destination-holder mkdf-tours-row mkdf-small-space mkdf-tours-columns-3">
                                                                                                        <div class="mkdf-tours-row-inner-holder mkdf-outer-space">
                                                                                                            <div class="mkdf-tours-list-grid-sizer"></div>
                                                                                                            <div class="mkdf-tours-list-grid-gutter"></div>

                                                                                                            @foreach($listthings as $discover)
                                                                                                                <div class="mkdf-tours-destination-list-item mkdf-tours-row-item mkdf-item-space mkdf-destionations-masonry-item mkdf-size-default post-1093 destinations type-destinations status-publish has-post-thumbnail hentry">
                                                                                                                    <div class="mkdf-tours-destination-item-holder">
                                                                                                                        <a href="{{ route('MoreDiscoverActivity',['id'=> $discover->id])}}">
                                                                                                                            <div class="mkdf-tours-destination-item-image">
                                                                                                                                <img src="discovercover/{{$discover->discoveractivity_coverimage}}" class="attachment-full size-full wp-post-image" alt="a" srcset="discovercover/{{$discover->discoveractivity_coverimage}}" />
                                                                                                                            </div>
                                                                                                                            <div class="mkdf-tours-destination-item-content">
                                                                                                                                <div class="mkdf-tours-destination-item-content-inner">
                                                                                                                                    <div class="mkdf-tours-destination-item-text">
                                                                                                                                        <div class="mkdf-tours-destination-custom-label">
                                                                                                                                            {{$discover->discovercaption}}
                                                                                                                                        </div>
                                                                                                                                        <h3 class="mkdf-tours-destination-item-title">
                                                                                                                                            {{$discover->discoveractivity_title}}
                                                                                                                                        </h3>
                                                                                                                                    </div>
                                                                                                                                </div>
                                                                                                                            </div>
                                                                                                                        </a>
                                                                                                                    </div>

                                                                                                                </div>
                                                                                                            @endforeach
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach

                </div>
            </div>
        </div>

    @include('layouts.newfooter')
@endsection

