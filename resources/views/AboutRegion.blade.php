@extends('layouts.newmaster')

@section('title', 'Kivu Belt')

@section('content')
    <style>

    </style>
    <div class="mkdf-wrapper">
        <div class="mkdf-wrapper-inner">
            @include('layouts.newtopmenu')
            <div class="mkdf-content" style="margin-top: -80px">
                <div class="mkdf-content-inner">
                    <div class="mkdf-full-width">
                        <div class="mkdf-full-width-inner">
                            <div class="mkdf-grid-row">
                                <div class="mkdf-page-content-holder mkdf-grid-col-12">
                                    <div class="mkdf-title-holder mkdf-centered-type mkdf-title-va-window-top mkdf-preload-background mkdf-has-bg-image mkdf-bg-parallax" style="height: 515px;background-color: #303030;background-image:url(frontend/assets/images/congonile.jpg); background-size: cover;" data-height="435">
                                        <div class="mkdf-title-image">
                                            <img itemprop="image" src="frontend/assets/images/congonile.jpg" alt="a" />
                                        </div>
                                        <div class="mkdf-title-wrapper" >
                                            <div class="mkdf-title-inner">
                                                <div class="mkdf-grid">
                                                    <h1 class="mkdf-page-title entry-title" >About Region</h1>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="mkdf-full-width">
                        <div class="mkdf-full-width-inner">
                            <div class="mkdf-grid-row">
                                <div class="mkdf-page-content-holder mkdf-grid-col-12">

                                    <div class="vc_row wpb_row vc_row-fluid vc_row-o-equal-height vc_row-o-content-middle vc_row-flex mkdf-content-aligment-center" style="background-color:#1c1c1c">
                                        @foreach($listregionrow1 as $listregionrow1s)
                                        <div class="mkdf-col-animated-bg wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12 vc_col-has-fill">
                                            <div class="vc_column-inner vc_custom_1521822946194">
                                                <div class="wpb_wrapper">
                                                    <div class="mkdf-elements-holder   mkdf-one-column  mkdf-responsive-mode-768 " >
                                                        <div class="mkdf-eh-item   mkdf-horizontal-alignment-center "  data-item-class="mkdf-eh-custom-7311" data-1280-1600="130px 15%" data-1024-1280="130px 10%" data-768-1024="100px 15%" data-680-768="100px 10%" data-680="100px 20px">
                                                            <div class="mkdf-eh-item-inner">
                                                                <div class="mkdf-eh-item-content mkdf-eh-custom-7311" style="padding: 138px 23%">
                                                                    <h1 class="mkdf-custom-font-holder  mkdf-cf-8920  " style="font-family: Kristi;line-height: 1em;font-weight: 400;color: #ffcc05" data-item-class="mkdf-cf-8920">
                                                                        {{$listregionrow1s->row_title}}
                                                                    </h1>
                                                                    <div class="vc_empty_space"   style="height: 18px" ><span class="vc_empty_space_inner"></span></div>
                                                                    {{--<div class="wpb_text_column wpb_content_element  vc_custom_1521823041611" >--}}
                                                                    {{--<div class="wpb_wrapper">--}}
                                                                    {{--<h2><span style="color: #ffffff;">You Explore World</span></h2>--}}
                                                                    {{--</div>--}}
                                                                    {{--</div>--}}
                                                                    <div class="vc_empty_space"   style="height: 15px" ><span class="vc_empty_space_inner"></span></div>
                                                                    <div class="wpb_text_column wpb_content_element " >
                                                                        <div class="wpb_wrapper">
                                                                            <p style="text-align: justify;">
                                                                                <span style="color: #ffffff;">
                                                                                     <?php
                                                                                    $text = $listregionrow1s->row_text;
                                                                                    echo $text;
                                                                                    ?>
                                                                                </span>
                                                                            </p>

                                                                        </div>
                                                                    </div>
                                                                    <div class="vc_empty_space"   style="height: 27px" ><span class="vc_empty_space_inner"></span></div>

                                                                    <div class="vc_empty_space"   style="height: 26px" ><span class="vc_empty_space_inner"></span></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12 vc_col-has-fill">
                                            <div class="vc_column-inner vc_custom_1519746966416" style="padding-top: 0px !important;background-image: url('AboutRegionRow1/{{$listregionrow1s->row_cover_pic}}') !important;background-position: center !important; background-repeat: inherit !important;background-size: cover !important;">
                                                <div class="wpb_wrapper">
                                                    <div class="mkdf-elements-holder   mkdf-one-column  mkdf-responsive-mode-768 " >
                                                        <div class="mkdf-eh-item    "  data-item-class="mkdf-eh-custom-2100" data-1280-1600="0px" data-1024-1280="0px" data-768-1024="300px" data-680-768="300px" data-680="200px">
                                                            <div class="mkdf-eh-item-inner">
                                                                <div class="mkdf-eh-item-content mkdf-eh-custom-2100" style="padding: 0px">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                                    </div>
                                    <div class="mkdf-row-grid-section-wrapper" style="padding-top: 0px !important;background-image: url(../../../frontend/assets/images/37887793102_013b13cc30_k.jpg) !important;background-position: center !important; background-repeat: inherit !important;background-size: cover !important;">
                                        <div class="mkdf-row-grid-section">
                                            <div class="vc_row wpb_row vc_row-fluid vc_custom_1521449273073" >
                                                <div class="wpb_column vc_column_container vc_col-sm-12">
                                                    <div class="vc_column-inner vc_custom_1518432408753">

                                                        <div class="vc_row wpb_row vc_row-fluid vc_custom_1521556287933 vc_row-has-fill vc_row-o-equal-height vc_row-o-content-middle vc_row-flex mkdf-content-aligment-left" >
                                                            @foreach($listregionrow2 as $listregionrow2s)
                                                            <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12 vc_col-has-fill">
                                                                <div class="vc_column-inner vc_custom_1518432670305" style="background-image: url('AboutRegionRow2/{{$listregionrow2s->row_cover_pic}}') !important; background-size: contain;">
                                                                    <div class="wpb_wrapper">
                                                                        <div class="mkdf-elements-holder   mkdf-one-column  mkdf-responsive-mode-768 " >
                                                                            <div class="mkdf-eh-item    "  data-item-class="mkdf-eh-custom-4325" data-1280-1600="0px 0px 0px 116px" data-1024-1280="0px 0px 0px 116px" data-768-1024="120px 0px 120px 211px " data-680-768="118px 0px 118px 152px " data-680="50px 0px 50px 33px">
                                                                                <div class="mkdf-eh-item-inner">
                                                                                    <div class="mkdf-eh-item-content mkdf-eh-custom-4325" style="padding: 0px 0px 0px 245px">

                                                                                        <div class="vc_empty_space"   style="height: 50px" ><span class="vc_empty_space_inner"></span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="mkdf-col-animated-bg wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12 vc_col-has-fill">
                                                                <div class="vc_column-inner vc_custom_1521556270901"><div class="wpb_wrapper">
                                                                        <div class="mkdf-elements-holder   mkdf-one-column  mkdf-responsive-mode-768 " >
                                                                            <div class="mkdf-eh-item    "  data-item-class="mkdf-eh-custom-8937" data-1280-1600=" 197px 15% 135px 80px" data-1024-1280="197px 12% 135px 70px" data-768-1024="107px 36% 107px 80px" data-680-768="107px 25% 107px 80px" data-680="107px 23px 107px">
                                                                                <div class="mkdf-eh-item-inner">
                                                                                    <div class="mkdf-eh-item-content mkdf-eh-custom-8937" style="padding: 197px 35% 135px 80px">
                                                                                        <div class="mkdf-section-title-holder  mkdf-st-standard mkdf-st-title-left mkdf-st-normal-space " style="padding: 0 0%;text-align: left">
                                                                                            <div class="mkdf-st-inner">
                                                                                                <h1 class="mkdf-custom-font-holder  mkdf-cf-8920  " style="font-family: Kristi;line-height: 1em;font-weight: 400;color: #ffcc05" data-item-class="mkdf-cf-8920">
                                                                                                    {{$listregionrow2s->row_title}}
                                                                                                </h1>
                                                                                                <p style="text-align: justify;"><span style="color: #818181;">
                                                                                                         <?php
                                                                                                        $text = $listregionrow2s->row_text;
                                                                                                        echo $text;
                                                                                                        ?>
                                                                                                    </span></p>
                                                                                            </div>
                                                                                        </div><div class="vc_empty_space"   style="height: 26px" ><span class="vc_empty_space_inner"></span></div>
                                                                                        <a itemprop="url" href="" target="_self"  class="mkdf-btn mkdf-btn-medium mkdf-btn-simple"  >
                                                                                            <span class="mkdf-btn-text">See More</span>
                                                                                        </a><div class="vc_empty_space"   style="height: 28px" ><span class="vc_empty_space_inner"></span></div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            @endforeach

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                    </div>
                                    <div class="vc_row wpb_row vc_row-fluid vc_row-o-equal-height vc_row-o-content-middle vc_row-flex mkdf-content-aligment-center" style="background-color:#1c1c1c">
                                        <div class="mkdf-col-animated-bg wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12 vc_col-has-fill">
                                            <div class="vc_column-inner vc_custom_1521822946194">
                                                <div class="wpb_wrapper">
                                                    <div class="mkdf-elements-holder   mkdf-one-column  mkdf-responsive-mode-768 " >
                                                        <div class="mkdf-eh-item   mkdf-horizontal-alignment-center "  data-item-class="mkdf-eh-custom-7311" data-1280-1600="130px 15%" data-1024-1280="130px 10%" data-768-1024="100px 15%" data-680-768="100px 10%" data-680="100px 20px">
                                                            <div class="mkdf-eh-item-inner">
                                                                <div class="mkdf-eh-item-content mkdf-eh-custom-7311" style="padding: 138px 23%">
                                                                    <h1 class="mkdf-custom-font-holder  mkdf-cf-8920  " style="font-family: Kristi;line-height: 1em;font-weight: 400;color: #ffcc05" data-item-class="mkdf-cf-8920">
                                                                        Explore Region
                                                                    </h1>
                                                                    <div class="vc_empty_space"   style="height: 18px" ><span class="vc_empty_space_inner"></span></div>
                                                                    {{--<div class="wpb_text_column wpb_content_element  vc_custom_1521823041611" >--}}
                                                                    {{--<div class="wpb_wrapper">--}}
                                                                    {{--<h2><span style="color: #ffffff;">You Explore World</span></h2>--}}
                                                                    {{--</div>--}}
                                                                    {{--</div>--}}
                                                                    <div class="vc_empty_space"   style="height: 15px" ><span class="vc_empty_space_inner"></span></div>
                                                                    <div class="wpb_text_column wpb_content_element " >
                                                                        <div class="wpb_wrapper">
                                                                            <p style="text-align: justify;">
                                                                                <span style="color: #ffffff;">
                                                                                    Regardless of how you travel, the area has a lot to offer: Visitors can stopover at one of the many tea and coffee plantations, track through the rain forests on the search of primates and rare bird species, enjoy water-based activities such as kayaking and visit local community projects to experience Rwandan history and culture .The entire trek is 227 km long and can be completed within 10 days of hiking or 5 days of biking. Additionally, there are boat services offered between Rusizi, Karongi and Rubavu. Regardless of how you travel, the area has a lot to offer: Visitors can stopover at one of the many tea and coffee plantations, track through the rain forests on the search of primates and rare bird species, enjoy waterbased activities such as kayaking and visit local community projects to experience Rwandan history and culture.
                                                                                </span>
                                                                            </p>

                                                                        </div>
                                                                    </div>
                                                                    <div class="vc_empty_space"   style="height: 27px" ><span class="vc_empty_space_inner"></span></div>

                                                                    <div class="vc_empty_space"   style="height: 26px" ><span class="vc_empty_space_inner"></span></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12 vc_col-has-fill">
                                            <div class="vc_column-inner vc_custom_1519746966416" style="padding-top: 0px !important;background-image: url(../../../frontend/assets/images/37935401435_f433416ee0_h.jpg) !important;background-position: center !important; background-repeat: inherit !important;background-size: cover !important;">
                                                <div class="wpb_wrapper">
                                                    <div class="mkdf-elements-holder   mkdf-one-column  mkdf-responsive-mode-768 " >
                                                        <div class="mkdf-eh-item    "  data-item-class="mkdf-eh-custom-2100" data-1280-1600="0px" data-1024-1280="0px" data-768-1024="300px" data-680-768="300px" data-680="200px">
                                                            <div class="mkdf-eh-item-inner">
                                                                <div class="mkdf-eh-item-content mkdf-eh-custom-2100" style="padding: 0px">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    @include('layouts.newfooter')
@endsection
