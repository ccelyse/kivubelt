@extends('layouts.master')

@section('title', 'Kivu Belt')

@section('content')

@include('layouts.topmenu')
<style>
    p{
        font-family: "Cabin", sans-serif !important;
    }
    .site-header {
        background: #fff !important;
    }
    .main-navigation--white > li > a {
        color: #603b12 !important;
    }
</style>
<div class="single-listing-header single-listing-header-2 parallax"></div>
<div class="page-content page-content--no-b-bottom page-content--no-t-bottom">
    <section class="single-listing single-listing--layout-4">
        <div class="listing-main bg-wild-sand">
            <div class="container">
                @foreach($getmore as $moreinfo)
                <style>
                    .single-listing-header-2 {
                        background-image: url('Placecoverimage/{{$moreinfo->generalimage}}');
                    }
                </style>
                <div class="row">

                    <div class="col-lg-8 order-lg-1 listing-section-container">
                        <div class="listing-header-wrapper bg-white hover-effect">
                            <header class="listing-header">
                                <div class="listing-header__container">
                                    <div class="listing-header__main">
                                        <div class="d-flex">


                                            <div class="listing-header__content">
                                                <div class="d-sm-flex align-items-sm-center">
                                                    <h2 class="listing-header__title">{{$moreinfo->placetitle}}</h2>
                                                </div>

                                            </div><!-- .listing-header__content -->
                                        </div><!-- .listing__wrapper -->
                                    </div><!-- .listing-header__main -->
                                </div><!-- .listing-header__container -->
                            </header><!-- .listing-header -->
                        </div><!-- .listing-section -->
                        <div id="gallery" class="listing-section bg-white hover-effect" data-matching-link="#gallery-link">
                            <div class="listing-section__header">
                                <h3 class="listing-section__title">Gallery</h3>
                            </div><!-- .listing-section__header -->
                            <?php
                            $id = $moreinfo->id;
                            $getpic = \App\EatDrinkGallery::where('eatdrinkid',$id)->get();
                            ?>

                            <div class="swiper-container listing-gallery-top">
                                <div class="swiper-wrapper">
                                    @foreach($getpic as $pic)
                                        <div class="swiper-slide">
                                            <img src="PlacesPictures/{{$pic->eatdrinkpicturename}}" alt="Listing Image">
                                        </div>
                                    @endforeach
                                </div><!-- .swiper-wrapper -->
                                <span class="ion-chevron-left c-white listing-button listing-button-prev"></span>
                                <span class="ion-chevron-right c-white listing-button listing-button-next"></span>
                            </div><!-- .listing-gallery-top -->

                            <div class="swiper-container listing-gallery-thumb">
                                <div class="swiper-wrapper">
                                    @foreach($getpic as $pic)
                                        <div class="swiper-slide">
                                            <img src="PlacesPictures/{{$pic->eatdrinkpicturename}}" alt="Listing Image">
                                        </div>
                                    @endforeach
                                    {{--<div class="swiper-slide is-selected">--}}
                                    {{--<img src="frontend/assets/images/37909793362_0a5ea10b85_o.jpg" alt="Listing Image">--}}
                                    {{--</div>--}}
                                    {{--<div class="swiper-slide">--}}
                                    {{--<img src="frontend/assets/images/Illume.jpg" alt="Listing Image">--}}
                                    {{--</div>--}}
                                    {{--<div class="swiper-slide">--}}
                                    {{--<img src="frontend/assets/images/9TeaEstate3.jpg" alt="Listing Image">--}}
                                    {{--</div>--}}
                                    {{--<div class="swiper-slide">--}}
                                    {{--<img src="frontend/assets/images/NY3.jpg" alt="Listing Image">--}}
                                    {{--</div>--}}
                                </div><!-- .swiper-wrapper -->
                            </div><!-- .listing-gallery-thumb -->

                        </div><!-- .listing-section -->


                        {{--<nav class="listing-nav listing-nav--b-margin bg-white">--}}
                            {{--<div class="container">--}}
                                {{--<ul class="min-list inline-list listing-tabs">--}}
                                    {{--<li class="listing-tab is-active">--}}
                                        {{--<a href="single-listing-4.html#about" id="about-link" class="listing-nav-link">--}}
                                            {{--About--}}
                                        {{--</a>--}}
                                    {{--</li><!-- .listing-tab -->--}}

                                    {{--<li class="listing-tab">--}}
                                        {{--<a href="single-listing-4.html#gallery" id="gallery-link" class="listing-nav-link">--}}
                                            {{--Gallery--}}
                                        {{--</a>--}}
                                    {{--</li><!-- .listing-tab -->--}}

                                    {{--<li class="listing-tab">--}}
                                        {{--<a href="single-listing-4.html#reviews" id="reviews-link" class="listing-nav-link">--}}
                                            {{--Reviews--}}
                                        {{--</a>--}}
                                    {{--</li><!-- .listing-tab -->--}}

                                    {{--<li class="listing-tab">--}}
                                        {{--<a href="single-listing-4.html#add-review" id="add-review-link" class="listing-nav-link">--}}
                                            {{--Add Review--}}
                                        {{--</a>--}}
                                    {{--</li><!-- .listing-tab -->--}}
                                {{--</ul><!-- .listing-tabs -->--}}
                            {{--</div>--}}
                        {{--</nav>--}}

                        <div id="about" class="listing-section bg-white hover-effect" data-matching-link="#about-link">
                            {{--<div class="listing-section__header">--}}
                                {{--<h3 class="listing-section__title">From the business</h3>--}}
                            {{--</div><!-- .listing-section__header -->--}}

                            <div class="c-dove-gray">
                                <?php
                                $text = $moreinfo->placegeneralinformation;
                                ?>
                                <p><?php echo $text; ?><p>
                            </div>
                        </div><!-- .listing-section -->


                    </div><!-- .col -->

                    <div class="col-lg-4 order-lg-0 listing-widget-container">
                        <div class="listing-widget bg-white hover-effect">
                            <p class="listing-timer__desc" style="font-size: 16px !important;">About Rwanda</p>
                        </div><!-- .listing-widget -->

                        <div class="listing-widget bg-white hover-effect">
                            <div class="listing-map-container">
                                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1144449.7472087909!2d29.219740633766484!3d-2.010427086545194!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x19c29654e73840e3%3A0x7490b026cbcca103!2sRwanda!5e0!3m2!1sen!2srw!4v1551773254575" width="100%" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
                            </div><!-- .listing-map-container -->

                            <ul class="min-list listing-contact-list">
                                <li class="d-flex align-items-center c-silver-charlice listing-contact">
                                    <i class="fa fa-building listing-contact__icon"></i>
                                    <span class="c-primary"><strong>Capital city:</strong> Kigali</span>
                                </li>

                                <li class="d-flex align-items-center c-silver-charlice listing-contact">
                                    <i class="fa fa-clock listing-contact__icon"></i>
                                    <a href="tel:+442077391628"><strong>Timezone:</strong> CAT </a>
                                </li>

                                <li class="d-flex align-items-center c-silver-charlice listing-contact">
                                    <i class="fa fa-globe listing-contact__icon"></i>
                                    <a href="single-listing-4.html#"><strong>Population :</strong> 12,374,397</a>
                                </li>
                                <li class="d-flex align-items-center c-silver-charlice listing-contact">
                                    <i class="fa fa-language listing-contact__icon"></i>
                                    <a href="single-listing-4.html#"><strong>Official language : </strong>Kinyarwanda, English , French , Swahili</a>
                                </li>
                                <li class="d-flex align-items-center c-silver-charlice listing-contact">
                                    <i class="fas fa-money-check listing-contact__icon"></i>
                                    <a href="single-listing-4.html#"><strong>Currency :</strong> RWF</a>
                                </li>
                                <li class="d-flex align-items-center c-silver-charlice listing-contact">
                                    <i class="fa fa-map listing-contact__icon"></i>
                                    <a href="single-listing-4.html#"><strong>Surface area :</strong> 26,338 Km²</a>
                                </li>
                                <li class="d-flex align-items-center c-silver-charlice listing-contact">
                                    <a href="single-listing-4.html#"><strong>Flag :</strong> <img src="frontend/assets/images/flag.png" style="    width: 100px;"> </a>
                                </li>
                            </ul><!-- .listing-contact-list -->
                        </div><!-- .listing-widget -->

                    </div><!-- .col -->
                </div><!-- .row -->
                @endforeach
            </div>
        </div>
    </section><!-- .single-listing -->
</div><!-- .page-content -->
@include('layouts.footer')
@endsection
