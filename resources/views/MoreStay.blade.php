@extends('layouts.newmaster')

@section('title', 'Kivu Belt')

@section('content')
    <style>
        p{
            font-family: Cabin,sans-serif !important;
            line-height: 2 !important;
        }
    </style>
    <div class="mkdf-wrapper">
        <div class="mkdf-wrapper-inner">
            @include('layouts.newtopmenu')
            <div class="mkdf-content" style="margin-top: -80px">
                <div class="mkdf-content-inner">
                    @foreach($getmore as $moreinfo)
                        <div class="mkdf-full-width">
                            <div class="mkdf-full-width-inner">
                                <div class="mkdf-grid-row">
                                    <div class="mkdf-page-content-holder mkdf-grid-col-12">
                                        <div class="mkdf-title-holder mkdf-centered-type mkdf-title-va-window-top mkdf-preload-background mkdf-has-bg-image mkdf-bg-parallax" style="height: 515px;background-color: #303030;background-image:url('Placecoverimage/{{$moreinfo->generalimage}}'); background-size: cover;" data-height="435">
                                            <div class="mkdf-title-image">
                                                <img itemprop="image" src="Placecoverimage/{{$moreinfo->generalimage}}" alt="a" />
                                            </div>
                                            <div class="mkdf-title-wrapper" >
                                                <div class="mkdf-title-inner">
                                                    <div class="mkdf-grid">
                                                        <h1 class="mkdf-page-title entry-title" >{{$moreinfo->thingstitle}}</h1>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="mkdf-container mkdf-default-page-template">
                            <div class="mkdf-container-inner clearfix">
                                <div class="mkdf-grid-row">
                                    <div class="mkdf-page-content-holder mkdf-grid-col-9">
                                        <div class="vc_row wpb_row vc_row-fluid vc_custom_1520853432029" >
                                            <div class="wpb_column vc_column_container vc_col-sm-12">
                                                <div class="vc_column-inner ">
                                                    <div class="wpb_wrapper">
                                                        <div class="wpb_text_column wpb_content_element  vc_custom_1519311743473" >
                                                            <div class="wpb_wrapper">
                                                                <h3>{{$moreinfo->placetitle}}</h3>
                                                            </div>
                                                        </div>
                                                        <div class="vc_empty_space"   style="height: 2px" >
                                                            <span class="vc_empty_space_inner">

                                                            </span>
                                                        </div>
                                                        <div class="wpb_text_column wpb_content_element " >
                                                            <div class="wpb_wrapper">
                                                                <?php
                                                                $text = $moreinfo->thingsgeneralinformation;
                                                                ?>
                                                                <p><?php echo $text; ?><p>
                                                            </div>
                                                        </div>


                                                        <div class="vc_empty_space"   style="height: 50px" ><span class="vc_empty_space_inner"></span></div>
                                                        <div class="mkdf-image-gallery  mkdf-ig-grid-type mkdf-normal-space  mkdf-image-behavior-lightbox">
                                                            <div class="mkdf-ig-inner mkdf-outer-space mkdf-ig-grid mkdf-ig-three-columns">

                                                                <?php
                                                                $id = $moreinfo->id;
                                                                $getpic = \App\StayGallery::where('stayid',$id)->get();
                                                                ?>

                                                                @foreach($getpic as $pic)
                                                                    <div class="mkdf-ig-image mkdf-item-space">
                                                                        <div class="mkdf-ig-image-inner">
                                                                            <a itemprop="image" class="mkdf-ig-lightbox" href="PlacesPictures/{{$pic->staygallery}}" data-rel="prettyPhoto[image_gallery_pretty_photo-405]" title="destination-1-gallery-2-img-1">
                                                                                <img width="600" height="525" src="PlacesPictures/{{$pic->staygallery}}" class="attachment-full size-full" alt="a" srcset="PlacesPictures/{{$pic->staygallery}}"/></a>
                                                                        </div>
                                                                    </div>
                                                                @endforeach

                                                                {{----}}
                                                                {{--<div class="mkdf-ig-image mkdf-item-space">--}}
                                                                {{--<div class="mkdf-ig-image-inner">--}}
                                                                {{--<a itemprop="image" class="mkdf-ig-lightbox" href="https://wanderers.mikado-themes.com/wp-content/uploads/2018/02/destination-1-gallery-2-img-2.jpg" data-rel="prettyPhoto[image_gallery_pretty_photo-405]" title="destination-1-gallery-2-img-2">--}}
                                                                {{--<img width="600" height="525" src="https://wanderers.mikado-themes.com/wp-content/uploads/2018/02/destination-1-gallery-2-img-2.jpg" class="attachment-full size-full" alt="a" srcset="https://wanderers.mikado-themes.com/wp-content/uploads/2018/02/destination-1-gallery-2-img-2.jpg 600w, https://wanderers.mikado-themes.com/wp-content/uploads/2018/02/destination-1-gallery-2-img-2-300x263.jpg 300w, https://wanderers.mikado-themes.com/wp-content/uploads/2018/02/destination-1-gallery-2-img-2-450x394.jpg 450w" sizes="(max-width: 600px) 100vw, 600px" />											</a>--}}
                                                                {{--</div>--}}
                                                                {{--</div>--}}
                                                                {{--<div class="mkdf-ig-image mkdf-item-space">--}}
                                                                {{--<div class="mkdf-ig-image-inner">--}}
                                                                {{--<a itemprop="image" class="mkdf-ig-lightbox" href="https://wanderers.mikado-themes.com/wp-content/uploads/2018/02/destination-1-gallery-2-img-3.jpg" data-rel="prettyPhoto[image_gallery_pretty_photo-405]" title="destination-1-gallery-2-img-3">--}}
                                                                {{--<img width="600" height="525" src="https://wanderers.mikado-themes.com/wp-content/uploads/2018/02/destination-1-gallery-2-img-3.jpg" class="attachment-full size-full" alt="a" srcset="https://wanderers.mikado-themes.com/wp-content/uploads/2018/02/destination-1-gallery-2-img-3.jpg 600w, https://wanderers.mikado-themes.com/wp-content/uploads/2018/02/destination-1-gallery-2-img-3-300x263.jpg 300w, https://wanderers.mikado-themes.com/wp-content/uploads/2018/02/destination-1-gallery-2-img-3-450x394.jpg 450w" sizes="(max-width: 600px) 100vw, 600px" />											</a>--}}
                                                                {{--</div>--}}
                                                                {{--</div>--}}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>

    @include('layouts.newfooter')
@endsection

