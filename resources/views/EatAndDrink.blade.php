@extends('layouts.master')

@section('title', 'Kivu Belt')

@section('content')
<style>
    .page-banner--layout-1 {
        background-image: url(frontend/assets/images/38450293282_491bef94d4_o.jpg);
        background-size:cover ;
    }
    .category--layout-2 .category {
        background-repeat: no-repeat;
        background-size: cover;
        background-position: center;
        padding: 70px;
    }
    .category__title {
        font-size: 1.9375rem !important;
        margin-bottom: 0;
    }
    .c-white {
        color: #fff !important;
    }
    .page-banner--layout-1 {
        padding: 300px 0 135px !important;
    }
    .main-navigation--white > li > a {
        color: #fff !important;
        font-weight: bold;
        font-size: 15px;
    }
</style>
@include('layouts.topmenu')
<section class="page-banner page-banner--layout-1 parallax">
    <div class="container">
        <div class="page-banner__container animated fadeInUp">
            <div class="page-banner__textcontent t-center">
                <h2 class="page-banner__title c-white">Eat and Drink</h2>
                {{--<p class="page-banner__subtitle c-white">Find the best places to stay, eat, drink, or visit.</p>--}}
            </div><!-- .page-banner__textcontent -->
        </div><!-- .page-banner__container -->
    </div><!-- .container -->
</section><!-- .page-banner -->
<section class="listing-list listing-list--layout-1">
    <div class="container">
        <div class="row">
            @foreach($listeatdrinks as $eat)
            <div class="col-md-4" style="padding-bottom: 15px">
                <div class="listing hover-effect">
                    <div class="listing__wrapper">
                        <div class="listing__thumbnail">
                            <a href="{{ route('MoreEatAndDrink',['id'=> $eat->id])}}">
                                <img src="Placecoverimage/{{$eat->coverimage}}" alt="{{$eat->placetitle}}">
                                <span class="label label--primary">{{$eat->placecategory}}</span>
                                <span class="favorite c-white">
                                  {{--<i class="fab fa-heart"></i>--}}
                                    <i class="fas fa-heart"></i>
                                </span>
                            </a>
                        </div><!-- .listing__thumbnail -->
                        <div class="listing__detail">
                            <h3 class="listing__title">
                                <a href="{{ route('MoreEatAndDrink',['id'=> $eat->id])}}">{{$eat->placetitle}}</a>
                            </h3>
                            <p class="listing__location c-dusty-gray">
                                <i class="fa fa-map-marker"></i>
                                {{$eat->placelocation}}
                            </p>

                        </div><!-- .listing__detail -->
                    </div><!-- .listing__wrapper -->
                </div><!-- .listing -->
            </div><!-- .col -->
            @endforeach

        </div><!-- .row -->

    </div><!-- .container -->
</section><!-- .listing-list -->

@include('layouts.footer')
@endsection
