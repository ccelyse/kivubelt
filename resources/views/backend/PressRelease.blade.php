@extends('backend.layout.master')

@section('title', 'Connect Africa')

@section('content')
    <body class="vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar" data-open="click" data-menu="vertical-menu" data-col="2-columns">
    @include('backend.layout.sidemenu')
    @include('backend.layout.upmenu')
    <style>
        .btn-secondary{
            color:#fff !important;
            background-color: #6a442b !important;
            border-color:#6a442b !important;
        }
        .btn-primary{
            background-color: #b4753c  !important;
            border-color:#b4753c  !important;
        }
        .btn-primary:hover{
            background-color: #b4753c !important;
            border-color:#b4753c !important;
        }
        .btn-dark {
            color: #000 !important;
             background-color: transparent;
             border-color: transparent;
        }
        .btn-dark:hover {
            color: #000 !important;
            background-color: transparent;
            border-color: transparent;
        }
        iframe{
            width: 100%;
        }

    </style>
    <script
    src="https://code.jquery.com/jquery-3.3.1.min.js"
    integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
    crossorigin="anonymous"></script>
    <script type="text/javascript">
        $(document).ready(function()
        {
            $('textarea').each(function () {
                this.setAttribute('style', 'height:' + (this.scrollHeight) + 'px;overflow-y:hidden;');
            }).on('input', function () {
                this.style.height = 'auto';
                this.style.height = (this.scrollHeight) + 'px';
            });

        });
    </script>

    <div class="app-content content">
        <div class="content-wrapper">
                <div class="content-body">
                    <!-- Basic Summernote start -->
                    <div class="row match-height">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    @if (session('success'))
                                        <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                                            {{ session('success') }}
                                        </div>
                                    @endif
                                    <h4 class="card-title" id="basic-layout-form">Press Information</h4>
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                            <li><a data-action="close"><i class="ft-x"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body">
                                        <form class="form-horizontal form-simple" method="POST" action="{{ url('AddPressRelease') }}" enctype="multipart/form-data">
                                            {{ csrf_field() }}
                                            <div class="form-body">

                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Press Release Title</label>
                                                            <input type="text" id="projectinput1" class="form-control" value="{{ old('news_title') }}" placeholder="News Title"
                                                                   name="news_title" required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Press Release Description</label>
                                                            <textarea  name="newsdescription" class="form-control" id="newsdescription" rows="3"  required></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Press Release Cover Pictures</label>
                                                            <input type="file"  name="presscoverpicture" class="form-control" id="presscoverpicture" rows="3"  required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <h4 class="form-section"><i class="fas fa-clipboard-check"></i> Press Release In details</h4>
                                                <div class="row">
                                                    <section id="basic" style="width: 100%;">
                                                        <div class="card">
                                                            <div class="card-content collapse show">
                                                                <div class="card-body">
                                                                    <form class="form-horizontal" action="#">
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-lg-12">
                                                                                    <textarea class="summernote"  name="news_details" id="news_details" required>{{ old('news_details') }}</textarea>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </section>
                                                </div>
                                                <button type="submit" class="btn btn-primary"> <i class="la la-check-square-o"></i> Save</button>
                                            </div>

                                        </form>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <section id="summernote-edit-save" hidden>
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h4 class="card-title">Summernote Click to edit</h4>
                                        <a class="heading-elements-toggle"><i class="la la-ellipsis-h font-medium-3"></i></a>
                                        <div class="heading-elements">
                                            <ul class="list-inline mb-0">
                                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                                <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                                <li><a data-action="close"><i class="ft-x"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="card-content collapse show">
                                        <div class="card-body">
                                            <form class="form-horizontal" action="#">
                                                <div class="form-group">
                                                    <button id="edit" class="btn btn-primary" type="button"><i class="la la-pencil"></i> Edit</button>
                                                    <button id="save" class="btn btn-success" type="button"><i class="la la-save"></i> Save</button>
                                                </div>
                                                <div class="form-group">
                                                    <div class="summernote-edit">
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    {{--<section id="summernote-edit-save" hidden>--}}
                        {{--<div class="row">--}}
                            {{--<div class="col-12">--}}
                                {{--<div class="card">--}}
                                    {{--<div class="card-header">--}}
                                        {{--<h4 class="card-title">Summernote Click to edit</h4>--}}
                                        {{--<a class="heading-elements-toggle"><i class="la la-ellipsis-h font-medium-3"></i></a>--}}
                                        {{--<div class="heading-elements">--}}
                                            {{--<ul class="list-inline mb-0">--}}
                                                {{--<li><a data-action="collapse"><i class="ft-minus"></i></a></li>--}}
                                                {{--<li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>--}}
                                                {{--<li><a data-action="expand"><i class="ft-maximize"></i></a></li>--}}
                                                {{--<li><a data-action="close"><i class="ft-x"></i></a></li>--}}
                                            {{--</ul>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                    {{--<div class="card-content collapse show">--}}
                                        {{--<div class="card-body">--}}
                                            {{--<form class="form-horizontal" action="#">--}}
                                                {{--<div class="form-group">--}}
                                                    {{--<button id="edit" class="btn btn-primary" type="button"><i class="la la-pencil"></i> Edit</button>--}}
                                                    {{--<button id="save" class="btn btn-success" type="button"><i class="la la-save"></i> Save</button>--}}
                                                {{--</div>--}}
                                                {{--<div class="form-group">--}}
                                                    {{--<div class="summernote-edit">--}}
                                                    {{--</div>--}}
                                                {{--</div>--}}
                                            {{--</form>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</section>--}}
                </div>
            <section id="form-control-repeater">
                <div class="row">
                    <div class="col-12">
                        <div class="card">

                            <div class="card-content collapse show">
                                <div class="card-body card-dashboard">
                                    <table class="table table-striped table-bordered dataex-html5-export table-responsive">
                                        <thead>
                                        <tr>
                                            <th>Press Release Title</th>
                                            <th>Press Release Description</th>
                                            <th>Press Release Details</th>
                                            <th>Date Created</th>
                                            <th>Edit</th>
                                            <th>Delete</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($listpress as $datas)
                                            <tr>
                                                <td>{{$datas->news_title}}</td>
                                                <td>{{$datas->newsdescription}}</td>
                                                <td>
                                                    <button type="button" class="btn btn-icon btn-outline-primary"
                                                            data-toggle="modal"
                                                            data-target="#eventdesc{{$datas->id}}">
                                                        View Press Release
                                                    </button>
                                                    <!-- Modal -->
                                                    <div class="modal fade text-left" id="eventdesc{{$datas->id}}" tabindex="-1"
                                                         role="dialog" aria-labelledby="myModalLabel1"
                                                         aria-hidden="true">
                                                        <div class="modal-dialog" role="document">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal"
                                                                            aria-label="Close">
                                                                        <span aria-hidden="true">&times;</span>
                                                                    </button>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <?php $var = $datas->news_details?>
                                                                    <div><?php echo $var; ?></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>{{$datas->created_at}}</td>
                                                <td>
                                                    <button type="button" class="btn btn-icon btn-outline-primary"
                                                            data-toggle="modal"
                                                            data-target="#editpress{{$datas->id}}">
                                                        Edit
                                                    </button>
                                                    <!-- Modal -->
                                                    <div class="modal fade text-left" id="editpress{{$datas->id}}" tabindex="-1"
                                                         role="dialog" aria-labelledby="myModalLabel1"
                                                         aria-hidden="true">
                                                        <div class="modal-dialog" role="document">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal"
                                                                            aria-label="Close">
                                                                        <span aria-hidden="true">&times;</span>
                                                                    </button>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <form class="form-horizontal form-simple" method="POST" action="{{ url('EditPressRelease') }}" enctype="multipart/form-data">
                                                                        {{ csrf_field() }}
                                                                        <div class="form-body">

                                                                            <div class="row">
                                                                                <div class="col-md-12">
                                                                                    <div class="form-group">
                                                                                        <label for="projectinput1">Press Release Title</label>
                                                                                        <input type="text" id="projectinput1" class="form-control" value="{{$datas->news_title}}" placeholder="News Title"
                                                                                               name="news_title">
                                                                                        <input type="text" id="projectinput1" class="form-control" value="{{$datas->id}}"
                                                                                               name="id">
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="col-md-12">
                                                                                    <div class="form-group">
                                                                                        <label for="projectinput1">Press Release Description</label>
                                                                                        <textarea  name="newsdescription" class="form-control" id="newsdescription" rows="3"  required>{{$datas->newsdescription}}</textarea>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <h4 class="form-section"><i class="fas fa-clipboard-check"></i> Press Release In details</h4>
                                                                            <div class="row">
                                                                                <section id="basic" style="width: 100%;">
                                                                                    <div class="card">
                                                                                        <div class="card-content collapse show">
                                                                                            <div class="card-body">
                                                                                                <form class="form-horizontal" action="#">
                                                                                                    <div class="form-group">
                                                                                                        <div class="row">
                                                                                                            <div class="col-lg-12">
                                                                                                                <textarea class="summernote"  name="news_details" id="news_details" required>{{$datas->news_details}}</textarea>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </form>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </section>
                                                                            </div>
                                                                            <button type="submit" class="btn btn-primary"> <i class="la la-check-square-o"></i> Update</button>
                                                                        </div>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td><a href="{{ route('backend.DeletePressRelease',['id'=> $datas->id])}}" class="btn btn-icon btn-outline-primary">Delete</a></td>

                                            </tr>
                                        @endforeach
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </section>
                    <!-- Summernote Click to edit end -->
                    <script src="backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
            <script src="backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
            <script src="backend/app-assets/vendors/js/editors/summernote/summernote.js" type="text/javascript"></script>
            <script src="backend/app-assets/js/scripts/editors/editor-summernote.min.js" type="text/javascript"></script>

            {{--<script src="backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>--}}
            <script src="backend/app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js" type="text/javascript"></script>
            <script src="backend/app-assets/js/scripts/ui/jquery-ui/date-pickers.min.js" type="text/javascript"></script>
            <script src="backend/app-assets/vendors/js/pickers/pickadate/picker.js" type="text/javascript"></script>
            <script src="backend/app-assets/vendors/js/pickers/pickadate/picker.date.js" type="text/javascript"></script>
            <script src="backend/app-assets/vendors/js/pickers/pickadate/picker.time.js" type="text/javascript"></script>
            <script src="backend/app-assets/vendors/js/pickers/pickadate/legacy.js" type="text/javascript"></script>
            <script src="backend/app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js"
                    type="text/javascript"></script>
            <script src="backend/app-assets/vendors/js/pickers/daterange/daterangepicker.js"
                    type="text/javascript"></script>
            <script src="backend/app-assets/js/scripts/pickers/dateTime/pick-a-datetime.js"
                    type="text/javascript"></script>
            <script src="backend/app-assets/vendors/js/tables/datatable/datatables.min.js" type="text/javascript"></script>
            <script src="backend/app-assets/js/core/app-menu.min.js" type="text/javascript"></script>
            <script src="backend/app-assets/js/core/app.min.js" type="text/javascript"></script>
            <script src="backend/app-assets/js/scripts/customizer.min.js" type="text/javascript"></script>
            <script src="backend/app-assets/js/scripts/tables/datatables/datatable-basic.js"
                    type="text/javascript"></script>


        </div>
    </div>

@endsection
