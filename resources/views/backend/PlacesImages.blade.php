@extends('backend.layout.master')

@section('title', 'Connect Africa')

@section('content')

    @include('backend.layout.sidemenu')
    @include('backend.layout.upmenu')
    <script
            src="https://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
            crossorigin="anonymous"></script>
    <style>
        .btn-dark {
            color: #000 !important;
            background-color: transparent;
            border-color: transparent;
        }
        .btn-dark:hover {
            color: #000 !important;
            background-color: transparent;
            border-color: transparent;
        }
        .removeimages{
            position: absolute;
            top: 0;
        }
    </style>

    <div class="app-content content">
        <div class="content-wrapper">
            <section id="complex-header">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <div class="form-grouppop">
                                    <button type="button" class="btn btn-login btn-min-width mr-1 mb-1"
                                            data-toggle="modal"
                                            data-target="#clearchargers">
                                        Add more images
                                    </button>
                                    <!-- Modal -->
                                    <div class="modal fade text-left" id="clearchargers" tabindex="-1"
                                         role="dialog" aria-labelledby="myModalLabel1"
                                         aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title" id="myModalLabel1">Add more images</h4>
                                                    <button type="button" class="close" data-dismiss="modal"
                                                            aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <form class="form-horizontal form-simple" method="POST"
                                                          action="{{ url('PlacesMoreImages') }}"
                                                          enctype="multipart/form-data">
                                                        {{ csrf_field() }}


                                                            <script type="text/javascript">
                                                                $(document).ready(function()
                                                                {
                                                                    $('.multi-field-wrapper').each(function() {
                                                                        var $wrapper = $('.multi-fields', this);
                                                                        $(".add-field", $(this)).click(function(e) {
                                                                            $('.multi-field:first-child', $wrapper).clone(true).appendTo($wrapper).find('input').val('').focus();
                                                                        });
                                                                        $('.multi-field .remove-field', $wrapper).click(function() {
                                                                            if ($('.multi-field', $wrapper).length > 1)
                                                                                $(this).parent('.multi-field').remove();
                                                                        });
                                                                    });
                                                                });
                                                            </script>
                                                            <div class="multi-field-wrapper">
                                                                <div class="multi-fields">
                                                                    <div class="row  multi-field">
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label for="projectinput1">Place Images</label>
                                                                                <input type="file" id="projectinput1" class="form-control"
                                                                                       name="placespicture[]">
                                                                            </div>
                                                                        </div>
                                                                        <button type="button" class="remove-field btn btn-dark" style="line-height: inherit !important;"><i class="fas fa-times-circle"></i></button>
                                                                        <button type="button" class="add-field btn btn-dark" style="line-height: inherit !important;"><i class="fas fa-plus-circle"></i></button>
                                                                    </div>
                                                                </div>
                                                            </div>


                                                            <input type="text" id="patient_id"
                                                                   class="form-control"
                                                                   name="id" value="<?php echo $id;?>" hidden>

                                                            <div class="col-md-12">
                                                                <button type="submit" class="btn btn-login">
                                                                    <i class="la la-check-square-o"></i> Save
                                                                </button>
                                                            </div>
                                                    </form>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </section>
            <div class="content-body">
                <div class="row">
                    @foreach($getimages as $images)
                        <div class="col-md-6">
                           <img src="PlacesPictures/{{$images->places_picture_name}}" style="width: 100%;padding-bottom: 10px">
                            <div class="removeimages">
                                <a href="{{ route('backend.DeletePlacesImages',['id'=> $images->id])}}" class="btn btn-icon btn-outline-primary">Delete</a>
                            </div>
                        </div>
                    @endforeach
                </div>
                <!-- Active Orders -->
            </div>
        </div>
    </div>
    <script src="backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
    <!-- ////////////////////////////////////////////////////////////////////////////-->

@endsection