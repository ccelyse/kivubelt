@extends('backend.layout.master')

@section('title', 'Connect Africa')

@section('content')

    @include('backend.layout.sidemenu')
    @include('backend.layout.upmenu')
<style>
    .btn-secondary{
        color:#fff !important;
        background-color: #6a442b !important;
        border-color:#6a442b !important;
    }
    #ui-datepicker-div{
        padding: 10px; table-responsive;
        background:#6b442b;
    }
    .ui-datepicker-prev,.ui-datepicker-next,.ui-datepicker-calendar{
        color: #fff !important;
        padding: 10px;
    }
    .btn-dark {
        color: #000 !important;
        background-color: transparent;
        border-color: transparent;
    }
    .btn-dark:hover {
        color: #000 !important;
        background-color: transparent;
        border-color: transparent;
    }
    .removeimages{
        position: absolute;
        top: 0;
    }
</style>
    <script
            src="https://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
            crossorigin="anonymous"></script>
    <script type="text/javascript">
        $(document).ready(function()
        {
            $('textarea').each(function () {
                this.setAttribute('style', 'height:' + (this.scrollHeight) + 'px;overflow-y:hidden;');
            }).on('input', function () {
                this.style.height = 'auto';
                this.style.height = (this.scrollHeight) + 'px';
            });

            $('.multi-field-wrapper').each(function() {
                var $wrapper = $('.multi-fields', this);
                $(".add-field", $(this)).click(function(e) {
                    $('.multi-field:first-child', $wrapper).clone(true).appendTo($wrapper).find('input').val('').focus();
                });
                $('.multi-field .remove-field', $wrapper).click(function() {
                    if ($('.multi-field', $wrapper).length > 1)
                        $(this).parent('.multi-field').remove();
                });
            });
            $('.multi-field-wrapper2').each(function() {
                var $wrapper = $('.multi-fields2', this);
                $(".add-field2", $(this)).click(function(e) {
                    $('.multi-field2:first-child', $wrapper).clone(true).appendTo($wrapper).find('input').val('').focus();
                });
                $('.multi-field2 .remove-field2', $wrapper).click(function() {
                    if ($('.multi-field2', $wrapper).length > 1)
                        $(this).parent('.multi-field2').remove();
                });
            });
            $('.multi-field-wrapper3').each(function() {
                var $wrapper = $('.multi-fields3', this);
                $(".add-field3", $(this)).click(function(e) {
                    $('.multi-field3:first-child', $wrapper).clone(true).appendTo($wrapper).find('input').val('').focus();
                });
                $('.multi-field3 .remove-field3', $wrapper).click(function() {
                    if ($('.multi-field3', $wrapper).length > 1)
                        $(this).parent('.multi-field3').remove();
                });
            });
        });
    </script>

    <div class="app-content content">
        <div class="content-wrapper">

            <div class="content-body">
                <section id="basic-form-layouts">
                    <div class="row">
                        <div class="col-12">
                            @if (session('success'))
                                <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                                    {{ session('success') }}
                                </div>
                            @endif

                                    {{--<div class="col-3">--}}
                                        {{--<img src="<?php echo $instagramdata; ?>" style="width:100%;">--}}
                                    {{--</div>--}}

                            @foreach($instagramdata as $data)
                               <div class="col-3">
                                   <img src="{{$data->node->display_url}}" style="width:100%;">
                               </div>
                            @endforeach
                        </div>
                    </div>
            </div>
                </section>

            </div>
        </div>
    </div>
    <script src="backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/ui/jquery-ui/date-pickers.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/picker.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/picker.date.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/picker.time.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/legacy.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js"
            type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/daterange/daterangepicker.js"
            type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/pickers/dateTime/pick-a-datetime.js"
            type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/tables/datatable/datatables.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/app-menu.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/app.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/customizer.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/tables/datatables/datatable-basic.js"
            type="text/javascript"></script>

@endsection
