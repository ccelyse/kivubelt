@extends('backend.layout.master')

@section('title', 'Connect Africa')

@section('content')

    @include('backend.layout.sidemenu')
    @include('backend.layout.upmenu')
<style>
    .btn-secondary{
        color:#fff !important;
        background-color: #6a442b !important;
        border-color:#6a442b !important;
    }
    #ui-datepicker-div{
        padding: 10px; table-responsive;
        background:#6b442b;
    }
    .ui-datepicker-prev,.ui-datepicker-next,.ui-datepicker-calendar{
        color: #fff !important;
        padding: 10px;
    }
    .btn-dark {
        color: #000 !important;
        background-color: transparent;
        border-color: transparent;
    }
    .btn-dark:hover {
        color: #000 !important;
        background-color: transparent;
        border-color: transparent;
    }
    .removeimages{
        position: absolute;
        top: 0;
    }
</style>
    <script
            src="https://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
            crossorigin="anonymous"></script>

    <div class="app-content content">
        <div class="content-wrapper">

            <div class="content-body">
                <section id="basic-form-layouts">
                    <div class="row match-height">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    @if (session('success'))
                                        <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                                            {{ session('success') }}
                                        </div>
                                    @endif
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>

                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body">
                                        <form class="form-horizontal form-simple" method="POST" action="{{ url('AddTourOperatorProfile') }}" enctype="multipart/form-data">
                                            {{ csrf_field() }}
                                            <div class="form-body">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="projectinput1">About Operator</label>
                                                            <textarea name="agent_about"  class="form-control" rows="10">{{ old('agent_about') }}</textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <script type="text/javascript">
                                                    $(document).ready(function()
                                                    {
                                                        $('.multi-field-wrapper').each(function() {
                                                            var $wrapper = $('.multi-fields', this);
                                                            $(".add-field", $(this)).click(function(e) {
                                                                $('.multi-field:first-child', $wrapper).clone(true).appendTo($wrapper).find('input').val('').focus();
                                                            });
                                                            $('.multi-field .remove-field', $wrapper).click(function() {
                                                                if ($('.multi-field', $wrapper).length > 1)
                                                                    $(this).parent('.multi-field').remove();
                                                            });
                                                        });
                                                    });
                                                </script>
                                                <div class="multi-field-wrapper">
                                                    <div class="multi-fields">
                                                        <div class="row  multi-field">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="projectinput1">Services</label>
                                                                    <input type="text" id="projectinput1" class="form-control"
                                                                           name="agent_profile_services[]" value="{{ old('agent_profile_services') }}">
                                                                </div>
                                                            </div>
                                                            <button type="button" class="remove-field btn btn-dark" style="line-height: inherit !important;"><i class="fas fa-times-circle"></i></button>
                                                            <button type="button" class="add-field btn btn-dark" style="line-height: inherit !important;"><i class="fas fa-plus-circle"></i></button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Email Address</label>
                                                            <input type="email" id="projectinput1" class="form-control"
                                                                   name="agent_emailaddress" value="{{ old('agent_emailaddress') }}">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Phone Address</label>
                                                            <input type="text" id="projectinput1" class="form-control"
                                                                   name="agent_phoneaddress" value="{{ old('agent_phoneaddress') }}">
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Address Map</label>
                                                            <textarea name="agent_mapddress" value="{{ old('agent_mapddress') }}" class="form-control" rows="4"></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <script type="text/javascript">
                                                    $(document).ready(function()
                                                    {
                                                        $('.multi-field-wrapper2').each(function() {
                                                            var $wrapper = $('.multi-fields2', this);
                                                            $(".add-field2", $(this)).click(function(e) {
                                                                $('.multi-field2:first-child', $wrapper).clone(true).appendTo($wrapper).find('input').val('').focus();
                                                            });
                                                            $('.multi-field2 .remove-field2', $wrapper).click(function() {
                                                                if ($('.multi-field2', $wrapper).length > 1)
                                                                    $(this).parent('.multi-field2').remove();
                                                            });
                                                        });
                                                    });
                                                </script>
                                                <div class="multi-field-wrapper2">
                                                    <div class="multi-fields2">
                                                        <div class="row  multi-field2">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="projectinput1">Video Link(Youtube Embed Codes)</label>
                                                                    <input type="text" id="projectinput1" class="form-control"
                                                                           name="agent_profile_videolink[]" value="{{ old('agent_profile_videolink') }}">
                                                                </div>
                                                            </div>
                                                            <button type="button" class="remove-field2 btn btn-dark" style="line-height: inherit !important;"><i class="fas fa-times-circle"></i></button>
                                                            <button type="button" class="add-field2 btn btn-dark" style="line-height: inherit !important;"><i class="fas fa-plus-circle"></i></button>
                                                        </div>
                                                    </div>
                                                </div>

                                                <script type="text/javascript">
                                                    $(document).ready(function()
                                                    {
                                                        $('.multi-field-wrapper3').each(function() {
                                                            var $wrapper = $('.multi-fields3', this);
                                                            $(".add-field3", $(this)).click(function(e) {
                                                                $('.multi-field3:first-child', $wrapper).clone(true).appendTo($wrapper).find('input').val('').focus();
                                                            });
                                                            $('.multi-field3 .remove-field3', $wrapper).click(function() {
                                                                if ($('.multi-field3', $wrapper).length > 1)
                                                                    $(this).parent('.multi-field3').remove();
                                                            });
                                                        });
                                                    });
                                                </script>
                                                <div class="multi-field-wrapper3">
                                                    <div class="multi-fields3">
                                                        <div class="row  multi-field3">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="projectinput1">Photo</label>
                                                                    <input type="file" id="projectinput1" class="form-control"
                                                                           name="agent_profile_photoname[]" value="{{ old('agent_profile_photoname') }}">
                                                                </div>
                                                            </div>
                                                            <button type="button" class="remove-field3 btn btn-dark" style="line-height: inherit !important;"><i class="fas fa-times-circle"></i></button>
                                                            <button type="button" class="add-field3 btn btn-dark" style="line-height: inherit !important;"><i class="fas fa-plus-circle"></i></button>
                                                        </div>
                                                    </div>
                                                </div>


                                                <div class="form-actions">
                                                    <button type="submit" class="btn btn-login">
                                                        <i class="la la-check-square-o"></i> Save
                                                    </button>
                                                </div>
                                            </div>
                                        </form>

                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </section>
                <section id="setting">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">About Tour Operator Information</h4>
                                    @if (session('success'))
                                        <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                                            {{ session('success') }}
                                        </div>
                                    @endif
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                            <li><a data-action="close"><i class="ft-x"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body card-dashboard">
                                        <table class="table table-striped table-bordered dataex-html5-export table-responsive">
                                            <thead>
                                            <tr>
                                                <th>About Agent</th>
                                                <th>Address Email</th>
                                                <th>Address Phone</th>
                                                <th>Address Map</th>
                                                <th>Address Services</th>
                                                <th>Agent Videos</th>
                                                <th>Agent Pictures</th>
                                                <th>Edit Profile</th>
                                                <th>Delete Profile</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($listoperatorinfo as $data)
                                                <tr>
                                                    <td>
                                                        <button type="button" class="btn btn-icon btn-outline-primary"
                                                                data-toggle="modal"
                                                                data-target="#about{{$data->id}}">
                                                            View About Agent
                                                        </button>
                                                        <!-- Modal -->
                                                        <div class="modal fade text-left" id="about{{$data->id}}" tabindex="-1"
                                                             role="dialog" aria-labelledby="myModalLabel1"
                                                             aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h4 class="modal-title" id="myModalLabel1"> About Agent</h4>
                                                                        <button type="button" class="close" data-dismiss="modal"
                                                                                aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                        <p>{{$data->agent_about}}</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>{{$data->agent_emailaddress}}</td>
                                                    <td>{{$data->agent_phoneaddress}}</td>
                                                    <td>
                                                        <button type="button" class="btn btn-icon btn-outline-primary"
                                                                data-toggle="modal"
                                                                data-target="#map{{$data->id}}">
                                                            View Map
                                                        </button>
                                                        <!-- Modal -->
                                                        <div class="modal fade text-left" id="map{{$data->id}}" tabindex="-1"
                                                             role="dialog" aria-labelledby="myModalLabel1"
                                                             aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h4 class="modal-title" id="myModalLabel1"> Address Map</h4>
                                                                        <button type="button" class="close" data-dismiss="modal"
                                                                                aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                        <style>
                                                                            iframe{
                                                                                width: 100% !important;
                                                                            }
                                                                        </style>
                                                                        <?php
                                                                        $map = $data->agent_mapddress;
                                                                        echo "<div>$map</div>";
                                                                        ?>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <button type="button" class="btn btn-icon btn-outline-primary"
                                                                data-toggle="modal"
                                                                data-target="#services{{$data->id}}">
                                                            View Services
                                                        </button>
                                                        <!-- Modal -->
                                                        <div class="modal fade text-left" id="services{{$data->id}}" tabindex="-1"
                                                             role="dialog" aria-labelledby="myModalLabel1"
                                                             aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h4 class="modal-title" id="myModalLabel1"> Services</h4>
                                                                        <button type="button" class="close" data-dismiss="modal"
                                                                                aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                        <?php
                                                                        $id = $data->id;
                                                                        $getservices = \App\AgentProfileServices::where('agent_profile_id',$id)->get();
                                                                        foreach ($getservices as $services){
                                                                            $linkdelete = route('backend.DeleteTourOperatorProfileServices',['id'=> $services->id]);
                                                                            echo "<div class='col-md-12'>$services->agent_profile_services <a href='$linkdelete' class='btn btn-dark'><i class='fas fa-times-circle'></i></a></div>";
                                                                        }
                                                                        ?>
                                                                            <form class="form-horizontal form-simple" method="POST" action="{{ url('AddMoreTourOperatorProfileService') }}" enctype="multipart/form-data">
                                                                                {{ csrf_field() }}
                                                                            <script type="text/javascript">
                                                                                $(document).ready(function()
                                                                                {
                                                                                    $('.multi-field-wrapper4').each(function() {
                                                                                        var $wrapper = $('.multi-fields4', this);
                                                                                        $(".add-field4", $(this)).click(function(e) {
                                                                                            $('.multi-field4:first-child', $wrapper).clone(true).appendTo($wrapper).find('input').val('').focus();
                                                                                        });
                                                                                        $('.multi-field4 .remove-field4', $wrapper).click(function() {
                                                                                            if ($('.multi-field4', $wrapper).length > 1)
                                                                                                $(this).parent('.multi-field4').remove();
                                                                                        });
                                                                                    });
                                                                                });
                                                                            </script>
                                                                            <h4 class="modal-title" id="myModalLabel1">Add More Services</h4>
                                                                            <input type="text" id="projectinput1" class="form-control"
                                                                                       name="id" value="{{$data->id}}" hidden>
                                                                            <div class="multi-field-wrapper4">
                                                                                <div class="multi-fields4">
                                                                                    <div class="row  multi-field4">
                                                                                        <div class="col-md-6">
                                                                                            <div class="form-group">
                                                                                                <label for="projectinput1">Services</label>
                                                                                                <input type="text" id="projectinput1" class="form-control"
                                                                                                       name="agent_profile_services[]" value="{{ old('agent_profile_services') }}">
                                                                                            </div>
                                                                                        </div>
                                                                                        <button type="button" class="remove-field4 btn btn-dark" style="line-height: inherit !important;"><i class="fas fa-times-circle"></i></button>
                                                                                        <button type="button" class="add-field4 btn btn-dark" style="line-height: inherit !important;"><i class="fas fa-plus-circle"></i></button>
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                                <div class="form-actions">
                                                                                    <button type="submit" class="btn btn-login">
                                                                                        <i class="la la-check-square-o"></i> Save
                                                                                    </button>
                                                                                </div>
                                                                            </form>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <button type="button" class="btn btn-icon btn-outline-primary"
                                                                data-toggle="modal"
                                                                data-target="#videos{{$data->id}}">
                                                            View Videos
                                                        </button>
                                                        <!-- Modal -->
                                                        <div class="modal fade text-left" id="videos{{$data->id}}" tabindex="-1"
                                                             role="dialog" aria-labelledby="myModalLabel1"
                                                             aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h4 class="modal-title" id="myModalLabel1"> Agent Videos</h4>
                                                                        <button type="button" class="close" data-dismiss="modal"
                                                                                aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                        <?php
                                                                        $id = $data->id;
                                                                        $getvideos = \App\AgentProfileVideo::where('agent_profile_id',$id)->get();
                                                                        foreach ($getvideos as $videos){
                                                                            $videsm= $videos->agent_profile_videolink;
                                                                            $linkdelete = route('backend.DeleteTourOperatorProfileVideos',['id'=> $videos->id]);
                                                                            echo "<div class='col-md-12'><div>$videsm</div><div class='removeimages'><a href='$linkdelete' class='btn btn-login'>Delete</a></div></div>";
                                                                        }
                                                                        ?>
                                                                            <form class="form-horizontal form-simple" method="POST" action="{{ url('AddMoreVideosTourOperatorProfile') }}" enctype="multipart/form-data">
                                                                                {{ csrf_field() }}
                                                                                <script type="text/javascript">
                                                                                    $(document).ready(function()
                                                                                    {
                                                                                        $('.multi-field-wrapper6').each(function() {
                                                                                            var $wrapper = $('.multi-fields6', this);
                                                                                            $(".add-field6", $(this)).click(function(e) {
                                                                                                $('.multi-field6:first-child', $wrapper).clone(true).appendTo($wrapper).find('input').val('').focus();
                                                                                            });
                                                                                            $('.multi-field6 .remove-field6', $wrapper).click(function() {
                                                                                                if ($('.multi-field6', $wrapper).length > 1)
                                                                                                    $(this).parent('.multi-field6').remove();
                                                                                            });
                                                                                        });
                                                                                    });
                                                                                </script>
                                                                                <h4 class="modal-title" id="myModalLabel1">Add More Videos</h4>
                                                                                <input type="text" id="projectinput1" class="form-control"
                                                                                       name="id" value="{{$data->id}}" hidden>
                                                                                <div class="multi-field-wrapper6">
                                                                                    <div class="multi-fields6">
                                                                                        <div class="row  multi-field6">
                                                                                            <div class="col-md-6">
                                                                                                <div class="form-group">
                                                                                                    <label for="projectinput1">Video Link(Youtube Embed Codes)</label>
                                                                                                    <input type="text" id="projectinput1" class="form-control"
                                                                                                           name="agent_profile_videolink[]" value="{{ old('agent_profile_videolink') }}">
                                                                                                </div>
                                                                                            </div>
                                                                                            <button type="button" class="remove-field6 btn btn-dark" style="line-height: inherit !important;"><i class="fas fa-times-circle"></i></button>
                                                                                            <button type="button" class="add-field6 btn btn-dark" style="line-height: inherit !important;"><i class="fas fa-plus-circle"></i></button>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="form-actions">
                                                                                    <button type="submit" class="btn btn-login">
                                                                                        <i class="la la-check-square-o"></i> Save
                                                                                    </button>
                                                                                </div>
                                                                            </form>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <button type="button" class="btn btn-icon btn-outline-primary"
                                                                data-toggle="modal"
                                                                data-target="#Pictures{{$data->id}}">
                                                            View Pictures
                                                        </button>
                                                        <!-- Modal -->
                                                        <div class="modal fade text-left" id="Pictures{{$data->id}}" tabindex="-1"
                                                             role="dialog" aria-labelledby="myModalLabel1"
                                                             aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h4 class="modal-title" id="myModalLabel1"> Pictures</h4>
                                                                        <button type="button" class="close" data-dismiss="modal"
                                                                                aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                        <?php
                                                                        $id = $data->id;
                                                                        $getpictures = \App\AgentProfilePhoto::where('agent_profile_id',$id)->get();
                                                                        foreach ($getpictures as $pictures){
                                                                            $linkdelete = route('backend.DeleteTourOperatorProfileImages',['id'=> $pictures->id]);
                                                                            echo "<div class='col-md-12' style='padding-bottom: 10px'><img src='TourOperator/$pictures->agent_profile_photoname' style='width: 100%'><div class='removeimages'><a href='$linkdelete' class='btn btn-login'>Delete</a></div></div>";
                                                                        }
                                                                        ?>
                                                                        <form class="form-horizontal form-simple" method="POST" action="{{ url('AddMoreImagesTourOperatorProfile') }}" enctype="multipart/form-data">
                                                                            {{ csrf_field() }}
                                                                            <script type="text/javascript">
                                                                                $(document).ready(function()
                                                                                {
                                                                                    $('.multi-field-wrapper7').each(function() {
                                                                                        var $wrapper = $('.multi-fields7', this);
                                                                                        $(".add-field7", $(this)).click(function(e) {
                                                                                            $('.multi-field7:first-child', $wrapper).clone(true).appendTo($wrapper).find('input').val('').focus();
                                                                                        });
                                                                                        $('.multi-field7 .remove-field7', $wrapper).click(function() {
                                                                                            if ($('.multi-field7', $wrapper).length > 1)
                                                                                                $(this).parent('.multi-field7').remove();
                                                                                        });
                                                                                    });
                                                                                });
                                                                            </script>
                                                                            <h4 class="modal-title" id="myModalLabel1">Add More Pictures</h4>
                                                                            <input type="text" id="projectinput1" class="form-control"
                                                                                   name="id" value="{{$data->id}}" hidden>
                                                                            <div class="multi-field-wrapper7">
                                                                                <div class="multi-fields7">
                                                                                    <div class="row  multi-field7">
                                                                                        <div class="col-md-6">
                                                                                            <div class="form-group">
                                                                                                <label for="projectinput1">Photo</label>
                                                                                                <input type="file" id="projectinput1" class="form-control"
                                                                                                       name="agent_profile_photoname[]" value="{{ old('agent_profile_photoname') }}">
                                                                                            </div>
                                                                                        </div>
                                                                                        <button type="button" class="remove-field7 btn btn-dark" style="line-height: inherit !important;"><i class="fas fa-times-circle"></i></button>
                                                                                        <button type="button" class="add-field7 btn btn-dark" style="line-height: inherit !important;"><i class="fas fa-plus-circle"></i></button>
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                            <div class="form-actions">
                                                                                <button type="submit" class="btn btn-login">
                                                                                    <i class="la la-check-square-o"></i> Save
                                                                                </button>
                                                                            </div>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <button type="button" class="btn btn-icon btn-outline-primary"
                                                                data-toggle="modal"
                                                                data-target="#editother{{$data->id}}">
                                                            Edit Other
                                                        </button>
                                                        <!-- Modal -->
                                                        <div class="modal fade text-left" id="editother{{$data->id}}" tabindex="-1"
                                                             role="dialog" aria-labelledby="myModalLabel1"
                                                             aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h4 class="modal-title" id="myModalLabel1"> About Us</h4>
                                                                        <button type="button" class="close" data-dismiss="modal"
                                                                                aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                        <form class="form-horizontal form-simple" method="POST" action="{{ url('EditTourOperatorProfileOther') }}" enctype="multipart/form-data">
                                                                            {{ csrf_field() }}
                                                                            <div class="row">
                                                                                <input type="text" id="projectinput1" class="form-control"
                                                                                       name="id" value="{{$data->id}}" hidden>
                                                                                <div class="col-md-12">
                                                                                    <div class="form-group">
                                                                                        <label for="projectinput1">About Tour Operator</label>
                                                                                        <textarea name="agent_about" class="form-control" rows="20">{{$data->agent_about}}</textarea>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="col-md-6">
                                                                                    <div class="form-group">
                                                                                        <label for="projectinput1">Email Address</label>
                                                                                        <input type="email" id="projectinput1" class="form-control"
                                                                                               name="agent_emailaddress" value="{{$data->agent_emailaddress}}">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-6">
                                                                                    <div class="form-group">
                                                                                        <label for="projectinput1">Phone Address</label>
                                                                                        <input type="text" id="projectinput1" class="form-control"
                                                                                               name="agent_phoneaddress" value="{{$data->agent_phoneaddress}}">
                                                                                    </div>
                                                                                </div>

                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="col-md-12">
                                                                                    <div class="form-group">
                                                                                        <label for="projectinput1">Address Map</label>
                                                                                        <textarea name="agent_mapddress" class="form-control" rows="4">{{$data->agent_mapddress}}</textarea>
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                            <div class="form-actions">
                                                                                <button type="submit" class="btn btn-login">
                                                                                    <i class="la la-check-square-o"></i> Save
                                                                                </button>
                                                                            </div>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td><a href="{{ route('backend.DeleteTourOperatorProfileOther',['id'=> $data->id])}}" class="btn btn-icon btn-outline-primary">Delete</a></td>
                                                </tr>
                                            @endforeach
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
    <script src="backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/ui/jquery-ui/date-pickers.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/picker.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/picker.date.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/picker.time.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/legacy.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js"
            type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/daterange/daterangepicker.js"
            type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/pickers/dateTime/pick-a-datetime.js"
            type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/tables/datatable/datatables.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/app-menu.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/app.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/customizer.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/tables/datatables/datatable-basic.js"
            type="text/javascript"></script>

@endsection
