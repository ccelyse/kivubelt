@extends('backend.layout.master')

@section('title', 'Connect Africa')

@section('content')

    @include('backend.layout.sidemenu')
    @include('backend.layout.upmenu')
<style>
    .btn-secondary{
        color:#fff !important;
        background-color: #6a442b !important;
        border-color:#6a442b !important;
    }
    #ui-datepicker-div{
        padding: 10px; table-responsive;
        background:#6b442b;
    }
    .ui-datepicker-prev,.ui-datepicker-next,.ui-datepicker-calendar{
        color: #fff !important;
        padding: 10px;
    }
    .btn-dark {
        color: #000 !important;
        background-color: transparent;
        border-color: transparent;
    }
    .btn-dark:hover {
        color: #000 !important;
        background-color: transparent;
        border-color: transparent;
    }
    .btn-outline-primary:hover{
        color: #00688b !important;
        background-color: transparent;
        border-color: #00688b !important;
    }
    .removeimages{
        position: absolute;
        top: 0;
    }
    .btn-outline-primary {
        border-color: transparent !important;
        color: #00688b !important;
    }
    .modal-title {
        margin-bottom: 0;
        font-weight: bold !important;
        padding:5px;
    }
    @media (min-width: 576px){
        .modal-dialog {
            max-width: 800px !important;
            margin: 1.75rem auto;
        }
    }
    .select2-container {
        width: 100% !important;
    }
    .select2-container--classic.select2-container--focus, .select2-container--default.select2-container--focus {
        outline: 0;
        width: 100% !important;
    }

</style>
    {{--<meta name="csrf-token" content="{{ csrf_token() }}">--}}
    <script
            src="https://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
            crossorigin="anonymous"></script>
    <script src="http://demo.itsolutionstuff.com/plugin/croppie.js"></script>
    <link rel="stylesheet" type="text/css" href="backend/app-assets/vendors/css/forms/selects/select2.min.css">
    <link rel="stylesheet" href="http://demo.itsolutionstuff.com/plugin/croppie.css">
    <script type="text/javascript">
        $(document).ready(function()
        {
            $('textarea').each(function () {
                this.setAttribute('style', 'height:' + (this.scrollHeight) + 'px;overflow-y:hidden;');
            }).on('input', function () {
                this.style.height = 'auto';
                this.style.height = (this.scrollHeight) + 'px';
            });

            $('.multi-field-wrapper').each(function() {
                var $wrapper = $('.multi-fields', this);
                $(".add-field", $(this)).click(function(e) {
                    $('.multi-field:first-child', $wrapper).clone(true).appendTo($wrapper).find('input').val('').focus();
                });
                $('.multi-field .remove-field', $wrapper).click(function() {
                    if ($('.multi-field', $wrapper).length > 1)
                        $(this).parent('.multi-field').remove();
                });
            });
            $('.multi-field-wrapper2').each(function() {
                var $wrapper = $('.multi-fields2', this);
                $(".add-field2", $(this)).click(function(e) {
                    $('.multi-field2:first-child', $wrapper).clone(true).appendTo($wrapper).find('input').val('').focus();
                });
                $('.multi-field2 .remove-field2', $wrapper).click(function() {
                    if ($('.multi-field2', $wrapper).length > 1)
                        $(this).parent('.multi-field2').remove();
                });
            });
            $('.multi-field-wrapper3').each(function() {
                var $wrapper = $('.multi-fields3', this);
                $(".add-field3", $(this)).click(function(e) {
                    $('.multi-field3:first-child', $wrapper).clone(true).appendTo($wrapper).find('input').val('').focus();
                });
                $('.multi-field3 .remove-field3', $wrapper).click(function() {
                    if ($('.multi-field3', $wrapper).length > 1)
                        $(this).parent('.multi-field3').remove();
                });
            });
        });
    </script>

    <div class="app-content content">
        <div class="content-wrapper">

            <div class="content-body">
                <section id="basic-form-layouts">
                    <div class="row">
                        <div class="col-12">
                            @if (session('success'))
                                <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                                    {{ session('success') }}
                                </div>
                            @endif

                            <div class="card profile-with-cover">
                                @if(0 == count($agentcover))
                                    <div class="card-img-top img-fluid bg-cover height-300" style="background: url('AgentPhoto/eastafrica.jpg') 50%;"></div>
                                @else
                                    <div class="card-img-top img-fluid bg-cover height-300" style="background: url('AgentPhoto/<?php echo $agentcover?>') 50%;"></div>
                                @endif

                                <div class="media profil-cover-details w-100">
                                    <div class="media-left pl-2 pt-2">
                                        <a href="#" class="profile-image">

                                            @if(0 == count($agentprofile))
                                                <img src="AgentPhoto/defaultprofile.png" class="rounded-circle img-border height-100" alt="Card image">
                                            @else
                                                <img src="AgentPhoto/<?php echo $agentprofile?>" class="rounded-circle img-border height-100" alt="Card image">
                                            @endif

                                        </a>
                                    </div>
                                    <nav class="navbar navbar-light navbar-profile align-self-end">
                                        <button class="navbar-toggler d-sm-none" type="button" data-toggle="collapse" aria-expanded="false" aria-label="Toggle navigation"></button>
                                        <nav class="navbar navbar-expand-lg">
                                            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                                                <ul class="navbar-nav mr-auto">
                                                    @if(0 == count($agentprofile))
                                                        <li class="nav-item active">
                                                            <a class="nav-link" href="#">
                                                                <button type="button" class="btn btn-icon btn-outline-primary"
                                                                        data-toggle="modal"
                                                                        data-target="#profilepicture">
                                                                    <i class="la la-line-chart"></i>  add profile picture
                                                                </button>
                                                                <!-- Modal -->
                                                                <div class="modal fade text-left" id="profilepicture" tabindex="-1"
                                                                     role="dialog" aria-labelledby="myModalLabel1"
                                                                     aria-hidden="true">
                                                                    <div class="modal-dialog" role="document">
                                                                        <div class="modal-content">
                                                                            <div class="modal-header">
                                                                                <h4 class="modal-title" id="myModalLabel1"> add profile picture</h4>
                                                                                <button type="button" class="close" onclick="location.reload();">
                                                                                    <span aria-hidden="true">close</span>
                                                                                </button>
                                                                            </div>
                                                                            <div class="modal-body">

                                                                                    <div class="row">

                                                                                        <div class="col-md-12" style="padding-top:30px;">
                                                                                            <strong>Upload Image:</strong>
                                                                                            <br/>
                                                                                            <br/>
                                                                                            <input type="file" id="upload" class="form-control" required>
                                                                                            <br/>
                                                                                            <button class="btn btn-success upload-result">Upload Image</button>
                                                                                        </div>

                                                                                    </div>

                                                                                    <div class="row">
                                                                                        <div class="col-md-5">
                                                                                            <div id="upload-demo" style="width:100%"></div>
                                                                                        </div>
                                                                                        <div class="col-md-5" style="">
                                                                                            <strong>After seeing your final profile picture click close button </strong>
                                                                                            <div id="upload-demo-i" style="background:#e1e1e1;width:100%;padding:30px;height:auto;margin-top:30px"></div>
                                                                                        </div>
                                                                                    </div>

                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </a>
                                                        </li>
                                                    @else

                                                        <li class="nav-item active">
                                                            <a class="nav-link" href="#">
                                                                <button type="button" class="btn btn-icon btn-outline-primary"
                                                                        data-toggle="modal"
                                                                        data-target="#updateprofile">
                                                                    <i class="la la-line-chart"></i>  Update profile picture
                                                                </button>
                                                                <!-- Modal -->
                                                                <div class="modal fade text-left" id="updateprofile" tabindex="-1"
                                                                     role="dialog" aria-labelledby="myModalLabel1"
                                                                     aria-hidden="true">
                                                                    <div class="modal-dialog" role="document">
                                                                        <div class="modal-content">
                                                                            <div class="modal-header">
                                                                                <h4 class="modal-title" id="myModalLabel1"> Update profile picture</h4>
                                                                                <button type="button" class="close" onclick="location.reload();">
                                                                                    <span aria-hidden="true">Close</span>
                                                                                </button>
                                                                            </div>
                                                                            <div class="modal-body">

                                                                                    <div class="row">

                                                                                        <div class="col-md-12" style="padding-top:30px;">
                                                                                            <strong>Upload Image:</strong>
                                                                                            <br/>
                                                                                            <br/>
                                                                                            <input type="file" id="upload2" class="form-control" required>
                                                                                            <br/>
                                                                                            <button class="btn btn-success upload-result2">Upload Image</button>
                                                                                        </div>

                                                                                    </div>

                                                                                    <div class="row">
                                                                                        <div class="col-md-5">
                                                                                            <div id="upload-demo2" style="width:100%"></div>
                                                                                        </div>
                                                                                        <div class="col-md-5" style="">
                                                                                            <strong>After seeing your final profile picture click close button </strong>
                                                                                            <div id="upload-demo-i2" style="background:#e1e1e1;width:100%;padding:30px;height:auto;margin-top:30px"></div>
                                                                                        </div>
                                                                                    </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </a>
                                                        </li>
                                                    @endif

                                                    @if(0 == count($agentcover))
                                                            <li class="nav-item">
                                                                <a class="nav-link" href="#">

                                                                    <button type="button" class="btn btn-icon btn-outline-primary"
                                                                            data-toggle="modal"
                                                                            data-target="#companywebsite">
                                                                        <i class="la la-user"></i>  Add cover picture
                                                                    </button>
                                                                    <!-- Modal -->
                                                                    <div class="modal fade text-left" id="companywebsite" tabindex="-1"
                                                                         role="dialog" aria-labelledby="myModalLabel1"
                                                                         aria-hidden="true">
                                                                        <div class="modal-dialog" role="document">
                                                                            <div class="modal-content">
                                                                                <div class="modal-header">
                                                                                    <h4 class="modal-title" id="myModalLabel1"> add cover picture</h4>
                                                                                    <button type="button" class="close" data-dismiss="modal"
                                                                                            aria-label="Close">
                                                                                        <span aria-hidden="true">&times;</span>
                                                                                    </button>
                                                                                </div>
                                                                                <div class="modal-body">

                                                                                    <form class="form-horizontal form-simple" method="POST" action="{{ url('AddAgentCoverPhoto') }}" enctype="multipart/form-data">
                                                                                        {{ csrf_field() }}

                                                                                        <div class="row">
                                                                                            <div class="col-md-12">
                                                                                                <div class="form-group">
                                                                                                    <label for="projectinput1">Profile picture</label>
                                                                                                    <input type="file" id="projectinput1" class="form-control"
                                                                                                           name="agent_profile_cover" >
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="form-actions">
                                                                                            <button type="submit" class="btn btn-login">
                                                                                                <i class="la la-check-square-o"></i> Save
                                                                                            </button>
                                                                                        </div>

                                                                                    </form>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </a>
                                                            </li>
                                                    @else
                                                            <li class="nav-item">
                                                                <a class="nav-link" href="#">

                                                                    <button type="button" class="btn btn-icon btn-outline-primary"
                                                                            data-toggle="modal"
                                                                            data-target="#coverupdate">
                                                                        <i class="la la-user"></i>  update cover picture
                                                                    </button>
                                                                    <!-- Modal -->
                                                                    <div class="modal fade text-left" id="coverupdate" tabindex="-1"
                                                                         role="dialog" aria-labelledby="myModalLabel1"
                                                                         aria-hidden="true">
                                                                        <div class="modal-dialog" role="document">
                                                                            <div class="modal-content">
                                                                                <div class="modal-header">
                                                                                    <h4 class="modal-title" id="myModalLabel1"> update cover picture</h4>
                                                                                    <button type="button" class="close" data-dismiss="modal"
                                                                                            aria-label="Close">
                                                                                        <span aria-hidden="true">&times;</span>
                                                                                    </button>
                                                                                </div>
                                                                                <div class="modal-body">

                                                                                    <form class="form-horizontal form-simple" method="POST" action="{{ url('UpdateAgentCoverPhoto') }}" enctype="multipart/form-data">
                                                                                        {{ csrf_field() }}

                                                                                        <div class="row">
                                                                                            <div class="col-md-12">
                                                                                                <div class="form-group">
                                                                                                    <label for="projectinput1">Profile picture</label>
                                                                                                    <input type="file" id="projectinput1" class="form-control"
                                                                                                           name="agent_profile_cover" >
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="col-md-12">
                                                                                                <div class="form-group">
                                                                                                    <label for="projectinput1">Profile picture</label>
                                                                                                    <input type="text" id="projectinput1" class="form-control" value="<?php echo $user_id?>"
                                                                                                           name="id" hidden>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="form-actions">
                                                                                            <button type="submit" class="btn btn-login">
                                                                                                <i class="la la-check-square-o"></i> Save
                                                                                            </button>
                                                                                        </div>

                                                                                    </form>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </a>
                                                            </li>
                                                    @endif

                                                </ul>
                                            </div>
                                        </nav>
                                    </nav>

                                </div>


                            </div>
                            </nav>
                        </div>
                    </div>
            </div>

                    <div class="row match-height">

                        <div class="col-md-12">
                            <h4 class="modal-title" id="myModalLabel1"> Company Details</h4>
                            <nav class="navbar navbar-light navbar-profile align-self-end">

                                <button class="navbar-toggler d-sm-none" type="button" data-toggle="collapse" aria-expanded="false" aria-label="Toggle navigation"></button>
                                <nav class="navbar navbar-expand-lg">
                                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                                        <ul class="navbar-nav mr-auto">
                                            <li class="nav-item active">
                                                <a class="nav-link" href="#">
                                                    <button type="button" class="btn btn-icon btn-outline-primary"
                                                            data-toggle="modal"
                                                            data-target="#companyaddress">
                                                        <i class="fas fa-address-card"></i>  Add Company Address
                                                    </button>
                                                    <!-- Modal -->
                                                    <div class="modal fade text-left" id="companyaddress" tabindex="-1"
                                                         role="dialog" aria-labelledby="myModalLabel1"
                                                         aria-hidden="true">
                                                        <div class="modal-dialog" role="document">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h4 class="modal-title" id="myModalLabel1"> Edit Your Company's Address</h4>
                                                                    <button type="button" class="close" data-dismiss="modal"
                                                                            aria-label="Close">
                                                                        <span aria-hidden="true">&times;</span>
                                                                    </button>
                                                                </div>
                                                                <div class="modal-body">


                                                                        @if(0 == count($listcompanyaddress))
                                                                        <form class="form-horizontal form-simple" method="POST" action="{{ url('AddAgentCompanyAddress') }}" enctype="multipart/form-data">
                                                                            {{ csrf_field() }}
                                                                            <div class="row">
                                                                                <div class="col-md-6">
                                                                                    <div class="form-group">
                                                                                        <label for="projectinput1">Street Address, P.O.Box</label>
                                                                                        <input type="text" id="projectinput1" class="form-control"
                                                                                               name="companystreetaddress" required>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-6">
                                                                                    <div class="form-group">
                                                                                        <label for="projectinput1">Office Building</label>
                                                                                        <input type="text" id="projectinput1" class="form-control"
                                                                                               name="companyofficebuiling" >
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="col-md-6">
                                                                                    <div class="form-group">
                                                                                        <label for="projectinput1">City</label>
                                                                                        <input type="text" id="projectinput1" class="form-control"
                                                                                               name="companycity" required>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-6">
                                                                                    <div class="form-group">
                                                                                        <label for="projectinput1">State,Province</label>
                                                                                        <input type="text" id="projectinput1" class="form-control"
                                                                                               name="companystateprovince" required>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="col-md-6">
                                                                                    <div class="form-group">
                                                                                        <label for="projectinput1">Zip/Postal Code</label>
                                                                                        <input type="text" id="projectinput1" class="form-control"
                                                                                               name="companyzipcode" >
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-6">
                                                                                    <div class="form-group">
                                                                                        <label for="projectinput1">Country</label>
                                                                                        <select name="companycountry" class="form-control" required>
                                                                                            @foreach($listcountry as $country)
                                                                                                <option value="{{$country->id}}">{{$country->name}}</option>
                                                                                            @endforeach
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-actions">
                                                                                <button type="submit" class="btn btn-login">
                                                                                    <i class="la la-check-square-o"></i> Save
                                                                                </button>
                                                                            </div>
                                                                    </form>
                                                                        @else()

                                                                            @foreach($listcompanyaddress as $address)
                                                                                <form class="form-horizontal form-simple" method="POST" action="{{ url('UpdateAgentCompanyAddress') }}" enctype="multipart/form-data">
                                                                                    {{ csrf_field() }}
                                                                                <div class="row">
                                                                                    <input type="text" id="projectinput1" class="form-control"
                                                                                           name="id" value="{{$address->id}}" hidden>
                                                                                    <div class="col-md-6">
                                                                                        <div class="form-group">
                                                                                            <label for="projectinput1">Street Address, P.O.Box</label>
                                                                                            <input type="text" id="projectinput1" class="form-control"
                                                                                                   name="companystreetaddress" value="{{$address->companystreetaddress}}">
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-6">
                                                                                        <div class="form-group">
                                                                                            <label for="projectinput1">Office Building</label>
                                                                                            <input type="text" id="projectinput1" class="form-control"
                                                                                                   name="companyofficebuiling" value="{{$address->companyofficebuiling}}">
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="row">
                                                                                    <div class="col-md-6">
                                                                                        <div class="form-group">
                                                                                            <label for="projectinput1">City</label>
                                                                                            <input type="text" id="projectinput1" class="form-control"
                                                                                                   name="companycity" value="{{$address->companycity}}">
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-6">
                                                                                        <div class="form-group">
                                                                                            <label for="projectinput1">State,Province</label>
                                                                                            <input type="text" id="projectinput1" class="form-control"
                                                                                                   name="companystateprovince" value="{{$address->companystateprovince}}">
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="row">
                                                                                    <div class="col-md-6">
                                                                                        <div class="form-group">
                                                                                            <label for="projectinput1">Zip/Postal Code</label>
                                                                                            <input type="text" id="projectinput1" class="form-control"
                                                                                                   name="companyzipcode" value="{{$address->companyzipcode}}">
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-6">
                                                                                        <div class="form-group">
                                                                                            <label for="projectinput1">Country</label>
                                                                                            <select name="companycountry" class="form-control">
                                                                                                <option value="{{$address->companycountry}}">{{$address->name}}</option>

                                                                                                @foreach($listcountry as $country)
                                                                                                    <option value="{{$country->id}}">{{$country->name}}</option>
                                                                                                @endforeach
                                                                                            </select>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="form-actions">
                                                                                    <button type="submit" class="btn btn-login">
                                                                                        <i class="la la-check-square-o"></i> Update
                                                                                    </button>
                                                                                </div>
                                                                                </form>
                                                                            @endforeach

                                                                        @endif
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="#">

                                                    <button type="button" class="btn btn-icon btn-outline-primary"
                                                            data-toggle="modal"
                                                            data-target="#companywebsite">
                                                        <i class="fas fa-globe-africa"></i>  Add Company Website
                                                    </button>
                                                    <!-- Modal -->
                                                    <div class="modal fade text-left" id="companywebsite" tabindex="-1"
                                                         role="dialog" aria-labelledby="myModalLabel1"
                                                         aria-hidden="true">
                                                        <div class="modal-dialog" role="document">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h4 class="modal-title" id="myModalLabel1"> Edit Your Company's Website</h4>
                                                                    <button type="button" class="close" data-dismiss="modal"
                                                                            aria-label="Close">
                                                                        <span aria-hidden="true">&times;</span>
                                                                    </button>
                                                                </div>
                                                                <div class="modal-body">
                                                                    @if(0 == count($listweb))
                                                                        <form class="form-horizontal form-simple" method="POST" action="{{ url('AddAgentCompanyWebsite') }}" enctype="multipart/form-data">
                                                                            {{ csrf_field() }}

                                                                            <div class="row">
                                                                                <div class="col-md-12">
                                                                                    <div class="form-group">
                                                                                        <label for="projectinput1">Website Url</label>
                                                                                        <input type="url" id="projectinput1" class="form-control"
                                                                                               name="companywebsite">
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-actions">
                                                                                <button type="submit" class="btn btn-login">
                                                                                    <i class="la la-check-square-o"></i> Save
                                                                                </button>
                                                                            </div>

                                                                        </form>
                                                                    @else()
                                                                        <form class="form-horizontal form-simple" method="POST" action="{{ url('UpdateAgentCompanyWebsite') }}" enctype="multipart/form-data">
                                                                            {{ csrf_field() }}
                                                                            @foreach($listweb as $web)

                                                                                <div class="row">
                                                                                    <div class="col-md-12">
                                                                                        <div class="form-group">
                                                                                            <label for="projectinput1">Website Url</label>
                                                                                            <input type="url" id="projectinput1" class="form-control"
                                                                                                   name="companywebsite" value="{{$web->companywebsite}}">
                                                                                            <input type="text" id="projectinput1" class="form-control"
                                                                                                   name="id" value="{{$web->id}}" hidden>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="form-actions">
                                                                                    <button type="submit" class="btn btn-login">
                                                                                        <i class="la la-check-square-o"></i> Update
                                                                                    </button>
                                                                                </div>
                                                                            @endforeach
                                                                        </form>
                                                                    @endif

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="#">

                                                    <button type="button" class="btn btn-icon btn-outline-primary"
                                                            data-toggle="modal"
                                                            data-target="#companyphone">
                                                        <i class="fas fa-phone"></i>  Add Company Phone
                                                    </button>
                                                    <!-- Modal -->
                                                    <div class="modal fade text-left" id="companyphone" tabindex="-1"
                                                         role="dialog" aria-labelledby="myModalLabel1"
                                                         aria-hidden="true">
                                                        <div class="modal-dialog" role="document">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h4 class="modal-title" id="myModalLabel1"> Edit Your Company's Phone</h4>
                                                                    <button type="button" class="close" data-dismiss="modal"
                                                                            aria-label="Close">
                                                                        <span aria-hidden="true">&times;</span>
                                                                    </button>
                                                                </div>
                                                                <div class="modal-body">
                                                                    @if(0 == count($listphone))
                                                                        <form class="form-horizontal form-simple" method="POST" action="{{ url('AddAgentCompanyPhone') }}" enctype="multipart/form-data">
                                                                            {{ csrf_field() }}
                                                                            <div class="row">
                                                                                <div class="col-md-12">
                                                                                    <div class="form-group">
                                                                                        <label for="projectinput1">Phone Number</label>
                                                                                        <input type="number" id="projectinput1" class="form-control"
                                                                                               name="companyphone">
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-actions">
                                                                                <button type="submit" class="btn btn-login">
                                                                                    <i class="la la-check-square-o"></i> Save
                                                                                </button>
                                                                            </div>
                                                                        </form>
                                                                    @else()
                                                                        <form class="form-horizontal form-simple" method="POST" action="{{ url('UpdateAgentCompanyPhone') }}" enctype="multipart/form-data">
                                                                            {{ csrf_field() }}
                                                                            @foreach($listphone as $phone)
                                                                                <div class="row">
                                                                                    <div class="col-md-12">
                                                                                        <div class="form-group">
                                                                                            <label for="projectinput1">Phone Number</label>
                                                                                            <input type="number" id="projectinput1" class="form-control"
                                                                                                   name="companyphone" value="{{$phone->companyphone}}">
                                                                                            <input type="text" id="projectinput1" class="form-control"
                                                                                                   name="id" value="{{$phone->id}}" hidden>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="form-actions">
                                                                                    <button type="submit" class="btn btn-login">
                                                                                        <i class="la la-check-square-o"></i> Update
                                                                                    </button>
                                                                                </div>
                                                                            @endforeach
                                                                        </form>
                                                                    @endif

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="#">

                                                    <button type="button" class="btn btn-icon btn-outline-primary"
                                                            data-toggle="modal"
                                                            data-target="#marketingmaterials">
                                                        <i class="la la-briefcase"></i>  Add Marketing Materials
                                                    </button>
                                                    <!-- Modal -->
                                                    <div class="modal fade text-left" id="marketingmaterials" tabindex="-1"
                                                         role="dialog" aria-labelledby="myModalLabel1"
                                                         aria-hidden="true">
                                                        <div class="modal-dialog" role="document">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h4 class="modal-title" id="myModalLabel1"> Edit Your Company's Marketing Materials</h4>
                                                                    <button type="button" class="close" data-dismiss="modal"
                                                                            aria-label="Close">
                                                                        <span aria-hidden="true">&times;</span>
                                                                    </button>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <form class="form-horizontal form-simple" method="POST" action="{{ url('AddAgentProfile') }}" enctype="multipart/form-data">
                                                                        <div class="row">
                                                                            <div class="col-md-12">
                                                                                <div class="form-group">
                                                                                    <label for="projectinput1">Marketing Materials (PDF)</label>
                                                                                    <input type="file" id="projectinput1" class="form-control"
                                                                                           name="agent_profile_photoname[]">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="multi-field-wrapper2">
                                                                            <div class="multi-fields2">
                                                                                <div class="row  multi-field2">
                                                                                    <div class="col-md-12">
                                                                                        <div class="form-group">
                                                                                            <label for="projectinput1">Video Link(Youtube Embed Codes)</label>
                                                                                            <input type="text" id="projectinput1" class="form-control"
                                                                                                   name="agent_profile_videolink[]" value="{{ old('agent_profile_videolink') }}">
                                                                                        </div>
                                                                                    </div>
                                                                                    <button type="button" class="remove-field2 btn btn-dark" style="line-height: inherit !important;"><i class="fas fa-times-circle"></i></button>
                                                                                    <button type="button" class="add-field2 btn btn-dark" style="line-height: inherit !important;"><i class="fas fa-plus-circle"></i></button>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="multi-field-wrapper3">
                                                                            <div class="multi-fields3">
                                                                                <div class="row  multi-field3">
                                                                                    <div class="col-md-12">
                                                                                        <div class="form-group">
                                                                                            <label for="projectinput1">Gallery</label>
                                                                                            <input type="file" id="projectinput1" class="form-control"
                                                                                                   name="agent_profile_photoname[]" value="{{ old('agent_profile_photoname') }}">
                                                                                        </div>
                                                                                    </div>
                                                                                    <button type="button" class="remove-field3 btn btn-dark" style="line-height: inherit !important;"><i class="fas fa-times-circle"></i></button>
                                                                                    <button type="button" class="add-field3 btn btn-dark" style="line-height: inherit !important;"><i class="fas fa-plus-circle"></i></button>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="form-actions">
                                                                            <button type="submit" class="btn btn-login">
                                                                                <i class="la la-check-square-o"></i> Save
                                                                            </button>
                                                                        </div>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </nav>
                            </nav>
                            <h4 class="modal-title" id="myModalLabel1"> Company Activities</h4>
                            <nav class="navbar navbar-light navbar-profile align-self-end">

                                <button class="navbar-toggler d-sm-none" type="button" data-toggle="collapse" aria-expanded="false" aria-label="Toggle navigation"></button>
                                <nav class="navbar navbar-expand-lg">
                                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                                        <ul class="navbar-nav mr-auto">
                                            <li class="nav-item active">
                                                <a class="nav-link" href="#">
                                                    <button type="button" class="btn btn-icon btn-outline-primary"
                                                            data-toggle="modal"
                                                            data-target="#companytypes">
                                                        <i class="la la-line-chart"></i>  Add Company Types
                                                    </button>
                                                    <!-- Modal -->
                                                    <div class="modal fade text-left" id="companytypes" tabindex="-1"
                                                         role="dialog" aria-labelledby="myModalLabel1"
                                                         aria-hidden="true">
                                                        <div class="modal-dialog" role="document">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h4 class="modal-title" id="myModalLabel1"> Edit Company Types</h4>
                                                                    <button type="button" class="close" data-dismiss="modal"
                                                                            aria-label="Close">
                                                                        <span aria-hidden="true">&times;</span>
                                                                    </button>
                                                                </div>
                                                                <div class="modal-body">


                                                                        @if(0 == count($listagentcompanytype))
                                                                        <form class="form-horizontal form-simple" method="POST" action="{{ url('AddAgentCompanyType') }}" enctype="multipart/form-data">
                                                                            {{ csrf_field() }}
                                                                            <div class="row">
                                                                                <div class="col-md-12">
                                                                                    <div class="form-group">
                                                                                        <select class="select2 form-control" name="agentcompanytype[]" multiple="multiple">
                                                                                            @foreach($listcompanytype as $types)
                                                                                                <option value="{{$types->id}}">{{$types->companytypes}}</option>
                                                                                            @endforeach
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-actions">
                                                                                <button type="submit" class="btn btn-login">
                                                                                    <i class="la la-check-square-o"></i> Save
                                                                                </button>
                                                                            </div>
                                                                        </form>
                                                                        @else()
                                                                            {{--<div class="row">--}}
                                                                                {{--<div class="col-md-12">--}}
                                                                                    {{--<div class="form-group">--}}

                                                                                        {{--<select class="select2 form-control" name="agentcompanytype[]" multiple="multiple" disabled>--}}
                                                                                            {{--@foreach($listagentcompanytype as $agenttype)--}}
                                                                                                {{--<option selected value="{{$agenttype->agentcompanytype}}">{{$agenttype->companytypes}}</option>--}}
                                                                                            {{--@endforeach--}}
                                                                                        {{--</select>--}}
                                                                                    {{--</div>--}}
                                                                                {{--</div>--}}
                                                                            {{--</div>--}}
                                                                            <form class="form-horizontal form-simple" method="POST" action="{{ url('EditAgentCompanyType') }}" enctype="multipart/form-data">
                                                                                {{ csrf_field() }}
                                                                            <div class="row">

                                                                                <div class="col-md-12">
                                                                                    <div class="form-group">
                                                                                        <label for="projectinput1">Add on more</label>
                                                                                        <select class="select2 form-control" name="agentcompanytype[]" multiple="multiple">
                                                                                            @foreach($listagentcompanytype as $agenttype)
                                                                                                <option selected value="{{$agenttype->agentcompanytype}}">{{$agenttype->companytypes}}</option>
                                                                                            @endforeach
                                                                                            @foreach($listcompanytype as $types)
                                                                                            <option value="{{$types->id}}">{{$types->companytypes}}</option>
                                                                                            @endforeach
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                                </div>
                                                                                <div class="form-actions">
                                                                                    <button type="submit" class="btn btn-login">
                                                                                        <i class="la la-check-square-o"></i> Update
                                                                                    </button>
                                                                                </div>

                                                                            </form>
                                                                        @endif

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="#">

                                                    <button type="button" class="btn btn-icon btn-outline-primary"
                                                            data-toggle="modal"
                                                            data-target="#agentmarketsegment">
                                                        <i class="la la-user"></i>  Add Company Market Segment
                                                    </button>
                                                    <!-- Modal -->
                                                    <div class="modal fade text-left" id="agentmarketsegment" tabindex="-1"
                                                         role="dialog" aria-labelledby="myModalLabel1"
                                                         aria-hidden="true">
                                                        <div class="modal-dialog" role="document">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h4 class="modal-title" id="myModalLabel1"> Edit Market Segment</h4>
                                                                    <button type="button" class="close" data-dismiss="modal"
                                                                            aria-label="Close">
                                                                        <span aria-hidden="true">&times;</span>
                                                                    </button>
                                                                </div>
                                                                <div class="modal-body">
                                                                    {{--<form class="form-horizontal form-simple" method="POST" action="{{ url('AddAgentMarketSegment') }}" enctype="multipart/form-data">--}}
                                                                    {{--{{ csrf_field() }}--}}
                                                                    {{--<div class="row">--}}
                                                                        {{--<div class="col-md-12">--}}
                                                                            {{--<div class="form-group">--}}
                                                                                {{--<select class="select2 form-control" name="agentcompanytype[]" multiple="multiple">--}}
                                                                                    {{--@foreach($listmarketsegment as $market)--}}
                                                                                        {{--<option value="{{$market->id}}">{{$market->marketsegments}}</option>--}}
                                                                                    {{--@endforeach--}}
                                                                                {{--</select>--}}
                                                                            {{--</div>--}}
                                                                        {{--</div>--}}
                                                                    {{--</div>--}}
                                                                    {{--<div class="form-actions">--}}
                                                                        {{--<button type="submit" class="btn btn-login">--}}
                                                                            {{--<i class="la la-check-square-o"></i> Save--}}
                                                                        {{--</button>--}}
                                                                    {{--</div>--}}
                                                                    {{--</form>--}}
                                                                    @if(0 == count($listagentmarketsegment))
                                                                        <form class="form-horizontal form-simple" method="POST" action="{{ url('AddAgentMarketSegment') }}" enctype="multipart/form-data">
                                                                            {{ csrf_field() }}

                                                                            <div class="row">
                                                                                <div class="col-md-12">
                                                                                    <div class="form-group">
                                                                                        <select class="select2 form-control" name="agentmarketsegment[]" multiple="multiple">
                                                                                            @foreach($listmarketsegment as $market)
                                                                                                <option value="{{$market->id}}">{{$market->marketsegments}}</option>
                                                                                            @endforeach
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-actions">
                                                                                <button type="submit" class="btn btn-login">
                                                                                    <i class="la la-check-square-o"></i> Save
                                                                                </button>
                                                                            </div>

                                                                        </form>
                                                                    @else()
                                                                        <form class="form-horizontal form-simple" method="POST" action="{{ url('EditAgentMarketSegment') }}" enctype="multipart/form-data">
                                                                            {{ csrf_field() }}
                                                                                <div class="row">
                                                                                    <div class="col-md-12">
                                                                                        <div class="form-group">
                                                                                            <select class="select2 form-control" name="agentmarketsegment[]" multiple="multiple">
                                                                                                @foreach($listagentmarketsegment as $agentmarket)
                                                                                                    <option value="{{$agentmarket->agentmarketsegment}}" selected>{{$agentmarket->marketsegments}}</option>
                                                                                                @endforeach
                                                                                                @foreach($listmarketsegment as $market)
                                                                                                    <option value="{{$market->id}}">{{$market->marketsegments}}</option>
                                                                                                @endforeach
                                                                                            </select>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="form-actions">
                                                                                    <button type="submit" class="btn btn-login">
                                                                                        <i class="la la-check-square-o"></i> Save
                                                                                    </button>
                                                                                </div>
                                                                        </form>
                                                                    @endif

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </a>
                                            </li>


                                        </ul>
                                    </div>
                                </nav>
                            </nav>
                            <h4 class="modal-title" id="myModalLabel1">Company Description</h4>
                            <div class="card">

                                <div class="card-header">
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>

                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body">

                                        @if(0 == count($listabout))
                                            <form class="form-horizontal form-simple" method="POST" action="{{ url('AddAgentCompanyAbout') }}" enctype="multipart/form-data">
                                                {{ csrf_field() }}
                                                <div class="form-body">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                {{--<label for="projectinput1">About Company</label>--}}
                                                                <textarea name="agent_about" class="form-control" row="4"></textarea>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-actions">
                                                        <button type="submit" class="btn btn-login">
                                                            <i class="la la-check-square-o"></i> Save
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>
                                        @else()
                                            @foreach($listabout as $about)
                                            <form class="form-horizontal form-simple" method="POST" action="{{ url('UpdateAgentCompanyAbout') }}" enctype="multipart/form-data">
                                                {{ csrf_field() }}
                                                <div class="form-body">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                {{--<label for="projectinput1">About Company</label>--}}
                                                                <textarea name="agent_about"  class="form-control" row="10">{{$about->agent_about}}</textarea>
                                                                <input type="text" id="projectinput1" class="form-control"
                                                                       name="id" value="{{$about->id}}" hidden>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-actions">
                                                        <button type="submit" class="btn btn-login">
                                                            <i class="la la-check-square-o"></i> Save
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>
                                            @endforeach
                                        @endif


                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </section>

            </div>
        </div>
    </div>
    <script type="text/javascript">

        $uploadCrop = $('#upload-demo').croppie({
            enableExif: true,
            viewport: {
                width: 200,
                height: 200,
                type: 'circle'
            },
            boundary: {
                width: 300,
                height: 300
            }
        });


        $('#upload').on('change', function () {
            var reader = new FileReader();
            reader.onload = function (e) {
                $uploadCrop.croppie('bind', {
                    url: e.target.result
                }).then(function(){
                    console.log('jQuery bind complete');
                });
            }
            reader.readAsDataURL(this.files[0]);
        });


        $('.upload-result').on('click', function (ev) {
            $uploadCrop.croppie('result', {
                type: 'canvas',
                size: 'viewport'
            }).then(function (resp) {
                $.ajax({
                    url: "/AddAgentProfilePhoto",
                    type: "POST",
                    data: {"image":resp, _token: '{{csrf_token()}}'},
                    success: function (data) {
                        html = '<img src="' + resp + '" />';
                        $("#upload-demo-i").html(html);
                    }
                });
            });
        });

    </script>

    <script type="text/javascript">

        $uploadCrop = $('#upload-demo2').croppie({
            enableExif: true,
            viewport: {
                width: 200,
                height: 200,
                type: 'circle'
            },
            boundary: {
                width: 300,
                height: 300
            }
        });


        $('#upload2').on('change', function () {
            var reader2 = new FileReader();
            reader2.onload = function (e) {
                $uploadCrop.croppie('bind', {
                    url: e.target.result
                }).then(function(){
                    console.log('jQuery bind complete');
                });
            }
            reader2.readAsDataURL(this.files[0]);
        });

        $('.upload-result2').on('click', function (ev) {
            $uploadCrop.croppie('result', {
                type: 'canvas',
                size: 'viewport'
            }).then(function (resp) {
                $.ajax({
                    url: "/UpdateAgentProfilePhoto",
                    type: "POST",
                    data: {"image":resp, _token: '{{csrf_token()}}'},
                    success: function (data) {
                        html = '<img src="' + resp + '" />';
                        $("#upload-demo-i2").html(html);
                    }
                });
            });
        });

    </script>
    <script src="backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/ui/jquery-ui/date-pickers.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/picker.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/picker.date.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/picker.time.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/legacy.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js"
            type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/daterange/daterangepicker.js"
            type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/pickers/dateTime/pick-a-datetime.js"
            type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/forms/select/select2.full.min.js"></script>
    <script src="backend/app-assets/js/scripts/forms/select/form-select2.min.js"></script>
    <script src="backend/app-assets/vendors/js/tables/datatable/datatables.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/app-menu.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/app.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/customizer.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/tables/datatables/datatable-basic.js"
            type="text/javascript"></script>

@endsection
