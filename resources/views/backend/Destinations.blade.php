@extends('backend.layout.master')

@section('title', 'Connect Africa')

@section('content')
    <body class="vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar" data-open="click" data-menu="vertical-menu" data-col="2-columns">
    @include('backend.layout.sidemenu')
    @include('backend.layout.upmenu')
    <style>
        .btn-secondary{
            color:#fff !important;
            background-color: #6a442b !important;
            border-color:#6a442b !important;
        }
        .btn-primary{
            background-color: #b4753c  !important;
            border-color:#b4753c  !important;
        }
        .btn-primary:hover{
            background-color: #b4753c !important;
            border-color:#b4753c !important;
        }
        iframe{
            width: 100%;
        }
        .btn-dark {
            color: #000 !important;
            background-color: transparent;
            border-color: transparent;
        }
        .btn-dark:hover {
            color: #000 !important;
            background-color: transparent;
            border-color: transparent;
        }
        .list-group-item {
            padding: 10px !important;
            margin-bottom: 10px !important;
            /* margin-bottom: 10px; */
        }
    </style>
    <script
            src="https://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
            crossorigin="anonymous"></script>
    <script type="text/javascript">
        $(document).ready(function()
        {
            $('textarea').each(function () {
                this.setAttribute('style', 'height:' + (this.scrollHeight) + 'px;overflow-y:hidden;');
            }).on('input', function () {
                this.style.height = 'auto';
                this.style.height = (this.scrollHeight) + 'px';
            });

        });
    </script>

    <div class="app-content content">
        <div class="content-wrapper">
                <div class="content-body">
                    <!-- Basic Summernote start -->
                    <div class="row match-height">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    @if (session('success'))
                                        <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                                            {{ session('success') }}
                                        </div>
                                    @endif
                                    <h4 class="card-title" id="basic-layout-form">Destination</h4>
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                            <li><a data-action="close"><i class="ft-x"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body">
                                        <form class="form-horizontal form-simple" method="POST" action="{{ url('AddDestination') }}" enctype="multipart/form-data">
                                            {{ csrf_field() }}
                                            <div class="form-body">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Destination Title</label>
                                                            <input type="text" id="projectinput1" class="form-control" value="{{ old('destinationtitle') }}" placeholder="Destination Title"
                                                                   name="destinationtitle" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Destination Image <strong>*Dimension 1000 X 750 *</strong></label>
                                                            <input type="file" id="projectinput1" class="form-control" value="{{ old('coverimage') }}"
                                                                   name="coverimage" required>
                                                            @if ($errors->has('coverimage'))
                                                                <span class="help-block" style="color:#e74040">
                                                                    <strong>{{ $errors->first('coverimage') }}</strong>
                                                                 </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Destination General information</label>
                                                            <textarea  name="destinationgeneralinformation" class="form-control" id="destinationgeneralinformation" rows="4"  required>{{ old('destinationgeneralinformation') }}</textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Destination Map</label>
                                                            <input type="text" id="projectinput1" class="form-control" value="{{ old('destinationmap') }}" placeholder="Destination Map"
                                                                   name="destinationmap" required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Destination Country</label>
                                                            <select name="destinationcountry" class="form-control">
                                                                @foreach($listcountryprofile as $data)
                                                                <option value="{{$data->id}}">{{$data->countryname}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Destination Cover Image</label>
                                                            <input type="file" id="projectinput1" class="form-control"
                                                                   name="destinationcoverimage" required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Top Destination</label>
                                                            <select class="form-control" name="topdestination">
                                                                <option value=""></option>
                                                                <option value="top">Top Destination</option>
                                                                <option value="normal">Normal Destination</option>
                                                            </select>
                                                            {{--<input type="text" id="projectinput1" class="form-control" value="{{ old('destinationphotocredit') }}" placeholder="Destination Map"--}}
                                                                   {{--name="destinationphotocredit" required>--}}
                                                        </div>
                                                    </div>
                                                </div>
                                                {{--<div class="row">--}}
                                                    {{--<div class="col-md-6">--}}
                                                        {{--<div class="form-group">--}}
                                                            {{--<label for="projectinput1">Photocredit</label>--}}
                                                            {{--<input type="text" id="projectinput1" class="form-control" value="{{ old('destinationphotocredit') }}" placeholder="Destination Map"--}}
                                                                   {{--name="destinationphotocredit" required>--}}
                                                        {{--</div>--}}
                                                    {{--</div>--}}
                                                {{--</div>--}}
                                                <button type="submit" class="btn btn-primary"> <i class="la la-check-square-o"></i> Save</button>
                                            </div>

                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <section id="html5">
                <div class="row">
                    <div class="col-12">
                        <div class="card">

                            <div class="card-content collapse show">
                                <div class="card-body card-dashboard">
                                    <table class="table table-striped table-bordered zero-configuration table-responsive">
                                        <thead>
                                        <tr>
                                            <th>Destination Title</th>
                                            <th>Top Destination</th>
                                            <th>Destination Country</th>
                                            <th>Destination General information</th>
                                            <th>Destination Image credit</th>
                                            <th>Destination Cover Image</th>
                                            <th>Destination Image</th>
                                            <th>Destination Map</th>
                                            <th>Date Created</th>
                                            <th>Edit</th>
                                            <th>Delete</th>

                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($listdestination as $data)
                                            <tr>
                                                <td>{{$data->destinationtitle}}</td>
                                                <td>{{$data->topdestination}}</td>
                                                <td>{{$data->countryname}}</td>
                                                <td>

                                                    <button type="button" class="btn btn-icon btn-outline-primary btn-min-width mr-1 mb-1"
                                                            data-toggle="modal"
                                                            data-target="#destinationinfo{{$data->id}}">Destination General information
                                                    </button>
                                                    <!-- Modal -->
                                                    <div class="modal fade text-left" id="destinationinfo{{$data->id}}" tabindex="-1"
                                                         role="dialog" aria-labelledby="myModalLabel1"
                                                         aria-hidden="true">
                                                        <div class="modal-dialog" role="document">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h4 class="modal-title" id="myModalLabel1">Destination General information</h4>
                                                                    <button type="button" class="close" data-dismiss="modal"
                                                                            aria-label="Close">
                                                                        <span aria-hidden="true">&times;</span>
                                                                    </button>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <p>{{$data->destinationgeneralinformation}}</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>{{$data->destinationphotocredit}}</td>
                                                <td><img src="Destination/{{$data->destinationcoverimage}}" style="width: 100%"></td>
                                                <td><img src="Destination/{{$data->coverimage}}" style="width: 100%"></td>
                                                <td>

                                                    <button type="button" class="btn btn-icon btn-outline-primary btn-min-width mr-1 mb-1"
                                                            data-toggle="modal"
                                                            data-target="#map{{$data->id}}">Destination Map
                                                    </button>
                                                    <!-- Modal -->
                                                    <div class="modal fade text-left" id="map{{$data->id}}" tabindex="-1"
                                                         role="dialog" aria-labelledby="myModalLabel1"
                                                         aria-hidden="true">
                                                        <div class="modal-dialog" role="document">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h4 class="modal-title" id="myModalLabel1">Destination Map</h4>
                                                                    <button type="button" class="close" data-dismiss="modal"
                                                                            aria-label="Close">
                                                                        <span aria-hidden="true">&times;</span>
                                                                  </button>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <?php
                                                                    $map = $data->destinationmap;
                                                                    echo "$map";
                                                                    ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>{{$data->created_at}}</td>
                                                <td>

                                                    <button type="button" class="btn btn-icon btn-outline-primary btn-min-width mr-1 mb-1"
                                                            data-toggle="modal"
                                                            data-target="#editnomie{{$data->id}}">Edit
                                                    </button>
                                                    <!-- Modal -->
                                                    <div class="modal fade text-left" id="editnomie{{$data->id}}" tabindex="-1"
                                                         role="dialog" aria-labelledby="myModalLabel1"
                                                         aria-hidden="true">
                                                        <div class="modal-dialog" role="document">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h4 class="modal-title" id="myModalLabel1">Edit</h4>
                                                                    <button type="button" class="close" data-dismiss="modal"
                                                                            aria-label="Close">
                                                                        <span aria-hidden="true">&times;</span>
                                                                    </button>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <form class="form-horizontal form-simple" method="POST"
                                                                          action="{{ url('EditDestination') }}"
                                                                          enctype="multipart/form-data">
                                                                            {{ csrf_field() }}
                                                                            <input type="text" name="id" value="{{$data->id}}" hidden>
                                                                            <div class="row">
                                                                                <div class="col-md-6">
                                                                                    <div class="form-group">
                                                                                        <label for="projectinput1">Destination Title</label>
                                                                                        <input type="text" id="projectinput1" class="form-control" value="{{ $data->destinationtitle }}"
                                                                                               name="destinationtitle" required>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-6">
                                                                                    <div class="form-group">
                                                                                        <label for="projectinput1">Cover Image</label>
                                                                                        <input type="file" id="projectinput1" class="form-control"
                                                                                               name="coverimage">
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        <div class="row">
                                                                            <div class="col-md-12">
                                                                                <div class="form-group">
                                                                                    <label for="projectinput1">Destination General information</label>
                                                                                    <textarea  name="destinationgeneralinformation" class="form-control" id="destinationgeneralinformation" rows="20"  required>{{ $data->destinationgeneralinformation }}</textarea>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-md-12">
                                                                                <div class="form-group">
                                                                                    <label for="projectinput1">Destination Map</label>
                                                                                    <input type="text" id="projectinput1" class="form-control" value="{{ $data->destinationmap }}" placeholder="Destination Map"
                                                                                           name="destinationmap" required>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-md-12">
                                                                                <div class="form-group">
                                                                                    <label for="projectinput1">Destination Country</label>
                                                                                    <select name="destinationcountry" class="form-control">
                                                                                        <option value="{{$data->destinationcountry}}">{{$data->countryname}}</option>
                                                                                        @foreach($listcountryprofile as $datas)
                                                                                            <option value="{{$datas->id}}">{{$datas->countryname}}</option>
                                                                                        @endforeach
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-12">
                                                                                <div class="form-group">
                                                                                    <label for="projectinput1">Destination Cover Image</label>
                                                                                    <input type="file" id="projectinput1" class="form-control"
                                                                                           name="destinationcoverimage" >
                                                                                </div>
                                                                            </div>

                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-md-12">
                                                                                <div class="form-group">
                                                                                    <label for="projectinput1">Top Destination</label>
                                                                                    <select class="form-control" name="topdestination" required>
                                                                                        <option value="{{ $data->topdestination }}">{{ $data->topdestination }}</option>
                                                                                        <option value="top">Top Destination</option>
                                                                                        <option value="normal">Normal Destination</option>
                                                                                    </select>
                                                                                    {{--<input type="text" id="projectinput1" class="form-control" value="{{ old('destinationphotocredit') }}" placeholder="Destination Map"--}}
                                                                                    {{--name="destinationphotocredit" required>--}}
                                                                                </div>
                                                                            </div>

                                                                        </div>

                                                                        <div class="row">
                                                                            <div class="col-md-12">
                                                                                <img src="Destination/{{ $data->coverimage }}" style="width:100%;padding-bottom: 10px">
                                                                            </div>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-md-12">
                                                                                <img src="Destination/{{ $data->destinationcoverimage }}" style="width:100%;padding-bottom: 10px">
                                                                            </div>
                                                                        </div>

                                                                        <div class="row">
                                                                            <div class="col-md-12">
                                                                                <button type="submit" class="btn btn-icon btn-outline-primary">
                                                                                    <i class="la la-check-square-o"></i> Save
                                                                                </button>
                                                                            </div>
                                                                        </div>

                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td><a href="{{ route('backend.DeleteDestination',['id'=> $data->id])}}" class="btn btn-icon btn-outline-primary">Delete</a></td>
                                            </tr>
                                        @endforeach
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
                    <!-- Summernote Click to edit end -->
            <script src="backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
            <script src="backend/app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js" type="text/javascript"></script>
            <script src="backend/app-assets/js/scripts/ui/jquery-ui/date-pickers.min.js" type="text/javascript"></script>
            <script src="backend/app-assets/vendors/js/pickers/pickadate/picker.js" type="text/javascript"></script>
            <script src="backend/app-assets/vendors/js/pickers/pickadate/picker.date.js" type="text/javascript"></script>
            <script src="backend/app-assets/vendors/js/pickers/pickadate/picker.time.js" type="text/javascript"></script>
            <script src="backend/app-assets/vendors/js/pickers/pickadate/legacy.js" type="text/javascript"></script>
            <script src="backend/app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js"
                    type="text/javascript"></script>
            <script src="backend/app-assets/vendors/js/pickers/daterange/daterangepicker.js"
                    type="text/javascript"></script>
            <script src="backend/app-assets/js/scripts/pickers/dateTime/pick-a-datetime.js"
                    type="text/javascript"></script>
            <script src="backend/app-assets/vendors/js/tables/datatable/datatables.min.js" type="text/javascript"></script>
            <script src="backend/app-assets/js/core/app-menu.min.js" type="text/javascript"></script>
            <script src="backend/app-assets/js/core/app.min.js" type="text/javascript"></script>
            <script src="backend/app-assets/js/scripts/customizer.min.js" type="text/javascript"></script>
            <script src="backend/app-assets/js/scripts/tables/datatables/datatable-basic.js"
                    type="text/javascript"></script>

        </div>
    </div>

@endsection
