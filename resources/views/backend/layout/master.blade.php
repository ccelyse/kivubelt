<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="">
    <title>@yield('title')</title>
    <link rel="apple-touch-icon" href="backend/app-assets/images/logo.png">
    <link rel="shortcut icon" type="image/x-icon" href="backend/app-assets/images/logo.png">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i%7CQuicksand:300,400,500,700"
          rel="stylesheet">
    <link href="https://maxcdn.icons8.com/fonts/line-awesome/1.1/css/line-awesome.min.css"
          rel="stylesheet">
    <!-- BEGIN VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="backend/app-assets/css/vendors.min.css">
    <!-- END VENDOR CSS-->
    <!-- BEGIN MODERN CSS-->
    <link rel="stylesheet" type="text/css" href="backend/app-assets/css/app.min.css">
    <!-- END MODERN CSS-->
    <!-- BEGIN Page Level CSS-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="backend/app-assets/css/core/menu/menu-types/vertical-menu.min.css">
    <link rel="stylesheet" type="text/css" href="backend/app-assets/css/core/colors/palette-gradient.min.css">
    <link rel="stylesheet" type="text/css" href="backend/app-assets/vendors/css/cryptocoins/cryptocoins.css">
    <!-- END Page Level CSS-->
    <!-- BEGIN Custom CSS-->
    <link rel="stylesheet" type="text/css" href="backend/assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="backend/app-assets/vendors/css/forms/icheck/icheck.css">
    <link rel="stylesheet" type="text/css" href="backend/app-assets/vendors/css/forms/icheck/custom.css">
    <!-- END VENDOR CSS-->
    <!-- BEGIN MODERN CSS-->
    <link rel="stylesheet" type="text/css" href="backend/app-assets/css/app.min.css">
    <!-- END MODERN CSS-->
    <!-- BEGIN Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="backend/app-assets/css/core/menu/menu-types/vertical-menu.min.css">
    <link rel="stylesheet" type="text/css" href="backend/app-assets/css/core/colors/palette-gradient.min.css">
    <link rel="stylesheet" type="text/css" href="backend/app-assets/css/pages/login-register.min.css">
    <link rel="stylesheet" type="text/css" href="backend/app-assets/vendors/css/tables/datatable/datatables.min.css">
    <link rel="stylesheet" type="text/css" href="backend/app-assets/vendors/css/editors/summernote.css">
    <link rel="stylesheet" type="text/css" href="backend/app-assets/vendors/css/editors/codemirror.css">
    <link rel="stylesheet" type="text/css" href="backend/app-assets/vendors/css/editors/theme/monokai.css">
    <link rel="stylesheet" type="text/css" href="backend/app-assets/vendors/css/forms/selects/select2.min.css">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- END Custom CSS-->
</head>
<body class="vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar" data-open="click" data-menu="vertical-menu" data-col="2-columns">
@yield('content')

{{--<script src="backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>--}}
<script src="backend/app-assets/js/scripts/editors/editor-summernote.min.js" type="text/javascript"></script>
<script src="backend/app-assets/vendors/js/editors/codemirror/lib/codemirror.js" type="text/javascript"></script>
<script src="backend/app-assets/vendors/js/editors/codemirror/mode/xml/xml.js" type="text/javascript"></script>
<script src="backend/app-assets/vendors/js/editors/summernote/summernote.js" type="text/javascript"></script>
<script src="backend/app-assets/vendors/js/charts/chart.min.js" type="text/javascript"></script>
<script src="backend/app-assets/vendors/js/charts/raphael-min.js" type="text/javascript"></script>
<script src="backend/app-assets/vendors/js/charts/morris.min.js" type="text/javascript"></script>
<script src="backend/app-assets/vendors/js/charts/jvector/jquery-jvectormap-2.0.3.min.js" type="text/javascript"></script>
<script src="backend/app-assets/vendors/js/charts/jvector/jquery-jvectormap-world-mill.js" type="text/javascript"></script>
<script src="backend/app-assets/data/jvector/visitor-data.js" type="text/javascript"></script>
<script src="backend/app-assets/js/core/app-menu.min.js" type="text/javascript"></script>
<script src="backend/app-assets/js/core/app.min.js" type="text/javascript"></script>
<script src="backend/app-assets/js/scripts/customizer.min.js" type="text/javascript"></script>
<script src="backend/app-assets/js/scripts/pages/dashboard-sales.min.js" type="text/javascript"></script>
<script src="backend/app-assets/vendors/js/tables/datatable/datatables.min.js" type="text/javascript"></script>
<script src="backend/app-assets/vendors/js/tables/datatable/dataTables.buttons.min.js" type="text/javascript"></script>
<script src="backend/app-assets/vendors/js/tables/vfs_fonts.js" type="text/javascript"></script>
<script src="backend/app-assets/js/scripts/tables/datatables/datatable-advanced.js" type="text/javascript"></script>



