<style>
    .main-menu.menu-light .navigation>li.open>a {
        color: #545766;
        background: #f5f5f5;
        border-right: 4px solid #6b442b !important;
    }
</style>
<div class="main-menu menu-fixed menu-light menu-accordion    menu-shadow " data-scroll-to-active="true">
    <div class="main-menu-content">
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
            <?php
            $user_id = \Auth::user()->id;
            $Userrole = \App\User::where('id',$user_id)->value('role');

            $MembersProfile = url('MembersProfile');
            $MemberAccount = url('MemberAccount');
            $CreateAccount = url('CreateAccount');
            $AccountList = url('AccountList');
            $ListOfTourismAgents = url('ListOfTourismAgents');
            $ListTourOperators = url('ListOfTourOperators');
            $Destination = url('Destinations');
            $CountryProfile = url('CountryProfiles');
            $Places = url('ThingToDo');
            $AgentAccount = url('AgentAccount');
            $AgentProfile = url('AgentProfile');
            $OperatorAccount = url('OperatorAccount');
            $AgentTourOperators = url('AgentTourOperators');
            $TourOperatorProfile = url('TourOperatorProfile');
            $Experience = url('Experience');
            $ActivityCategory = url('ActivityCategory');
            $RegisteringProcess = url('RegisteringProcess');
            $Event = url('Event');
            $Dashboard = url('Dashboard');
            $Gallery = url('Gallery');
            $IGGallery = url('IgApi');
            $CompanyTypes = url('CompanyTypes');
            $MarketSegments = url('MarketSegments');
            $PartnerFinder = url('PartnerFinder');
            $PressRelease = url('PressReleases');
            $Stay = url('Stay');
            $AddEatAndDrink = url('AddEatAndDrink');

            $homerow = url('HomeRowOne');
            $HomeRowTwo = url('HomeRowTwo');
            $HomeRowThree = url('HomeRowThree');
            $HomeRowThree = url('HomeRowThree');
            $AboutRwandaRow1 = url('AboutRwandaRowOne');
            $AboutRwandaRowTwo = url('AboutRwandaRowTwo');
            $AboutRegionRowOne = url('AboutRegionRowOne');
            $AboutRegionRowTwo = url('AboutRegionRowTwo');
            $DiscoverActivity = url('DiscoverActivity');

            switch ($Userrole) {
                case "0":
                    echo "<li class=' nav-item'><a href='$Dashboard'><i class='fas fa-chart-line'></i><span class='menu-title' data-i18n='nav.dash.main'>Dashboard</span></a> </li>";
                    echo "<li class=' nav-item'><a href='#'><i class='fas fa-user-circle'></i><span class='menu-title' data-i18n='nav.templates.main'>Account</span></a> <ul class='menu-content'> <li class=' nav-item'><a href='$CreateAccount'><i class='fas fa-user-alt'></i><span class='menu-title' data-i18n='nav.dash.main'>Create Account</span></a> </li><li class=' nav-item'><a href=''><i class='fas fa-user-circle'></i><span class='menu-title' data-i18n='nav.dash.main'>Account List</span></a> </li></ul></li>";
                    echo "<li class=' nav-item'><a href='#'><i class='fas fa-user-circle'></i><span class='menu-title' data-i18n='nav.templates.main'>Homepage</span></a> <ul class='menu-content'> <li class=' nav-item'><a href='$homerow'><i class='fas fa-user-alt'></i><span class='menu-title' data-i18n='nav.dash.main'>Home Row 1</span></a> </li><li class=' nav-item'><a href='$HomeRowTwo'><i class='fas fa-user-circle'></i><span class='menu-title' data-i18n='nav.dash.main'>Home Row 2</span></a> </li><li class=' nav-item'><a href='$HomeRowThree'><i class='fas fa-user-circle'></i><span class='menu-title' data-i18n='nav.dash.main'>Home Row 3</span></a> </li></ul></li>";
                    echo "<li class=' nav-item'><a href='#'><i class='fas fa-user-circle'></i><span class='menu-title' data-i18n='nav.templates.main'>About Rwanda</span></a> <ul class='menu-content'> <li class=' nav-item'><a href='$AboutRwandaRow1'><i class='fas fa-user-alt'></i><span class='menu-title' data-i18n='nav.dash.main'>About Rwanda Row 1</span></a> </li><li class=' nav-item'><a href='$AboutRwandaRowTwo'><i class='fas fa-user-circle'></i><span class='menu-title' data-i18n='nav.dash.main'>About Rwanda Row 2</span></a> </li></ul></li>";
                    echo "<li class=' nav-item'><a href='#'><i class='fas fa-user-circle'></i><span class='menu-title' data-i18n='nav.templates.main'>About Region</span></a> <ul class='menu-content'> <li class=' nav-item'><a href='$AboutRegionRowOne'><i class='fas fa-user-alt'></i><span class='menu-title' data-i18n='nav.dash.main'>About Rwanda Row 1</span></a> </li><li class=' nav-item'><a href='$AboutRegionRowTwo'><i class='fas fa-user-circle'></i><span class='menu-title' data-i18n='nav.dash.main'>About Rwanda Row 2</span></a> </li></ul></li>";
//                    echo "<li class=' nav-item'><a href='$ListOfTourismAgents'><i class='fas fa-users'></i><span class='menu-title' data-i18n='nav.dash.main'>Tourism Agents</span></a> </li>";
//                    echo "<li class=' nav-item'><a href='$CountryProfile'><i class='fas fa-globe'></i><span class='menu-title' data-i18n='nav.dash.main'>Country Profile</span></a> </li>";
//                    echo "<li class=' nav-item'><a href='$Destination'><i class='fas fa-plane-departure'></i><span class='menu-title' data-i18n='nav.dash.main'>Destinations</span></a> </li>";
                    echo "<li class=' nav-item'><a href='$Places'><i class='fas fa-map-marked-alt'></i><span class='menu-title' data-i18n='nav.dash.main'>Discover</span></a> </li>";
                    echo "<li class=' nav-item'><a href='$DiscoverActivity'><i class='fas fa-map-marked-alt'></i><span class='menu-title' data-i18n='nav.dash.main'>Discover Activity</span></a> </li>";
                    echo "<li class=' nav-item'><a href='$Stay'><i class='fas fa-hotel'></i><span class='menu-title' data-i18n='nav.dash.main'>Stay</span></a> </li>";
                    echo "<li class=' nav-item'><a href='$AddEatAndDrink'><i class='fas fa-utensils'></i><span class='menu-title' data-i18n='nav.dash.main'>Eat & Drink</span></a> </li>";
//                    echo "<li class=' nav-item'><a href='$ActivityCategory'><i class='fas fa-dice'></i><span class='menu-title' data-i18n='nav.dash.main'>Activity Category</span></a></li>";
//                    echo "<li class=' nav-item'><a href='$RegisteringProcess'><i class='fas fa-edit'></i><span class='menu-title' data-i18n='nav.dash.main'>Registering Process</span></a></li>";
//                    echo "<li class=' nav-item'><a href='$Gallery'><i class='fas fa-camera-retro'></i><span class='menu-title' data-i18n='nav.dash.main'>Gallery</span></a></li>";
//                    echo "<li class=' nav-item'><a href='$IGGallery'><i class='fas fa-camera-retro'></i><span class='menu-title' data-i18n='nav.dash.main'>IG Gallery</span></a></li>";
//                    echo "<li class=' nav-item'><a href='$PressRelease'><i class='fas fa-camera-retro'></i><span class='menu-title' data-i18n='nav.dash.main'>Press Release</span></a></li>";
//                    echo "<li class=' nav-item'><a href='#'><i class='fas fa-cog'></i><span class='menu-title' data-i18n='nav.templates.main'>Users Profile Settings</span></a> <ul class='menu-content'> <li class=' nav-item'><a href='$CompanyTypes'><i class='fas fa-user-alt'></i><span class='menu-title' data-i18n='nav.dash.main'>Company Types</span></a> </li><li class=' nav-item'><a href='$MarketSegments'><i class='fas fa-user-circle'></i><span class='menu-title' data-i18n='nav.dash.main'>Market Segments</span></a> </li></ul></li>";
//
//                    echo "<li class=' nav-item'><a href='$Places'><i class='fas fa-users'></i><span class='menu-title' data-i18n='nav.dash.main'>Experiences</span></a> </li>";
                    break;

                case "1":

                    break;

                case "agent":
                    echo "<li class=' nav-item'><a href='$AgentAccount'><i class='fas fa-user-circle'></i><span class='menu-title' data-i18n='nav.dash.main'>Account</span></a> </li>";
                    echo "<li class=' nav-item'><a href='$AgentProfile'><i class='fas fa-user'></i><span class='menu-title' data-i18n='nav.dash.main'>Profile</span></a> </li>";
                    echo "<li class=' nav-item'><a href='$Experience'><i class='fas fa-box-open'></i><span class='menu-title' data-i18n='nav.dash.main'>Add Package</span></a> </li>";
                    echo "<li class=' nav-item'><a href='$Event'><i class='fas fa-calendar-check'></i><span class='menu-title' data-i18n='nav.dash.main'>Events</span></a> </li>";
                    echo "<li class=' nav-item'><a href='$PartnerFinder'><i class='fas fa-calendar-check'></i><span class='menu-title' data-i18n='nav.dash.main'>Partner Finder</span></a> </li>";
                    break;

                case "operator":
                    echo "<li class=' nav-item'><a href='$AgentTourOperators'><i class='fas fa-user-circle'></i><span class='menu-title' data-i18n='nav.dash.main'>Account</span></a> </li>";
                    echo "<li class=' nav-item'><a href='$TourOperatorProfile'><i class='fas fa-users'></i><span class='menu-title' data-i18n='nav.dash.main'>Profile</span></a> </li>";
                    echo "<li class=' nav-item'><a href='$Experience'><i class='fas fa-users'></i><span class='menu-title' data-i18n='nav.dash.main'>Experiences</span></a> </li>";
                    break;
                default:
                    return redirect()->back();
                    break;
            }
            ?>

            {{--<li class=" nav-item"><a href="#"><i class="fas fa-user-circle"></i><span class="menu-title" data-i18n="nav.templates.main">Account</span></a>--}}
                {{--<ul class="menu-content">--}}
                    {{--<li class=" nav-item"><a href="{{url('CreateAccount')}}"><i class="fas fa-user-alt"></i><span class="menu-title" data-i18n="nav.dash.main">Create Account</span></a>--}}
                    {{--</li>--}}
                    {{--<li class=" nav-item"><a href="{{url('AccountList')}}"><i class="fas fa-user-circle"></i><span class="menu-title" data-i18n="nav.dash.main">Account List</span></a>--}}
                    {{--</li>--}}
                {{--</ul>--}}
            {{--</li>--}}

            {{--<li class=" nav-item"><a href="{{url('ListOfMembers')}}"><i class="fas fa-users"></i><span class="menu-title" data-i18n="nav.dash.main">Members</span></a>--}}
            {{--</li>--}}


            {{--<li class=" nav-item"><a href="#"><i class="fas fa-newspaper"></i><span class="menu-title" data-i18n="">News</span></a>--}}
                {{--<ul class="menu-content">--}}
                    {{--<li class=" nav-item"><a href="{{url('AddNews')}}"><i class="fas fa-newspaper"></i><span class="menu-title" data-i18n="nav.dash.main">Add News</span></a>--}}
                    {{--</li>--}}
                    {{--<li class=" nav-item"><a href="{{url('NewsList')}}"><i class="fas fa-edit"></i><span class="menu-title" data-i18n="nav.dash.main">News List</span></a>--}}
                    {{--</li>--}}
                {{--</ul>--}}
            {{--</li>--}}

            {{--<li class=" nav-item"><a href="#"><i class="fas fa-envelope-open"></i><span class="menu-title" data-i18n="">Communication</span></a>--}}
                {{--<ul class="menu-content">--}}
                    {{--<li class=" nav-item"><a href="{{url('SendEmail')}}"><i class="fas fa-envelope-open"></i><span class="menu-title" data-i18n="nav.dash.main">Send an Email</span></a>--}}
                    {{--</li>--}}
                    {{--<li class=" nav-item"><a href="{{url('SendSMS')}}"><i class="fas fa-comment"></i><span class="menu-title" data-i18n="nav.dash.main">Send an SMS</span></a>--}}
                    {{--</li>--}}
                {{--</ul>--}}
            {{--</li>--}}

        </ul>
    </div>
</div>


