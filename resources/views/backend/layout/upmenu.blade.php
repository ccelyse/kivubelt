<style>
    .header-navbar .navbar-container ul.nav li a.menu-toggle, .header-navbar .navbar-container ul.nav li a.nav-link-label {
        padding: 1.7rem 1rem 1.6rem;
        /* margin-bottom: 10px; */
        position: relative;
        bottom: 10px;
    }
    .bg-info {
        background-color: #00688c !important
    }
    .header-navbar .navbar-header .navbar-brand .brand-logo {
        width: 80px;
    }

</style>
<nav class="header-navbar navbar-expand-md navbar navbar-with-menu navbar-without-dd-arrow fixed-top navbar-semi-light bg-info navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-header">
            <ul class="nav navbar-nav flex-row">
                <li class="nav-item mobile-menu d-md-none mr-auto"><a class="nav-link nav-menu-main menu-toggle hidden-xs" href="{{url('/Dashboard')}}"><i class="ft-menu font-large-1"></i></a></li>
                <li class="nav-item">
                    <a class="navbar-brand" href="{{url('/Dashboard')}}">
                        <img class="brand-logo" alt="rha" src="backend/app-assets/images/logo.png">
                        {{--<h3 class="brand-text">RHA</h3>--}}
                    </a>
                </li>
                <li class="nav-item d-md-none">
                    <a class="nav-link open-navbar-container" data-toggle="collapse" data-target="#navbar-mobile"><i class="la la-ellipsis-v"></i></a>
                </li>
            </ul>
        </div>
        <div class="navbar-container content">
            <div class="collapse navbar-collapse" id="navbar-mobile">
                <ul class="nav navbar-nav mr-auto float-left">

                </ul>
                <ul class="nav navbar-nav float-right">
                    <li class="dropdown dropdown-notification nav-item">
                            <a class="dropdown-toggle nav-link dropdown-user-link" href="{{url('/Dashboard')}}" data-toggle="dropdown">
                                <span class="mr-1">Hello,
                                  <span class="user-name text-bold-700">{{ Auth::user()->name }}</span>
                                </span>
                            </a>
                    </li>
                    <li class="dropdown dropdown-notification nav-item"><a class="nav-link nav-link-label" href="#" data-toggle="dropdown"><i class="ficon ft-bell"></i>
                            <span class="badge badge-pill badge-default badge-danger badge-default badge-up badge-glow">
                                {{Auth::user()->unreadNotifications->count()}}
                            </span></a>
                        <ul class="dropdown-menu dropdown-menu-media dropdown-menu-right">

                            {{--<li class="dropdown-menu-footer"><a class="dropdown-item text-muted text-center" href="">Read all notifications</a></li>--}}
                            <li class="scrollable-container media-list w-100">
                                @if(0 == Auth::user()->unreadNotifications->count())

                                   <a href='#'>
                                       <div class='media' style='background: #e4e5ec;'>
                                           <div class='media-left align-self-center'><i class='ft-plus-square icon-bg-circle bg-cyan'></i></div>
                                           <div class='media-body'>
                                               <h6 class='media-heading'>You don't have any notification</h6>
                                           </div>
                                       </div>
                                   </a>
                                @else

                                <?php
                                $user_idd = \Auth::user();
                                $user = App\User::find($user_idd);

                                function get_timeago( $ptime )
                                {
                                    $estimate_time = time() - $ptime;

                                    if( $estimate_time < 1 )
                                    {
                                        return 'less than 1 second ago';
                                    }

                                    $condition = array(
                                        12 * 30 * 24 * 60 * 60  =>  'year',
                                        30 * 24 * 60 * 60       =>  'month',
                                        24 * 60 * 60            =>  'day',
                                        60 * 60                 =>  'hour',
                                        60                      =>  'minute',
                                        1                       =>  'second'
                                    );

                                    foreach( $condition as $secs => $str )
                                    {
                                        $d = $estimate_time / $secs;

                                        if( $d >= 1 )
                                        {
                                            $r = round( $d );
                                            return 'about ' . $r . ' ' . $str . ( $r > 1 ? 's' : '' ) . ' ago';
                                        }
                                    }
                                }
                                //
                                foreach ($user_idd->unreadNotifications as $notification) {

                                    $eventby = $notification->data['eventby'];
                                    $eventtitle = $notification->data['eventtitle'];
                                    $eventpurpose = $notification->data['eventpurpose'];
                                    $eventid = $notification->data['eventid'];
                                    $created_at = $notification->created_at;

//                                    echo timeAgo('2013-05-01 00:22:35');
                                    $lastTimeLoggedOut = \Illuminate\Support\Carbon::parse($created_at)->diffForHumans();
                                    $link = '';
                                    $link2 = $link.$eventid;
                                    echo "<a href='$link2'><div class='media' style='background: #e4e5ec;'> <div class='media-left align-self-center'><i class='ft-plus-square icon-bg-circle bg-cyan'></i></div><div class='media-body'> <h6 class='media-heading'>$eventby has added a  $eventpurpose </h6> <p class='notification-text font-small-3 text-muted'> $eventtitle</p><small> <time class='media-meta text-muted' datetime='2015-06-11T18:29:20+08:00'>$lastTimeLoggedOut</time></small> </div></div></a>";
//
//                                                   $notification->data['getpatient'];
                                }
//                                foreach ($user_idd->readNotifications as $notification) {
//
//                                    $noti = $notification->data['getpatient'];
//                                    $created_at = $notification->created_at;
//                                    $notid = $notification->data['patientrecoddateid'];
////                                    echo timeAgo('2013-05-01 00:22:35');
//                                    $lastTimeLoggedOut = \Illuminate\Support\Carbon::parse($created_at)->diffForHumans();
//                                    $link = 'DoctorLaboratoryTechTest?patientrecoddateid=';
//                                    $link2 = $link.$notid;
////                                    $lastTimeLoggedOut = \Illuminate\Support\Carbon::parse('2018-12-29 22:42:27')->diffForHumans();
//                                    echo "<a href='$link2'><div class='media' style='background: #e4e5ec;'> <div class='media-left align-self-center'><i class='ft-plus-square icon-bg-circle bg-cyan'></i></div><div class='media-body'> <h6 class='media-heading'>$eventby has added a  $eventpurpose </h6> <p class='notification-text font-small-3 text-muted'> $eventtitle</p><small> <time class='media-meta text-muted' datetime='2015-06-11T18:29:20+08:00'>$lastTimeLoggedOut</time></small> </div></div></a>";
////
////                                                   $notification->data['getpatient'];
//                                }


                                ?>
                                @endif
                            </li>

                        </ul>
                    </li>
                    <li class="dropdown dropdown-user nav-item">
                        <a class="dropdown-toggle nav-link dropdown-user-link" href="{{url('/Dashboard')}}" data-toggle="dropdown">
                        <span class="mr-1">
                         </span><i class="fas fa-user-circle" style="font-size: 20px"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                        <i class="ft-power"></i> Logout</a>
                                </div>
                    </li>



                        {{--<li class="dropdown dropdown-notification nav-item"><a class="nav-link nav-link-label" href="#" data-toggle="dropdown"><i class="ficon ft-bell"></i><span class="badge badge-pill badge-default badge-danger badge-default badge-up badge-glow">--}}

                            {{--</span></a>--}}
                            {{--<ul class="dropdown-menu dropdown-menu-media dropdown-menu-right">--}}
                                {{--<li class="dropdown-menu-footer"><a class="dropdown-item text-muted text-center" href="">Read all notifications</a></li>--}}
                                {{--<li class="scrollable-container media-list w-100">--}}


                                {{--</li>--}}

                            {{--</ul>--}}
                        {{--</li>--}}

                </ul>
            </div>
        </div>
    </div>
</nav>