@extends('backend.layout.master')

@section('title', 'Connect Africa')

@section('content')
    <link rel="stylesheet" type="text/css" href="backend/app-assets/css/pages/timeline.min.css">
    <body class="vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar" data-open="click" data-menu="vertical-menu" data-col="2-columns">
    @include('backend.layout.sidemenu')
    @include('backend.layout.upmenu')
    <style>
        .btn-secondary{
            color:#fff !important;
            background-color: #6a442b !important;
            border-color:#6a442b !important;
        }
        .btn-primary{
            background-color: #b4753c  !important;
            border-color:#b4753c  !important;
        }
        .btn-primary:hover{
            background-color: #b4753c !important;
            border-color:#b4753c !important;
        }
        .btn-dark1 {
            color: #000 !important;
            background-color: transparent;
            border-color: #b4753c !important;
        }
        .btn-dark1:hover {
            color: #000 !important;
            background-color: transparent;
            border-color: transparent;
        }
        .btn-dark {
            color: #000 !important;
            background-color: transparent;
            border-color: transparent;
        }
        .btn-dark:hover {
            color: #000 !important;
            background-color: transparent;
            border-color: #b4753c !important;
        }

    </style>
    <script
            src="https://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
            crossorigin="anonymous"></script>

    <div class="app-content content">
        <div class="content-wrapper">
            <section>
                <div class="card">
                    <div class="card-header">
                        @if (session('success'))
                            <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                                {{ session('success') }}
                            </div>
                        @endif
                        <h4 class="card-title" id="basic-layout-form">Activity Information</h4>
                        <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                <li><a data-action="close"><i class="ft-x"></i></a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="card-content collapse show">
                        <div class="card-body">
                           <div class="form-body">
                                   <section id="timeline" class="timeline-center timeline-wrapper" style="padding-bottom: 10px">
                                        <h3 class="page-title text-center">Activity in details</h3>
                                        <ul class="timeline">
                                            <li class="timeline-line"></li>
                                        </ul>
                                       @foreach($listexperience as $details)
                                           <div class="multi-field-wrapper">
                                               <div class="multi-fields">
                                                   <div class="multi-field">
                                                       <ul class="timeline" style="margin-top: 10px;">
                                                           <li class="timeline-line"></li>
                                                           <li class="timeline-item">
                                                               <div class="timeline-badge">
                                                                   <span class="bg-red bg-lighten-1" data-toggle="tooltip" data-placement="right" title="Activity in details"><i class="la la-plane"></i></span>
                                                               </div>
                                                               <div class="timeline-card card border-grey border-lighten-2">
                                                                   <div class="card-header">
                                                                       <h4 class="card-title"><a href="#">Day</a></h4>
                                                                   </div>
                                                                   <div class="card-content">
                                                                       <div class="row">
                                                                           <div class="col-md-12">
                                                                               <div class="form-group">
                                                                                   <input type="text" id="projectinput1" class="form-control" value="{{$details->activity_day}}"
                                                                                          name="activity_day" required>
                                                                               </div>
                                                                           </div>
                                                                           <div class="col-md-12">
                                                                               <div class="form-group">
                                                                                   <label for="projectinput1">Activity day title</label>
                                                                                   <input type="text" id="projectinput1" class="form-control" value="{{$details->activity_day_title}}"
                                                                                          name="activity_day_title" required>
                                                                               </div>
                                                                           </div>
                                                                       </div>

                                                                       <div class="multi-field-wrapper2">
                                                                           <div class="multi-fields2">
                                                                               <div class="row  multi-field2">
                                                                                       <?php
                                                                                           $id = $details->id;
                                                                                           $getimages = \App\ExperienceDetailsImage::where('activity_details_id',$id)->get();
                                                                                           foreach ($getimages as $images){
                                                                                               $im = "ActivityImages/$images->activity_details_image";
                                                                                               echo "<div class='col-md-12'><img src='$im' style='width: 100%;padding-bottom: 10px'></div>";
                                                                                           }
                                                                                       ?>

                                                                               </div>
                                                                           </div>
                                                                       </div>

                                                                   </div>
                                                               </div>
                                                           </li>
                                                           <li class="timeline-item mt-3">
                                                               <div class="timeline-card card border-grey border-lighten-2">
                                                                   <div class="card-header">
                                                                       <h4 class="card-title"><a href="#">Activity in detail</a></h4>
                                                                   </div>
                                                                   <div class="col-md-12">
                                                                       <div class="form-group">
                                                                           <label for="projectinput1">Activity in detail</label>
                                                                           <textarea  name="activity_details" class="form-control" id="activity_details" rows="10"  required>{{ $details->activity_details }}</textarea>
                                                                       </div>
                                                                   </div>
                                                               </div>
                                                           </li>

                                                       </ul>
                                                   </div>
                                               </div>
                                           </div>
                                       @endforeach

                                    </section>
                                </div>
                        </div>
                    </div>
                </div>
            </section>

        </div>
    </div>

    <!-- BEGIN VENDOR JS-->
    <script src="backend/app-assets/vendors/js/vendors.min.js"></script>
    <script src="backend/app-assets/js/scripts/pages/timeline.min.js"></script>

@endsection