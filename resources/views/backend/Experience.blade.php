@extends('backend.layout.master')

@section('title', 'Connect Africa')

@section('content')
    <link rel="stylesheet" type="text/css" href="backend/app-assets/css/pages/timeline.min.css">
    <body class="vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar" data-open="click" data-menu="vertical-menu" data-col="2-columns">
    @include('backend.layout.sidemenu')
    @include('backend.layout.upmenu')
    <style>
        .btn-secondary{
            color:#fff !important;
            background-color: #6a442b !important;
            border-color:#6a442b !important;
        }
        .btn-primary{
            background-color: #b4753c  !important;
            border-color:#b4753c  !important;
        }
        .btn-primary:hover{
            background-color: #b4753c !important;
            border-color:#b4753c !important;
        }
        .btn-dark1 {
            color: #000 !important;
            background-color: transparent;
            border-color: #b4753c !important;
        }
        .btn-dark1:hover {
            color: #000 !important;
            background-color: transparent;
            border-color: transparent;
        }
        .btn-dark {
            color: #000 !important;
            background-color: transparent;
            border-color: transparent;
        }
        .btn-dark:hover {
            color: #000 !important;
            background-color: transparent;
            border-color: #b4753c !important;
        }

    </style>
    <script
            src="https://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
            crossorigin="anonymous"></script>

<div class="app-content content">
    <div class="content-wrapper">
        <section>
            <div class="card">
                <div class="card-header">
                    @if (session('success'))
                        <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                            {{ session('success') }}
                        </div>
                    @endif
                    <h4 class="card-title" id="basic-layout-form">Activity Information</h4>
                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                            <li><a data-action="close"><i class="ft-x"></i></a></li>
                        </ul>
                    </div>
                </div>

                <div class="card-content collapse show">
                    <div class="card-body">
                        <form class="form-horizontal form-simple" method="POST" action="{{ url('AddActivity') }}" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="form-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="projectinput1">Activity Title</label>
                                            <input type="text" id="projectinput1" class="form-control" value="{{ old('activity_title') }}" placeholder="Activity Title"
                                                   name="activity_title" required>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="projectinput1">Activity Summary</label>
                                            <textarea  name="activity_summary" class="form-control" id="activity_summary" rows="10" >{{ old('activity_summary') }}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <script type="text/javascript">
                                    $(document).ready(function()
                                    {
                                        $('.multi-field-wrapper').each(function() {
                                            var $wrapper = $('.multi-fields', this);
                                            $(".add-field", $(this)).click(function(e) {
                                                $('.multi-field:first-child', $wrapper).clone(true).appendTo($wrapper).find('input').val('').focus();
                                            });
                                            $('.multi-field .remove-field', $wrapper).click(function() {
                                                if ($('.multi-field', $wrapper).length > 1)
                                                    $(this).parent('.multi-field').remove();
                                            });
                                        });
                                    });
                                    $(document).ready(function()
                                    {
                                        $('.multi-field-wrapper2').each(function() {
                                            var $wrapper = $('.multi-fields2', this);
                                            $(".add-field2", $(this)).click(function(e) {
                                                $('.multi-field2:first-child', $wrapper).clone(true).appendTo($wrapper).find('input').val('').focus();
                                            });
                                            $('.multi-field2 .remove-field2', $wrapper).click(function() {
                                                if ($('.multi-field2', $wrapper).length > 1)
                                                    $(this).parent('.multi-field2').remove();
                                            });
                                        });
                                    });
                                </script>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="projectinput1">Activity Price</label>
                                            <input type="number" id="projectinput1" class="form-control" value="{{ old('activity_price') }}"
                                                   name="activity_price">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="projectinput1">Activity Number Of Guest</label>
                                            <input type="number" id="projectinput1" class="form-control" value="{{ old('activity_numberofguest') }}" placeholder="10"
                                                   name="activity_numberofguest">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="projectinput1">Activity Information PDF</label>
                                            <input type="file" id="projectinput1" class="form-control"
                                                   name="activity_information_pdf">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="projectinput1">Here is Activity Information PDF  <i class="fas fa-hand-point-down"></i></label>
                                        <a href="#" class="btn btn-icon btn-outline-primary">Download Activity Information Sample PDF</a>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="projectinput1">Activity Cover Picture <strong>*Dimension 1000 X 750 *</strong></label>
                                            <input type="file" id="projectinput1" class="form-control"
                                                   name="activity_coverimage">
                                            @if ($errors->has('activity_coverimage'))
                                                <span class="help-block" style="color:#e74040">
                                                    <strong>{{ $errors->first('activity_coverimage') }}</strong>
                                                 </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="projectinput1">Activity Destination</label>
                                            <select class="form-control" name="activity_destination">
                                                @foreach($listdestination as $destination)
                                                    <option value="{{$destination->id}}">{{$destination->destinationtitle}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="projectinput1">Activity Place</label>
                                            <select class="form-control" name="activity_place">
                                                @foreach($listplaces as $places)
                                                    <option value="{{$places->id}}">{{$places->placetitle}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <section id="timeline" class="timeline-center timeline-wrapper" style="padding-bottom: 10px">
                                    <h3 class="page-title text-center">Activity in details</h3>
                                    <ul class="timeline">
                                        <li class="timeline-line"></li>
                                    </ul>
                                    <div class="multi-field-wrapper">
                                        <div class="multi-fields">
                                            <div class="multi-field">
                                                <ul class="timeline" style="margin-top: 10px;">
                                                    <li class="timeline-line"></li>
                                                    <li class="timeline-item">
                                                        <div class="timeline-badge">
                                                            <span class="bg-red bg-lighten-1" data-toggle="tooltip" data-placement="right" title="Activity in details"><i class="la la-plane"></i></span>
                                                        </div>
                                                        <div class="timeline-card card border-grey border-lighten-2">
                                                            <div class="card-header">
                                                                <h4 class="card-title"><a href="#">Day</a></h4>
                                                            </div>
                                                            <div class="card-content">
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div class="form-group">
                                                                            <input type="text" id="projectinput1" class="form-control" value="{{ old('activity_day') }}" placeholder="Day 1"
                                                                                   name="activity_day[]">
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-12">
                                                                        <div class="form-group">
                                                                            <label for="projectinput1">Activity day title</label>
                                                                            <input type="text" id="projectinput1" class="form-control" value="{{ old('activity_day_title') }}" placeholder="Activity day title"
                                                                                   name="activity_day_title[]" required>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                {{--<script type="text/javascript">--}}
                                                                {{----}}
                                                                {{--</script>--}}
                                                                <div class="multi-field-wrapper2">
                                                                    <div class="multi-fields2">
                                                                        <div class="row  multi-field2">
                                                                            <div class="col-md-9">
                                                                                <div class="form-group">
                                                                                    <label for="projectinput1">Detail image</label>
                                                                                    <input type="file" id="projectinput1" class="form-control"
                                                                                           name="activity_details_image[]">
                                                                                </div>
                                                                            </div>
                                                                            <button type="button" class="remove-field2 btn btn-dark" style="line-height: inherit !important;"><i class="fas fa-times-circle"></i></button>
                                                                            <button type="button" class="add-field2 btn btn-dark" style="line-height: inherit !important;"><i class="fas fa-plus-circle"></i></button>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li class="timeline-item mt-3">
                                                        {{--<div class="timeline-badge">--}}
                                                        {{--<span class="avatar avatar-online" data-toggle="tooltip" data-placement="left" title="Eu pid nunc urna integer"><img src="backend/app-assets/images/portrait/small/avatar-s-14.png" alt="avatar"></span>--}}
                                                        {{--</div>--}}
                                                        <div class="timeline-card card border-grey border-lighten-2">
                                                            <div class="card-header">
                                                                <h4 class="card-title"><a href="#">Activity in detail</a></h4>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <div class="form-group">
                                                                    <label for="projectinput1">Activity in detail</label>
                                                                    <textarea  name="activity_details" class="form-control" id="activity_details" rows="10"  required>{{ old('placegeneralinformation') }}</textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>

                                                </ul>
                                                <button type="button" class="remove-field btn btn-dark1" style="line-height: inherit !important;"><i class="fas fa-times-circle"></i>Remove a days</button>
                                                <button type="button" class="add-field btn btn-dark1" style="line-height: inherit !important;"><i class="fas fa-plus-circle"></i>Add more days</button>

                                            </div>
                                        </div>
                                    </div>
                                </section>
                                <button type="submit" class="btn btn-primary"> <i class="la la-check-square-o"></i> Save</button>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </section>
        <section id="setting">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Activity list</h4>
                            @if (session('success'))
                                <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                                    {{ session('success') }}
                                </div>
                            @endif
                            <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                            <div class="heading-elements">
                                <ul class="list-inline mb-0">
                                    <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                    <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                    <li><a data-action="close"><i class="ft-x"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body card-dashboard">
                                <table class="table table-striped table-bordered dataex-html5-export table-responsive">
                                    <thead>
                                    <tr>
                                        <th>Activity Title</th>
                                        <th>Activity Summary</th>
                                        <th>Activity Price</th>
                                        <th>Activity Number Of Guest</th>
                                        <th>Activity Information PDF</th>
                                        <th>Activity Cover Picture</th>
                                        <th>Activity Destination</th>
                                        <th>Activity Place</th>
                                        <th>Activity Details</th>
                                        <th>Edit</th>
                                        <th>Delete</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($listactivity as $data)
                                        <tr>
                                            <td>{{$data->activity_title}}</td>
                                            <td>
                                                <button type="button" class="btn btn-icon btn-outline-primary"
                                                        data-toggle="modal"
                                                        data-target="#summary{{$data->id}}">
                                                    Activity Summary
                                                </button>
                                                <!-- Modal -->
                                                <div class="modal fade text-left" id="summary{{$data->id}}" tabindex="-1"
                                                     role="dialog" aria-labelledby="myModalLabel1"
                                                     aria-hidden="true">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h4 class="modal-title" id="myModalLabel1"> Activity Summary</h4>
                                                                <button type="button" class="close" data-dismiss="modal"
                                                                        aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <p>{{$data->activity_summary}}</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>{{$data->activity_price}}</td>
                                            <td>{{$data->activity_numberofguest}}</td>
                                            <td><a href="ActivityPdf/{{$data->activity_information_pdf}}" class="btn btn-icon btn-outline-primary">Activity Information PDF</a></td>
                                            <td><img src="ActivityCoverImage/{{$data->activity_coverimage}}" style="width:100%"> </td>
                                            <td>{{$data->destinationtitle}}</td>
                                            <td>{{$data->placetitle}}</td>
                                            <td><a href="{{ route('backend.ActivityDetails',['id'=> $data->id])}}" class="btn btn-icon btn-outline-primary">Activity Details</a></td>
                                            <td>
                                                <button type="button" class="btn btn-icon btn-outline-primary"
                                                        data-toggle="modal"
                                                        data-target="#editactivity{{$data->id}}">
                                                    Edit
                                                </button>
                                                <!-- Modal -->
                                                <div class="modal fade text-left" id="editactivity{{$data->id}}" tabindex="-1"
                                                     role="dialog" aria-labelledby="myModalLabel1"
                                                     aria-hidden="true">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h4 class="modal-title" id="myModalLabel1"> Activity Summary</h4>
                                                                <button type="button" class="close" data-dismiss="modal"
                                                                        aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <form class="form-horizontal form-simple" method="POST" action="{{ url('AddActivity') }}" enctype="multipart/form-data">
                                                                    {{ csrf_field() }}
                                                                    <div class="form-body">
                                                                        <div class="row">
                                                                            <div class="col-md-12">
                                                                                <div class="form-group">
                                                                                    <label for="projectinput1">Activity Title</label>
                                                                                    <input type="text" id="projectinput1" class="form-control" value="{{$data->activity_title}}" placeholder="Activity Title"
                                                                                           name="activity_title" required>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-12">
                                                                                <div class="form-group">
                                                                                    <label for="projectinput1">Activity Summary</label>
                                                                                    <textarea  name="activity_summary" class="form-control" id="activity_summary" rows="10"  required>{{$data->activity_summary}}</textarea>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <script type="text/javascript">
                                                                            $(document).ready(function()
                                                                            {
                                                                                $('.multi-field-wrapper').each(function() {
                                                                                    var $wrapper = $('.multi-fields', this);
                                                                                    $(".add-field", $(this)).click(function(e) {
                                                                                        $('.multi-field:first-child', $wrapper).clone(true).appendTo($wrapper).find('input').val('').focus();
                                                                                    });
                                                                                    $('.multi-field .remove-field', $wrapper).click(function() {
                                                                                        if ($('.multi-field', $wrapper).length > 1)
                                                                                            $(this).parent('.multi-field').remove();
                                                                                    });
                                                                                });
                                                                            });
                                                                            $(document).ready(function()
                                                                            {
                                                                                $('.multi-field-wrapper2').each(function() {
                                                                                    var $wrapper = $('.multi-fields2', this);
                                                                                    $(".add-field2", $(this)).click(function(e) {
                                                                                        $('.multi-field2:first-child', $wrapper).clone(true).appendTo($wrapper).find('input').val('').focus();
                                                                                    });
                                                                                    $('.multi-field2 .remove-field2', $wrapper).click(function() {
                                                                                        if ($('.multi-field2', $wrapper).length > 1)
                                                                                            $(this).parent('.multi-field2').remove();
                                                                                    });
                                                                                });
                                                                            });
                                                                        </script>
                                                                        <div class="row">
                                                                            <div class="col-md-6">
                                                                                <div class="form-group">
                                                                                    <label for="projectinput1">Activity Price</label>
                                                                                    <input type="number" id="projectinput1" class="form-control" value="{{$data->activity_price}}"
                                                                                           name="activity_price" required>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-6">
                                                                                <div class="form-group">
                                                                                    <label for="projectinput1">Activity Number Of Guest</label>
                                                                                    <input type="number" id="projectinput1" class="form-control" value="{{$data->activity_numberofguest}}" placeholder="10"
                                                                                           name="activity_numberofguest" required>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-md-12">
                                                                                <div class="form-group">
                                                                                    <label for="projectinput1">Activity Information PDF</label>
                                                                                    <input type="file" id="projectinput1" class="form-control"
                                                                                           name="activity_information_pdf">
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="row">
                                                                            <div class="col-md-6">
                                                                                <div class="form-group">
                                                                                    <label for="projectinput1">Activity Cover Picture</label>
                                                                                    <input type="file" id="projectinput1" class="form-control"
                                                                                           name="activity_coverimage">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-6">
                                                                                <div class="form-group">
                                                                                    <label for="projectinput1">Activity Destination</label>
                                                                                    <select class="form-control" name="activity_destination">
                                                                                        <option value="{{$data->destinationtitle}}">{{$data->destinationtitle}}</option>
                                                                                        @foreach($listdestination as $destination)
                                                                                            <option value="{{$destination->id}}">{{$destination->destinationtitle}}</option>
                                                                                        @endforeach
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-md-6">
                                                                                <div class="form-group">
                                                                                    <label for="projectinput1">Activity Place</label>
                                                                                    <select class="form-control" name="activity_place">
                                                                                        <option value="{{$data->activity_place}}">{{$data->placetitle}}</option>
                                                                                        @foreach($listplaces as $places)
                                                                                            <option value="{{$places->id}}">{{$places->placetitle}}</option>
                                                                                        @endforeach
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-md-12">
                                                                                <img src="ActivityCoverImage/{{$data->activity_coverimage}}" style="width: 100%;padding-bottom: 10px">
                                                                            </div>
                                                                        </div>
                                                                        <button type="submit" class="btn btn-primary"> <i class="la la-check-square-o"></i> Save</button>
                                                                    </div>

                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td></td>
                                        </tr>
                                    @endforeach
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

</div>
</div>

<!-- BEGIN VENDOR JS-->
<script src="backend/app-assets/vendors/js/vendors.min.js"></script>
<script src="backend/app-assets/js/scripts/pages/timeline.min.js"></script>

@endsection