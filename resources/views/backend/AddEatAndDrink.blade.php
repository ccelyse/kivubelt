@extends('backend.layout.master')

@section('title', 'Connect Africa')

@section('content')
    <body class="vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar" data-open="click" data-menu="vertical-menu" data-col="2-columns">
    @include('backend.layout.sidemenu')
    @include('backend.layout.upmenu')
    <style>
        .btn-secondary{
            color:#fff !important;
            background-color: #6a442b !important;
            border-color:#6a442b !important;
        }
        .btn-primary{
            background-color: #b4753c  !important;
            border-color:#b4753c  !important;
        }
        .btn-primary:hover{
            background-color: #b4753c !important;
            border-color:#b4753c !important;
        }
        .btn-dark {
            color: #000 !important;
             background-color: transparent;
             border-color: transparent;
        }
        .btn-dark:hover {
            color: #000 !important;
            background-color: transparent;
            border-color: transparent;
        }
        iframe{
            width: 100%;
        }

    </style>
    <script
    src="https://code.jquery.com/jquery-3.3.1.min.js"
    integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
    crossorigin="anonymous"></script>
    <script type="text/javascript">
        $(document).ready(function()
        {
            $('textarea').each(function () {
                this.setAttribute('style', 'height:' + (this.scrollHeight) + 'px;overflow-y:hidden;');
            }).on('input', function () {
                this.style.height = 'auto';
                this.style.height = (this.scrollHeight) + 'px';
            });

        });
    </script>

    <div class="app-content content">
        <div class="content-wrapper">
                <div class="content-body">
                    <!-- Basic Summernote start -->
                    <div class="row match-height">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    @if (session('success'))
                                        <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                                            {{ session('success') }}
                                        </div>
                                    @endif
                                    <h4 class="card-title" id="basic-layout-form">Things Information</h4>
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                            <li><a data-action="close"><i class="ft-x"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body">
                                        <form class="form-horizontal form-simple" method="POST" action="{{ url('UploadEatAndDrink') }}" enctype="multipart/form-data">
                                            {{ csrf_field() }}
                                            <div class="form-body">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Eat & Drink Title</label>
                                                            <input type="text" id="projectinput1" class="form-control" value="{{ old('placetitle') }}"
                                                                   name="placetitle" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Eat & Drink Cover Image <strong>*Dimension 1000 X 750 *</strong></label>
                                                            <input type="file" id="projectinput1" class="form-control" value="{{ old('coverimage') }}"
                                                                   name="coverimage" required>
                                                            @if ($errors->has('coverimage'))
                                                                <span class="help-block" style="color:#e74040">
                                                                    <strong>{{ $errors->first('coverimage') }}</strong>
                                                                 </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Eat & Drink General Picture</label>
                                                            <input type="file" id="projectinput1" class="form-control" value="{{ old('generalimage') }}"
                                                                   name="generalimage" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Eat & Drink Place Location</label>
                                                            <input type="text" id="projectinput1" class="form-control" value="{{ old('placelocation') }}"
                                                                   name="placelocation" required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Eat & Drink Category</label>
                                                            <input type="text" id="projectinput1" class="form-control" value="{{ old('placecategory') }}"
                                                                   name="placecategory" required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <section id="basic" style="width: 100%;">
                                                        <div class="card">
                                                            <div class="card-content collapse show">
                                                                <div class="card-body">
                                                                    <form class="form-horizontal" action="#">
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-lg-12">
                                                                                    <textarea class="summernote"  name="placegeneralinformation" id="placegeneralinformation" required>{{ old('placegeneralinformation') }}</textarea>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </section>
                                                </div>

                                                <script type="text/javascript">
                                                    $(document).ready(function()
                                                    {
                                                        $('.multi-field-wrapper').each(function() {
                                                            var $wrapper = $('.multi-fields', this);
                                                            $(".add-field", $(this)).click(function(e) {
                                                                $('.multi-field:first-child', $wrapper).clone(true).appendTo($wrapper).find('input').val('').focus();
                                                            });
                                                            $('.multi-field .remove-field', $wrapper).click(function() {
                                                                if ($('.multi-field', $wrapper).length > 1)
                                                                    $(this).parent('.multi-field').remove();
                                                            });
                                                        });
                                                    });
                                                </script>
                                                <div class="multi-field-wrapper">
                                                    <div class="multi-fields">
                                                        <div class="row  multi-field">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="projectinput1">Eat & Drink Gallery</label>
                                                                    <input type="file" id="projectinput1" class="form-control"
                                                                           name="eatanddrinkgallery[]">
                                                                </div>
                                                            </div>
                                                                <button type="button" class="remove-field btn btn-dark" style="line-height: inherit !important;"><i class="fas fa-times-circle"></i></button>
                                                                <button type="button" class="add-field btn btn-dark" style="line-height: inherit !important;"><i class="fas fa-plus-circle"></i></button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <button type="submit" class="btn btn-primary"> <i class="la la-check-square-o"></i> Save</button>
                                            </div>

                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <section id="summernote-edit-save" hidden>
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h4 class="card-title">Summernote Click to edit</h4>
                                        <a class="heading-elements-toggle"><i class="la la-ellipsis-h font-medium-3"></i></a>
                                        <div class="heading-elements">
                                            <ul class="list-inline mb-0">
                                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                                <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                                <li><a data-action="close"><i class="ft-x"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="card-content collapse show">
                                        <div class="card-body">
                                            <form class="form-horizontal" action="#">
                                                <div class="form-group">
                                                    <button id="edit" class="btn btn-primary" type="button"><i class="la la-pencil"></i> Edit</button>
                                                    <button id="save" class="btn btn-success" type="button"><i class="la la-save"></i> Save</button>
                                                </div>
                                                <div class="form-group">
                                                    <div class="summernote-edit">
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            <section id="form-control-repeater">
                <div class="row">
                    <div class="col-12">
                        <div class="card">

                            <div class="card-content collapse show">
                                <div class="card-body card-dashboard">
                                    <table class="table table-striped table-bordered zero-configuration table-responsive">
                                        <thead>
                                        <tr>
                                            <th>Eat & Drink Title</th>
                                            <th>Eat & Drink Location</th>
                                            <th>Eat & Drink Category</th>
                                            <th>Eat & Drink General information</th>
                                            <th>Eat & Drink General Images</th>
                                            <th>Eat & Drink Images</th>
                                            <th>Eat & Drink Gallery</th>
                                            <th>Date Created</th>
                                            <th>Edit</th>
                                            <th>Delete</th>

                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($listplaces as $data)
                                            <tr>
                                                <td>{{$data->placetitle}}</td>
                                                <td>{{$data->placelocation}}</td>
                                                <td>{{$data->placecategory}}</td>
                                                <td>
                                                    <button type="button" class="btn btn-icon btn-outline-primary btn-min-width mr-1 mb-1"
                                                            data-toggle="modal"
                                                            data-target="#editnomie{{$data->id}}">View Place General information
                                                    </button>
                                                    <!-- Modal -->
                                                    <div class="modal fade text-left" id="editnomie{{$data->id}}" tabindex="-1"
                                                         role="dialog" aria-labelledby="myModalLabel1"
                                                         aria-hidden="true">
                                                        <div class="modal-dialog" role="document">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h4 class="modal-title" id="myModalLabel1">Place General information</h4>
                                                                    <button type="button" class="close" data-dismiss="modal"
                                                                            aria-label="Close">
                                                                        <span aria-hidden="true">&times;</span>
                                                                    </button>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <?php
                                                                        $text = $data->placegeneralinformation;
                                                                    ?>
                                                                    <p><?php echo $text; ?></p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td><img src="Placecoverimage/{{$data->generalimage}}" style="width:100%"></td>
                                                <td><img src="Placecoverimage/{{$data->coverimage}}" style="width:100%"></td>
                                                <td>
                                                    <button type="button" class="btn btn-icon btn-outline-primary btn-min-width mr-1 mb-1"
                                                            data-toggle="modal"
                                                            data-target="#gallerypic{{$data->id}}">View Gallery
                                                    </button>
                                                    <!-- Modal -->
                                                    <div class="modal fade text-left" id="gallerypic{{$data->id}}" tabindex="-1"
                                                         role="dialog" aria-labelledby="myModalLabel1"
                                                         aria-hidden="true">
                                                        <div class="modal-dialog" role="document">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h4 class="modal-title" id="myModalLabel1">View Gallery</h4>
                                                                    <button type="button" class="close" data-dismiss="modal"
                                                                            aria-label="Close">
                                                                        <span aria-hidden="true">&times;</span>
                                                                    </button>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <?php
                                                                    $text = $data->id;
                                                                    $getpictures = \App\EatDrinkGallery::where('eatdrinkid',$text)->get();
                                                                    ?>
                                                                    @foreach($getpictures as $pictures)
                                                                            <div class="col-md-12">
                                                                                <img src="PlacesPictures/{{$pictures->eatdrinkpicturename}}" style="width:100%; padding-bottom: 10px">
                                                                            </div>
                                                                    @endforeach
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>{{$data->created_at}}</td>
                                                <td>
                                                    <button type="button" class="btn btn-icon btn-outline-primary btn-min-width mr-1 mb-1"
                                                            data-toggle="modal"
                                                            data-target="#editeatanddrink{{$data->id}}">Edit
                                                    </button>
                                                    <!-- Modal -->
                                                    <div class="modal fade text-left" id="editeatanddrink{{$data->id}}" tabindex="-1"
                                                         role="dialog" aria-labelledby="myModalLabel1"
                                                         aria-hidden="true">
                                                        <div class="modal-dialog" role="document">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h4 class="modal-title" id="myModalLabel1">Edit</h4>
                                                                    <button type="button" class="close" data-dismiss="modal"
                                                                            aria-label="Close">
                                                                        <span aria-hidden="true">&times;</span>
                                                                    </button>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <form class="form-horizontal form-simple" method="POST" action="{{ url('EditEatAndDrink') }}" enctype="multipart/form-data">
                                                                        {{ csrf_field() }}
                                                                        <div class="form-body">
                                                                            <div class="row">
                                                                                <div class="col-md-12">
                                                                                    <div class="form-group">
                                                                                        <label for="projectinput1">Eat & Drink Title</label>
                                                                                        <input type="text" id="projectinput1" class="form-control" value="{{$data->placetitle}}"
                                                                                               name="placetitle">
                                                                                        <input type="text" id="projectinput1" class="form-control" value="{{$data->id}}"
                                                                                               name="id" hidden>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-12">
                                                                                    <div class="form-group">
                                                                                        <label for="projectinput1">Eat & Drink Cover Image <strong>*Dimension 1000 X 750 *</strong></label>
                                                                                        <input type="file" id="projectinput1" class="form-control"
                                                                                               name="coverimage">
                                                                                        @if ($errors->has('coverimage'))
                                                                                            <span class="help-block" style="color:#e74040">
                                                                                                 <strong>{{ $errors->first('coverimage') }}</strong>
                                                                                             </span>
                                                                                        @endif
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                            <div class="row">
                                                                                <div class="col-md-12">
                                                                                    <div class="form-group">
                                                                                        <label for="projectinput1">Eat & Drink General Picture</label>
                                                                                        <input type="file" id="projectinput1" class="form-control"
                                                                                               name="generalimage">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-12">
                                                                                    <div class="form-group">
                                                                                        <label for="projectinput1">Eat & Drink Place Location</label>
                                                                                        <input type="text" id="projectinput1" class="form-control" value="{{$data->placelocation}}"
                                                                                               name="placelocation" required>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="col-md-12">
                                                                                    <div class="form-group">
                                                                                        <label for="projectinput1">Eat & Drink Category</label>
                                                                                        <input type="text" id="projectinput1" class="form-control" value="{{$data->placecategory}}"
                                                                                               name="placecategory">
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row">
                                                                                <section id="basic" style="width: 100%;">
                                                                                    <div class="card">
                                                                                        <div class="card-content collapse show">
                                                                                            <div class="card-body">
                                                                                                <form class="form-horizontal" action="#">
                                                                                                    <div class="form-group">
                                                                                                        <div class="row">
                                                                                                            <div class="col-lg-12">
                                                                                                                <textarea class="summernote"  name="placegeneralinformation" id="placegeneralinformation" required>{{$data->placegeneralinformation}}</textarea>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </form>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </section>
                                                                            </div>
                                                                            <button type="submit" class="btn btn-primary"> <i class="la la-check-square-o"></i> Update</button>
                                                                        </div>

                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td><a href="{{ route('backend.DeleteEatAndDrink',['id'=> $data->id])}}" class="btn btn-icon btn-outline-primary">Delete</a></td>
                                            </tr>
                                        @endforeach
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
                    <!-- Summernote Click to edit end -->
            <script src="backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
            <script src="backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
            <script src="backend/app-assets/vendors/js/editors/summernote/summernote.js" type="text/javascript"></script>
            <script src="backend/app-assets/js/scripts/editors/editor-summernote.min.js" type="text/javascript"></script>

            {{--<script src="backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>--}}
            <script src="backend/app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js" type="text/javascript"></script>
            <script src="backend/app-assets/js/scripts/ui/jquery-ui/date-pickers.min.js" type="text/javascript"></script>
            <script src="backend/app-assets/vendors/js/pickers/pickadate/picker.js" type="text/javascript"></script>
            <script src="backend/app-assets/vendors/js/pickers/pickadate/picker.date.js" type="text/javascript"></script>
            <script src="backend/app-assets/vendors/js/pickers/pickadate/picker.time.js" type="text/javascript"></script>
            <script src="backend/app-assets/vendors/js/pickers/pickadate/legacy.js" type="text/javascript"></script>
            <script src="backend/app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js"
                    type="text/javascript"></script>
            <script src="backend/app-assets/vendors/js/pickers/daterange/daterangepicker.js"
                    type="text/javascript"></script>
            <script src="backend/app-assets/js/scripts/pickers/dateTime/pick-a-datetime.js"
                    type="text/javascript"></script>
            <script src="backend/app-assets/vendors/js/tables/datatable/datatables.min.js" type="text/javascript"></script>
            <script src="backend/app-assets/js/core/app-menu.min.js" type="text/javascript"></script>
            <script src="backend/app-assets/js/core/app.min.js" type="text/javascript"></script>
            <script src="backend/app-assets/js/scripts/customizer.min.js" type="text/javascript"></script>
            <script src="backend/app-assets/js/scripts/tables/datatables/datatable-basic.js"
                    type="text/javascript"></script>


        </div>
    </div>

@endsection
