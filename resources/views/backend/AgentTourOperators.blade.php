@extends('backend.layout.master')

@section('title', 'Connect Africa')

@section('content')

    @include('backend.layout.sidemenu')
    @include('backend.layout.upmenu')
    <div class="app-content content">
        <div class="content-wrapper">

            <div class="content-body"><!-- HTML5 export buttons table -->
                <section id="html5">
                    <div class="row">

                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Connect Africa Operator Account Information List</h4>
                                    @if (session('success'))
                                        <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                                            {{ session('success') }}
                                        </div>
                                    @endif
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                            <li><a data-action="close"><i class="ft-x"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body card-dashboard">
                                        <table class="table table-striped table-bordered dataex-html5-export table-responsive">
                                            <thead>
                                            <tr>
                                                <th>Company Name</th>
                                                <th>Phone Number</th>
                                                <th>Email</th>
                                                <th>Edit</th>
                                                {{--<th>Delete</th>--}}
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($listagentinfo as $data)
                                                <tr>
                                                    <td>{{$data->companyname}}</td>
                                                    <td>{{$data->phonenumber}}</td>
                                                    <td>{{$data->email}}</td>
                                                    <td>
                                                        <button type="button" class="btn btn-icon btn-outline-primary"
                                                                data-toggle="modal"
                                                                data-target="#updateaccount{{$data->id}}">
                                                            Edit
                                                        </button>
                                                        <!-- Modal -->
                                                        <div class="modal fade text-left" id="updateaccount{{$data->id}}" tabindex="-1"
                                                             role="dialog" aria-labelledby="myModalLabel1"
                                                             aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h4 class="modal-title" id="myModalLabel1"> Update Account information</h4>
                                                                        <button type="button" class="close" data-dismiss="modal"
                                                                                aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                        <form class="form-horizontal form-simple" method="POST" action="{{ url('EditAgentTourOperators') }}" enctype="multipart/form-data">
                                                                            {{ csrf_field() }}
                                                                            <div class="form-body">
                                                                                <div class="row">
                                                                                    <div class="col-md-6">
                                                                                        <div class="form-group">
                                                                                            <label for="projectinput1">Company Name</label>
                                                                                            <input type="text" id="projectinput1" class="form-control"
                                                                                                   name="companyname" value="{{$data->companyname}}">
                                                                                            <input type="text" id="projectinput1" class="form-control"
                                                                                                   name="id" value="{{$data->id}}">
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-6">
                                                                                        <div class="form-group">
                                                                                            <label for="projectinput1">email</label>
                                                                                            <input type="email" id="projectinput1" class="form-control"
                                                                                                   name="email" value="{{$data->email}}">
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="row">
                                                                                    <div class="col-md-6">
                                                                                        <div class="form-group">
                                                                                            <label for="projectinput1">Phone number</label>
                                                                                            <input type="number" id="projectinput1" class="form-control"
                                                                                                   name="phonenumber" value="{{$data->phonenumber}}">
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-6">
                                                                                        <div class="form-group">
                                                                                            <label for="projectinput1">Password</label>
                                                                                            <input type="password" id="projectinput1" class="form-control"
                                                                                                   name="password">
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="form-actions">
                                                                                    <button type="submit" class="btn btn-login">
                                                                                        <i class="la la-check-square-o"></i> Save
                                                                                    </button>
                                                                                </div>
                                                                            </div>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    {{--<td><a href="{{ route('backend.DeleteAccount',['id'=> $data->id])}}" class="btn btn-icon btn-outline-primary">Delete</a></td>--}}
                                                </tr>
                                            @endforeach
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>

    <script src="backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/ui/jquery-ui/date-pickers.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/picker.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/picker.date.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/picker.time.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/legacy.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js"
            type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/daterange/daterangepicker.js"
            type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/pickers/dateTime/pick-a-datetime.js"
            type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/tables/datatable/datatables.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/app-menu.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/app.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/customizer.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/tables/datatables/datatable-basic.js"
            type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/tables/datatable/dataTables.buttons.min.js"></script>
    <script src="backend/app-assets/vendors/js/tables/datatable/buttons.bootstrap4.min.js"></script>
    <script src="backend/app-assets/vendors/js/tables/jszip.min.js"></script>
    <script src="backend/app-assets/vendors/js/tables/pdfmake.min.js"></script>
    <script src="backend/app-assets/vendors/js/tables/vfs_fonts.js"></script>
    <script src="backend/app-assets/vendors/js/tables/buttons.html5.min.js"></script>
    <script src="backend/app-assets/vendors/js/tables/buttons.print.min.js"></script>
    <script src="backend/app-assets/vendors/js/tables/buttons.colVis.min.js"></script>
    <script src="backend/app-assets/js/scripts/tables/datatables-extensions/datatable-button/datatable-html5.js"></script>


@endsection

