@extends('backend.layout.master')

@section('title', 'Connect Africa')

@section('content')
    <link rel="stylesheet" type="text/css" href="backend/app-assets/css/pages/timeline.min.css">
    <body class="vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar" data-open="click" data-menu="vertical-menu" data-col="2-columns">
    @include('backend.layout.sidemenu')
    @include('backend.layout.upmenu')
    <style>
        .btn-secondary{
            color:#fff !important;
            background-color: #6a442b !important;
            border-color:#6a442b !important;
        }
        .btn-primary{
            background-color: #b4753c  !important;
            border-color:#b4753c  !important;
        }
        .btn-primary:hover{
            background-color: #b4753c !important;
            border-color:#b4753c !important;
        }
        .btn-dark1 {
            color: #000 !important;
            background-color: transparent;
            border-color: #b4753c !important;
        }
        .btn-dark1:hover {
            color: #000 !important;
            background-color: transparent;
            border-color: transparent;
        }
        .btn-dark {
            color: #000 !important;
            background-color: transparent;
            border-color: transparent;
        }
        .btn-dark:hover {
            color: #000 !important;
            background-color: transparent;
            border-color: #b4753c !important;
        }

    </style>
    <script
            src="https://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
            crossorigin="anonymous"></script>
    <script type="text/javascript">

        $(document).ready(function(){
            $('.search-box input[type="text"]').on("keyup input", function() {
                /* Get input value on change */
                var inputVal = $(this).val();
                var resultDropdown = $(this).siblings(".result");

                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: "POST",
                    url: "SearchUserData",
                    data: {
                        'companyname': inputVal,

                    },
                    success: function (data) {
                        $("#display").html(data);
                    }
                });
            });
        });


    </script>
    <script type="text/javascript">
        $(document).ready(function()
        {
            $('textarea').each(function () {
                this.setAttribute('style', 'height:' + (this.scrollHeight) + 'px;overflow-y:hidden;');
            }).on('input', function () {
                this.style.height = 'auto';
                this.style.height = (this.scrollHeight) + 'px';
            });

        });
    </script>

<div class="app-content content">
    <div class="content-wrapper">

        <section id="complex-header">
            <div class="row">
                <div class="col-lg-12">


                    <div class="card">
                        <div class="card-header">
                            <div class="row skin skin-flat">
                                <div class="col-md-6">
                                    <h5 style="font-weight: bold">Search by company type</h5>
                                    @foreach($listcompantype as $type)
                                        <fieldset> <input type="checkbox" id="{{$type->id}}"> <label for="{{$type->id}}">{{$type->companytypes}}</label> </fieldset>
                                    @endforeach
                                </div>
                                <div class="col-md-6">
                                    <h5 style="font-weight: bold">Search by market segment</h5>
                                    @foreach($listmarketsegment as $segment)
                                        <fieldset> <input type="checkbox" id="{{$segment->id}}"> <label for="{{$segment->id}}">{{$segment->marketsegments}}</label> </fieldset>
                                    @endforeach
                                </div>
                                <div class="col-lg-12">
                                    <div class="search-box">
                                        <fieldset class="form-group position-relative mb-0"> <input type="text" class="form-control form-control-xl input-xl" id="searchpatient" placeholder="Search"> </fieldset>
                                    </div>

                                    <p id="display"></p>

                                    {{--<table class="table table-striped table-bordered table-responsive" style="margin-top: 10px">--}}
                                        {{--<thead>--}}
                                        {{--<tr>--}}
                                            {{--<th>Patient Name</th>--}}
                                            {{--<th>Patient Gender</th>--}}
                                            {{--<th>Patient Phone number</th>--}}
                                            {{--<th>Patient Insurance</th>--}}
                                            {{--<th>Patient PID</th>--}}
                                        {{--</tr>--}}
                                        {{--</thead>--}}
                                        {{--<tbody id="display"> </tbody>--}}
                                    {{--</table>--}}
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </section>

</div>
</div>

<!-- BEGIN VENDOR JS-->
<script src="backend/app-assets/vendors/js/vendors.min.js"></script>
<script src="backend/app-assets/js/scripts/pages/timeline.min.js"></script>
    <script src="backend/app-assets/vendors/js/forms/icheck/icheck.min.js"></script>
    <!-- END MODERN JS-->
    <!-- BEGIN PAGE LEVEL JS-->
    <script src="backend/app-assets/js/scripts/forms/checkbox-radio.min.js"></script>

@endsection