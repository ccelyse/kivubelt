@extends('backend.layout.master')

@section('title', 'Connect Africa')

@section('content')
    <link rel="stylesheet" type="text/css" href="backend/app-assets/css/pages/timeline.min.css">
    <body class="vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar" data-open="click" data-menu="vertical-menu" data-col="2-columns">
    @include('backend.layout.sidemenu')
    @include('backend.layout.upmenu')
    <style>
        .btn-secondary{
            color:#fff !important;
            background-color: #6a442b !important;
            border-color:#6a442b !important;
        }
        .btn-primary{
            background-color: #b4753c  !important;
            border-color:#b4753c  !important;
        }
        .btn-primary:hover{
            background-color: #b4753c !important;
            border-color:#b4753c !important;
        }
        .btn-dark1 {
            color: #000 !important;
            background-color: transparent;
            border-color: #b4753c !important;
        }
        .btn-dark1:hover {
            color: #000 !important;
            background-color: transparent;
            border-color: transparent;
        }
        .btn-dark {
            color: #000 !important;
            background-color: transparent;
            border-color: transparent;
        }
        .btn-dark:hover {
            color: #000 !important;
            background-color: transparent;
            border-color: #b4753c !important;
        }

    </style>
    <script
            src="https://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
            crossorigin="anonymous"></script>
    <script type="text/javascript">
        $(document).ready(function()
        {
            $('textarea').each(function () {
                this.setAttribute('style', 'height:' + (this.scrollHeight) + 'px;overflow-y:hidden;');
            }).on('input', function () {
                this.style.height = 'auto';
                this.style.height = (this.scrollHeight) + 'px';
            });

        });
    </script>

<div class="app-content content">
    <div class="content-wrapper">
        <section>
            <div class="card">
                <div class="card-header">
                    @if (session('success'))
                        <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                            {{ session('success') }}
                        </div>
                    @endif
                    <h4 class="card-title" id="basic-layout-form">Event Information</h4>
                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                            <li><a data-action="close"><i class="ft-x"></i></a></li>
                        </ul>
                    </div>
                </div>

                <div class="card-content collapse show">
                    <div class="card-body">
                        <form class="form-horizontal form-simple" method="POST" action="{{ url('AddEvent') }}" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="form-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="projectinput1">Event Name</label>
                                            <input type="text" id="projectinput1" class="form-control" value="{{ old('eventname') }}"
                                                   name="eventname" required>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="projectinput1">Event Description</label>
                                            <textarea  name="eventdescription" class="form-control" id="eventdescription" rows="4"  required>{{ old('eventdescription') }}</textarea>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="projectinput1">Start Date</label>
                                            <input type="date" id="projectinput1" class="form-control"
                                                   name="eventstartdate">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="projectinput1">Start Time</label>
                                        <input type="time" id="projectinput1" class="form-control"
                                               name="eventstarttime" value="13:45:00">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="projectinput1">End Date</label>
                                            <input type="date" id="projectinput1" class="form-control"
                                                   name="eventenddate">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="projectinput1">End Time</label>
                                        <input type="time" id="projectinput1" class="form-control"
                                               name="eventendtime" value="13:45:00">
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="projectinput1">Event Photo</label>
                                            <input type="file" id="projectinput1" class="form-control"
                                                   name="eventimagename">
                                            @if ($errors->has('eventimagename'))
                                                <span class="help-block" style="color:#e74040">
                                                    <strong>{{ $errors->first('eventimagename') }}</strong>
                                                 </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="projectinput1">Event Location</label>
                                        <input type="text" id="projectinput1" class="form-control"
                                               name="eventlocation">
                                    </div>
                                </div>

                                <button type="submit" class="btn btn-primary"> <i class="la la-check-square-o"></i> Save</button>
                            </div>

                        </form>

                    </div>
                </div>
            </div>
        </section>
        <section id="setting">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Your event list</h4>
                            @if (session('success'))
                                <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                                    {{ session('success') }}
                                </div>
                            @endif
                            <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                            <div class="heading-elements">
                                <ul class="list-inline mb-0">
                                    <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                    <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                    <li><a data-action="close"><i class="ft-x"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body card-dashboard">
                                <table class="table table-striped table-bordered dataex-html5-export table-responsive">
                                    <thead>
                                    <tr>
                                        <th>Event Name</th>
                                        <th>Event Description</th>
                                        <th>Start Date</th>
                                        <th>Start Time</th>
                                        <th>End Date</th>
                                        <th>End Time</th>
                                        <th>Event Location</th>
                                        <th>Event Photo</th>
                                        <th>Edit</th>
                                        <th>Delete</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($listevent as $data)
                                        <tr>
                                            <td>{{$data->eventname}}</td>
                                            <td>
                                                <button type="button" class="btn btn-icon btn-outline-primary"
                                                        data-toggle="modal"
                                                        data-target="#eventdesc{{$data->id}}">
                                                    View event description
                                                </button>
                                                <!-- Modal -->
                                                <div class="modal fade text-left" id="eventdesc{{$data->id}}" tabindex="-1"
                                                     role="dialog" aria-labelledby="myModalLabel1"
                                                     aria-hidden="true">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h4 class="modal-title" id="myModalLabel1"> Activity Summary</h4>
                                                                <button type="button" class="close" data-dismiss="modal"
                                                                        aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body">
                                                               <p>{{$data->eventdescription}}</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>{{$data->eventstartdate}}</td>
                                            <td>{{$data->eventstarttime}}</td>
                                            <td>{{$data->eventenddate}}</td>
                                            <td>{{$data->eventendtime}}</td>
                                            <td>{{$data->eventlocation}}</td>
                                            <td><img src="Eventimages/{{$data->eventimagename}}" style="width:100%"> </td>
                                            <td>
                                                <button type="button" class="btn btn-icon btn-outline-primary"
                                                        data-toggle="modal"
                                                        data-target="#editevent{{$data->id}}">
                                                    Edit
                                                </button>
                                                <!-- Modal -->
                                                <div class="modal fade text-left" id="editevent{{$data->id}}" tabindex="-1"
                                                     role="dialog" aria-labelledby="myModalLabel1"
                                                     aria-hidden="true">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h4 class="modal-title" id="myModalLabel1"> Activity Summary</h4>
                                                                <button type="button" class="close" data-dismiss="modal"
                                                                        aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <form class="form-horizontal form-simple" method="POST" action="{{ url('EditEvent') }}" enctype="multipart/form-data">
                                                                    {{ csrf_field() }}
                                                                    <div class="form-body">
                                                                        <div class="row">
                                                                            <div class="col-md-12">
                                                                                <div class="form-group">
                                                                                    <label for="projectinput1">Event Name</label>
                                                                                    <input type="text" id="projectinput1" class="form-control" value="{{$data->eventname}}"
                                                                                           name="eventname" required>
                                                                                    <input type="text" id="projectinput1" class="form-control" value="{{$data->id}}"
                                                                                           name="id" hidden>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-12">
                                                                                <div class="form-group">
                                                                                    <label for="projectinput1">Event Description</label>
                                                                                    <textarea  name="eventdescription" class="form-control" id="eventdescription" rows="10"  required>{{$data->eventdescription}}</textarea>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="row">
                                                                            <div class="col-md-6">
                                                                                <div class="form-group">
                                                                                    <label for="projectinput1">Start Date</label>
                                                                                    <input type="date" id="projectinput1" class="form-control"
                                                                                           name="eventstartdate" value="{{$data->eventstartdate}}">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-6">
                                                                                <label for="projectinput1">Start Time</label>
                                                                                <input type="time" id="projectinput1" class="form-control"
                                                                                       name="eventstarttime" value="{{$data->eventstarttime}}">
                                                                            </div>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-md-6">
                                                                                <div class="form-group">
                                                                                    <label for="projectinput1">End Date</label>
                                                                                    <input type="date" id="projectinput1" class="form-control"
                                                                                           name="eventenddate" value="{{$data->eventenddate}}">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-6">
                                                                                <label for="projectinput1">End Time</label>
                                                                                <input type="time" id="projectinput1" class="form-control"
                                                                                       name="eventendtime" value="{{$data->eventendtime}}">
                                                                            </div>
                                                                        </div>

                                                                        <div class="row">
                                                                            <div class="col-md-6">
                                                                                <div class="form-group">
                                                                                    <label for="projectinput1">Event Photo</label>
                                                                                    <input type="file" id="projectinput1" class="form-control"
                                                                                           name="eventimagename">
                                                                                    @if ($errors->has('eventimagename'))
                                                                                        <span class="help-block" style="color:#e74040">
                                                    <strong>{{ $errors->first('eventimagename') }}</strong>
                                                 </span>
                                                                                    @endif
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-6">
                                                                                <label for="projectinput1">Event Location</label>
                                                                                <input type="text" id="projectinput1" class="form-control"
                                                                                       name="eventlocation" value="{{$data->eventlocation}}">
                                                                            </div>
                                                                            <div class="col-md-12" style="padding-bottom: 10px">
                                                                                <img src="Eventimages/{{$data->eventimagename}}" style="width:100%">
                                                                            </div>
                                                                        </div>

                                                                        <button type="submit" class="btn btn-primary"> <i class="la la-check-square-o"></i> Update</button>
                                                                    </div>

                                                                </form>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td><a href="{{ route('backend.DeleteEvent',['id'=> $data->id])}}" class="btn btn-icon btn-outline-primary">Delete</a></td>

                                        </tr>
                                    @endforeach
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

</div>
</div>

<!-- BEGIN VENDOR JS-->
<script src="backend/app-assets/vendors/js/vendors.min.js"></script>
<script src="backend/app-assets/js/scripts/pages/timeline.min.js"></script>

@endsection