@extends('backend.layout.master')

@section('title', 'Connect Africa')

@section('content')
    <body class="vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar" data-open="click" data-menu="vertical-menu" data-col="2-columns">
    @include('backend.layout.sidemenu')
    @include('backend.layout.upmenu')
    <style>
        .btn-secondary{
            color:#fff !important;
            background-color: #6a442b !important;
            border-color:#6a442b !important;
        }
        .btn-primary{
            background-color: #b4753c  !important;
            border-color:#b4753c  !important;
        }
        .btn-primary:hover{
            background-color: #b4753c !important;
            border-color:#b4753c !important;
        }
        iframe{
            width: 100%;
        }
        .btn-dark {
            color: #000 !important;
            background-color: transparent;
            border-color: transparent;
        }
        .btn-dark:hover {
            color: #000 !important;
            background-color: transparent;
            border-color: transparent;
        }
        .list-group-item {
            padding: 10px !important;
            margin-bottom: 10px !important;
            /* margin-bottom: 10px; */
        }
    </style>
    <script
            src="https://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
            crossorigin="anonymous"></script>
    <script type="text/javascript">
        $(document).ready(function()
        {
            $('textarea').each(function () {
                this.setAttribute('style', 'height:' + (this.scrollHeight) + 'px;overflow-y:hidden;');
            }).on('input', function () {
                this.style.height = 'auto';
                this.style.height = (this.scrollHeight) + 'px';
            });

        });
    </script>

    <div class="app-content content">
        <div class="content-wrapper">
                <div class="content-body">
                    <!-- Basic Summernote start -->
                    <div class="row match-height">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    @if (session('success'))
                                        <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                                            {{ session('success') }}
                                        </div>
                                    @endif
                                    <h4 class="card-title" id="basic-layout-form">Country Profile</h4>
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                            <li><a data-action="close"><i class="ft-x"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body">
                                        <form class="form-horizontal form-simple" method="POST" action="{{ url('AddCountryProfile') }}" enctype="multipart/form-data">
                                            {{ csrf_field() }}
                                            <div class="form-body">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Country name</label>
                                                            <input type="text" id="projectinput1" class="form-control" value="{{ old('countryname') }}" placeholder="Country name"
                                                                   name="countryname" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Country Image (1000*750)</label>
                                                            <input type="file" id="projectinput1" class="form-control" value="{{ old('coverimage') }}"
                                                                   name="coverimage" required>
                                                            @if ($errors->has('coverimage'))
                                                                <span class="help-block" style="color:#e74040">
                                                                    <strong>{{ $errors->first('coverimage') }}</strong>
                                                                 </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Country General information</label>
                                                            <textarea  name="countrygeneralinformation" class="form-control" id="countrygeneralinformation" rows="4"  required>{{ old('countrygeneralinformation') }}</textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Country Map</label>
                                                            <input type="text" id="projectinput1" class="form-control" value="{{ old('countrymap') }}" placeholder="Country Map"
                                                                   name="countrymap" required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Country Language</label>
                                                            <input type="text" id="projectinput1" class="form-control" value="{{ old('countrylanguage') }}" placeholder="Language"
                                                                   name="countrylanguage" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Country Currency</label>
                                                            <input type="text" id="projectinput1" class="form-control" value="{{ old('countrycurrency') }}" placeholder="Currency"
                                                                   name="countrycurrency" required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Country Capital</label>
                                                            <input type="text" id="projectinput1" class="form-control"  placeholder="Country Capital"
                                                                   name="destinationcapital" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Country Population</label>
                                                            <input type="text" id="projectinput1" class="form-control"  placeholder="Country Population"
                                                                   name="destinationpopulation" required>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Country Surface area</label>
                                                            <input type="text" id="projectinput1" class="form-control"  placeholder="Country Surface area"
                                                                   name="destinationsurfacearea" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Country Flag</label>
                                                            <input type="file" id="projectinput1" class="form-control"
                                                                   name="destinationflag" required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Country Timezone</label>
                                                            <input type="text" id="projectinput1" class="form-control"  placeholder="Country Time zone"
                                                                   name="countrytimezone" required>
                                                        </div>
                                                    </div>
                                                </div>

                                                <button type="submit" class="btn btn-primary"> <i class="la la-check-square-o"></i> Save</button>
                                            </div>

                                        </form>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <section id="html5">
                <div class="row">
                    <div class="col-12">
                        <div class="card">

                            <div class="card-content collapse show">
                                <div class="card-body card-dashboard">
                                    <table class="table table-striped table-bordered dataex-html5-export table-responsive">
                                        <thead>
                                        <tr>
                                            <th>Country Name</th>
                                            <th>Country  Official Language</th>
                                            <th>Country Currency</th>
                                            <th>Country Capital</th>
                                            <th>Country Population</th>
                                            <th>Country Surface area</th>
                                            <th>Country Timezone</th>
                                            <th>Country flag</th>
                                            <th>Country Image</th>
                                            <th>Country Map</th>
                                            <th>Country General information</th>
                                            <th>Date Created</th>
                                            <th>Edit</th>
                                            <th>Delete</th>

                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($CountryProfile as $data)
                                            <tr>
                                                <td>{{$data->countryname}}</td>
                                                <td>{{$data->countrylanguage}}</td>
                                                <td>{{$data->countrycurrency}}</td>
                                                <td>{{$data->destinationcapital}}</td>
                                                <td>{{$data->destinationpopulation}}</td>
                                                <td>{{$data->destinationsurfacearea}}</td>
                                                <td>{{$data->countrytimezone}}</td>
                                                <td><img src="DestinationFlag/{{$data->destinationflag}}" style="width: 100%"></td>
                                                <td><img src="CountryProfile/{{$data->coverimage}}" style="width: 100%"></td>
                                                <td>

                                                    <button type="button" class="btn btn-icon btn-outline-primary btn-min-width mr-1 mb-1"
                                                            data-toggle="modal"
                                                            data-target="#map{{$data->id}}">Country Map
                                                    </button>
                                                    <!-- Modal -->
                                                    <div class="modal fade text-left" id="map{{$data->id}}" tabindex="-1"
                                                         role="dialog" aria-labelledby="myModalLabel1"
                                                         aria-hidden="true">
                                                        <div class="modal-dialog" role="document">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h4 class="modal-title" id="myModalLabel1">Destination Map</h4>
                                                                    <button type="button" class="close" data-dismiss="modal"
                                                                            aria-label="Close">
                                                                        <span aria-hidden="true">&times;</span>
                                                                    </button>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <?php
                                                                    $map = $data->countrymap;
                                                                    echo "$map";
                                                                    ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>

                                                    <button type="button" class="btn btn-icon btn-outline-primary btn-min-width mr-1 mb-1"
                                                            data-toggle="modal"
                                                            data-target="#destinationinfo{{$data->id}}">Country General information
                                                    </button>
                                                    <!-- Modal -->
                                                    <div class="modal fade text-left" id="destinationinfo{{$data->id}}" tabindex="-1"
                                                         role="dialog" aria-labelledby="myModalLabel1"
                                                         aria-hidden="true">
                                                        <div class="modal-dialog" role="document">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h4 class="modal-title" id="myModalLabel1">Country General information</h4>
                                                                    <button type="button" class="close" data-dismiss="modal"
                                                                            aria-label="Close">
                                                                        <span aria-hidden="true">&times;</span>
                                                                    </button>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <p>{{$data->countrygeneralinformation}}</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>{{$data->created_at}}</td>
                                                <td>

                                                    <button type="button" class="btn btn-icon btn-outline-primary btn-min-width mr-1 mb-1"
                                                            data-toggle="modal"
                                                            data-target="#editnomie{{$data->id}}">Edit
                                                    </button>
                                                    <!-- Modal -->
                                                    <div class="modal fade text-left" id="editnomie{{$data->id}}" tabindex="-1"
                                                         role="dialog" aria-labelledby="myModalLabel1"
                                                         aria-hidden="true">
                                                        <div class="modal-dialog" role="document">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h4 class="modal-title" id="myModalLabel1">Edit</h4>
                                                                    <button type="button" class="close" data-dismiss="modal"
                                                                            aria-label="Close">
                                                                        <span aria-hidden="true">&times;</span>
                                                                    </button>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <form class="form-horizontal form-simple" method="POST" action="{{ url('EditCountryProfile') }}" enctype="multipart/form-data">
                                                                        {{ csrf_field() }}
                                                                        <div class="form-body">
                                                                            <div class="row">
                                                                                <div class="col-md-6">
                                                                                    <div class="form-group">
                                                                                        <label for="projectinput1">Country name</label>
                                                                                        <input type="text" id="projectinput1" class="form-control" value="{{$data->countryname}}"
                                                                                               name="countryname" required>
                                                                                        <input type="text" id="projectinput1" class="form-control" value="{{$data->id}}"
                                                                                               name="id" hidden>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-6">
                                                                                    <div class="form-group">
                                                                                        <label for="projectinput1">Country Image (1000*750)</label>
                                                                                        <input type="file" id="projectinput1" class="form-control" value="{{ old('coverimage') }}"
                                                                                               name="coverimage">
                                                                                        @if ($errors->has('coverimage'))
                                                                                            <span class="help-block" style="color:#e74040">
                                                                    <strong>{{ $errors->first('coverimage') }}</strong>
                                                                 </span>
                                                                                        @endif
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="col-md-12">
                                                                                    <div class="form-group">
                                                                                        <label for="projectinput1">Country General information</label>
                                                                                        <textarea  name="countrygeneralinformation" class="form-control" id="countrygeneralinformation" rows="4"  required>{{$data->countrygeneralinformation}}</textarea>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="col-md-12">
                                                                                    <div class="form-group">
                                                                                        <label for="projectinput1">Country Map</label>
                                                                                        <input type="text" id="projectinput1" class="form-control" value="{{$data->countrymap}}"
                                                                                               name="countrymap" >
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="col-md-12">
                                                                                    <div class="form-group">
                                                                                        <label for="projectinput1">Country Language</label>
                                                                                        <input type="text" id="projectinput1" class="form-control" value="{{$data->countrylanguage}}"
                                                                                               name="countrylanguage" >
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-12">
                                                                                    <div class="form-group">
                                                                                        <label for="projectinput1">Country Currency</label>
                                                                                        <input type="text" id="projectinput1" class="form-control" value="{{$data->countrycurrency}}"
                                                                                               name="countrycurrency" >
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                            <div class="row">
                                                                                <div class="col-md-12">
                                                                                    <div class="form-group">
                                                                                        <label for="projectinput1">Country Capital</label>
                                                                                        <input type="text" id="projectinput1" class="form-control" value="{{$data->destinationcapital}}"  placeholder="Country Capital"
                                                                                               name="destinationcapital" >
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-12">
                                                                                    <div class="form-group">
                                                                                        <label for="projectinput1">Country Population</label>
                                                                                        <input type="text" id="projectinput1" class="form-control" value="{{$data->destinationpopulation}}"  placeholder="Country Population"
                                                                                               name="destinationpopulation" >
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="col-md-12">
                                                                                    <div class="form-group">
                                                                                        <label for="projectinput1">Country Surface area</label>
                                                                                        <input type="text" id="projectinput1" class="form-control" value="{{$data->destinationsurfacearea}}"  placeholder="Country Surface area"
                                                                                               name="destinationsurfacearea" >
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-12">
                                                                                    <div class="form-group">
                                                                                        <label for="projectinput1">Country Flag</label>
                                                                                        <input type="file" id="projectinput1" class="form-control"
                                                                                               name="destinationflag" >
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="col-md-12">
                                                                                    <div class="form-group">
                                                                                        <label for="projectinput1">Country Timezone</label>
                                                                                        <input type="text" id="projectinput1" class="form-control"  placeholder="Country Time zone"
                                                                                               name="countrytimezone" >
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                            <div class="row">
                                                                                <div class="col-md-12" style="padding-bottom: 10px">
                                                                                    <img src="CountryProfile/{{$data->coverimage}}" style="width: 100%">
                                                                                </div>
                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="col-md-12" style="padding-bottom: 10px">
                                                                                    <img src="DestinationFlag/{{$data->destinationflag}}" style="width: 100%">
                                                                                </div>
                                                                            </div>
                                                                            <button type="submit" class="btn btn-primary"> <i class="la la-check-square-o"></i> Save</button>
                                                                        </div>

                                                                    </form>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td><a href="{{ route('backend.DeleteDestination',['id'=> $data->id])}}" class="btn btn-icon btn-outline-primary">Delete</a></td>
                                            </tr>
                                        @endforeach
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
                    <!-- Summernote Click to edit end -->
                    <script src="backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
                    <script src="backend/app-assets/vendors/js/editors/summernote/summernote.js" type="text/javascript"></script>
                    <script src="backend/app-assets/js/scripts/editors/editor-summernote.min.js" type="text/javascript"></script>

        </div>
    </div>

@endsection
