@extends('backend.layout.master')

@section('title', 'Kivu Belt')

@section('content')
    <body class="vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar" data-open="click" data-menu="vertical-menu" data-col="2-columns">
    @include('backend.layout.sidemenu')
    @include('backend.layout.upmenu')
    <style>
        .btn-secondary{
            color:#fff !important;
            background-color: #6a442b !important;
            border-color:#6a442b !important;
        }
        .btn-primary{
            background-color: #b4753c  !important;
            border-color:#b4753c  !important;
        }
        .btn-primary:hover{
            background-color: #b4753c !important;
            border-color:#b4753c !important;
        }
        .btn-dark {
            color: #000 !important;
             background-color: transparent;
             border-color: transparent;
        }
        .btn-dark:hover {
            color: #000 !important;
            background-color: transparent;
            border-color: transparent;
        }
        iframe{
            width: 100%;
        }

    </style>
    <script
    src="https://code.jquery.com/jquery-3.3.1.min.js"
    integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
    crossorigin="anonymous"></script>
    <script type="text/javascript">
        $(document).ready(function()
        {
            $('textarea').each(function () {
                this.setAttribute('style', 'height:' + (this.scrollHeight) + 'px;overflow-y:hidden;');
            }).on('input', function () {
                this.style.height = 'auto';
                this.style.height = (this.scrollHeight) + 'px';
            });

        });
    </script>

    <div class="app-content content">
        <div class="content-wrapper">
                <div class="content-body">
                    <!-- Basic Summernote start -->
                    <div class="row match-height">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    @if (session('success'))
                                        <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                                            {{ session('success') }}
                                        </div>
                                    @endif
                                    <h4 class="card-title" id="basic-layout-form">Discover Activity Information</h4>
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                            <li><a data-action="close"><i class="ft-x"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body">
                                        <form class="form-horizontal form-simple" method="POST" action="{{ url('AddDiscoverActivity') }}" enctype="multipart/form-data">
                                            {{ csrf_field() }}
                                            <div class="form-body">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Discover Activity  Title</label>
                                                            <input type="text" id="projectinput1" class="form-control" value="{{ old('discoveractivity_title') }}"
                                                                   name="discoveractivity_title" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Discover Activity Cover Image <strong>*Dimension 1000 X 750 *</strong></label>
                                                            <input type="file" id="projectinput1" class="form-control"
                                                                   name="discoveractivity_coverimage" required>
                                                            @if ($errors->has('discoveractivity_coverimage'))
                                                                <span class="help-block" style="color:#e74040">
                                                                    <strong>{{ $errors->first('discoveractivity_coverimage') }}</strong>
                                                                 </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Discover Activity General Picture</label>
                                                            <input type="file" id="projectinput1" class="form-control"
                                                                   name="discoveractivity_generalpicture" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Discover Category</label>
                                                            <select name="discoveractivity_category" class="form-control" id="DefaultSelect">
                                                                @foreach($listplaces as $listplacess)
                                                                <option value="{{$listplacess->id}}">{{$listplacess->placetitle}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <section id="basic" style="width: 100%;">
                                                        <div class="card">
                                                            <div class="card-content collapse show">
                                                                <div class="card-body">
                                                                    <form class="form-horizontal" action="#">
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-lg-12">
                                                                                    <textarea class="summernote"  name="discoveractivity_text" id="discoveractivity_text" required>{{ old('discoveractivity_text') }}</textarea>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </section>
                                                </div>
                                                <script type="text/javascript">
                                                    $(document).ready(function()
                                                    {
                                                        $('.multi-field-wrapper').each(function() {
                                                            var $wrapper = $('.multi-fields', this);
                                                            $(".add-field", $(this)).click(function(e) {
                                                                $('.multi-field:first-child', $wrapper).clone(true).appendTo($wrapper).find('input').val('').focus();
                                                            });
                                                            $('.multi-field .remove-field', $wrapper).click(function() {
                                                                if ($('.multi-field', $wrapper).length > 1)
                                                                    $(this).parent('.multi-field').remove();
                                                            });
                                                        });
                                                    });
                                                </script>
                                                <div class="multi-field-wrapper">
                                                    <div class="multi-fields">
                                                        <div class="row  multi-field">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="projectinput1">Discover  Gallery</label>
                                                                    <input type="file" id="projectinput1" class="form-control"
                                                                           name="thingstodogallery[]">
                                                                </div>
                                                            </div>
                                                            <button type="button" class="remove-field btn btn-dark" style="line-height: inherit !important;"><i class="fas fa-times-circle"></i></button>
                                                            <button type="button" class="add-field btn btn-dark" style="line-height: inherit !important;"><i class="fas fa-plus-circle"></i></button>
                                                        </div>
                                                    </div>
                                                </div>

                                                <button type="submit" class="btn btn-primary"> <i class="la la-check-square-o"></i> Save</button>
                                            </div>

                                        </form>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <section id="summernote-edit-save" hidden>
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h4 class="card-title">Summernote Click to edit</h4>
                                        <a class="heading-elements-toggle"><i class="la la-ellipsis-h font-medium-3"></i></a>
                                        <div class="heading-elements">
                                            <ul class="list-inline mb-0">
                                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                                <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                                <li><a data-action="close"><i class="ft-x"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="card-content collapse show">
                                        <div class="card-body">
                                            <form class="form-horizontal" action="#">
                                                <div class="form-group">
                                                    <button id="edit" class="btn btn-primary" type="button"><i class="la la-pencil"></i> Edit</button>
                                                    <button id="save" class="btn btn-success" type="button"><i class="la la-save"></i> Save</button>
                                                </div>
                                                <div class="form-group">
                                                    <div class="summernote-edit">
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            <section id="form-control-repeater">
                <div class="row">
                    <div class="col-12">
                        <div class="card">

                            <div class="card-content collapse show">
                                <div class="card-body card-dashboard">
                                    <table class="table table-striped table-bordered zero-configuration table-responsive">
                                        <thead>
                                        <tr>
                                            <th>Discover Activity Title</th>
                                            <th>Discover Activity General information</th>
                                            <th>Discover Activity General Images</th>
                                            <th>Discover Activity Cover Images</th>
                                            <th>Date Created</th>
                                            <th>Edit</th>
                                            <th>Delete</th>

                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($discovers as $data)
                                            <tr>
                                                <td>{{$data->discoveractivity_title}}</td>
                                                <td>
                                                    <button type="button" class="btn btn-icon btn-outline-primary btn-min-width mr-1 mb-1"
                                                            data-toggle="modal"
                                                            data-target="#editnomie{{$data->id}}">View Discover Activity General information
                                                    </button>
                                                    <!-- Modal -->
                                                    <div class="modal fade text-left" id="editnomie{{$data->id}}" tabindex="-1"
                                                         role="dialog" aria-labelledby="myModalLabel1"
                                                         aria-hidden="true">
                                                        <div class="modal-dialog" role="document">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h4 class="modal-title" id="myModalLabel1">View Discover General information</h4>
                                                                    <button type="button" class="close" data-dismiss="modal"
                                                                            aria-label="Close">
                                                                        <span aria-hidden="true">&times;</span>
                                                                    </button>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <p>{{$data->placegeneralinformation}}</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td><img src="discover/{{$data->discoveractivity_generalpicture}}" style="width:100%"></td>
                                                <td><img src="discovercover/{{$data->discoveractivity_coverimage}}" style="width:100%"></td>
                                                <td>{{$data->created_at}}</td>
                                                <td>

                                                    <button type="button" class="btn btn-icon btn-outline-primary btn-min-width mr-1 mb-1"
                                                            data-toggle="modal"
                                                            data-target="#editplace{{$data->id}}">Edit
                                                    </button>
                                                    <!-- Modal -->
                                                    <div class="modal fade text-left" id="editplace{{$data->id}}" tabindex="-1"
                                                         role="dialog" aria-labelledby="myModalLabel1"
                                                         aria-hidden="true">
                                                        <div class="modal-dialog" role="document">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h4 class="modal-title" id="myModalLabel1">Edit</h4>
                                                                    <button type="button" class="close" data-dismiss="modal"
                                                                            aria-label="Close">
                                                                        <span aria-hidden="true">&times;</span>
                                                                    </button>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <form class="form-horizontal form-simple" method="POST" action="{{ url('EditDiscoverActivity') }}" enctype="multipart/form-data">
                                                                        {{ csrf_field() }}
                                                                        <div class="form-body">
                                                                            <div class="row">
                                                                                <div class="col-md-6">
                                                                                    <div class="form-group">
                                                                                        <label for="projectinput1">Discover Activity  Title</label>
                                                                                        <input type="text" id="projectinput1" class="form-control" value="{{$data->discoveractivity_title}}"
                                                                                               name="discoveractivity_title">
                                                                                        <input type="text" id="projectinput1" class="form-control" value="{{$data->id}}"
                                                                                               name="id" hidden>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-6">
                                                                                    <div class="form-group">
                                                                                        <label for="projectinput1">Discover Activity Cover Image <strong>*Dimension 1000 X 750 *</strong></label>
                                                                                        <input type="file" id="projectinput1" class="form-control"
                                                                                               name="discoveractivity_coverimage">
                                                                                        @if ($errors->has('discoveractivity_coverimage'))
                                                                                            <span class="help-block" style="color:#e74040">
                                                                    <strong>{{ $errors->first('discoveractivity_coverimage') }}</strong>
                                                                 </span>
                                                                                        @endif
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                            <div class="row">
                                                                                <div class="col-md-6">
                                                                                    <div class="form-group">
                                                                                        <label for="projectinput1">Discover Activity General Picture</label>
                                                                                        <input type="file" id="projectinput1" class="form-control"
                                                                                               name="discoveractivity_generalpicture">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-6">
                                                                                    <div class="form-group">
                                                                                        <label for="projectinput1">Discover Category</label>
                                                                                        <select name="discoveractivity_category" class="form-control" id="DefaultSelect">
                                                                                            <option value="{{$data->discoveractivity_category}}">{{$data->placetitle}}</option>
                                                                                            @foreach($listplaces as $listplacess)
                                                                                                <option value="{{$listplacess->id}}">{{$listplacess->placetitle}}</option>
                                                                                            @endforeach
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row">
                                                                                <section id="basic" style="width: 100%;">
                                                                                    <div class="card">
                                                                                        <div class="card-content collapse show">
                                                                                            <div class="card-body">
                                                                                                <form class="form-horizontal" action="#">
                                                                                                    <div class="form-group">
                                                                                                        <div class="row">
                                                                                                            <div class="col-lg-12">
                                                                                                                <textarea class="summernote"  name="discoveractivity_text" id="discoveractivity_text">{{$data->discoveractivity_text}}</textarea>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </form>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>

                                                                                </section>
                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="col-md-6">
                                                                                    <p>Discover Activity General Picture</p>
                                                                                    <img src="discover/{{$data->discoveractivity_generalpicture}}" style="width: 100%">
                                                                                </div>
                                                                                <div class="col-md-6">
                                                                                    <p>Discover Activity Cover Image </p>
                                                                                    <img src="discovercover/{{$data->discoveractivity_coverimage}}" style="width: 100%">
                                                                                </div>
                                                                            </div>
                                                                            <button type="submit" class="btn btn-primary"> <i class="la la-check-square-o"></i> Save</button>
                                                                        </div>

                                                                    </form>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td><a href="{{ route('backend.DeleteDiscoverActivity',['id'=> $data->id])}}" class="btn btn-icon btn-outline-primary">Delete</a></td>
                                            </tr>
                                        @endforeach
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
                    <!-- Summernote Click to edit end -->
            <script src="backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
            <script src="backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
            <script src="backend/app-assets/vendors/js/editors/summernote/summernote.js" type="text/javascript"></script>
            <script src="backend/app-assets/js/scripts/editors/editor-summernote.min.js" type="text/javascript"></script>

            {{--<script src="backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>--}}
            <script src="backend/app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js" type="text/javascript"></script>
            <script src="backend/app-assets/js/scripts/ui/jquery-ui/date-pickers.min.js" type="text/javascript"></script>
            <script src="backend/app-assets/vendors/js/pickers/pickadate/picker.js" type="text/javascript"></script>
            <script src="backend/app-assets/vendors/js/pickers/pickadate/picker.date.js" type="text/javascript"></script>
            <script src="backend/app-assets/vendors/js/pickers/pickadate/picker.time.js" type="text/javascript"></script>
            <script src="backend/app-assets/vendors/js/pickers/pickadate/legacy.js" type="text/javascript"></script>
            <script src="backend/app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js"
                    type="text/javascript"></script>
            <script src="backend/app-assets/vendors/js/pickers/daterange/daterangepicker.js"
                    type="text/javascript"></script>
            <script src="backend/app-assets/js/scripts/pickers/dateTime/pick-a-datetime.js"
                    type="text/javascript"></script>
            <script src="backend/app-assets/vendors/js/tables/datatable/datatables.min.js" type="text/javascript"></script>
            <script src="backend/app-assets/js/core/app-menu.min.js" type="text/javascript"></script>
            <script src="backend/app-assets/js/core/app.min.js" type="text/javascript"></script>
            <script src="backend/app-assets/js/scripts/customizer.min.js" type="text/javascript"></script>
            <script src="backend/app-assets/js/scripts/tables/datatables/datatable-basic.js"
                    type="text/javascript"></script>


        </div>
    </div>

@endsection
