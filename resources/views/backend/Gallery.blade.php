@extends('backend.layout.master')

@section('title', 'Visit East Africa')

@section('content')
    <body class="vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar" data-open="click" data-menu="vertical-menu" data-col="2-columns">
    @include('backend.layout.sidemenu')
    @include('backend.layout.upmenu')
    <style>
        .btn-secondary{
            color:#fff !important;
            background-color: #6a442b !important;
            border-color:#6a442b !important;
        }
        .btn-primary{
            background-color: #b4753c  !important;
            border-color:#b4753c  !important;
        }
        .btn-primary:hover{
            background-color: #b4753c !important;
            border-color:#b4753c !important;
        }
        .btn-dark {
            color: #000 !important;
             background-color: transparent;
             border-color: transparent;
        }
        .btn-dark:hover {
            color: #000 !important;
            background-color: transparent;
            border-color: transparent;
        }
        iframe{
            width: 100%;
        }

    </style>
    <script
    src="https://code.jquery.com/jquery-3.3.1.min.js"
    integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
    crossorigin="anonymous"></script>

    <div class="app-content content">
        <div class="content-wrapper">
                <div class="content-body">
                    <!-- Basic Summernote start -->
                    <div class="row match-height">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    @if (session('success'))
                                        <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                                            {{ session('success') }}
                                        </div>
                                    @endif
                                    <h4 class="card-title" id="basic-layout-form">Gallery</h4>
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                            <li><a data-action="close"><i class="ft-x"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body">
                                        <form class="form-horizontal form-simple" method="POST" action="{{ url('AddGallery') }}" enctype="multipart/form-data">
                                            {{ csrf_field() }}
                                            <div class="form-body">
                                                <script type="text/javascript">
                                                    $(document).ready(function()
                                                    {
                                                        $('.multi-field-wrapper').each(function() {
                                                            var $wrapper = $('.multi-fields', this);
                                                            $(".add-field", $(this)).click(function(e) {
                                                                $('.multi-field:first-child', $wrapper).clone(true).appendTo($wrapper).find('input').val('').focus();
                                                            });
                                                            $('.multi-field .remove-field', $wrapper).click(function() {
                                                                if ($('.multi-field', $wrapper).length > 1)
                                                                    $(this).parent('.multi-field').remove();
                                                            });
                                                        });
                                                    });
                                                </script>
                                                <div class="multi-field-wrapper">
                                                    <div class="multi-fields">
                                                        <div class="row  multi-field">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="projectinput1">Gallery Image</label>
                                                                    <input type="file" id="projectinput1" class="form-control"
                                                                           name="gallery[]">
                                                                </div>
                                                            </div>
                                                                <button type="button" class="remove-field btn btn-dark" style="line-height: inherit !important;"><i class="fas fa-times-circle"></i></button>
                                                                <button type="button" class="add-field btn btn-dark" style="line-height: inherit !important;"><i class="fas fa-plus-circle"></i></button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <button type="submit" class="btn btn-primary"> <i class="la la-check-square-o"></i> Save</button>

                                            </div>

                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            <section id="form-control-repeater">
                <div class="row">
                    <div class="col-12">
                        <div class="card">

                            <div class="card-content collapse show">
                                <div class="card-body card-dashboard">
                                    <table class="table table-striped table-bordered dataex-html5-export table-responsive">
                                        <thead>
                                        <tr>
                                            <th>Image</th>
                                            <th>Date Created</th>
                                            <th>Delete</th>

                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($listgallery as $data)
                                            <tr>
                                                <td><img src="GalleryPictures/{{$data->galleryname}}" style="width:100%"></td>
                                                <td>{{$data->created_at}}</td>
                                                <td><a href="{{ route('backend.DeleteGallery',['id'=> $data->id])}}" class="btn btn-icon btn-outline-primary">Delete</a></td>
                                            </tr>
                                        @endforeach
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
                    <!-- Summernote Click to edit end -->
                    <script src="backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
                    <script src="backend/app-assets/vendors/js/forms/repeater/jquery.repeater.min.js"></script>
                    <script src="backend/app-assets/vendors/js/editors/summernote/summernote.js" type="text/javascript"></script>
                    <script src="backend/app-assets/js/scripts/editors/editor-summernote.min.js" type="text/javascript"></script>
                    <script src="backend/app-assets/js/scripts/forms/form-repeater.js"></script>


        </div>
    </div>

@endsection
