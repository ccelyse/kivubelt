@extends('layouts.newmaster')

@section('title', 'Kivu Belt')

@section('content')
    <style>
        .MsoNormal{
            color:#fff !important;
        }
        .MsoNormal span{
            font-size: 16px !important;
             font-family: Cabin,sans-serif !important;
        }
        .tp-static-layers{
            top: 100px !important;
        }
        .tp-parallax-wrap{
            top: 150px !important;
        }
        .owl-item{
            width: 50%;
            margin-right: 30px;
        }
        .vc_custom_1521449273073{
            padding-top: 50px !important;
            padding-bottom: 0px !important;
        }
        .mkdf-video-button-holder .mkdf-video-button-text-wrapper-inner .mkdf-vb-subtitle {
            line-height: 1em;
            font-size: 50px;
            font-family: Kristi;
            color: #ffcc05;
            position: relative !important;
            bottom: 50px !important;
        }
    </style>
<div class="mkdf-wrapper">
    <div class="mkdf-wrapper-inner">
        @include('layouts.newtopmenu')
        <div class="mkdf-content" style="margin-top: -80px">
            <div class="mkdf-content-inner">
                <div class="mkdf-full-width">
                    <div class="mkdf-full-width-inner">
                        <div class="mkdf-grid-row">
                            <div class="mkdf-page-content-holder mkdf-grid-col-12">
                                <div class="vc_row wpb_row vc_row-fluid" ><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner "><div class="wpb_wrapper"><link href="https://fonts.googleapis.com/css?family=Kristi:400%7CPlayfair+Display:700%7CCabin:400" rel="stylesheet" property="stylesheet" type="text/css" media="all">
                                                <div id="rev_slider_1_2_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-source="gallery" style="margin:0px auto;background:#333333;padding:0px;margin-top:0px;margin-bottom:0px;">
                                                    <!-- START REVOLUTION SLIDER 5.4.7.2 fullwidth mode -->
                                                    <div id="rev_slider_1_2" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.4.7.2">
                                                        <ul>	<!-- SLIDE  -->
                                                            <li data-index="rs-4" data-transition="crossfade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="300"  data-thumb=""  data-delay="2340"  data-rotate="0"  data-fstransition="fade" data-fsmasterspeed="500" data-fsslotamount="7" data-saveperformance="off"  data-title="First" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                                                                <!-- MAIN IMAGE -->
                                                                <img src="frontend/assets/images/kayaka12.jpeg"  alt="a" title="slider-1-background-3"  width="1920" height="950" data-bgposition="right center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="8" class="rev-slidebg" data-no-retina>
                                                                <!-- LAYERS -->

                                                                <!-- LAYER NR. 1 -->
                                                                <div class="tp-caption  "
                                                                     id="slide-4-layer-5"
                                                                     data-x="['center','center','center','center']" data-hoffset="['-216','-171','-52','416']"
                                                                     data-y="['middle','middle','middle','middle']" data-voffset="['-130','-121','-242','-142']"
                                                                     data-width="none"
                                                                     data-height="none"
                                                                     data-whitespace="nowrap"
                                                                     data-visibility="['on','on','on','off']"

                                                                     data-type="image"
                                                                     data-basealign="slide"
                                                                     data-responsive_offset="off"
                                                                     data-responsive="off"
                                                                     data-frames='[{"delay":400,"speed":800,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Elastic.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                                                                     data-textAlign="['inherit','inherit','inherit','inherit']"
                                                                     data-paddingtop="[0,0,0,0]"
                                                                     data-paddingright="[0,0,0,0]"
                                                                     data-paddingbottom="[0,0,0,0]"
                                                                     data-paddingleft="[0,0,0,0]"

                                                                     style="z-index: 5;top: 54px;"><img src="newfrontend/wp-content/uploads/2018/02/slider-1-img-1.png" alt="a" data-ww="['126px','90px','90px','90px']" data-hh="['46px','33px','33px','33px']" width="126" height="46" data-no-retina> </div>

                                                                <!-- LAYER NR. 2 -->
                                                                <div class="tp-caption  "
                                                                     id="slide-4-layer-6"
                                                                     data-x="['center','center','center','center']" data-hoffset="['-199','-171','-47','0']"
                                                                     data-y="['middle','middle','middle','middle']" data-voffset="['-177','-157','-282','-300']"
                                                                     data-fontsize="['70','47','47','47']"
                                                                     data-width="none"
                                                                     data-height="none"
                                                                     data-whitespace="nowrap"

                                                                     data-type="text"
                                                                     data-responsive_offset="off"
                                                                     data-responsive="off"
                                                                     data-frames='[{"delay":500,"speed":800,"frame":"0","from":"x:20px;y:-30px;skX:20px;opacity:0;","to":"o:1;","ease":"Elastic.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                                                                     data-textAlign="['inherit','inherit','inherit','inherit']"
                                                                     data-paddingtop="[0,0,0,0]"
                                                                     data-paddingright="[0,0,0,0]"
                                                                     data-paddingbottom="[0,0,0,0]"
                                                                     data-paddingleft="[0,0,0,0]"

                                                                     style="z-index: 6; white-space: nowrap; font-size: 70px; line-height: 80px; font-weight: 400; color: #ffcc05; letter-spacing: 0px;font-family:Kristi;top: 54px;">exciting, life-changing, unique  </div>
                                                            </li>

                                                            <li data-index="rs-1" data-transition="crossfade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="800"  data-thumb=""  data-delay="3500"  data-rotate="0"  data-saveperformance="off"  data-title="Second" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                                                                <!-- MAIN IMAGE -->
                                                                <img src="frontend/assets/images/island.jpeg"  alt="a" title="coming-soon-background-img"  width="1920" height="1100" data-bgposition="left center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="8" class="rev-slidebg" data-no-retina>
                                                                <!-- LAYERS -->

                                                                <!-- LAYER NR. 5 -->
                                                                <div class="tp-caption  "
                                                                     id="slide-1-layer-5"
                                                                     data-x="['center','center','center','center']" data-hoffset="['-216','-171','-52','416']"
                                                                     data-y="['middle','middle','middle','middle']" data-voffset="['-130','-121','-242','-142']"
                                                                     data-width="none"
                                                                     data-height="none"
                                                                     data-whitespace="nowrap"
                                                                     data-visibility="['on','on','on','off']"

                                                                     data-type="image"
                                                                     data-basealign="slide"
                                                                     data-responsive_offset="off"
                                                                     data-responsive="off"
                                                                     data-frames='[{"delay":400,"speed":800,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Elastic.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                                                                     data-textAlign="['inherit','inherit','inherit','inherit']"
                                                                     data-paddingtop="[0,0,0,0]"
                                                                     data-paddingright="[0,0,0,0]"
                                                                     data-paddingbottom="[0,0,0,0]"
                                                                     data-paddingleft="[0,0,0,0]"

                                                                     style="z-index: 5;top: 54px;"><img src="newfrontend/wp-content/uploads/2018/02/slider-1-img-1.png" alt="a" data-ww="['126px','90px','90px','90px']" data-hh="['46px','33px','33px','33px']" width="126" height="46" data-no-retina> </div>

                                                                <!-- LAYER NR. 6 -->
                                                                <div class="tp-caption  "
                                                                     id="slide-1-layer-6"
                                                                     data-x="['center','center','center','center']" data-hoffset="['-211','-171','-47','0']"
                                                                     data-y="['middle','middle','middle','middle']" data-voffset="['-179','-157','-282','-300']"
                                                                     data-fontsize="['70','47','47','47']"
                                                                     data-width="none"
                                                                     data-height="none"
                                                                     data-whitespace="nowrap"

                                                                     data-type="text"
                                                                     data-responsive_offset="off"
                                                                     data-responsive="off"
                                                                     data-frames='[{"delay":500,"speed":800,"frame":"0","from":"x:20px;y:-30px;skX:20px;opacity:0;","to":"o:1;","ease":"Elastic.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                                                                     data-textAlign="['inherit','inherit','inherit','inherit']"
                                                                     data-paddingtop="[0,0,0,0]"
                                                                     data-paddingright="[0,0,0,0]"
                                                                     data-paddingbottom="[0,0,0,0]"
                                                                     data-paddingleft="[0,0,0,0]"

                                                                     style="z-index: 6; white-space: nowrap; font-size: 70px; line-height: 80px; font-weight: 400; color: #ffcc05; letter-spacing: 0px;font-family:Kristi;top: 54px;">exciting, life-changing, unique  </div>
                                                            </li>
                                                            <li data-index="rs-5" data-transition="crossfade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="800"  data-thumb=""  data-delay="3500"  data-rotate="0"  data-saveperformance="off"  data-title="Second" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                                                                <!-- MAIN IMAGE -->
                                                                <img src="frontend/assets/images/kak.jpeg"  alt="a" title="coming-soon-background-img"  width="1920" height="1100" data-bgposition="left center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="8" class="rev-slidebg" data-no-retina>
                                                                <!-- LAYERS -->

                                                                <!-- LAYER NR. 5 -->
                                                                <div class="tp-caption  "
                                                                     id="slide-1-layer-5"
                                                                     data-x="['center','center','center','center']" data-hoffset="['-216','-171','-52','416']"
                                                                     data-y="['middle','middle','middle','middle']" data-voffset="['-130','-121','-242','-142']"
                                                                     data-width="none"
                                                                     data-height="none"
                                                                     data-whitespace="nowrap"
                                                                     data-visibility="['on','on','on','off']"

                                                                     data-type="image"
                                                                     data-basealign="slide"
                                                                     data-responsive_offset="off"
                                                                     data-responsive="off"
                                                                     data-frames='[{"delay":400,"speed":800,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Elastic.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                                                                     data-textAlign="['inherit','inherit','inherit','inherit']"
                                                                     data-paddingtop="[0,0,0,0]"
                                                                     data-paddingright="[0,0,0,0]"
                                                                     data-paddingbottom="[0,0,0,0]"
                                                                     data-paddingleft="[0,0,0,0]"

                                                                     style="z-index: 5;top: 54px;"><img src="newfrontend/wp-content/uploads/2018/02/slider-1-img-1.png" alt="a" data-ww="['126px','90px','90px','90px']" data-hh="['46px','33px','33px','33px']" width="126" height="46" data-no-retina> </div>

                                                                <!-- LAYER NR. 6 -->
                                                                <div class="tp-caption  "
                                                                     id="slide-1-layer-6"
                                                                     data-x="['center','center','center','center']" data-hoffset="['-211','-171','-47','0']"
                                                                     data-y="['middle','middle','middle','middle']" data-voffset="['-179','-157','-282','-300']"
                                                                     data-fontsize="['70','47','47','47']"
                                                                     data-width="none"
                                                                     data-height="none"
                                                                     data-whitespace="nowrap"

                                                                     data-type="text"
                                                                     data-responsive_offset="off"
                                                                     data-responsive="off"
                                                                     data-frames='[{"delay":500,"speed":800,"frame":"0","from":"x:20px;y:-30px;skX:20px;opacity:0;","to":"o:1;","ease":"Elastic.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                                                                     data-textAlign="['inherit','inherit','inherit','inherit']"
                                                                     data-paddingtop="[0,0,0,0]"
                                                                     data-paddingright="[0,0,0,0]"
                                                                     data-paddingbottom="[0,0,0,0]"
                                                                     data-paddingleft="[0,0,0,0]"

                                                                     style="z-index: 6; white-space: nowrap; font-size: 70px; line-height: 80px; font-weight: 400; color: #ffcc05; letter-spacing: 0px;font-family:Kristi;top: 54px;">exciting, life-changing, unique  </div>
                                                            </li>
                                                        </ul>
                                                        <div style="" class="tp-static-layers">

                                                            <!-- LAYER NR. 7 -->
                                                            <div class="tp-caption   tp-static-layer"
                                                                 id="slider-1-layer-11"
                                                                 data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                                                                 data-y="['middle','middle','middle','middle']" data-voffset="['-92','-92','-177','-192']"
                                                                 data-fontsize="['90','70','70','50']"
                                                                 data-lineheight="['96','96','80','60']"
                                                                 data-width="['none','none','463','274']"
                                                                 data-height="none"
                                                                 data-whitespace="['nowrap','nowrap','normal','normal']"

                                                                 data-type="text"
                                                                 data-basealign="slide"
                                                                 data-responsive_offset="off"
                                                                 data-responsive="off"
                                                                 data-startslide="0"
                                                                 data-endslide="2"
                                                                 data-frames='[{"delay":150,"speed":500,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power2.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                                                                 data-textAlign="['center','center','center','center']"
                                                                 data-paddingtop="[0,0,0,0]"
                                                                 data-paddingright="[0,0,0,0]"
                                                                 data-paddingbottom="[0,0,0,0]"
                                                                 data-paddingleft="[0,0,0,0]"

                                                                 style="z-index: 11; white-space: nowrap; font-size: 90px; line-height: 96px; font-weight: 700; color: #ffffff; letter-spacing: 0px;font-family:Playfair Display;">Your Adventure Begins </div>

                                                            <!-- LAYER NR. 8 -->


                                                            <!-- LAYER NR. 9 -->
                                                            <div class="tp-caption   tp-static-layer"
                                                                 id="slider-1-layer-15"
                                                                 data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                                                                 data-y="['middle','middle','middle','bottom']" data-voffset="['225','159','205','0']"
                                                                 data-width="['1300','900','620','100%']"
                                                                 data-height="['100','100','none','none']"
                                                                 data-whitespace="['nowrap','nowrap','normal','normal']"

                                                                 data-type="text"
                                                                 data-basealign="slide"
                                                                 data-responsive_offset="off"
                                                                 data-responsive="off"
                                                                 data-startslide="0"
                                                                 data-endslide="2"
                                                                 data-frames='[{"delay":450,"speed":500,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power2.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                                                                 data-textAlign="['inherit','inherit','inherit','inherit']"
                                                                 data-paddingtop="[0,0,0,0]"
                                                                 data-paddingright="[0,0,0,0]"
                                                                 data-paddingbottom="[0,0,0,0]"
                                                                 data-paddingleft="[0,0,0,0]"

                                                                 style="z-index: 13; min-width: 1300px; max-width: 1300px; max-width: 100px; max-width: 100px; white-space: nowrap; font-size: 20px; line-height: 22px; font-weight: 400; color: #ffffff; letter-spacing: 0px;font-family:Cabin;"><div class="mkdf-tours-filter-holder mkdf-tours-filter-horizontal mkdf-tours-filter-skin-light">

                                                                </div> </div>
                                                        </div>
                                                        <script>var htmlDiv = document.getElementById("rs-plugin-settings-inline-css"); var htmlDivCss="";
                                                            if(htmlDiv) {
                                                                htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
                                                            }else{
                                                                var htmlDiv = document.createElement("div");
                                                                htmlDiv.innerHTML = "<style>" + htmlDivCss + "</style>";
                                                                document.getElementsByTagName("head")[0].appendChild(htmlDiv.childNodes[0]);
                                                            }
                                                        </script>
                                                        <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>	</div>
                                                    <script>var htmlDiv = document.getElementById("rs-plugin-settings-inline-css"); var htmlDivCss="";
                                                        if(htmlDiv) {
                                                            htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
                                                        }else{
                                                            var htmlDiv = document.createElement("div");
                                                            htmlDiv.innerHTML = "<style>" + htmlDivCss + "</style>";
                                                            document.getElementsByTagName("head")[0].appendChild(htmlDiv.childNodes[0]);
                                                        }
                                                    </script>
                                                    <script type="text/javascript">
                                                        if (setREVStartSize!==undefined) setREVStartSize(
                                                            {c: '#rev_slider_1_2', responsiveLevels: [1920,1720,950,480], gridwidth: [1300,1150,600,300], gridheight: [785,607,890,780], sliderLayout: 'fullwidth'});

                                                        var revapi1,
                                                            tpj;
                                                        (function() {
                                                            if (!/loaded|interactive|complete/.test(document.readyState)) document.addEventListener("DOMContentLoaded",onLoad); else onLoad();
                                                            function onLoad() {
                                                                if (tpj===undefined) { tpj = jQuery; if("off" == "on") tpj.noConflict();}
                                                                if(tpj("#rev_slider_1_2").revolution == undefined){
                                                                    revslider_showDoubleJqueryError("#rev_slider_1_2");
                                                                }else{
                                                                    revapi1 = tpj("#rev_slider_1_2").show().revolution({
                                                                        sliderType:"standard",
                                                                        jsFileLocation:"newfrontend/wp-content/plugins/revslider/public/assets/js/",
                                                                        sliderLayout:"fullwidth",
                                                                        dottedOverlay:"none",
                                                                        delay:3500,
                                                                        navigation: {
                                                                            keyboardNavigation:"off",
                                                                            keyboard_direction: "horizontal",
                                                                            mouseScrollNavigation:"off",
                                                                            mouseScrollReverse:"default",
                                                                            onHoverStop:"off",
                                                                            arrows: {
                                                                                style:"wanderers",
                                                                                enable:true,
                                                                                hide_onmobile:true,
                                                                                hide_under:1051,
                                                                                hide_onleave:false,
                                                                                tmp:'',
                                                                                left: {
                                                                                    h_align:"left",
                                                                                    v_align:"center",
                                                                                    h_offset:40,
                                                                                    v_offset:0
                                                                                },
                                                                                right: {
                                                                                    h_align:"right",
                                                                                    v_align:"center",
                                                                                    h_offset:43,
                                                                                    v_offset:0
                                                                                }
                                                                            }
                                                                            ,
                                                                            bullets: {
                                                                                enable:true,
                                                                                hide_onmobile:true,
                                                                                hide_under:600,
                                                                                hide_over:1050,
                                                                                style:"wanderers",
                                                                                hide_onleave:false,
                                                                                direction:"horizontal",
                                                                                h_align:"center",
                                                                                v_align:"bottom",
                                                                                h_offset:0,
                                                                                v_offset:25,
                                                                                space:5,
                                                                                tmp:''
                                                                            }
                                                                        },
                                                                        responsiveLevels:[1920,1720,950,480],
                                                                        visibilityLevels:[1920,1720,950,480],
                                                                        gridwidth:[1300,1150,600,300],
                                                                        gridheight:[785,607,890,780],
                                                                        lazyType:"none",
                                                                        parallax: {
                                                                            type:"mouse",
                                                                            origo:"enterpoint",
                                                                            speed:400,
                                                                            speedbg:0,
                                                                            speedls:0,
                                                                            levels:[5,10,15,20,25,30,35,40,45,46,47,48,49,50,51,55],
                                                                        },
                                                                        shadow:0,
                                                                        spinner:"off",
                                                                        stopLoop:"off",
                                                                        stopAfterLoops:-1,
                                                                        stopAtSlide:-1,
                                                                        shuffle:"off",
                                                                        autoHeight:"off",
                                                                        disableProgressBar:"on",
                                                                        hideThumbsOnMobile:"off",
                                                                        hideSliderAtLimit:0,
                                                                        hideCaptionAtLimit:0,
                                                                        hideAllCaptionAtLilmit:0,
                                                                        debugMode:false,
                                                                        fallbacks: {
                                                                            simplifyAll:"off",
                                                                            nextSlideOnWindowFocus:"off",
                                                                            disableFocusListener:false,
                                                                        }
                                                                    });
                                                                }; /* END OF revapi call */

                                                            }; /* END OF ON LOAD FUNCTION */
                                                        }()); /* END OF WRAPPING FUNCTION */
                                                    </script>
                                                    <script>
                                                        var htmlDivCss = unescape("%23rev_slider_1_2%20.wanderers.tparrows%20%7B%0A%09cursor%3Apointer%3B%0A%20%20%20%20background-color%3A%20transparent%3B%0A%09width%3A60px%3B%0A%09height%3A60px%3B%0A%09position%3Aabsolute%3B%0A%09display%3Ablock%3B%0A%09z-index%3A100%3B%0A%20%20%20%0A%7D%0A%0A%23rev_slider_1_2%20.wanderers.tparrows%3Abefore%20%7B%0A%09font-family%3A%20%22ElegantIcons%22%3B%0A%09font-size%3A60px%3B%0A%09color%3Argb%28255%2C%20255%2C%20255%29%3B%0A%09display%3Ablock%3B%0A%09line-height%3A%2060px%3B%0A%09text-align%3A%20center%3B%0A%20%20%20%20-webkit-transition%3A%20color%20.175s%20cubic-bezier%280.18%2C%200.43%2C%200.58%2C%201%29%3B%0A%20%20%09-moz-transition%3A%20color%20.175s%20cubic-bezier%280.18%2C%200.43%2C%200.58%2C%201%29%3B%0A%20%20%20%20transition%3A%20color%20.175s%20cubic-bezier%280.18%2C%200.43%2C%200.58%2C%201%29%3B%0A%7D%0A%0A%23rev_slider_1_2%20.wanderers.tparrows%3Ahover%3Abefore%20%7B%0A%20%20%20%20color%3A%20rgb%28255%2C%20204%2C%205%29%3B%0A%7D%0A%0A%23rev_slider_1_2%20.wanderers.tparrows.tp-leftarrow%3Abefore%20%7B%0A%09content%3A%20%22%5C34%22%3B%0A%20%20%20%20margin-left%3A-3px%3B%0A%7D%0A%23rev_slider_1_2%20.wanderers.tparrows.tp-rightarrow%3Abefore%20%7B%0A%09content%3A%20%22%5C35%22%3B%0A%20%20%20%20margin-right%3A-3px%3B%0A%7D%0A%23rev_slider_1_2%20.wanderers.tp-bullets%20%7B%0A%7D%0A%0A%23rev_slider_1_2%20.wanderers%20.tp-bullet%20%7B%0A%09width%3A8px%3B%0A%09height%3A8px%3B%0A%09position%3Aabsolute%3B%0A%09border-radius%3A50%25%3B%0A%09cursor%3A%20pointer%3B%0A%20%20%20%20background-color%3A%20rgb%28183%2C%20183%2C%20183%29%3B%0A%20%20%20%20box-sizing%3A%20border-box%3B%0A%20%20%20%20-webkit-transition%3A%20background-color%20175ms%20cubic-bezier%28.18%2C.43%2C.58%2C1%29%2C%20border%20175ms%20cubic-bezier%28.18%2C.43%2C.58%2C1%29%3B%0A%20%20%20%20-moz-transition%3A%20background-color%20175ms%20cubic-bezier%28.18%2C.43%2C.58%2C1%29%2C%20border%20175ms%20cubic-bezier%28.18%2C.43%2C.58%2C1%29%3B%0A%20%20%20%20transition%3A%20background-color%20175ms%20cubic-bezier%28.18%2C.43%2C.58%2C1%29%2C%20border%20175ms%20cubic-bezier%28.18%2C.43%2C.58%2C1%29%3B%0A%7D%0A%0A%23rev_slider_1_2%20.wanderers%20.tp-bullet%3Ahover%2C%0A%23rev_slider_1_2%20.wanderers%20.tp-bullet.selected%20%7B%0A%20%20%20%20background-color%3A%20rgb%28255%2C%20204%2C%200%29%3B%0A%7D%0A");
                                                        var htmlDiv = document.getElementById('rs-plugin-settings-inline-css');
                                                        if(htmlDiv) {
                                                            htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
                                                        }
                                                        else{
                                                            var htmlDiv = document.createElement('div');
                                                            htmlDiv.innerHTML = '<style>' + htmlDivCss + '</style>';
                                                            document.getElementsByTagName('head')[0].appendChild(htmlDiv.childNodes[0]);
                                                        }
                                                    </script>
                                                </div><!-- END REVOLUTION SLIDER -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="vc_row wpb_row vc_row-fluid vc_custom_1521446912670 vc_row-has-fill" >
                                    <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-has-fill">
                                        <div class="vc_column-inner vc_custom_1521446892225">
                                            <div class="wpb_wrapper">
                                                @foreach($listhomerow1 as $listhomerow1s)
                                                <div class="mkdf-row-grid-section-wrapper " style="padding-top: 0px !important;background-image: url('Homerow1/{{$listhomerow1s->row_cover_pic}}') !important;background-position: center !important; background-repeat: inherit !important;background-size: cover !important;">
                                                    <div class="mkdf-row-grid-section">
                                                        <div class="vc_row wpb_row vc_inner vc_row-fluid vc_custom_1518168566331 vc_row-o-equal-height vc_row-o-content-middle vc_row-flex" >
                                                            <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-7 vc_col-md-12">
                                                                <div class="vc_column-inner vc_custom_1518169901755">
                                                                    <div class="wpb_wrapper">
                                                                        <div class="mkdf-elements-holder   mkdf-one-column  mkdf-responsive-mode-768 " >
                                                                            <div class="mkdf-eh-item    "  data-item-class="mkdf-eh-custom-3378" data-768-1024="0px" data-680-768="0px" data-680="0px">
                                                                                <div class="mkdf-eh-item-inner">
                                                                                    <div class="mkdf-eh-item-content mkdf-eh-custom-3378" style="padding: 0px 15px 0px 0px ">
                                                                                        <div class="mkdf-video-button-holder  mkdf-vb-has-img ">
                                                                                            <div class="mkdf-video-button-image">
                                                                                                <img width="900" height="623" src="frontend/assets/images/videoscre.png" class="attachment-full size-full" alt="a" srcset="frontend/assets/images/videoscre.png" sizes="(max-width: 900px) 100vw, 900px" />	</div>
                                                                                            <div class="mkdf-video-button-text-holder">
                                                                                                <div class="mkdf-video-button-text-wrapper">
                                                                                                    <div class="mkdf-video-button-text-wrapper-inner">
                                                                                                        <div class="mkdf-vb-subtitle"> Kivu Belt </div>
                                                                                                        {{--<h3 class="mkdf-vb-title"> Watch Our Video </h3>--}}
                                                                                                        <p class="mkdf-vb-excerpt"> </p>
                                                                                                        <a class="mkdf-video-button-play"  href="https://youtu.be/r9WMKteGZt0" target="_self" data-rel="prettyPhoto[video_button_pretty_photo_307]">
                                                                                                        <span class="mkdf-video-button-play-inner">
                                                                                                            <span class="arrow_triangle-right_alt"></span>
                                                                                                        </span>
                                                                                                        </a>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>		</div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="vc_empty_space"   style="height: 30px" >
                                                                            <span class="vc_empty_space_inner">

                                                                            </span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-5 vc_col-md-12"><div class="vc_column-inner vc_custom_1518169907323"><div class="wpb_wrapper"><div class="mkdf-elements-holder   mkdf-one-column  mkdf-responsive-mode-768 " ><div class="mkdf-eh-item    "  data-item-class="mkdf-eh-custom-1078" data-1280-1600="0px" data-1024-1280="0px" data-768-1024="0px" data-680-768="0px" data-680="0px">
                                                                                <div class="mkdf-eh-item-inner">
                                                                                    <div class="mkdf-eh-item-content mkdf-eh-custom-1078" style="padding: 0px 50px 0px 0px">
                                                                                        <div class="mkdf-section-title-holder  mkdf-st-standard mkdf-st-title-left mkdf-st-normal-space " style="padding: 0 0%;text-align: left">

                                                                                            <div class="mkdf-st-inner">
                                                                                                <h2 class="mkdf-st-title" style="color: #fff !important;">
                                                                                                    {{$listhomerow1s->row_title}}</h2>
                                                                                                <p class="mkdf-st-text" style="color: #fff;font-size: 19px;line-height: 30px;margin-top: 25px">
                                                                                                    <?php
                                                                                                        $text = $listhomerow1s->row_text;
                                                                                                        echo $text;
                                                                                                    ?>
                                                                                                </p>
                                                                                            </div>
                                                                                        </div><div class="vc_empty_space"   style="height: 37px" ><span class="vc_empty_space_inner"></span></div>
                                                                                        {{--<a itemprop="url" href="" target="_self"  class="mkdf-btn mkdf-btn-medium mkdf-btn-solid"  >--}}
                                                                                            {{--<span class="mkdf-btn-text">See More</span>--}}
                                                                                        {{--</a>--}}
                                                                                        <div class="vc_empty_space"   style="height: 54px" ><span class="vc_empty_space_inner"></span></div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="mkdf-row-grid-section-wrapper " style="padding-top: 0px !important;background-image: url(../../../frontend/assets/images/NH_OO_Activities_Chimpanzee_Trek_6571_MASTER-compressed.jpg) !important;background-position: center !important; background-repeat: inherit !important;background-size: contain !important;">
                                                    <div class="mkdf-row-grid-section">
                                                        <div class="vc_row wpb_row vc_inner vc_row-fluid vc_custom_1521446919727" >
                                                            <div class="wpb_column vc_column_container vc_col-sm-12">
                                                                <div class="vc_column-inner vc_custom_1518169893412">
                                                                    <div class="wpb_wrapper">
                                                                        <div class="mkdf-tours-carousel-holder mkdf-carousel-pagination">
                                                                            <div class="mkdf-tours-row mkdf-normal-space">
                                                                                <div class="mkdf-st-inner">
                                                                                    <h2 class="mkdf-st-title" style="color:#fff;text-align: center;padding-bottom: 15px"> DISCOVER THE KIVU BELT </h2>
                                                                                </div>
                                                                                <div class="mkdf-tours-carousel mkdf-tours-row-inner-holder mkdf-owl-slider" data-number-of-items="3" data-enable-loop="yes" data-enable-navigation="no" data-enable-pagination="yes">

                                                                                    @foreach($listthings as $discover)
                                                                                    <div class="mkdf-tours-gallery-item mkdf-tours-row-item mkdf-item-space mkdf-tour-item-has-rating post-57 tour-item type-tour-item status-publish has-post-thumbnail hentry tour-category-seaside tour-attribute-accommodation tour-attribute-guide tour-attribute-insurance tour-attribute-meals">
                                                                                        <div class="mkdf-tours-gallery-item-image-holder">
                                                                                            <div class="mkdf-tours-gallery-item-image">
                                                                                                <img width="800" height="932" src="Placecoverimage/{{$discover->coverimage}}" class="attachment-full size-full" alt="a" srcset="Placecoverimage/{{$discover->coverimage}}" />
                                                                                                <div class="mkdf-tours-gallery-item-content-holder">
                                                                                                    <div class="mkdf-tours-gallery-item-content-inner">
                                                                                                        <div class="mkdf-tours-gallery-main-info">
                                                                                                            <span class="mkdf-tours-gallery-item-label-holder">

                                                                                                                <span class="mkdf-tour-item-label">
                                                                                                                    <span class="mkdf-tour-item-label-inner">{{$discover->discovercaption}}</span>
                                                                                                                </span>

                                                                                                            </span>
                                                                                                            <div class="mkdf-tours-gallery-title-holder">
                                                                                                                <h3 class="mkdf-tour-title">
                                                                                                                    {{$discover->placetitle}}                           </h3>
                                                                                                            </div>
                                                                                                            {{--<div class="mkdf-tours-gallery-item-excerpt">--}}
                                                                                                                {{--<div class="mkdf-tours-gallery-item-excerpt-inner">--}}
                                                                                                                    {{--Lorem ipsum dolor sit amet, conse&hellip;--}}
                                                                                                                {{--</div>--}}
                                                                                                            {{--</div>--}}
                                                                                                        </div>
                                                                                                        {{--<span class="mkdf-tours-gallery-item-price-holder">--}}
                                                                                                            {{--<span class="mkdf-tours-price-holder">--}}
                                                                                                            {{--<span class="mkdf-tours-item-price ">$1350</span>--}}
                                                                                                                {{--</span>--}}

                                                                                                        {{--</span>--}}
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                            <a class="mkdf-tours-gallery-item-link" href="{{ route('MoreDiscover',['id'=> $discover->id])}}"></a>
                                                                                        </div>
                                                                                    </div>
                                                                                    @endforeach

                                                                                    </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                @endforeach

                                                    <div class="vc_row wpb_row vc_row-fluid vc_row-o-equal-height vc_row-o-content-middle vc_row-flex mkdf-content-aligment-center" style="background-color:#1c1c1c">

                                                        @foreach($listhomerow2 as $listhomerow2s)
                                                        <div class="mkdf-col-animated-bg wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12 vc_col-has-fill">
                                                            <div class="vc_column-inner vc_custom_1521806228644" style="    background-image: none !important;">
                                                                <div class="wpb_wrapper">
                                                                    <div class="mkdf-elements-holder   mkdf-one-column  mkdf-responsive-mode-768 " >
                                                                        <div class="mkdf-eh-item   mkdf-horizontal-alignment-center "  data-item-class="mkdf-eh-custom-5723" data-1280-1600="130px 15%" data-1024-1280="130px 10%" data-768-1024="90px 10%" data-680-768="90px 10%" data-680="90px 20px">
                                                                            <div class="mkdf-eh-item-inner">
                                                                                <div class="mkdf-eh-item-content mkdf-eh-custom-5723" style="padding: 140px 23%">
                                                                                    <h1 class="mkdf-custom-font-holder  mkdf-cf-9084  " style="font-family: Kristi;line-height: 1em;font-weight: 400;color: #ffcc05" data-item-class="mkdf-cf-9084">
                                                                                        Rwanda</h1><div class="vc_empty_space"   style="height: 18px" ><span class="vc_empty_space_inner"></span></div>

                                                                                    <div class="wpb_text_column wpb_content_element  vc_custom_1522058628416" >
                                                                                        <div class="wpb_wrapper">
                                                                                            <h2><span style="color: #ffffff;">{{$listhomerow2s->row_title}} </span></h2>

                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="vc_empty_space"   style="height: 15px" ><span class="vc_empty_space_inner"></span></div>

                                                                                    <div class="wpb_text_column wpb_content_element " >
                                                                                        <div class="wpb_wrapper">
                                                                                            <p><span style="color: #ffffff;">
                                                                                                    <?php
                                                                                                    $text = $listhomerow2s->row_text;
                                                                                                    echo $text;
                                                                                                    ?>
                                                                                                </span></p>

                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="vc_empty_space"   style="height: 27px" ><span class="vc_empty_space_inner"></span></div>
                                                                                    <a itemprop="url" href="{{'AboutRwanda'}}" target="_self"  class="mkdf-btn mkdf-btn-medium mkdf-btn-simple"  >
                                                                                        <span class="mkdf-btn-text">See more</span>
                                                                                    </a>
                                                                                    <div class="vc_empty_space"   style="height: 25px" ><span class="vc_empty_space_inner"></span></div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12 vc_col-has-fill">
                                                            <div class="vc_column-inner vc_custom_1518534932573" style="padding-top: 0px !important;background-image: url('Homerow2/{{$listhomerow2s->row_cover_pic}}') !important;background-position: center !important; background-repeat: inherit !important;background-size: cover !important;">
                                                                <div class="wpb_wrapper">
                                                                    <div class="mkdf-elements-holder   mkdf-one-column  mkdf-responsive-mode-768 " >
                                                                        <div class="mkdf-eh-item"  data-item-class="mkdf-eh-custom-3019" data-1280-1600="0px" data-1024-1280="0px" data-768-1024="300px" data-680-768="300px" data-680="200px">
                                                                            <div class="mkdf-eh-item-inner">
                                                                                <div class="mkdf-eh-item-content mkdf-eh-custom-3019" style="padding: 0px">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        @endforeach
                                                    </div>


                                                    <div class="mkdf-row-grid-section-wrapper" style="padding-top: 0px !important;background-image: url(../../../frontend/assets/images/37887793102_013b13cc30_k-compressed.jpg) !important;background-position: center !important; background-repeat: inherit !important;background-size: contain !important;">
                                                        <div class="mkdf-row-grid-section">
                                                            <div class="vc_row wpb_row vc_row-fluid vc_custom_1521449273073"  style="padding-top: 50px !important; padding-bottom: 0px !important;">
                                                                <div class="wpb_column vc_column_container vc_col-sm-12">
                                                                    <div class="vc_column-inner vc_custom_1518432408753">
                                                                        @foreach($listhomerow3 as $listhomerow3s)

                                                                            <div class="vc_row wpb_row vc_row-fluid vc_custom_1521556287933 vc_row-has-fill vc_row-o-equal-height vc_row-o-content-middle vc_row-flex mkdf-content-aligment-left" >
                                                                                <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12 vc_col-has-fill">
                                                                                    <div class="vc_column-inner vc_custom_1518432670305" style="background-image: url('Homerow3/{{$listhomerow3s->row_cover_pic}}') !important; background-size: cover;">
                                                                                        <div class="wpb_wrapper">
                                                                                            <div class="mkdf-elements-holder   mkdf-one-column  mkdf-responsive-mode-768 " >
                                                                                                <div class="mkdf-eh-item    "  data-item-class="mkdf-eh-custom-4325" data-1280-1600="0px 0px 0px 116px" data-1024-1280="0px 0px 0px 116px" data-768-1024="120px 0px 120px 211px " data-680-768="118px 0px 118px 152px " data-680="50px 0px 50px 33px">
                                                                                                    <div class="mkdf-eh-item-inner">
                                                                                                        <div class="mkdf-eh-item-content mkdf-eh-custom-4325" style="padding: 0px 0px 0px 245px">

                                                                                                            <div class="vc_empty_space"   style="height: 50px" ><span class="vc_empty_space_inner"></span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="mkdf-col-animated-bg wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12 vc_col-has-fill">
                                                                                    <div class="vc_column-inner vc_custom_1521556270901"><div class="wpb_wrapper">
                                                                                            <div class="mkdf-elements-holder   mkdf-one-column  mkdf-responsive-mode-768 " >
                                                                                                <div class="mkdf-eh-item    "  data-item-class="mkdf-eh-custom-8937" data-1280-1600=" 197px 15% 135px 80px" data-1024-1280="197px 12% 135px 70px" data-768-1024="107px 36% 107px 80px" data-680-768="107px 25% 107px 80px" data-680="107px 23px 107px">
                                                                                                    <div class="mkdf-eh-item-inner">
                                                                                                        <div class="mkdf-eh-item-content mkdf-eh-custom-8937" style="padding: 197px 35% 135px 80px">
                                                                                                            <div class="mkdf-section-title-holder  mkdf-st-standard mkdf-st-title-left mkdf-st-normal-space " style="padding: 0 0%;text-align: left">
                                                                                                                <div class="mkdf-st-inner">
                                                                                                                    <h2 class="mkdf-st-title" >
                                                                                                                        {{$listhomerow3s->row_title}} 		</h2>
                                                                                                                    <p class="mkdf-st-text" style="color: #818181;font-size: 16px;line-height: 30px;margin-top: 25px">
                                                                                                                        <?php
                                                                                                                        $text = $listhomerow3s->row_text;
                                                                                                                        echo $text;
                                                                                                                        ?>
                                                                                                                    </p>
                                                                                                                </div>
                                                                                                            </div><div class="vc_empty_space"   style="height: 26px" ><span class="vc_empty_space_inner"></span></div>
                                                                                                            <a itemprop="url" href="{{'AboutRegion'}}" target="_self"  class="mkdf-btn mkdf-btn-medium mkdf-btn-simple"  >
                                                                                                            <span class="mkdf-btn-text">See more</span>
                                                                                                            </a>
                                                                                                            <div class="vc_empty_space"   style="height: 28px" ><span class="vc_empty_space_inner"></span></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>

                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                @endforeach
                                                                            </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div> <!-- close div.content_inner -->

                                                    <div class="mkdf-row-grid-section-wrapper" style="padding-top: 0px !important;background: #fff;background-position: center !important; background-repeat: inherit !important;background-size: cover !important;">
                                                        <div class="mkdf-row-grid-section">
                                                            <div class="vc_row wpb_row vc_row-fluid vc_custom_1521449273073" >
                                                                <div class="wpb_column vc_column_container vc_col-sm-12">
                                                                    <div class="vc_column-inner vc_custom_1518432408753">
                                                                        <article class="mkdf-tour-item-wrapper">
                                                                            <div class="mkdf-tour-item-section mkdf-tab-container" id="tour-item-info-id">
                                                                                <div class="mkdf-tour-gallery-item-holder">

                                                                                    <h2 class="mkdf-st-title" style="text-align: center;">
                                                                                        Impressions
                                                                                    </h2>

                                                                                    <div class="mkdf-tour-gallery clearfix">
                                                                                        <?php
                                                                                        $insta_source = file_get_contents('https://www.instagram.com/kivubelt_/'); // instagrame tag url
                                                                                        $shards = explode('window._sharedData = ', $insta_source);
                                                                                        $insta_json = explode(';</script>', $shards[1]);
                                                                                        $insta_array = json_decode($insta_json[0]);
                                                                                        $instagramdata = $insta_array->entry_data->ProfilePage[0]->graphql->user->edge_owner_to_timeline_media->edges;
                                                                                        $secig = json_encode($instagramdata);
                                                                                        $limit = array_slice($instagramdata,0,5);

                                                                                        ?>
                                                                                        @foreach($limit as $instagramdatas)
                                                                                            <div class="mkdf-tour-gallery-item">
                                                                                                <a href="{{$instagramdatas->node->thumbnail_src}}" data-rel="prettyPhoto[gallery_excerpt_pretty_photo]">
                                                                                                    <img width="550" height="550" src="{{$instagramdatas->node->thumbnail_src}}" class="attachment-wanderers_mkdf_square size-wanderers_mkdf_square" alt="a" srcset="{{$instagramdatas->node->thumbnail_src}}" sizes="(max-width: 550px) 100vw, 550px" />                            </a>
                                                                                            </div>
                                                                                        @endforeach

                                                                                        <div style="text-align:center;padding-bottom:15px;">
                                                                                            <a itemprop="url" href="{{'Impressions'}}" target="_self" class="mkdf-btn mkdf-btn-medium mkdf-btn-solid">
                                                                                                <span class="mkdf-btn-text">See More</span>
                                                                                            </a>
                                                                                        </div>

                                                                                    </div>

                                                                                    {{--<a itemprop="url" href="#" target="_self" class="mkdf-btn mkdf-btn-medium mkdf-btn-solid mkdf-st-title">--}}
                                                                                    {{--<span class="mkdf-btn-text">See More</span>--}}
                                                                                    {{--</a>--}}


                                                                                </div>
                                                                            </div>
                                                                        </article>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div> <!-- close div.content_inner -->


                                                    <div class="mkdf-row-grid-section-wrapper" style="padding-top: 0px !important;background: #fff;background-position: center !important; background-repeat: inherit !important;background-size: cover !important;">
                                                        <div class="mkdf-row-grid-section">
                                                            <div class="vc_row wpb_row vc_row-fluid vc_custom_1521449273073" style="padding-top: 0px !important;padding-bottom: 0px !important;">
                                                                <div class="wpb_column vc_column_container vc_col-sm-12">
                                                                    <div class="vc_column-inner vc_custom_1518432408753" >
                                                                        <div class="mkdf-tours-carousel-holder mkdf-carousel-pagination">
                                                                            <div class="mkdf-tours-row mkdf-normal-space">
                                                                                <div class="mkdf-tours-carousel mkdf-tours-row-inner-holder mkdf-owl-slider" data-number-of-items="2" data-enable-loop="no" data-enable-navigation="no" data-enable-pagination="no">
                                                                                    <div class="mkdf-tours-standard-item mkdf-tours-row-item mkdf-item-space post-57 tour-item type-tour-item status-publish has-post-thumbnail hentry tour-category-seaside">
                                                                                        <div class="mkdf-tours-standard-item-image-holder">
                                                                                            <a href="#">
                                                                                                <img width="800" height="550" src="frontend/assets/images/13.jpg" class="attachment-wanderers_mkdf_landscape size-wanderers_mkdf_landscape" alt="a" />
                                                                                            </a>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="mkdf-tours-standard-item mkdf-tours-row-item mkdf-item-space post-1036 tour-item type-tour-item status-publish has-post-thumbnail hentry tour-category-adventure" style="float: right;">
                                                                                        <div class="mkdf-tours-standard-item-image-holder">
                                                                                            <a href="#">
                                                                                                <img width="800" height="550" src="frontend/assets/images/p3.jpg" class="attachment-wanderers_mkdf_landscape size-wanderers_mkdf_landscape" alt="a" />
                                                                                            </a>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div> <!-- close div.content_inner -->

                                                    {{--<div class="mkdf-grid-row-medium-gutter">--}}
                                                        {{--<div class="mkdf-grid-col-12">--}}
                                                            {{----}}
                                                        {{--</div>--}}
                                                    {{--</div>--}}

                                            </div>  <!-- close div.content -->

    </div> <!-- close div.mkdf-wrapper-inner  -->


</div> <!-- close div.mkdf-wrapper -->


@include('layouts.newfooter')
@endsection