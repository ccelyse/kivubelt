@extends('layouts.master')

@section('title', 'Kivu Belt')

@section('content')
<style>
    .page-banner--layout-1 {
        background-image: url(frontend/assets/images/lakekivu2.jpg);
        background-size:cover ;
    }
    .category--layout-2 .category {
        background-repeat: no-repeat;
        background-size: cover;
        background-position: center;
        padding: 70px;
    }
    .category__title {
        font-size: 1.9375rem !important;
        margin-bottom: 0;
    }
    .c-white {
        color: #fff !important;
    }
</style>
@include('layouts.topmenu')
<section class="page-banner page-banner--layout-1 parallax">
    <div class="container">
        <div class="page-banner__container animated fadeInUp">
            <div class="page-banner__textcontent t-center">
                <h2 class="page-banner__title c-white">Get in Touch</h2>
                {{--<p class="page-banner__subtitle c-white">Find the best places to stay, eat, drink, or visit.</p>--}}
            </div><!-- .page-banner__textcontent -->
        </div><!-- .page-banner__container -->
    </div><!-- .container -->
</section><!-- .page-banner -->
<section class="contact-container">
    <div class="container">
        {{--<h2 class="page-section__title">Get in Touch</h2>--}}
        {{--<p class="c-dove-gray">We're available from Monday to Friday, 07:30-19:00 to take your call</p>--}}

        <div class="contact-list">
            <div class="row">
                <div class="col-md-6 col-lg-3">
                    <div class="contact t-center">
                        <div class="contact-icon bg-primary">
                            <i class="fa fa-map-marker"></i>
                        </div>
                        <h3 class="contact-type">Contact Person</h3>
                        <p class="c-dove-gray">Henry Mugweri </p>
                        <p class="c-dove-gray">Destination Kivu Belt Coordinator</p>
                    </div><!-- .contact -->
                </div><!-- .col -->

                <div class="col-md-6 col-lg-3">
                    <div class="contact t-center">
                        <div class="contact-icon bg-primary">
                            <i class="fa fa-mobile"></i>
                        </div>
                        <h3 class="contact-type">Phone</h3>
                        <a href="tel:+841234567890" class="c-dove-gray">+250 785 257 998</a>
                    </div><!-- .contact -->
                </div><!-- .col -->

                <div class="col-md-6 col-lg-3">
                    <div class="contact t-center">
                        <div class="contact-icon bg-primary">
                            <i class="fa fa-envelope"></i>
                        </div>
                        <h3 class="contact-type">Mail</h3>
                        <a href="mailto:listiry@listiry.com" class="c-dove-gray">kivudmu@rwandatourismchamber.org</a>
                    </div><!-- .contact -->
                </div><!-- .col -->

                <div class="col-md-6 col-lg-3">
                    <div class="contact t-center">
                        <div class="contact-icon bg-primary">
                            <i class="fa fa-share-alt"></i>
                        </div>
                        <h3 class="contact-type">Social</h3>
                        <ul class="min-list inline-list social-list">
                            <li>
                                <a href="#">
                                    {{--<i class="fa fa-facebook-f"></i>--}}
                                    <i class="fab fa-facebook"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="fab fa-twitter"></i>
                                    {{--<i class="fa fa-twitter"></i>--}}
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="fab fa-instagram"></i>
                                    {{--<i class="fa fa-instagram"></i>--}}
                                </a>
                            </li>

                        </ul>
                    </div><!-- .contact -->
                </div><!-- .col -->
            </div><!-- .row -->
        </div><!-- .contact-list -->

        <div class="row" style="padding-bottom: 15px">
            <div class="col-md-6">
                <h2 class="page-section__title">Our Location</h2>
                <div class="contact-map-container">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d1017860.7216320136!2d29.342305182781942!3d-1.956551059578042!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2srw!4v1551807998039" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div><!-- .contact-map-container -->
            </div><!-- .col -->

            <div class="col-md-6">
                <h2 class="page-section__title">Contact Us</h2>
                <form action="#" method="post">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name">Your full name *</label>
                                <input
                                        type="text"
                                        id="name"
                                        name="name"
                                        class="form-input form-input--large form-input--border-c-alto"
                                        placeholder="John"
                                        required
                                >
                            </div><!-- .form-group -->
                        </div><!-- .col -->

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="email">Your email *</label>
                                <input
                                        type="email"
                                        id="email"
                                        name="email"
                                        class="form-input form-input--large form-input--border-c-alto"
                                        placeholder="john@gmail.com"
                                        required
                                >
                            </div><!-- .form-group -->
                        </div><!-- .col -->

                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="message">Your message</label>
                                <textarea
                                        name="message"
                                        id="message"
                                        rows="10"
                                        class="form-input form-input--large form-input--border-c-alto"
                                        placeholder="Your comment"></textarea>
                            </div><!-- .form-group -->
                        </div><!-- .col -->

                        <div class="col-md-12">
                            <div class="form-group--submit">
                                <button class="button button--large button--square button--primary" type="submit">Send Message</button>
                            </div>
                        </div><!-- .col -->
                    </div><!-- .row -->
                </form>
            </div><!-- .col -->
        </div><!-- .row -->
    </div><!-- .container -->
</section><!-- .contact -->

@include('layouts.footer')
@endsection
