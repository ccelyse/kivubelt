<!DOCTYPE html>
<html lang="en-US">

<head>

    <meta property="og:url" content="https://wanderers.mikado-themes.com/elements/tour-carousel/" />
    <meta property="og:type" content="article" />
    <meta property="og:title" content="Tour Carousel" />
    <meta property="og:description" content="fun Ultimate Thailand $1350 / per person Lorem ipsum dolor sit amet, conse tetur adipiscing elit. Mauris a rutrum&hellip; 12 Days 11 Nights Discover India $2650 / per person Lorem ipsum dolor sit amet, conse tetur adipiscing elit. Mauris a rutrum&hellip; 10 Days 9 Nights Forest Adventure $1350 / per person Lorem ipsum dolor sit amet, conse tetur adipiscing elit. Mauris a rutrum&hellip; 11 Days 10 Nights China Tour $1500 / per person Lorem ipsum dolor sit amet, conse tetur adipiscing elit. Mauris a rutrum&hellip; 10 Days 9 [...]" />
    <meta property="og:image" content="https://wanderers.mikado-themes.com/wp-content/themes/wanderers/assets/img/open_graph.jpg" />

    <meta charset="UTF-8" />
    <link rel="profile" href="http://gmpg.org/xfn/11" />

    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=yes">
    <title>Tour Carousel &#8211; Wanderers</title>
    <script type="application/javascript">
        var mkdfToursAjaxURL = "https://wanderers.mikado-themes.com/wp-admin/admin-ajax.php"
    </script>
    <!-- Google Tag Manager for WordPress by gtm4wp.com -->
    <script data-cfasync="false" type="text/javascript">
        //<![CDATA[
        var gtm4wp_datalayer_name = "dataLayer";
        var dataLayer = dataLayer || [];
        //]]>
    </script>
    <!-- End Google Tag Manager for WordPress by gtm4wp.com -->
    <link rel='dns-prefetch' href='//apis.google.com' />
    <link rel='dns-prefetch' href='//toolbar.qodeinteractive.com' />
    <link rel='dns-prefetch' href='//maps.googleapis.com' />
    <link rel='dns-prefetch' href='//fonts.googleapis.com' />
    <link rel='dns-prefetch' href='//s.w.org' />
    <link rel="alternate" type="application/rss+xml" title="Wanderers &raquo; Feed" href="https://wanderers.mikado-themes.com/feed/" />
    <link rel="alternate" type="application/rss+xml" title="Wanderers &raquo; Comments Feed" href="https://wanderers.mikado-themes.com/comments/feed/" />
    <script type="text/javascript">
        window._wpemojiSettings = {
            "baseUrl": "https:\/\/s.w.org\/images\/core\/emoji\/11\/72x72\/",
            "ext": ".png",
            "svgUrl": "https:\/\/s.w.org\/images\/core\/emoji\/11\/svg\/",
            "svgExt": ".svg",
            "source": {
                "concatemoji": "https:\/\/wanderers.mikado-themes.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.9.10"
            }
        };
        ! function(a, b, c) {
            function d(a, b) {
                var c = String.fromCharCode;
                l.clearRect(0, 0, k.width, k.height), l.fillText(c.apply(this, a), 0, 0);
                var d = k.toDataURL();
                l.clearRect(0, 0, k.width, k.height), l.fillText(c.apply(this, b), 0, 0);
                var e = k.toDataURL();
                return d === e
            }

            function e(a) {
                var b;
                if (!l || !l.fillText) return !1;
                switch (l.textBaseline = "top", l.font = "600 32px Arial", a) {
                    case "flag":
                        return !(b = d([55356, 56826, 55356, 56819], [55356, 56826, 8203, 55356, 56819])) && (b = d([55356, 57332, 56128, 56423, 56128, 56418, 56128, 56421, 56128, 56430, 56128, 56423, 56128, 56447], [55356, 57332, 8203, 56128, 56423, 8203, 56128, 56418, 8203, 56128, 56421, 8203, 56128, 56430, 8203, 56128, 56423, 8203, 56128, 56447]), !b);
                    case "emoji":
                        return b = d([55358, 56760, 9792, 65039], [55358, 56760, 8203, 9792, 65039]), !b
                }
                return !1
            }

            function f(a) {
                var c = b.createElement("script");
                c.src = a, c.defer = c.type = "text/javascript", b.getElementsByTagName("head")[0].appendChild(c)
            }
            var g, h, i, j, k = b.createElement("canvas"),
                l = k.getContext && k.getContext("2d");
            for (j = Array("flag", "emoji"), c.supports = {
                everything: !0,
                everythingExceptFlag: !0
            }, i = 0; i < j.length; i++) c.supports[j[i]] = e(j[i]), c.supports.everything = c.supports.everything && c.supports[j[i]], "flag" !== j[i] && (c.supports.everythingExceptFlag = c.supports.everythingExceptFlag && c.supports[j[i]]);
            c.supports.everythingExceptFlag = c.supports.everythingExceptFlag && !c.supports.flag, c.DOMReady = !1, c.readyCallback = function() {
                c.DOMReady = !0
            }, c.supports.everything || (h = function() {
                c.readyCallback()
            }, b.addEventListener ? (b.addEventListener("DOMContentLoaded", h, !1), a.addEventListener("load", h, !1)) : (a.attachEvent("onload", h), b.attachEvent("onreadystatechange", function() {
                "complete" === b.readyState && c.readyCallback()
            })), g = c.source || {}, g.concatemoji ? f(g.concatemoji) : g.wpemoji && g.twemoji && (f(g.twemoji), f(g.wpemoji)))
        }(window, document, window._wpemojiSettings);
    </script>
    <style type="text/css">
        img.wp-smiley,
        img.emoji {
            display: inline !important;
            border: none !important;
            box-shadow: none !important;
            height: 1em !important;
            width: 1em !important;
            margin: 0 .07em !important;
            vertical-align: -0.1em !important;
            background: none !important;
            padding: 0 !important;
        }
    </style>
    <link rel='stylesheet' id='js_composer_front-css' href='https://wanderers.mikado-themes.com/wp-content/plugins/js_composer/assets/css/js_composer.min.css' type='text/css' media='all' />
    <link rel='stylesheet' id='contact-form-7-css' href='https://wanderers.mikado-themes.com/wp-content/plugins/contact-form-7/includes/css/styles.css' type='text/css' media='all' />
    <link rel='stylesheet' id='mkdf-membership-style-css' href='https://wanderers.mikado-themes.com/wp-content/plugins/mkdf-membership/assets/css/membership.min.css' type='text/css' media='all' />
    <link rel='stylesheet' id='mkdf-membership-responsive-style-css' href='https://wanderers.mikado-themes.com/wp-content/plugins/mkdf-membership/assets/css/membership-responsive.min.css' type='text/css' media='all' />
    <link rel='stylesheet' id='wanderers-mkdf-modules-css' href='https://wanderers.mikado-themes.com/wp-content/themes/wanderers/assets/css/modules.min.css' type='text/css' media='all' />
    <link rel='stylesheet' id='mkdf-tours-style-css' href='https://wanderers.mikado-themes.com/wp-content/plugins/mkdf-tours/assets/css/tours.min.css' type='text/css' media='all' />
    <link rel='stylesheet' id='wanderers-mkdf-modules-responsive-css' href='https://wanderers.mikado-themes.com/wp-content/themes/wanderers/assets/css/modules-responsive.min.css' type='text/css' media='all' />
    <link rel='stylesheet' id='mkdf-tours-responsive-style-css' href='https://wanderers.mikado-themes.com/wp-content/plugins/mkdf-tours/assets/css/tours-responsive.min.css' type='text/css' media='all' />
    <link rel='stylesheet' id='nouislider-css' href='https://wanderers.mikado-themes.com/wp-content/plugins/mkdf-tours/assets/css/nouislider.min.css' type='text/css' media='all' />
    <link rel='stylesheet' id='rabbit_css-css' href='https://toolbar.qodeinteractive.com/_toolbar/assets/css/rbt-modules.css?ver=4.9.10' type='text/css' media='all' />
    <link rel='stylesheet' id='rs-plugin-settings-css' href='https://wanderers.mikado-themes.com/wp-content/plugins/revslider/public/assets/css/settings.css' type='text/css' media='all' />
    <style id='rs-plugin-settings-inline-css' type='text/css'>
        #rs-demo-id {}
    </style>
    <link rel='stylesheet' id='wanderer-mkdf-default-style-css' href='https://wanderers.mikado-themes.com/wp-content/themes/wanderers/style.css' type='text/css' media='all' />
    <link rel='stylesheet' id='mkdf-font_awesome-css' href='https://wanderers.mikado-themes.com/wp-content/themes/wanderers/assets/css/font-awesome/css/font-awesome.min.css' type='text/css' media='all' />
    <link rel='stylesheet' id='mkdf-font_elegant-css' href='https://wanderers.mikado-themes.com/wp-content/themes/wanderers/assets/css/elegant-icons/style.min.css' type='text/css' media='all' />
    <link rel='stylesheet' id='mkdf-ion_icons-css' href='https://wanderers.mikado-themes.com/wp-content/themes/wanderers/assets/css/ion-icons/css/ionicons.min.css' type='text/css' media='all' />
    <link rel='stylesheet' id='mkdf-linea_icons-css' href='https://wanderers.mikado-themes.com/wp-content/themes/wanderers/assets/css/linea-icons/style.css' type='text/css' media='all' />
    <link rel='stylesheet' id='mkdf-linear_icons-css' href='https://wanderers.mikado-themes.com/wp-content/themes/wanderers/assets/css/linear-icons/style.css' type='text/css' media='all' />
    <link rel='stylesheet' id='mkdf-simple_line_icons-css' href='https://wanderers.mikado-themes.com/wp-content/themes/wanderers/assets/css/simple-line-icons/simple-line-icons.css' type='text/css' media='all' />
    <link rel='stylesheet' id='mkdf-dripicons-css' href='https://wanderers.mikado-themes.com/wp-content/themes/wanderers/assets/css/dripicons/dripicons.css' type='text/css' media='all' />
    <link rel='stylesheet' id='mediaelement-css' href='https://wanderers.mikado-themes.com/wp-includes/js/mediaelement/mediaelementplayer-legacy.min.css' type='text/css' media='all' />
    <link rel='stylesheet' id='wp-mediaelement-css' href='https://wanderers.mikado-themes.com/wp-includes/js/mediaelement/wp-mediaelement.min.css' type='text/css' media='all' />
    <link rel='stylesheet' id='wanderers-mkdf-woo-css' href='https://wanderers.mikado-themes.com/wp-content/themes/wanderers/assets/css/woocommerce.min.css' type='text/css' media='all' />
    <style id='wanderers-mkdf-woo-inline-css' type='text/css'>
        /* generated in /home/mikadothemes/wanderers/public_html/wp-content/themes/wanderers/framework/admin/options/general/map.php wanderers_mkdf_page_general_style function */

        .page-id-1417.mkdf-boxed .mkdf-wrapper {
            background-attachment: fixed;
        }
        /* generated in /home/mikadothemes/wanderers/public_html/wp-content/themes/wanderers/functions.php wanderers_mkdf_content_padding_top function */

        .page-id-1417 .mkdf-content .mkdf-content-inner > .mkdf-container > .mkdf-container-inner,
        .page-id-1417 .mkdf-content .mkdf-content-inner > .mkdf-full-width > .mkdf-full-width-inner {
            padding-top: 0px !important;
        }
    </style>
    <link rel='stylesheet' id='wanderers-mkdf-woo-responsive-css' href='https://wanderers.mikado-themes.com/wp-content/themes/wanderers/assets/css/woocommerce-responsive.min.css' type='text/css' media='all' />
    <link rel='stylesheet' id='wanderers-mkdf-style-dynamic-css' href='https://wanderers.mikado-themes.com/wp-content/themes/wanderers/assets/css/style_dynamic.css' type='text/css' media='all' />
    <link rel='stylesheet' id='wanderers-mkdf-style-dynamic-responsive-css' href='https://wanderers.mikado-themes.com/wp-content/themes/wanderers/assets/css/style_dynamic_responsive.css' type='text/css' media='all' />
    <link rel='stylesheet' id='wanderers-mkdf-google-fonts-css' href='https://fonts.googleapis.com/css?family=Cabin%3A300%2C400%2C500%2C600%2C700%2C900%7CPlayfair+Display%3A300%2C400%2C500%2C600%2C700%2C900%7CMontserrat%3A300%2C400%2C500%2C600%2C700%2C900%7CKristi%3A300%2C400%2C500%2C600%2C700%2C900&#038;subset=latin-ext&#038;ver=1.0.0' type='text/css' media='all' />
    <script type='text/javascript' src='https://wanderers.mikado-themes.com/wp-includes/js/jquery/jquery.js'></script>
    <script type='text/javascript' src='https://wanderers.mikado-themes.com/wp-includes/js/jquery/jquery-migrate.min.js'></script>
    <script type='text/javascript' src='https://apis.google.com/js/platform.js'></script>
    <script type='text/javascript' src='https://wanderers.mikado-themes.com/wp-content/plugins/revslider/public/assets/js/jquery.themepunch.tools.min.js'></script>
    <script type='text/javascript' src='https://wanderers.mikado-themes.com/wp-content/plugins/revslider/public/assets/js/jquery.themepunch.revolution.min.js'></script>
    <script type='text/javascript'>
        /* <![CDATA[ */
        var wc_add_to_cart_params = {
            "ajax_url": "\/wp-admin\/admin-ajax.php",
            "wc_ajax_url": "\/?wc-ajax=%%endpoint%%",
            "i18n_view_cart": "View cart",
            "cart_url": "https:\/\/wanderers.mikado-themes.com\/cart\/",
            "is_cart": "",
            "cart_redirect_after_add": "no"
        };
        /* ]]> */
    </script>
    <script type='text/javascript' src='https://wanderers.mikado-themes.com/wp-content/plugins/woocommerce/assets/js/frontend/add-to-cart.min.js'></script>
    <script type='text/javascript' src='https://wanderers.mikado-themes.com/wp-content/plugins/duracelltomi-google-tag-manager/js/gtm4wp-form-move-tracker.js'></script>
    <script type='text/javascript' src='https://wanderers.mikado-themes.com/wp-content/plugins/js_composer/assets/js/vendors/woocommerce-add-to-cart.js'></script>
    <script type='text/javascript'>
        var mejsL10n = {
            "language": "en",
            "strings": {
                "mejs.install-flash": "You are using a browser that does not have Flash player enabled or installed. Please turn on your Flash player plugin or download the latest version from https:\/\/get.adobe.com\/flashplayer\/",
                "mejs.fullscreen-off": "Turn off Fullscreen",
                "mejs.fullscreen-on": "Go Fullscreen",
                "mejs.download-video": "Download Video",
                "mejs.fullscreen": "Fullscreen",
                "mejs.time-jump-forward": ["Jump forward 1 second", "Jump forward %1 seconds"],
                "mejs.loop": "Toggle Loop",
                "mejs.play": "Play",
                "mejs.pause": "Pause",
                "mejs.close": "Close",
                "mejs.time-slider": "Time Slider",
                "mejs.time-help-text": "Use Left\/Right Arrow keys to advance one second, Up\/Down arrows to advance ten seconds.",
                "mejs.time-skip-back": ["Skip back 1 second", "Skip back %1 seconds"],
                "mejs.captions-subtitles": "Captions\/Subtitles",
                "mejs.captions-chapters": "Chapters",
                "mejs.none": "None",
                "mejs.mute-toggle": "Mute Toggle",
                "mejs.volume-help-text": "Use Up\/Down Arrow keys to increase or decrease volume.",
                "mejs.unmute": "Unmute",
                "mejs.mute": "Mute",
                "mejs.volume-slider": "Volume Slider",
                "mejs.video-player": "Video Player",
                "mejs.audio-player": "Audio Player",
                "mejs.ad-skip": "Skip ad",
                "mejs.ad-skip-info": ["Skip in 1 second", "Skip in %1 seconds"],
                "mejs.source-chooser": "Source Chooser",
                "mejs.stop": "Stop",
                "mejs.speed-rate": "Speed Rate",
                "mejs.live-broadcast": "Live Broadcast",
                "mejs.afrikaans": "Afrikaans",
                "mejs.albanian": "Albanian",
                "mejs.arabic": "Arabic",
                "mejs.belarusian": "Belarusian",
                "mejs.bulgarian": "Bulgarian",
                "mejs.catalan": "Catalan",
                "mejs.chinese": "Chinese",
                "mejs.chinese-simplified": "Chinese (Simplified)",
                "mejs.chinese-traditional": "Chinese (Traditional)",
                "mejs.croatian": "Croatian",
                "mejs.czech": "Czech",
                "mejs.danish": "Danish",
                "mejs.dutch": "Dutch",
                "mejs.english": "English",
                "mejs.estonian": "Estonian",
                "mejs.filipino": "Filipino",
                "mejs.finnish": "Finnish",
                "mejs.french": "French",
                "mejs.galician": "Galician",
                "mejs.german": "German",
                "mejs.greek": "Greek",
                "mejs.haitian-creole": "Haitian Creole",
                "mejs.hebrew": "Hebrew",
                "mejs.hindi": "Hindi",
                "mejs.hungarian": "Hungarian",
                "mejs.icelandic": "Icelandic",
                "mejs.indonesian": "Indonesian",
                "mejs.irish": "Irish",
                "mejs.italian": "Italian",
                "mejs.japanese": "Japanese",
                "mejs.korean": "Korean",
                "mejs.latvian": "Latvian",
                "mejs.lithuanian": "Lithuanian",
                "mejs.macedonian": "Macedonian",
                "mejs.malay": "Malay",
                "mejs.maltese": "Maltese",
                "mejs.norwegian": "Norwegian",
                "mejs.persian": "Persian",
                "mejs.polish": "Polish",
                "mejs.portuguese": "Portuguese",
                "mejs.romanian": "Romanian",
                "mejs.russian": "Russian",
                "mejs.serbian": "Serbian",
                "mejs.slovak": "Slovak",
                "mejs.slovenian": "Slovenian",
                "mejs.spanish": "Spanish",
                "mejs.swahili": "Swahili",
                "mejs.swedish": "Swedish",
                "mejs.tagalog": "Tagalog",
                "mejs.thai": "Thai",
                "mejs.turkish": "Turkish",
                "mejs.ukrainian": "Ukrainian",
                "mejs.vietnamese": "Vietnamese",
                "mejs.welsh": "Welsh",
                "mejs.yiddish": "Yiddish"
            }
        };
    </script>
    <script type='text/javascript' src='https://wanderers.mikado-themes.com/wp-includes/js/mediaelement/mediaelement-and-player.min.js'></script>
    <script type='text/javascript' src='https://wanderers.mikado-themes.com/wp-includes/js/mediaelement/mediaelement-migrate.min.js'></script>
    <script type='text/javascript'>
        /* <![CDATA[ */
        var _wpmejsSettings = {
            "pluginPath": "\/wp-includes\/js\/mediaelement\/",
            "classPrefix": "mejs-",
            "stretching": "responsive"
        };
        /* ]]> */
    </script>
    <link rel='https://api.w.org/' href='https://wanderers.mikado-themes.com/wp-json/' />
    <link rel="EditURI" type="application/rsd+xml" title="RSD" href="https://wanderers.mikado-themes.com/xmlrpc.php?rsd" />
    <link rel="wlwmanifest" type="application/wlwmanifest+xml" href="https://wanderers.mikado-themes.com/wp-includes/wlwmanifest.xml" />
    <meta name="generator" content="WordPress 4.9.10" />
    <meta name="generator" content="WooCommerce 3.3.4" />
    <link rel="canonical" href="https://wanderers.mikado-themes.com/elements/tour-carousel/" />
    <link rel='shortlink' href='https://wanderers.mikado-themes.com/?p=1417' />
    <link rel="alternate" type="application/json+oembed" href="https://wanderers.mikado-themes.com/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fwanderers.mikado-themes.com%2Felements%2Ftour-carousel%2F" />
    <link rel="alternate" type="text/xml+oembed" href="https://wanderers.mikado-themes.com/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fwanderers.mikado-themes.com%2Felements%2Ftour-carousel%2F&#038;format=xml" />

    <!-- Google Tag Manager for WordPress by gtm4wp.com -->
    <script data-cfasync="false" type="text/javascript">
        //<![CDATA[
        dataLayer.push({
            "pagePostType": "page",
            "pagePostType2": "single-page",
            "pagePostAuthor": "admin"
        }); //]]>
    </script>
    <script data-cfasync="false">
        //<![CDATA[
        (function(w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start': new Date().getTime(),
                event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s),
                dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                '//www.googletagmanager.com/gtm.' + 'js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-KTQ2BTD'); //]]>
    </script>
    <!-- End Google Tag Manager -->
    <!-- End Google Tag Manager for WordPress by gtm4wp.com -->
    <noscript>
        <style>
            .woocommerce-product-gallery {
                opacity: 1 !important;
            }
        </style>
    </noscript>
    <meta name="generator" content="Powered by WPBakery Page Builder - drag and drop page builder for WordPress." />
    <!--[if lte IE 9]><link rel="stylesheet" type="text/css" href="https://wanderers.mikado-themes.com/wp-content/plugins/js_composer/assets/css/vc_lte_ie9.min.css" media="screen"><![endif]-->
    <meta name="generator" content="Powered by Slider Revolution 5.4.7.2 - responsive, Mobile-Friendly Slider Plugin for WordPress with comfortable drag and drop interface." />
    <link rel="icon" href="https://wanderers.mikado-themes.com/wp-content/uploads/2018/02/favicon-dark.png" sizes="32x32" />
    <link rel="icon" href="https://wanderers.mikado-themes.com/wp-content/uploads/2018/02/favicon-dark.png" sizes="192x192" />
    <link rel="apple-touch-icon-precomposed" href="https://wanderers.mikado-themes.com/wp-content/uploads/2018/02/favicon-dark.png" />
    <meta name="msapplication-TileImage" content="https://wanderers.mikado-themes.com/wp-content/uploads/2018/02/favicon-dark.png" />
    <script type="text/javascript">
        function setREVStartSize(e) {
            try {
                e.c = jQuery(e.c);
                var i = jQuery(window).width(),
                    t = 9999,
                    r = 0,
                    n = 0,
                    l = 0,
                    f = 0,
                    s = 0,
                    h = 0;
                if (e.responsiveLevels && (jQuery.each(e.responsiveLevels, function(e, f) {
                        f > i && (t = r = f, l = e), i > f && f > r && (r = f, n = e)
                    }), t > r && (l = n)), f = e.gridheight[l] || e.gridheight[0] || e.gridheight, s = e.gridwidth[l] || e.gridwidth[0] || e.gridwidth, h = i / s, h = h > 1 ? 1 : h, f = Math.round(h * f), "fullscreen" == e.sliderLayout) {
                    var u = (e.c.width(), jQuery(window).height());
                    if (void 0 != e.fullScreenOffsetContainer) {
                        var c = e.fullScreenOffsetContainer.split(",");
                        if (c) jQuery.each(c, function(e, i) {
                            u = jQuery(i).length > 0 ? u - jQuery(i).outerHeight(!0) : u
                        }), e.fullScreenOffset.split("%").length > 1 && void 0 != e.fullScreenOffset && e.fullScreenOffset.length > 0 ? u -= jQuery(window).height() * parseInt(e.fullScreenOffset, 0) / 100 : void 0 != e.fullScreenOffset && e.fullScreenOffset.length > 0 && (u -= parseInt(e.fullScreenOffset, 0))
                    }
                    f = u
                } else void 0 != e.minHeight && f < e.minHeight && (f = e.minHeight);
                e.c.closest(".rev_slider_wrapper").css({
                    height: f
                })
            } catch (d) {
                console.log("Failure at Presize of Slider:" + d)
            }
        };
    </script>
    <style type="text/css" id="wp-custom-css">
        .widget.widget_categories ul li.cat-item-51 {
            display: none;
        }

        .mkdf-woo-single-page .star-rating::after {
            content: '';
            background-color: #fff;
            width: 1px;
            height: 100%;
            position: absolute;
            right: 0;
        }
    </style>
    <style type="text/css" data-type="vc_shortcodes-custom-css">
        .vc_custom_1521216884465 {
            padding-top: 100px !important;
            padding-bottom: 94px !important;
        }

        .vc_custom_1521647449096 {
            padding-top: 100px !important;
            padding-bottom: 94px !important;
        }

        .vc_custom_1521216884465 {
            padding-top: 100px !important;
            padding-bottom: 94px !important;
        }

        .vc_custom_1521647458816 {
            padding-top: 100px !important;
            padding-bottom: 94px !important;
        }
    </style>
    <noscript>
        <style type="text/css">
            .wpb_animate_when_almost_visible {
                opacity: 1;
            }
        </style>
    </noscript>
</head>

<body class="page-template page-template-full-width page-template-full-width-php page page-id-1417 page-child parent-pageid-1170 mkdf-core-1.0 mkdf-social-login-1.0 mkdf-tours-1.0 wanderers-ver-1.0 mkdf-grid-1300 mkdf-disable-global-padding-bottom mkdf-light-header mkdf-sticky-header-on-scroll-down-up mkdf-dropdown-animate-height mkdf-header-standard mkdf-menu-area-shadow-disable mkdf-menu-area-in-grid-shadow-disable mkdf-menu-area-border-disable mkdf-menu-area-in-grid-border-disable mkdf-logo-area-border-disable mkdf-header-vertical-shadow-disable mkdf-header-vertical-border-disable mkdf-side-menu-slide-from-right mkdf-woocommerce-columns-3 mkdf-woo-small-space mkdf-woo-pl-info-below-image mkdf-woo-single-thumb-on-left-side mkdf-woo-single-has-pretty-photo mkdf-default-mobile-header mkdf-sticky-up-mobile-header mkdf-header-top-enabled wpb-js-composer js-comp-ver-5.4.7 vc_responsive mkdf-search-covers-header" itemscope itemtype="http://schema.org/WebPage">
<section class="mkdf-side-menu">
    <div class="mkdf-close-side-menu-holder">
        <a class="mkdf-close-side-menu" href="#" target="_self">
            <span aria-hidden="true" class="mkdf-icon-font-elegant icon_close "></span> </a>
    </div>
    <div id="media_image-2" class="widget mkdf-sidearea widget_media_image">
        <a href="https://wanderers.mikado-themes.com"><img width="162" height="52" src="https://wanderers.mikado-themes.com/wp-content/uploads/2018/02/logo-sidearea.png" class="image wp-image-1467  attachment-full size-full" alt="logo" style="max-width: 100%; height: auto;" /></a>
    </div>
    <div class="widget mkdf-custom-font-widget">
        <p class="mkdf-custom-font-holder  mkdf-cf-6377  " style="font-size: 18px;line-height: 30px;color: #b4b4b4;margin: 20px 0px 35px" data-item-class="mkdf-cf-6377">
            Proin gravida nibh vel velit auctor aliquet. Aenean sollic itudin, lorem quis bibendum auctornisi elit consequat ipsum, nec nibh id elit. Lorem ipsum dolor.</p>
    </div>
    <div id="text-12" class="widget mkdf-sidearea widget_text">
        <div class="mkdf-widget-title-holder">
            <h5 class="mkdf-widget-title">Latest Posts</h5></div>
        <div class="textwidget"></div>
    </div>
    <div class="widget mkdf-separator-widget">
        <div class="mkdf-separator-holder clearfix  mkdf-separator-center mkdf-separator-normal">
            <div class="mkdf-separator" style="border-style: solid;margin-top: 0px;margin-bottom: 2px"></div>
        </div>
    </div>
    <div class="widget mkdf-blog-list-widget">
        <div class="mkdf-blog-list-holder mkdf-bl-simple mkdf-bl-one-column mkdf-small-space mkdf-bl-pag-no-pagination" data-type=simple data-number-of-posts=2 data-number-of-columns=1 data-space-between-items=small data-orderby=date data-order=DESC data-image-size=thumbnail data-title-tag=h5 data-excerpt-length=40 data-post-info-section=yes data-post-info-image=yes data-post-info-author=yes data-post-info-date=yes data-post-info-category=yes data-post-info-comments=no data-post-info-like=no data-post-info-share=no data-pagination-type=no-pagination data-max-num-pages=20 data-next-page=2>
            <div class="mkdf-bl-wrapper mkdf-outer-space">
                <ul class="mkdf-blog-list">
                    <li class="mkdf-bl-item mkdf-item-space clearfix">
                        <div class="mkdf-bli-inner">

                            <div class="mkdf-post-image">
                                <a itemprop="url" href="https://wanderers.mikado-themes.com/solo-traveler-learns-on-the-road/" title="Solo Traveler Learns On The Road">
                                    <img width="150" height="150" src="https://wanderers.mikado-themes.com/wp-content/uploads/2018/02/blog-post-1-150x150.jpg" class="attachment-thumbnail size-thumbnail wp-post-image" alt="a" srcset="https://wanderers.mikado-themes.com/wp-content/uploads/2018/02/blog-post-1-150x150.jpg 150w, https://wanderers.mikado-themes.com/wp-content/uploads/2018/02/blog-post-1-550x550.jpg 550w, https://wanderers.mikado-themes.com/wp-content/uploads/2018/02/blog-post-1-100x100.jpg 100w" sizes="(max-width: 150px) 100vw, 150px" /> </a>
                            </div>
                            <div class="mkdf-bli-content">

                                <h5 itemprop="name" class="entry-title mkdf-post-title">
                                    <a itemprop="url" href="https://wanderers.mikado-themes.com/solo-traveler-learns-on-the-road/" title="Solo Traveler Learns On The Road">
                                        Solo Traveler Learns On The Road            </a>
                                </h5>
                                <div itemprop="dateCreated" class="mkdf-post-info-date entry-date published updated">
                                    <a itemprop="url" href="https://wanderers.mikado-themes.com/2018/03/">

                                        <span class="icon_calendar"></span> March 8, 2018 </a>
                                    <meta itemprop="interactionCount" content="UserComments: 0" />
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="mkdf-bl-item mkdf-item-space clearfix">
                        <div class="mkdf-bli-inner">

                            <div class="mkdf-post-image">
                                <a itemprop="url" href="https://wanderers.mikado-themes.com/use-your-social-network-to-travel/" title="Use Your Social Network To Travel">
                                    <img width="150" height="150" src="https://wanderers.mikado-themes.com/wp-content/uploads/2018/02/blog-post-4-150x150.jpg" class="attachment-thumbnail size-thumbnail wp-post-image" alt="a" srcset="https://wanderers.mikado-themes.com/wp-content/uploads/2018/02/blog-post-4-150x150.jpg 150w, https://wanderers.mikado-themes.com/wp-content/uploads/2018/02/blog-post-4-550x550.jpg 550w, https://wanderers.mikado-themes.com/wp-content/uploads/2018/02/blog-post-4-100x100.jpg 100w" sizes="(max-width: 150px) 100vw, 150px" /> </a>
                            </div>
                            <div class="mkdf-bli-content">

                                <h5 itemprop="name" class="entry-title mkdf-post-title">
                                    <a itemprop="url" href="https://wanderers.mikado-themes.com/use-your-social-network-to-travel/" title="Use Your Social Network To Travel">
                                        Use Your Social Network To Travel            </a>
                                </h5>
                                <div itemprop="dateCreated" class="mkdf-post-info-date entry-date published updated">
                                    <a itemprop="url" href="https://wanderers.mikado-themes.com/2018/03/">

                                        <span class="icon_calendar"></span> March 7, 2018 </a>
                                    <meta itemprop="interactionCount" content="UserComments: 0" />
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div id="text-13" class="widget mkdf-sidearea widget_text">
        <div class="textwidget">
            <p>
            <div class="mkdf-iwt clearfix  mkdf-iwt-icon-left mkdf-iwt-icon-tiny">
                <a itemprop="url" href="tel:167712444227" target="_blank">
                    <div class="mkdf-iwt-icon">

                                <span class="mkdf-icon-shortcode mkdf-normal   mkdf-icon-tiny" data-hover-color="#ffcc05" data-color="#9b9b9b">
                    <span aria-hidden="true" class="mkdf-icon-font-elegant icon_phone mkdf-icon-element" style="color: #9b9b9b;font-size:14px" ></span> </span>
                    </div>
                    <div class="mkdf-iwt-content" style="padding-left: 14px">
                        <h6 class="mkdf-iwt-title" style="color: #9b9b9b;margin-top: 10px">
                            <span class="mkdf-iwt-title-text">1-677-124-44227</span>
                        </h6>
                    </div>
                </a>
            </div>
            <div class="vc_empty_space" style="height: 4px"><span class="vc_empty_space_inner"></span></div>
            <div class="mkdf-iwt clearfix  mkdf-iwt-icon-left mkdf-iwt-icon-tiny">
                <a itemprop="url" href="mailto:wanderers.wp.theme@gmail.com" target="_self">
                    <div class="mkdf-iwt-icon">

                                <span class="mkdf-icon-shortcode mkdf-normal   mkdf-icon-tiny" data-hover-color="#ffcc05" data-color="#9b9b9b">
                    <span aria-hidden="true" class="mkdf-icon-font-elegant icon_mail mkdf-icon-element" style="color: #9b9b9b;font-size:14px" ></span> </span>
                    </div>
                    <div class="mkdf-iwt-content" style="padding-left: 14px">
                        <h6 class="mkdf-iwt-title" style="color: #9b9b9b;margin-top: 10px">
                            <span class="mkdf-iwt-title-text">wanderers@mikado-themes.com</span>
                        </h6>
                    </div>
                </a>
            </div>
            </p>
        </div>
    </div>
    <div class="widget mkdf-separator-widget">
        <div class="mkdf-separator-holder clearfix  mkdf-separator-center mkdf-separator-normal">
            <div class="mkdf-separator" style="border-style: solid;margin-top: 0px;margin-bottom: 2px"></div>
        </div>
    </div>
    <a class="mkdf-social-icon-widget-holder mkdf-icon-has-hover" data-hover-color="#ffcc05" style="color: #808285;;font-size: 13px;margin: 0px 8px 0px 0px;" href="https://twitter.com/MikadoThemes?lang=en" target="_blank">
        <span class="mkdf-social-icon-widget  social_twitter    "></span> </a>

    <a class="mkdf-social-icon-widget-holder mkdf-icon-has-hover" data-hover-color="#ffcc05" style="color: #808285;;font-size: 13px;margin: 0px 8px;" href="https://www.facebook.com/Mikado-Themes-884182241781117" target="_blank">
        <span class="mkdf-social-icon-widget  social_facebook    "></span> </a>

    <a class="mkdf-social-icon-widget-holder mkdf-icon-has-hover" data-hover-color="#ffcc05" style="color: #808285;;font-size: 13px;margin: 0px 8px;" href="https://www.instagram.com/mikadothemes/" target="_blank">
        <span class="mkdf-social-icon-widget  social_instagram    "></span> </a>

    <a class="mkdf-social-icon-widget-holder mkdf-icon-has-hover" data-hover-color="#ffcc05" style="color: #808285;;font-size: 13px;margin: 0px 8px;" href="http://www.tumblr.com" target="_blank">
        <span class="mkdf-social-icon-widget  social_tumblr    "></span> </a>

    <a class="mkdf-social-icon-widget-holder mkdf-icon-has-hover" data-hover-color="#ffcc05" style="color: #808285;;font-size: 13px;margin: 0px 8px;" href="http://www.linkedin.com" target="_blank">
        <span class="mkdf-social-icon-widget  social_linkedin    "></span> </a>

    <a class="mkdf-social-icon-widget-holder mkdf-icon-has-hover" data-hover-color="#ffcc05" style="color: #808285;;font-size: 13px;margin: 0px 8px;" href="https://www.pinterest.com/mikadothemes/" target="_blank">
        <span class="mkdf-social-icon-widget  social_pinterest    "></span> </a>
</section>
<div class="mkdf-wrapper">
    <div class="mkdf-wrapper-inner">

        <div class="mkdf-top-bar">

            <div class="mkdf-grid">

                <div class="mkdf-vertical-align-containers">
                    <div class="mkdf-position-left">
                        <div class="mkdf-position-left-inner">
                            <div id="text-4" class="widget widget_text mkdf-top-bar-widget">
                                <div class="textwidget">
                                    <p>Follow Us:&nbsp;&nbsp;</p>
                                </div>
                            </div>
                            <a class="mkdf-social-icon-widget-holder mkdf-icon-has-hover" data-hover-color="#ffcc05" style="color: #ffffff;;font-size: 12px;margin: 0px 8px 0px 1px;" href="https://www.instagram.com/mikadothemes/" target="_blank">
                                <span class="mkdf-social-icon-widget  social_instagram    "></span> </a>

                            <a class="mkdf-social-icon-widget-holder mkdf-icon-has-hover" data-hover-color="#ffcc05" style="color: #ffffff;;font-size: 12px;margin: 0px 6px 0px 4px;" href="https://www.facebook.com/Mikado-Themes-884182241781117" target="_blank">
                                <span class="mkdf-social-icon-widget  social_facebook    "></span> </a>

                            <a class="mkdf-social-icon-widget-holder mkdf-icon-has-hover" data-hover-color="#ffcc05" style="color: #ffffff;;font-size: 12px;margin: 0px 4px;" href="https://twitter.com/MikadoThemes?lang=en" target="_blank">
                                <span class="mkdf-social-icon-widget  social_twitter    "></span> </a>

                            <a class="mkdf-social-icon-widget-holder mkdf-icon-has-hover" data-hover-color="#ffcc05" style="color: #ffffff;;font-size: 12px;margin: 0px 41px 0px 8px;" href="http://www.tumblr.com" target="_blank">
                                <span class="mkdf-social-icon-widget  social_tumblr    "></span> </a>
                            <div id="text-5" class="widget widget_text mkdf-top-bar-widget">
                                <div class="textwidget">
                                    <div class="mkdf-iwt clearfix  mkdf-iwt-icon-left mkdf-iwt-icon-tiny">
                                        <a itemprop="url" href="tel:167712444227" target="_blank">
                                            <div class="mkdf-iwt-icon">

                                                    <span class="mkdf-icon-shortcode mkdf-normal   mkdf-icon-tiny" data-hover-color="#f0cd2f" data-color="#fff">
                    <span aria-hidden="true" class="mkdf-icon-font-elegant icon_phone mkdf-icon-element" style="color: #fff;font-size:14px" ></span> </span>
                                            </div>
                                            <div class="mkdf-iwt-content" style="padding-left: 14px">
                                                <p class="mkdf-iwt-title" style="color: #fff;margin-top: 3px">
                                                    <span class="mkdf-iwt-title-text">1-677-124-44227</span>
                                                </p>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="mkdf-position-right">
                        <div class="mkdf-position-right-inner">
                            <div class="widget mkdf-login-register-widget mkdf-user-not-logged-in">
                                <a href="#" class="mkdf-login-opener">
                                    <span class="mkdf-login-text">Login</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

        <header class="mkdf-page-header">

            <div class="mkdf-menu-area mkdf-menu-right">

                <div class="mkdf-grid">

                    <div class="mkdf-vertical-align-containers">
                        <div class="mkdf-position-left">
                            <div class="mkdf-position-left-inner">

                                <div class="mkdf-logo-wrapper">
                                    <a itemprop="url" href="https://wanderers.mikado-themes.com/" style="height: 41px;">
                                        <img itemprop="image" class="mkdf-normal-logo" src="https://wanderers.mikado-themes.com/wp-content/uploads/2018/02/logo-dark.png" width="256" height="82" alt="logo" />
                                        <img itemprop="image" class="mkdf-dark-logo" src="https://wanderers.mikado-themes.com/wp-content/uploads/2018/02/logo-dark.png" width="256" height="82" alt="dark logo" /> <img itemprop="image" class="mkdf-light-logo" src="https://wanderers.mikado-themes.com/wp-content/uploads/2018/02/logo-light.png" width="256" height="82" alt="light logo" /> </a>
                                </div>

                            </div>
                        </div>
                        <div class="mkdf-position-right">
                            <div class="mkdf-position-right-inner">

                                <nav class="mkdf-main-menu mkdf-drop-down mkdf-default-nav">
                                    <ul id="menu-main-menu" class="clearfix">
                                        <li id="nav-menu-item-34" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home  narrow"><a href="https://wanderers.mikado-themes.com/" class=""><span class="item_outer"><span class="item_text">Home</span></span></a></li>
                                        <li id="nav-menu-item-40" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children  has_sub narrow"><a href="#" class=" no_link" onclick="JavaScript: return false;"><span class="item_outer"><span class="item_text">Pages</span><i class="mkdf-menu-arrow fa fa-angle-down"></i></span></a>
                                            <div class="second">
                                                <div class="inner">
                                                    <ul>
                                                        <li id="nav-menu-item-637" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/about-us/" class=""><span class="item_outer"><span class="item_text">About Us</span></span></a></li>
                                                        <li id="nav-menu-item-914" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/our-team/" class=""><span class="item_outer"><span class="item_text">Our Team</span></span></a></li>
                                                        <li id="nav-menu-item-815" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/contact/" class=""><span class="item_outer"><span class="item_text">Contact</span></span></a></li>
                                                        <li id="nav-menu-item-915" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/faq/" class=""><span class="item_outer"><span class="item_text">FAQ</span></span></a></li>
                                                        <li id="nav-menu-item-816" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/coming-soon/" class=""><span class="item_outer"><span class="item_text">Coming Soon</span></span></a></li>
                                                        <li id="nav-menu-item-633" class="menu-item menu-item-type-custom menu-item-object-custom "><a href="https://wanderers.mikado-themes.com/404" class=""><span class="item_outer"><span class="item_text">404</span></span></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </li>
                                        <li id="nav-menu-item-41" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children  has_sub narrow"><a href="#" class=" no_link" onclick="JavaScript: return false;"><span class="item_outer"><span class="item_text">Destinations</span><i class="mkdf-menu-arrow fa fa-angle-down"></i></span></a>
                                            <div class="second">
                                                <div class="inner">
                                                    <ul>
                                                        <li id="nav-menu-item-312" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/destination-list/" class=""><span class="item_outer"><span class="item_text">Destination List</span></span></a></li>
                                                        <li id="nav-menu-item-42" class="menu-item menu-item-type-post_type menu-item-object-destinations "><a href="https://wanderers.mikado-themes.com/destinations/thailand/" class=""><span class="item_outer"><span class="item_text">Destination Item</span></span></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </li>
                                        <li id="nav-menu-item-43" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children  has_sub narrow"><a href="#" class=" no_link" onclick="JavaScript: return false;"><span class="item_outer"><span class="item_text">Tours</span><i class="mkdf-menu-arrow fa fa-angle-down"></i></span></a>
                                            <div class="second">
                                                <div class="inner">
                                                    <ul>
                                                        <li id="nav-menu-item-86" class="menu-item menu-item-type-post_type menu-item-object-tour-item "><a href="https://wanderers.mikado-themes.com/tour-item/ultimate-thailand/" class=""><span class="item_outer"><span class="item_text">Tour Single</span></span></a></li>
                                                        <li id="nav-menu-item-319" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children sub"><a href="#" class=""><span class="item_outer"><span class="item_text">Tour Lists</span></span></a>
                                                            <ul>
                                                                <li id="nav-menu-item-2273" class="menu-item menu-item-type-custom menu-item-object-custom "><a href="https://wanderers.mikado-themes.com/tour-search-page/?view_type=standard" class=""><span class="item_outer"><span class="item_text">Standard</span></span></a></li>
                                                                <li id="nav-menu-item-2274" class="menu-item menu-item-type-custom menu-item-object-custom "><a href="https://wanderers.mikado-themes.com/tour-search-page/?view_type=gallery" class=""><span class="item_outer"><span class="item_text">Gallery</span></span></a></li>
                                                                <li id="nav-menu-item-333" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/tour-list-carousel/" class=""><span class="item_outer"><span class="item_text">Carousel</span></span></a></li>
                                                                <li id="nav-menu-item-2275" class="menu-item menu-item-type-custom menu-item-object-custom "><a href="https://wanderers.mikado-themes.com/tour-search-page/?view_type=list" class=""><span class="item_outer"><span class="item_text">Search Page</span></span></a></li>
                                                            </ul>
                                                        </li>
                                                        <li id="nav-menu-item-1281" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children sub"><a href="#" class=""><span class="item_outer"><span class="item_text">Dashboard</span></span></a>
                                                            <ul>
                                                                <li id="nav-menu-item-1284" class="menu-item menu-item-type-custom menu-item-object-custom "><a href="https://wanderers.mikado-themes.com/dashboard/?user-action=profile" class=""><span class="item_outer"><span class="item_text">My Profile</span></span></a></li>
                                                                <li id="nav-menu-item-1282" class="menu-item menu-item-type-custom menu-item-object-custom "><a href="https://wanderers.mikado-themes.com/dashboard/?user-action=edit-profile" class=""><span class="item_outer"><span class="item_text">Edit Profile</span></span></a></li>
                                                                <li id="nav-menu-item-1283" class="menu-item menu-item-type-custom menu-item-object-custom "><a href="https://wanderers.mikado-themes.com/dashboard/?user-action=my-bookings" class=""><span class="item_outer"><span class="item_text">My Bookings</span></span></a></li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </li>
                                        <li id="nav-menu-item-45" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children  has_sub narrow"><a href="#" class=" no_link" onclick="JavaScript: return false;"><span class="item_outer"><span class="item_text">Blog</span><i class="mkdf-menu-arrow fa fa-angle-down"></i></span></a>
                                            <div class="second">
                                                <div class="inner">
                                                    <ul>
                                                        <li id="nav-menu-item-525" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/blog-list/" class=""><span class="item_outer"><span class="item_text">Standard List</span></span></a></li>
                                                        <li id="nav-menu-item-625" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/blog-masonry/" class=""><span class="item_outer"><span class="item_text">Masonry List</span></span></a></li>
                                                        <li id="nav-menu-item-516" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children sub"><a href="#" class=""><span class="item_outer"><span class="item_text">Single</span></span></a>
                                                            <ul>
                                                                <li id="nav-menu-item-515" class="menu-item menu-item-type-post_type menu-item-object-post "><a href="https://wanderers.mikado-themes.com/solo-traveler-learns-on-the-road/" class=""><span class="item_outer"><span class="item_text">Standard Post</span></span></a></li>
                                                                <li id="nav-menu-item-547" class="menu-item menu-item-type-post_type menu-item-object-post "><a href="https://wanderers.mikado-themes.com/new-ways-to-become-a-better-travel-blogger/" class=""><span class="item_outer"><span class="item_text">Gallery Post</span></span></a></li>
                                                                <li id="nav-menu-item-546" class="menu-item menu-item-type-post_type menu-item-object-post "><a href="https://wanderers.mikado-themes.com/use-your-social-network-to-travel/" class=""><span class="item_outer"><span class="item_text">Audio Post</span></span></a></li>
                                                                <li id="nav-menu-item-543" class="menu-item menu-item-type-post_type menu-item-object-post "><a href="https://wanderers.mikado-themes.com/travel-the-world-in-only-20-days/" class=""><span class="item_outer"><span class="item_text">Video Post</span></span></a></li>
                                                                <li id="nav-menu-item-544" class="menu-item menu-item-type-post_type menu-item-object-post "><a href="https://wanderers.mikado-themes.com/best-designed-wordpress-themes/" class=""><span class="item_outer"><span class="item_text">Link Post</span></span></a></li>
                                                                <li id="nav-menu-item-545" class="menu-item menu-item-type-post_type menu-item-object-post "><a href="https://wanderers.mikado-themes.com/marc-nicolaus-richmond/" class=""><span class="item_outer"><span class="item_text">Quote Post</span></span></a></li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </li>
                                        <li id="nav-menu-item-46" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children  has_sub narrow"><a href="#" class=" no_link" onclick="JavaScript: return false;"><span class="item_outer"><span class="item_text">Shop</span><i class="mkdf-menu-arrow fa fa-angle-down"></i></span></a>
                                            <div class="second">
                                                <div class="inner">
                                                    <ul>
                                                        <li id="nav-menu-item-39" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/shop/" class=""><span class="item_outer"><span class="item_text">List</span></span></a></li>
                                                        <li id="nav-menu-item-362" class="menu-item menu-item-type-post_type menu-item-object-product "><a href="https://wanderers.mikado-themes.com/shop/biking-helmet/" class=""><span class="item_outer"><span class="item_text">Single</span></span></a></li>
                                                        <li id="nav-menu-item-47" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children sub"><a href="#" class=""><span class="item_outer"><span class="item_text">Shop Pages</span></span></a>
                                                            <ul>
                                                                <li id="nav-menu-item-36" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/my-account/" class=""><span class="item_outer"><span class="item_text">My account</span></span></a></li>
                                                                <li id="nav-menu-item-37" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/checkout/" class=""><span class="item_outer"><span class="item_text">Checkout</span></span></a></li>
                                                                <li id="nav-menu-item-38" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/cart/" class=""><span class="item_outer"><span class="item_text">Cart</span></span></a></li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </li>
                                        <li id="nav-menu-item-44" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-ancestor menu-item-has-children mkdf-active-item has_sub wide"><a href="#" class=" current  no_link" onclick="JavaScript: return false;"><span class="item_outer"><span class="item_text">Elements</span><i class="mkdf-menu-arrow fa fa-angle-down"></i></span></a>
                                            <div class="second">
                                                <div class="inner">
                                                    <ul>
                                                        <li id="nav-menu-item-1195" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-ancestor current-menu-parent menu-item-has-children sub"><a href="#" class=""><span class="item_outer"><span class="item_text">Featured</span></span></a>
                                                            <ul>
                                                                <li id="nav-menu-item-1194" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/tour-list-standard/" class=""><span class="item_outer"><span class="item_text">Tour List Standard</span></span></a></li>
                                                                <li id="nav-menu-item-1409" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/tour-list-gallery/" class=""><span class="item_outer"><span class="item_text">Tour List Gallery</span></span></a></li>
                                                                <li id="nav-menu-item-1408" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/tour-list-masonry/" class=""><span class="item_outer"><span class="item_text">Tour List Masonry</span></span></a></li>
                                                                <li id="nav-menu-item-1419" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-1417 current_page_item "><a href="https://wanderers.mikado-themes.com/elements/tour-carousel/" class=""><span class="item_outer"><span class="item_text">Tour Carousel</span></span></a></li>
                                                                <li id="nav-menu-item-1539" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/tour-filter/" class=""><span class="item_outer"><span class="item_text">Tour Filter</span></span></a></li>
                                                                <li id="nav-menu-item-1499" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/destination-list-shortcode-2/" class=""><span class="item_outer"><span class="item_text">Destination List</span></span></a></li>
                                                                <li id="nav-menu-item-1190" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/parallax-sections/" class=""><span class="item_outer"><span class="item_text">Parallax Sections</span></span></a></li>
                                                                <li id="nav-menu-item-1189" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/video-button/" class=""><span class="item_outer"><span class="item_text">Video Button</span></span></a></li>
                                                            </ul>
                                                        </li>
                                                        <li id="nav-menu-item-1196" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children sub"><a href="#" class=""><span class="item_outer"><span class="item_text">Classic</span></span></a>
                                                            <ul>
                                                                <li id="nav-menu-item-1217" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/accordions-and-toogles/" class=""><span class="item_outer"><span class="item_text">Accordions &#038; Toogles</span></span></a></li>
                                                                <li id="nav-menu-item-1229" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/tabs/" class=""><span class="item_outer"><span class="item_text">Tabs</span></span></a></li>
                                                                <li id="nav-menu-item-1228" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/buttons/" class=""><span class="item_outer"><span class="item_text">Buttons</span></span></a></li>
                                                                <li id="nav-menu-item-1216" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/icon-with-text/" class=""><span class="item_outer"><span class="item_text">Icon With Text</span></span></a></li>
                                                                <li id="nav-menu-item-1839" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/icon-list/" class=""><span class="item_outer"><span class="item_text">Icon List</span></span></a></li>
                                                                <li id="nav-menu-item-1226" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/google-maps/" class=""><span class="item_outer"><span class="item_text">Google Maps</span></span></a></li>
                                                                <li id="nav-menu-item-1215" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/contact-form/" class=""><span class="item_outer"><span class="item_text">Contact Form</span></span></a></li>
                                                                <li id="nav-menu-item-1213" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/clients/" class=""><span class="item_outer"><span class="item_text">Clients</span></span></a></li>
                                                            </ul>
                                                        </li>
                                                        <li id="nav-menu-item-1197" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children sub"><a href="#" class=""><span class="item_outer"><span class="item_text">Infographic</span></span></a>
                                                            <ul>
                                                                <li id="nav-menu-item-1255" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/counters/" class=""><span class="item_outer"><span class="item_text">Counters</span></span></a></li>
                                                                <li id="nav-menu-item-1254" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/countdown/" class=""><span class="item_outer"><span class="item_text">Countdown</span></span></a></li>
                                                                <li id="nav-menu-item-1248" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/progress-bar/" class=""><span class="item_outer"><span class="item_text">Progress Bar</span></span></a></li>
                                                                <li id="nav-menu-item-1246" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/blog-list/" class=""><span class="item_outer"><span class="item_text">Blog List</span></span></a></li>
                                                                <li id="nav-menu-item-1192" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/team/" class=""><span class="item_outer"><span class="item_text">Team</span></span></a></li>
                                                                <li id="nav-menu-item-1214" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/top-reviews/" class=""><span class="item_outer"><span class="item_text">Top Reviews</span></span></a></li>
                                                                <li id="nav-menu-item-1971" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/call-to-action/" class=""><span class="item_outer"><span class="item_text">Call To Action</span></span></a></li>
                                                            </ul>
                                                        </li>
                                                        <li id="nav-menu-item-1198" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children sub"><a href="#" class=""><span class="item_outer"><span class="item_text">Typography</span></span></a>
                                                            <ul>
                                                                <li id="nav-menu-item-1279" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/headings/" class=""><span class="item_outer"><span class="item_text">Headings</span></span></a></li>
                                                                <li id="nav-menu-item-1278" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/columns/" class=""><span class="item_outer"><span class="item_text">Columns</span></span></a></li>
                                                                <li id="nav-menu-item-1277" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/blockquote/" class=""><span class="item_outer"><span class="item_text">Blockquote</span></span></a></li>
                                                                <li id="nav-menu-item-1276" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/dropcaps/" class=""><span class="item_outer"><span class="item_text">Dropcaps</span></span></a></li>
                                                                <li id="nav-menu-item-1275" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/highlights/" class=""><span class="item_outer"><span class="item_text">Highlights</span></span></a></li>
                                                                <li id="nav-menu-item-1274" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/separators/" class=""><span class="item_outer"><span class="item_text">Separators</span></span></a></li>
                                                                <li id="nav-menu-item-1273" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/title-subtitle/" class=""><span class="item_outer"><span class="item_text">Title &#038; Subtitle</span></span></a></li>
                                                                <li id="nav-menu-item-1272" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/custom-font/" class=""><span class="item_outer"><span class="item_text">Custom Font</span></span></a></li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </nav>

                                <div class="mkdf-shopping-cart-holder" style="padding: 0 13px 0px">
                                    <div class="mkdf-shopping-cart-inner">
                                        <a itemprop="url" class="mkdf-header-cart" href="https://wanderers.mikado-themes.com/cart/">
                                            <span class="mkdf-cart-icon icon_bag_alt"></span>
                                            <span class="mkdf-cart-number">0</span>
                                        </a>
                                        <div class="mkdf-shopping-cart-dropdown">
                                            <ul>
                                                <li class="mkdf-empty-cart">No products in the cart.</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>

                                <a style="margin: 0 21px 0px 14px;" class="mkdf-search-opener mkdf-icon-has-hover" href="javascript:void(0)">
                                        <span class="mkdf-search-opener-wrapper">
                <span class="mkdf-icon-font-elegant icon_search "></span> </span>
                                </a>

                                <a class="mkdf-side-menu-button-opener mkdf-icon-has-hover" href="javascript:void(0)">
                                        <span class="mkdf-side-menu-icon">
        		<span aria-hidden="true" class="mkdf-icon-font-elegant icon_menu " ></span> </span>
                                </a>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <div class="mkdf-sticky-header">
                <div class="mkdf-sticky-holder mkdf-menu-right">
                    <div class="mkdf-grid">
                        <div class="mkdf-vertical-align-containers">
                            <div class="mkdf-position-left">
                                <div class="mkdf-position-left-inner">

                                    <div class="mkdf-logo-wrapper">
                                        <a itemprop="url" href="https://wanderers.mikado-themes.com/" style="height: 41px;">
                                            <img itemprop="image" class="mkdf-normal-logo" src="https://wanderers.mikado-themes.com/wp-content/uploads/2018/02/logo-dark.png" width="256" height="82" alt="logo" />
                                            <img itemprop="image" class="mkdf-dark-logo" src="https://wanderers.mikado-themes.com/wp-content/uploads/2018/02/logo-dark.png" width="256" height="82" alt="dark logo" /> <img itemprop="image" class="mkdf-light-logo" src="https://wanderers.mikado-themes.com/wp-content/uploads/2018/02/logo-light.png" width="256" height="82" alt="light logo" /> </a>
                                    </div>

                                </div>
                            </div>
                            <div class="mkdf-position-right">
                                <div class="mkdf-position-right-inner">

                                    <nav class="mkdf-main-menu mkdf-drop-down mkdf-sticky-nav">
                                        <ul id="menu-main-menu-1" class="clearfix">
                                            <li id="sticky-nav-menu-item-34" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home  narrow"><a href="https://wanderers.mikado-themes.com/" class=""><span class="item_outer"><span class="item_text">Home</span><span class="plus"></span></span></a></li>
                                            <li id="sticky-nav-menu-item-40" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children  has_sub narrow"><a href="#" class=" no_link" onclick="JavaScript: return false;"><span class="item_outer"><span class="item_text">Pages</span><span class="plus"></span><i class="mkdf-menu-arrow fa fa-angle-down"></i></span></a>
                                                <div class="second">
                                                    <div class="inner">
                                                        <ul>
                                                            <li id="sticky-nav-menu-item-637" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/about-us/" class=""><span class="item_outer"><span class="item_text">About Us</span><span class="plus"></span></span></a></li>
                                                            <li id="sticky-nav-menu-item-914" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/our-team/" class=""><span class="item_outer"><span class="item_text">Our Team</span><span class="plus"></span></span></a></li>
                                                            <li id="sticky-nav-menu-item-815" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/contact/" class=""><span class="item_outer"><span class="item_text">Contact</span><span class="plus"></span></span></a></li>
                                                            <li id="sticky-nav-menu-item-915" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/faq/" class=""><span class="item_outer"><span class="item_text">FAQ</span><span class="plus"></span></span></a></li>
                                                            <li id="sticky-nav-menu-item-816" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/coming-soon/" class=""><span class="item_outer"><span class="item_text">Coming Soon</span><span class="plus"></span></span></a></li>
                                                            <li id="sticky-nav-menu-item-633" class="menu-item menu-item-type-custom menu-item-object-custom "><a href="https://wanderers.mikado-themes.com/404" class=""><span class="item_outer"><span class="item_text">404</span><span class="plus"></span></span></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </li>
                                            <li id="sticky-nav-menu-item-41" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children  has_sub narrow"><a href="#" class=" no_link" onclick="JavaScript: return false;"><span class="item_outer"><span class="item_text">Destinations</span><span class="plus"></span><i class="mkdf-menu-arrow fa fa-angle-down"></i></span></a>
                                                <div class="second">
                                                    <div class="inner">
                                                        <ul>
                                                            <li id="sticky-nav-menu-item-312" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/destination-list/" class=""><span class="item_outer"><span class="item_text">Destination List</span><span class="plus"></span></span></a></li>
                                                            <li id="sticky-nav-menu-item-42" class="menu-item menu-item-type-post_type menu-item-object-destinations "><a href="https://wanderers.mikado-themes.com/destinations/thailand/" class=""><span class="item_outer"><span class="item_text">Destination Item</span><span class="plus"></span></span></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </li>
                                            <li id="sticky-nav-menu-item-43" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children  has_sub narrow"><a href="#" class=" no_link" onclick="JavaScript: return false;"><span class="item_outer"><span class="item_text">Tours</span><span class="plus"></span><i class="mkdf-menu-arrow fa fa-angle-down"></i></span></a>
                                                <div class="second">
                                                    <div class="inner">
                                                        <ul>
                                                            <li id="sticky-nav-menu-item-86" class="menu-item menu-item-type-post_type menu-item-object-tour-item "><a href="https://wanderers.mikado-themes.com/tour-item/ultimate-thailand/" class=""><span class="item_outer"><span class="item_text">Tour Single</span><span class="plus"></span></span></a></li>
                                                            <li id="sticky-nav-menu-item-319" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children sub"><a href="#" class=""><span class="item_outer"><span class="item_text">Tour Lists</span><span class="plus"></span></span></a>
                                                                <ul>
                                                                    <li id="sticky-nav-menu-item-2273" class="menu-item menu-item-type-custom menu-item-object-custom "><a href="https://wanderers.mikado-themes.com/tour-search-page/?view_type=standard" class=""><span class="item_outer"><span class="item_text">Standard</span><span class="plus"></span></span></a></li>
                                                                    <li id="sticky-nav-menu-item-2274" class="menu-item menu-item-type-custom menu-item-object-custom "><a href="https://wanderers.mikado-themes.com/tour-search-page/?view_type=gallery" class=""><span class="item_outer"><span class="item_text">Gallery</span><span class="plus"></span></span></a></li>
                                                                    <li id="sticky-nav-menu-item-333" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/tour-list-carousel/" class=""><span class="item_outer"><span class="item_text">Carousel</span><span class="plus"></span></span></a></li>
                                                                    <li id="sticky-nav-menu-item-2275" class="menu-item menu-item-type-custom menu-item-object-custom "><a href="https://wanderers.mikado-themes.com/tour-search-page/?view_type=list" class=""><span class="item_outer"><span class="item_text">Search Page</span><span class="plus"></span></span></a></li>
                                                                </ul>
                                                            </li>
                                                            <li id="sticky-nav-menu-item-1281" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children sub"><a href="#" class=""><span class="item_outer"><span class="item_text">Dashboard</span><span class="plus"></span></span></a>
                                                                <ul>
                                                                    <li id="sticky-nav-menu-item-1284" class="menu-item menu-item-type-custom menu-item-object-custom "><a href="https://wanderers.mikado-themes.com/dashboard/?user-action=profile" class=""><span class="item_outer"><span class="item_text">My Profile</span><span class="plus"></span></span></a></li>
                                                                    <li id="sticky-nav-menu-item-1282" class="menu-item menu-item-type-custom menu-item-object-custom "><a href="https://wanderers.mikado-themes.com/dashboard/?user-action=edit-profile" class=""><span class="item_outer"><span class="item_text">Edit Profile</span><span class="plus"></span></span></a></li>
                                                                    <li id="sticky-nav-menu-item-1283" class="menu-item menu-item-type-custom menu-item-object-custom "><a href="https://wanderers.mikado-themes.com/dashboard/?user-action=my-bookings" class=""><span class="item_outer"><span class="item_text">My Bookings</span><span class="plus"></span></span></a></li>
                                                                </ul>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </li>
                                            <li id="sticky-nav-menu-item-45" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children  has_sub narrow"><a href="#" class=" no_link" onclick="JavaScript: return false;"><span class="item_outer"><span class="item_text">Blog</span><span class="plus"></span><i class="mkdf-menu-arrow fa fa-angle-down"></i></span></a>
                                                <div class="second">
                                                    <div class="inner">
                                                        <ul>
                                                            <li id="sticky-nav-menu-item-525" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/blog-list/" class=""><span class="item_outer"><span class="item_text">Standard List</span><span class="plus"></span></span></a></li>
                                                            <li id="sticky-nav-menu-item-625" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/blog-masonry/" class=""><span class="item_outer"><span class="item_text">Masonry List</span><span class="plus"></span></span></a></li>
                                                            <li id="sticky-nav-menu-item-516" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children sub"><a href="#" class=""><span class="item_outer"><span class="item_text">Single</span><span class="plus"></span></span></a>
                                                                <ul>
                                                                    <li id="sticky-nav-menu-item-515" class="menu-item menu-item-type-post_type menu-item-object-post "><a href="https://wanderers.mikado-themes.com/solo-traveler-learns-on-the-road/" class=""><span class="item_outer"><span class="item_text">Standard Post</span><span class="plus"></span></span></a></li>
                                                                    <li id="sticky-nav-menu-item-547" class="menu-item menu-item-type-post_type menu-item-object-post "><a href="https://wanderers.mikado-themes.com/new-ways-to-become-a-better-travel-blogger/" class=""><span class="item_outer"><span class="item_text">Gallery Post</span><span class="plus"></span></span></a></li>
                                                                    <li id="sticky-nav-menu-item-546" class="menu-item menu-item-type-post_type menu-item-object-post "><a href="https://wanderers.mikado-themes.com/use-your-social-network-to-travel/" class=""><span class="item_outer"><span class="item_text">Audio Post</span><span class="plus"></span></span></a></li>
                                                                    <li id="sticky-nav-menu-item-543" class="menu-item menu-item-type-post_type menu-item-object-post "><a href="https://wanderers.mikado-themes.com/travel-the-world-in-only-20-days/" class=""><span class="item_outer"><span class="item_text">Video Post</span><span class="plus"></span></span></a></li>
                                                                    <li id="sticky-nav-menu-item-544" class="menu-item menu-item-type-post_type menu-item-object-post "><a href="https://wanderers.mikado-themes.com/best-designed-wordpress-themes/" class=""><span class="item_outer"><span class="item_text">Link Post</span><span class="plus"></span></span></a></li>
                                                                    <li id="sticky-nav-menu-item-545" class="menu-item menu-item-type-post_type menu-item-object-post "><a href="https://wanderers.mikado-themes.com/marc-nicolaus-richmond/" class=""><span class="item_outer"><span class="item_text">Quote Post</span><span class="plus"></span></span></a></li>
                                                                </ul>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </li>
                                            <li id="sticky-nav-menu-item-46" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children  has_sub narrow"><a href="#" class=" no_link" onclick="JavaScript: return false;"><span class="item_outer"><span class="item_text">Shop</span><span class="plus"></span><i class="mkdf-menu-arrow fa fa-angle-down"></i></span></a>
                                                <div class="second">
                                                    <div class="inner">
                                                        <ul>
                                                            <li id="sticky-nav-menu-item-39" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/shop/" class=""><span class="item_outer"><span class="item_text">List</span><span class="plus"></span></span></a></li>
                                                            <li id="sticky-nav-menu-item-362" class="menu-item menu-item-type-post_type menu-item-object-product "><a href="https://wanderers.mikado-themes.com/shop/biking-helmet/" class=""><span class="item_outer"><span class="item_text">Single</span><span class="plus"></span></span></a></li>
                                                            <li id="sticky-nav-menu-item-47" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children sub"><a href="#" class=""><span class="item_outer"><span class="item_text">Shop Pages</span><span class="plus"></span></span></a>
                                                                <ul>
                                                                    <li id="sticky-nav-menu-item-36" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/my-account/" class=""><span class="item_outer"><span class="item_text">My account</span><span class="plus"></span></span></a></li>
                                                                    <li id="sticky-nav-menu-item-37" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/checkout/" class=""><span class="item_outer"><span class="item_text">Checkout</span><span class="plus"></span></span></a></li>
                                                                    <li id="sticky-nav-menu-item-38" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/cart/" class=""><span class="item_outer"><span class="item_text">Cart</span><span class="plus"></span></span></a></li>
                                                                </ul>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </li>
                                            <li id="sticky-nav-menu-item-44" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-ancestor menu-item-has-children mkdf-active-item has_sub wide"><a href="#" class=" current  no_link" onclick="JavaScript: return false;"><span class="item_outer"><span class="item_text">Elements</span><span class="plus"></span><i class="mkdf-menu-arrow fa fa-angle-down"></i></span></a>
                                                <div class="second">
                                                    <div class="inner">
                                                        <ul>
                                                            <li id="sticky-nav-menu-item-1195" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-ancestor current-menu-parent menu-item-has-children sub"><a href="#" class=""><span class="item_outer"><span class="item_text">Featured</span><span class="plus"></span></span></a>
                                                                <ul>
                                                                    <li id="sticky-nav-menu-item-1194" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/tour-list-standard/" class=""><span class="item_outer"><span class="item_text">Tour List Standard</span><span class="plus"></span></span></a></li>
                                                                    <li id="sticky-nav-menu-item-1409" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/tour-list-gallery/" class=""><span class="item_outer"><span class="item_text">Tour List Gallery</span><span class="plus"></span></span></a></li>
                                                                    <li id="sticky-nav-menu-item-1408" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/tour-list-masonry/" class=""><span class="item_outer"><span class="item_text">Tour List Masonry</span><span class="plus"></span></span></a></li>
                                                                    <li id="sticky-nav-menu-item-1419" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-1417 current_page_item "><a href="https://wanderers.mikado-themes.com/elements/tour-carousel/" class=""><span class="item_outer"><span class="item_text">Tour Carousel</span><span class="plus"></span></span></a></li>
                                                                    <li id="sticky-nav-menu-item-1539" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/tour-filter/" class=""><span class="item_outer"><span class="item_text">Tour Filter</span><span class="plus"></span></span></a></li>
                                                                    <li id="sticky-nav-menu-item-1499" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/destination-list-shortcode-2/" class=""><span class="item_outer"><span class="item_text">Destination List</span><span class="plus"></span></span></a></li>
                                                                    <li id="sticky-nav-menu-item-1190" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/parallax-sections/" class=""><span class="item_outer"><span class="item_text">Parallax Sections</span><span class="plus"></span></span></a></li>
                                                                    <li id="sticky-nav-menu-item-1189" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/video-button/" class=""><span class="item_outer"><span class="item_text">Video Button</span><span class="plus"></span></span></a></li>
                                                                </ul>
                                                            </li>
                                                            <li id="sticky-nav-menu-item-1196" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children sub"><a href="#" class=""><span class="item_outer"><span class="item_text">Classic</span><span class="plus"></span></span></a>
                                                                <ul>
                                                                    <li id="sticky-nav-menu-item-1217" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/accordions-and-toogles/" class=""><span class="item_outer"><span class="item_text">Accordions &#038; Toogles</span><span class="plus"></span></span></a></li>
                                                                    <li id="sticky-nav-menu-item-1229" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/tabs/" class=""><span class="item_outer"><span class="item_text">Tabs</span><span class="plus"></span></span></a></li>
                                                                    <li id="sticky-nav-menu-item-1228" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/buttons/" class=""><span class="item_outer"><span class="item_text">Buttons</span><span class="plus"></span></span></a></li>
                                                                    <li id="sticky-nav-menu-item-1216" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/icon-with-text/" class=""><span class="item_outer"><span class="item_text">Icon With Text</span><span class="plus"></span></span></a></li>
                                                                    <li id="sticky-nav-menu-item-1839" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/icon-list/" class=""><span class="item_outer"><span class="item_text">Icon List</span><span class="plus"></span></span></a></li>
                                                                    <li id="sticky-nav-menu-item-1226" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/google-maps/" class=""><span class="item_outer"><span class="item_text">Google Maps</span><span class="plus"></span></span></a></li>
                                                                    <li id="sticky-nav-menu-item-1215" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/contact-form/" class=""><span class="item_outer"><span class="item_text">Contact Form</span><span class="plus"></span></span></a></li>
                                                                    <li id="sticky-nav-menu-item-1213" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/clients/" class=""><span class="item_outer"><span class="item_text">Clients</span><span class="plus"></span></span></a></li>
                                                                </ul>
                                                            </li>
                                                            <li id="sticky-nav-menu-item-1197" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children sub"><a href="#" class=""><span class="item_outer"><span class="item_text">Infographic</span><span class="plus"></span></span></a>
                                                                <ul>
                                                                    <li id="sticky-nav-menu-item-1255" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/counters/" class=""><span class="item_outer"><span class="item_text">Counters</span><span class="plus"></span></span></a></li>
                                                                    <li id="sticky-nav-menu-item-1254" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/countdown/" class=""><span class="item_outer"><span class="item_text">Countdown</span><span class="plus"></span></span></a></li>
                                                                    <li id="sticky-nav-menu-item-1248" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/progress-bar/" class=""><span class="item_outer"><span class="item_text">Progress Bar</span><span class="plus"></span></span></a></li>
                                                                    <li id="sticky-nav-menu-item-1246" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/blog-list/" class=""><span class="item_outer"><span class="item_text">Blog List</span><span class="plus"></span></span></a></li>
                                                                    <li id="sticky-nav-menu-item-1192" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/team/" class=""><span class="item_outer"><span class="item_text">Team</span><span class="plus"></span></span></a></li>
                                                                    <li id="sticky-nav-menu-item-1214" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/top-reviews/" class=""><span class="item_outer"><span class="item_text">Top Reviews</span><span class="plus"></span></span></a></li>
                                                                    <li id="sticky-nav-menu-item-1971" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/call-to-action/" class=""><span class="item_outer"><span class="item_text">Call To Action</span><span class="plus"></span></span></a></li>
                                                                </ul>
                                                            </li>
                                                            <li id="sticky-nav-menu-item-1198" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children sub"><a href="#" class=""><span class="item_outer"><span class="item_text">Typography</span><span class="plus"></span></span></a>
                                                                <ul>
                                                                    <li id="sticky-nav-menu-item-1279" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/headings/" class=""><span class="item_outer"><span class="item_text">Headings</span><span class="plus"></span></span></a></li>
                                                                    <li id="sticky-nav-menu-item-1278" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/columns/" class=""><span class="item_outer"><span class="item_text">Columns</span><span class="plus"></span></span></a></li>
                                                                    <li id="sticky-nav-menu-item-1277" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/blockquote/" class=""><span class="item_outer"><span class="item_text">Blockquote</span><span class="plus"></span></span></a></li>
                                                                    <li id="sticky-nav-menu-item-1276" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/dropcaps/" class=""><span class="item_outer"><span class="item_text">Dropcaps</span><span class="plus"></span></span></a></li>
                                                                    <li id="sticky-nav-menu-item-1275" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/highlights/" class=""><span class="item_outer"><span class="item_text">Highlights</span><span class="plus"></span></span></a></li>
                                                                    <li id="sticky-nav-menu-item-1274" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/separators/" class=""><span class="item_outer"><span class="item_text">Separators</span><span class="plus"></span></span></a></li>
                                                                    <li id="sticky-nav-menu-item-1273" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/title-subtitle/" class=""><span class="item_outer"><span class="item_text">Title &#038; Subtitle</span><span class="plus"></span></span></a></li>
                                                                    <li id="sticky-nav-menu-item-1272" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/custom-font/" class=""><span class="item_outer"><span class="item_text">Custom Font</span><span class="plus"></span></span></a></li>
                                                                </ul>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </nav>

                                    <div class="mkdf-shopping-cart-holder" style="padding: 0 13px 0px">
                                        <div class="mkdf-shopping-cart-inner">
                                            <a itemprop="url" class="mkdf-header-cart" href="https://wanderers.mikado-themes.com/cart/">
                                                <span class="mkdf-cart-icon icon_bag_alt"></span>
                                                <span class="mkdf-cart-number">0</span>
                                            </a>
                                            <div class="mkdf-shopping-cart-dropdown">
                                                <ul>
                                                    <li class="mkdf-empty-cart">No products in the cart.</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>

                                    <a style="margin: 0 21px 0px 14px;" class="mkdf-search-opener mkdf-icon-has-hover" href="javascript:void(0)">
                                            <span class="mkdf-search-opener-wrapper">
                <span class="mkdf-icon-font-elegant icon_search "></span> </span>
                                    </a>

                                    <a class="mkdf-side-menu-button-opener mkdf-icon-has-hover" href="javascript:void(0)">
                                            <span class="mkdf-side-menu-icon">
        		<span aria-hidden="true" class="mkdf-icon-font-elegant icon_menu " ></span> </span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <form action="https://wanderers.mikado-themes.com/" class="mkdf-search-cover" method="get">
                <div class="mkdf-container">
                    <div class="mkdf-container-inner clearfix">
                        <div class="mkdf-form-holder-outer">
                            <div class="mkdf-form-holder">
                                <div class="mkdf-form-holder-inner">
                                    <span class="icon_search"></span>
                                    <input type="text" placeholder="Search on site..." name="s" class="mkdf_search_field" autocomplete="off" />
                                    <a class="mkdf-search-close" href="#">
                                        <span aria-hidden="true" class="mkdf-icon-font-elegant icon_close "></span> </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </header>

        <header class="mkdf-mobile-header">

            <div class="mkdf-mobile-header-inner">
                <div class="mkdf-mobile-header-holder">
                    <div class="mkdf-grid">
                        <div class="mkdf-vertical-align-containers">
                            <div class="mkdf-vertical-align-containers">
                                <div class="mkdf-mobile-menu-opener">
                                    <a href="javascript:void(0)">
                                            <span class="mkdf-mobile-menu-icon">
										<span aria-hidden="true" class="mkdf-icon-font-elegant icon_menu " ></span> </span>
                                    </a>
                                </div>
                                <div class="mkdf-position-center">
                                    <div class="mkdf-position-center-inner">

                                        <div class="mkdf-mobile-logo-wrapper">
                                            <a itemprop="url" href="https://wanderers.mikado-themes.com/" style="height: 41px">
                                                <img itemprop="image" src="https://wanderers.mikado-themes.com/wp-content/uploads/2018/02/logo-dark.png" width="256" height="82" alt="Mobile Logo" />
                                            </a>
                                        </div>

                                    </div>
                                </div>
                                <div class="mkdf-position-right">
                                    <div class="mkdf-position-right-inner">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <nav class="mkdf-mobile-nav">
                    <div class="mkdf-grid">
                        <ul id="menu-main-menu-2" class="">
                            <li id="mobile-menu-item-34" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home "><a href="https://wanderers.mikado-themes.com/" class=""><span>Home</span></a></li>
                            <li id="mobile-menu-item-40" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children  has_sub">
                                <h6><span>Pages</span></h6><span class="mobile_arrow"><i class="mkdf-sub-arrow fa fa-angle-right"></i><i class="fa fa-angle-down"></i></span>
                                <ul class="sub_menu">
                                    <li id="mobile-menu-item-637" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/about-us/" class=""><span>About Us</span></a></li>
                                    <li id="mobile-menu-item-914" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/our-team/" class=""><span>Our Team</span></a></li>
                                    <li id="mobile-menu-item-815" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/contact/" class=""><span>Contact</span></a></li>
                                    <li id="mobile-menu-item-915" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/faq/" class=""><span>FAQ</span></a></li>
                                    <li id="mobile-menu-item-816" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/coming-soon/" class=""><span>Coming Soon</span></a></li>
                                    <li id="mobile-menu-item-633" class="menu-item menu-item-type-custom menu-item-object-custom "><a href="https://wanderers.mikado-themes.com/404" class=""><span>404</span></a></li>
                                </ul>
                            </li>
                            <li id="mobile-menu-item-41" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children  has_sub">
                                <h6><span>Destinations</span></h6><span class="mobile_arrow"><i class="mkdf-sub-arrow fa fa-angle-right"></i><i class="fa fa-angle-down"></i></span>
                                <ul class="sub_menu">
                                    <li id="mobile-menu-item-312" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/destination-list/" class=""><span>Destination List</span></a></li>
                                    <li id="mobile-menu-item-42" class="menu-item menu-item-type-post_type menu-item-object-destinations "><a href="https://wanderers.mikado-themes.com/destinations/thailand/" class=""><span>Destination Item</span></a></li>
                                </ul>
                            </li>
                            <li id="mobile-menu-item-43" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children  has_sub">
                                <h6><span>Tours</span></h6><span class="mobile_arrow"><i class="mkdf-sub-arrow fa fa-angle-right"></i><i class="fa fa-angle-down"></i></span>
                                <ul class="sub_menu">
                                    <li id="mobile-menu-item-86" class="menu-item menu-item-type-post_type menu-item-object-tour-item "><a href="https://wanderers.mikado-themes.com/tour-item/ultimate-thailand/" class=""><span>Tour Single</span></a></li>
                                    <li id="mobile-menu-item-319" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children  has_sub"><a href="#" class=" mkdf-mobile-no-link"><span>Tour Lists</span></a><span class="mobile_arrow"><i class="mkdf-sub-arrow fa fa-angle-right"></i><i class="fa fa-angle-down"></i></span>
                                        <ul class="sub_menu">
                                            <li id="mobile-menu-item-2273" class="menu-item menu-item-type-custom menu-item-object-custom "><a href="https://wanderers.mikado-themes.com/tour-search-page/?view_type=standard" class=""><span>Standard</span></a></li>
                                            <li id="mobile-menu-item-2274" class="menu-item menu-item-type-custom menu-item-object-custom "><a href="https://wanderers.mikado-themes.com/tour-search-page/?view_type=gallery" class=""><span>Gallery</span></a></li>
                                            <li id="mobile-menu-item-333" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/tour-list-carousel/" class=""><span>Carousel</span></a></li>
                                            <li id="mobile-menu-item-2275" class="menu-item menu-item-type-custom menu-item-object-custom "><a href="https://wanderers.mikado-themes.com/tour-search-page/?view_type=list" class=""><span>Search Page</span></a></li>
                                        </ul>
                                    </li>
                                    <li id="mobile-menu-item-1281" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children  has_sub"><a href="#" class=" mkdf-mobile-no-link"><span>Dashboard</span></a><span class="mobile_arrow"><i class="mkdf-sub-arrow fa fa-angle-right"></i><i class="fa fa-angle-down"></i></span>
                                        <ul class="sub_menu">
                                            <li id="mobile-menu-item-1284" class="menu-item menu-item-type-custom menu-item-object-custom "><a href="https://wanderers.mikado-themes.com/dashboard/?user-action=profile" class=""><span>My Profile</span></a></li>
                                            <li id="mobile-menu-item-1282" class="menu-item menu-item-type-custom menu-item-object-custom "><a href="https://wanderers.mikado-themes.com/dashboard/?user-action=edit-profile" class=""><span>Edit Profile</span></a></li>
                                            <li id="mobile-menu-item-1283" class="menu-item menu-item-type-custom menu-item-object-custom "><a href="https://wanderers.mikado-themes.com/dashboard/?user-action=my-bookings" class=""><span>My Bookings</span></a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <li id="mobile-menu-item-45" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children  has_sub">
                                <h6><span>Blog</span></h6><span class="mobile_arrow"><i class="mkdf-sub-arrow fa fa-angle-right"></i><i class="fa fa-angle-down"></i></span>
                                <ul class="sub_menu">
                                    <li id="mobile-menu-item-525" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/blog-list/" class=""><span>Standard List</span></a></li>
                                    <li id="mobile-menu-item-625" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/blog-masonry/" class=""><span>Masonry List</span></a></li>
                                    <li id="mobile-menu-item-516" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children  has_sub"><a href="#" class=" mkdf-mobile-no-link"><span>Single</span></a><span class="mobile_arrow"><i class="mkdf-sub-arrow fa fa-angle-right"></i><i class="fa fa-angle-down"></i></span>
                                        <ul class="sub_menu">
                                            <li id="mobile-menu-item-515" class="menu-item menu-item-type-post_type menu-item-object-post "><a href="https://wanderers.mikado-themes.com/solo-traveler-learns-on-the-road/" class=""><span>Standard Post</span></a></li>
                                            <li id="mobile-menu-item-547" class="menu-item menu-item-type-post_type menu-item-object-post "><a href="https://wanderers.mikado-themes.com/new-ways-to-become-a-better-travel-blogger/" class=""><span>Gallery Post</span></a></li>
                                            <li id="mobile-menu-item-546" class="menu-item menu-item-type-post_type menu-item-object-post "><a href="https://wanderers.mikado-themes.com/use-your-social-network-to-travel/" class=""><span>Audio Post</span></a></li>
                                            <li id="mobile-menu-item-543" class="menu-item menu-item-type-post_type menu-item-object-post "><a href="https://wanderers.mikado-themes.com/travel-the-world-in-only-20-days/" class=""><span>Video Post</span></a></li>
                                            <li id="mobile-menu-item-544" class="menu-item menu-item-type-post_type menu-item-object-post "><a href="https://wanderers.mikado-themes.com/best-designed-wordpress-themes/" class=""><span>Link Post</span></a></li>
                                            <li id="mobile-menu-item-545" class="menu-item menu-item-type-post_type menu-item-object-post "><a href="https://wanderers.mikado-themes.com/marc-nicolaus-richmond/" class=""><span>Quote Post</span></a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <li id="mobile-menu-item-46" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children  has_sub">
                                <h6><span>Shop</span></h6><span class="mobile_arrow"><i class="mkdf-sub-arrow fa fa-angle-right"></i><i class="fa fa-angle-down"></i></span>
                                <ul class="sub_menu">
                                    <li id="mobile-menu-item-39" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/shop/" class=""><span>List</span></a></li>
                                    <li id="mobile-menu-item-362" class="menu-item menu-item-type-post_type menu-item-object-product "><a href="https://wanderers.mikado-themes.com/shop/biking-helmet/" class=""><span>Single</span></a></li>
                                    <li id="mobile-menu-item-47" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children  has_sub"><a href="#" class=" mkdf-mobile-no-link"><span>Shop Pages</span></a><span class="mobile_arrow"><i class="mkdf-sub-arrow fa fa-angle-right"></i><i class="fa fa-angle-down"></i></span>
                                        <ul class="sub_menu">
                                            <li id="mobile-menu-item-36" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/my-account/" class=""><span>My account</span></a></li>
                                            <li id="mobile-menu-item-37" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/checkout/" class=""><span>Checkout</span></a></li>
                                            <li id="mobile-menu-item-38" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/cart/" class=""><span>Cart</span></a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <li id="mobile-menu-item-44" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-ancestor menu-item-has-children mkdf-active-item has_sub">
                                <h6><span>Elements</span></h6><span class="mobile_arrow"><i class="mkdf-sub-arrow fa fa-angle-right"></i><i class="fa fa-angle-down"></i></span>
                                <ul class="sub_menu">
                                    <li id="mobile-menu-item-1195" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-ancestor current-menu-parent menu-item-has-children  has_sub"><a href="#" class=" mkdf-mobile-no-link"><span>Featured</span></a><span class="mobile_arrow"><i class="mkdf-sub-arrow fa fa-angle-right"></i><i class="fa fa-angle-down"></i></span>
                                        <ul class="sub_menu">
                                            <li id="mobile-menu-item-1194" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/tour-list-standard/" class=""><span>Tour List Standard</span></a></li>
                                            <li id="mobile-menu-item-1409" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/tour-list-gallery/" class=""><span>Tour List Gallery</span></a></li>
                                            <li id="mobile-menu-item-1408" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/tour-list-masonry/" class=""><span>Tour List Masonry</span></a></li>
                                            <li id="mobile-menu-item-1419" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-1417 current_page_item "><a href="https://wanderers.mikado-themes.com/elements/tour-carousel/" class=""><span>Tour Carousel</span></a></li>
                                            <li id="mobile-menu-item-1539" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/tour-filter/" class=""><span>Tour Filter</span></a></li>
                                            <li id="mobile-menu-item-1499" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/destination-list-shortcode-2/" class=""><span>Destination List</span></a></li>
                                            <li id="mobile-menu-item-1190" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/parallax-sections/" class=""><span>Parallax Sections</span></a></li>
                                            <li id="mobile-menu-item-1189" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/video-button/" class=""><span>Video Button</span></a></li>
                                        </ul>
                                    </li>
                                    <li id="mobile-menu-item-1196" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children  has_sub"><a href="#" class=" mkdf-mobile-no-link"><span>Classic</span></a><span class="mobile_arrow"><i class="mkdf-sub-arrow fa fa-angle-right"></i><i class="fa fa-angle-down"></i></span>
                                        <ul class="sub_menu">
                                            <li id="mobile-menu-item-1217" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/accordions-and-toogles/" class=""><span>Accordions &#038; Toogles</span></a></li>
                                            <li id="mobile-menu-item-1229" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/tabs/" class=""><span>Tabs</span></a></li>
                                            <li id="mobile-menu-item-1228" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/buttons/" class=""><span>Buttons</span></a></li>
                                            <li id="mobile-menu-item-1216" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/icon-with-text/" class=""><span>Icon With Text</span></a></li>
                                            <li id="mobile-menu-item-1839" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/icon-list/" class=""><span>Icon List</span></a></li>
                                            <li id="mobile-menu-item-1226" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/google-maps/" class=""><span>Google Maps</span></a></li>
                                            <li id="mobile-menu-item-1215" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/contact-form/" class=""><span>Contact Form</span></a></li>
                                            <li id="mobile-menu-item-1213" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/clients/" class=""><span>Clients</span></a></li>
                                        </ul>
                                    </li>
                                    <li id="mobile-menu-item-1197" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children  has_sub"><a href="#" class=" mkdf-mobile-no-link"><span>Infographic</span></a><span class="mobile_arrow"><i class="mkdf-sub-arrow fa fa-angle-right"></i><i class="fa fa-angle-down"></i></span>
                                        <ul class="sub_menu">
                                            <li id="mobile-menu-item-1255" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/counters/" class=""><span>Counters</span></a></li>
                                            <li id="mobile-menu-item-1254" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/countdown/" class=""><span>Countdown</span></a></li>
                                            <li id="mobile-menu-item-1248" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/progress-bar/" class=""><span>Progress Bar</span></a></li>
                                            <li id="mobile-menu-item-1246" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/blog-list/" class=""><span>Blog List</span></a></li>
                                            <li id="mobile-menu-item-1192" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/team/" class=""><span>Team</span></a></li>
                                            <li id="mobile-menu-item-1214" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/top-reviews/" class=""><span>Top Reviews</span></a></li>
                                            <li id="mobile-menu-item-1971" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/call-to-action/" class=""><span>Call To Action</span></a></li>
                                        </ul>
                                    </li>
                                    <li id="mobile-menu-item-1198" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children  has_sub"><a href="#" class=" mkdf-mobile-no-link"><span>Typography</span></a><span class="mobile_arrow"><i class="mkdf-sub-arrow fa fa-angle-right"></i><i class="fa fa-angle-down"></i></span>
                                        <ul class="sub_menu">
                                            <li id="mobile-menu-item-1279" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/headings/" class=""><span>Headings</span></a></li>
                                            <li id="mobile-menu-item-1278" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/columns/" class=""><span>Columns</span></a></li>
                                            <li id="mobile-menu-item-1277" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/blockquote/" class=""><span>Blockquote</span></a></li>
                                            <li id="mobile-menu-item-1276" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/dropcaps/" class=""><span>Dropcaps</span></a></li>
                                            <li id="mobile-menu-item-1275" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/highlights/" class=""><span>Highlights</span></a></li>
                                            <li id="mobile-menu-item-1274" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/separators/" class=""><span>Separators</span></a></li>
                                            <li id="mobile-menu-item-1273" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/title-subtitle/" class=""><span>Title &#038; Subtitle</span></a></li>
                                            <li id="mobile-menu-item-1272" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="https://wanderers.mikado-themes.com/elements/custom-font/" class=""><span>Custom Font</span></a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </nav>

            </div>

            <form action="https://wanderers.mikado-themes.com/" class="mkdf-search-cover" method="get">
                <div class="mkdf-container">
                    <div class="mkdf-container-inner clearfix">
                        <div class="mkdf-form-holder-outer">
                            <div class="mkdf-form-holder">
                                <div class="mkdf-form-holder-inner">
                                    <span class="icon_search"></span>
                                    <input type="text" placeholder="Search on site..." name="s" class="mkdf_search_field" autocomplete="off" />
                                    <a class="mkdf-search-close" href="#">
                                        <span aria-hidden="true" class="mkdf-icon-font-elegant icon_close "></span> </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </header>

        <a id='mkdf-back-to-top' href='#'>
                <span class="mkdf-icon-stack">
                    <i class="mkdf-icon-font-awesome fa fa-angle-up "></i>                </span>
            <span class="mkdf-icon-stack mkdf-btt-text">
                    Top                </span>
        </a>

        <div class="mkdf-content" style="margin-top: -80px">
            <div class="mkdf-content-inner">
                <div class="mkdf-title-holder mkdf-centered-type mkdf-title-va-window-top mkdf-preload-background mkdf-has-bg-image mkdf-bg-parallax" style="height: 515px;background-color: #303030;background-image:url(https://wanderers.mikado-themes.com/wp-content/uploads/2018/02/title-img.jpg);" data-height="435">
                    <div class="mkdf-title-image">
                        <img itemprop="image" src="https://wanderers.mikado-themes.com/wp-content/uploads/2018/02/title-img.jpg" alt="a" />
                    </div>
                    <div class="mkdf-title-wrapper">
                        <div class="mkdf-title-inner">
                            <div class="mkdf-grid">
                                <h1 class="mkdf-page-title entry-title">Tour Carousel</h1>
                                <h6 class="mkdf-page-subtitle" style="padding: 0 20%">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris lobortis, nonimper diet est. Praesent vulputate at enim sit amet mattis.</h6>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="mkdf-full-width">
                    <div class="mkdf-full-width-inner">
                        <div class="mkdf-grid-row">
                            <div class="mkdf-page-content-holder mkdf-grid-col-12">
                                <div class="mkdf-row-grid-section-wrapper ">
                                    <div class="mkdf-row-grid-section">
                                        <div class="vc_row wpb_row vc_row-fluid vc_custom_1521216884465">
                                            <div class="wpb_column vc_column_container vc_col-sm-12">
                                                <div class="vc_column-inner ">
                                                    <div class="wpb_wrapper">
                                                        <div class="mkdf-tours-carousel-holder mkdf-carousel-pagination">
                                                            <div class="mkdf-tours-row mkdf-normal-space">
                                                                <div class="mkdf-tours-carousel mkdf-tours-row-inner-holder mkdf-owl-slider" data-number-of-items="3" data-enable-loop="yes" data-enable-navigation="no" data-enable-pagination="yes">

                                                                    <div class="mkdf-tours-standard-item mkdf-tours-row-item mkdf-item-space post-57 tour-item type-tour-item status-publish has-post-thumbnail hentry tour-category-seaside tour-attribute-accommodation tour-attribute-guide tour-attribute-insurance tour-attribute-meals">
                                                                        <div class="mkdf-tours-standard-item-image-holder">
                                                                            <a href="https://wanderers.mikado-themes.com/tour-item/ultimate-thailand/">
                                                                                <img width="800" height="550" src="https://wanderers.mikado-themes.com/wp-content/uploads/2018/02/tour-1-featured-img-800x550.jpg" class="attachment-wanderers_mkdf_landscape size-wanderers_mkdf_landscape" alt="a" /> </a>
                                                                            <span class="mkdf-tours-standard-item-label-holder">

		<span class="mkdf-tour-item-label">
			<span class="mkdf-tour-item-label-inner">
				fun			</span>
                                                                                </span>

                                                                                </span>
                                                                        </div>

                                                                        <div class="mkdf-tours-standard-item-content-holder">
                                                                            <div class="mkdf-tours-standard-item-content-inner">
                                                                                <div class="mkdf-tours-standard-item-title-price-holder">
                                                                                    <h4 class="mkdf-tour-title">
                                                                                        <a href="https://wanderers.mikado-themes.com/tour-item/ultimate-thailand/">Ultimate Thailand</a>
                                                                                    </h4>
                                                                                    <span class="mkdf-tours-standard-item-price-holder">

		<span class="mkdf-tours-price-holder">
							<span class="mkdf-tours-item-price ">$1350</span>
                                                                                        </span>

                                                                                        <span class="mkdf-tours-item-price-additional-text">
                        / per person                    </span>
                                                                                        </span>
                                                                                </div>

                                                                                <div class="mkdf-tours-standard-item-excerpt">
                                                                                    Lorem ipsum dolor sit amet, conse tetur adipiscing elit. Mauris a rutrum&hellip; </div>
                                                                            </div>
                                                                            <div class="mkdf-tours-standard-item-bottom-content">
                                                                                <div class="mkdf-tours-standard-item-bottom-item">

                                                                                    <div class="mkdf-tour-duration-holder">
                                                                                            <span class="mkdf-tour-duration-icon mkdf-tour-info-icon">
					<span class="icon_clock"></span>
                                                                                            </span>
                                                                                        <span class="mkdf-tour-info-label">
					12 Days  11 Nights				</span>
                                                                                    </div>

                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="mkdf-tours-standard-item mkdf-tours-row-item mkdf-item-space post-1036 tour-item type-tour-item status-publish has-post-thumbnail hentry tour-category-adventure review-criteria-accommodation review-criteria-destination review-criteria-meals review-criteria-transport review-criteria-value_for_money tour-attribute-accommodation tour-attribute-guide tour-attribute-insurance tour-attribute-meals">
                                                                        <div class="mkdf-tours-standard-item-image-holder">
                                                                            <a href="https://wanderers.mikado-themes.com/tour-item/discover-india/">
                                                                                <img width="800" height="550" src="https://wanderers.mikado-themes.com/wp-content/uploads/2018/03/tour-2-featured-img-800x550.jpg" class="attachment-wanderers_mkdf_landscape size-wanderers_mkdf_landscape" alt="a" /> </a>
                                                                            <span class="mkdf-tours-standard-item-label-holder">

		<span class="mkdf-tour-item-label">
			<span class="mkdf-tour-item-label-inner">
							</span>
                                                                                </span>

                                                                                </span>
                                                                        </div>

                                                                        <div class="mkdf-tours-standard-item-content-holder">
                                                                            <div class="mkdf-tours-standard-item-content-inner">
                                                                                <div class="mkdf-tours-standard-item-title-price-holder">
                                                                                    <h4 class="mkdf-tour-title">
                                                                                        <a href="https://wanderers.mikado-themes.com/tour-item/discover-india/">Discover India</a>
                                                                                    </h4>
                                                                                    <span class="mkdf-tours-standard-item-price-holder">

		<span class="mkdf-tours-price-holder">
							<span class="mkdf-tours-item-price ">$2650</span>
                                                                                        </span>

                                                                                        <span class="mkdf-tours-item-price-additional-text">
                        / per person                    </span>
                                                                                        </span>
                                                                                </div>

                                                                                <div class="mkdf-tours-standard-item-excerpt">
                                                                                    Lorem ipsum dolor sit amet, conse tetur adipiscing elit. Mauris a rutrum&hellip; </div>
                                                                            </div>
                                                                            <div class="mkdf-tours-standard-item-bottom-content">
                                                                                <div class="mkdf-tours-standard-item-bottom-item">

                                                                                    <div class="mkdf-tour-duration-holder">
                                                                                            <span class="mkdf-tour-duration-icon mkdf-tour-info-icon">
					<span class="icon_clock"></span>
                                                                                            </span>
                                                                                        <span class="mkdf-tour-info-label">
					10 Days  9 Nights				</span>
                                                                                    </div>

                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="mkdf-tours-standard-item mkdf-tours-row-item mkdf-item-space post-1039 tour-item type-tour-item status-publish has-post-thumbnail hentry tour-category-adventure review-criteria-accommodation review-criteria-destination review-criteria-meals review-criteria-transport review-criteria-value_for_money tour-attribute-accommodation tour-attribute-guide tour-attribute-insurance tour-attribute-meals">
                                                                        <div class="mkdf-tours-standard-item-image-holder">
                                                                            <a href="https://wanderers.mikado-themes.com/tour-item/forest-adventure/">
                                                                                <img width="800" height="550" src="https://wanderers.mikado-themes.com/wp-content/uploads/2018/03/tour-3-featured-img-800x550.jpg" class="attachment-wanderers_mkdf_landscape size-wanderers_mkdf_landscape" alt="a" /> </a>
                                                                            <span class="mkdf-tours-standard-item-label-holder">

		<span class="mkdf-tour-item-label">
			<span class="mkdf-tour-item-label-inner">
							</span>
                                                                                </span>

                                                                                </span>
                                                                        </div>

                                                                        <div class="mkdf-tours-standard-item-content-holder">
                                                                            <div class="mkdf-tours-standard-item-content-inner">
                                                                                <div class="mkdf-tours-standard-item-title-price-holder">
                                                                                    <h4 class="mkdf-tour-title">
                                                                                        <a href="https://wanderers.mikado-themes.com/tour-item/forest-adventure/">Forest Adventure</a>
                                                                                    </h4>
                                                                                    <span class="mkdf-tours-standard-item-price-holder">

		<span class="mkdf-tours-price-holder">
							<span class="mkdf-tours-item-price ">$1350</span>
                                                                                        </span>

                                                                                        <span class="mkdf-tours-item-price-additional-text">
                        / per person                    </span>
                                                                                        </span>
                                                                                </div>

                                                                                <div class="mkdf-tours-standard-item-excerpt">
                                                                                    Lorem ipsum dolor sit amet, conse tetur adipiscing elit. Mauris a rutrum&hellip; </div>
                                                                            </div>
                                                                            <div class="mkdf-tours-standard-item-bottom-content">
                                                                                <div class="mkdf-tours-standard-item-bottom-item">

                                                                                    <div class="mkdf-tour-duration-holder">
                                                                                            <span class="mkdf-tour-duration-icon mkdf-tour-info-icon">
					<span class="icon_clock"></span>
                                                                                            </span>
                                                                                        <span class="mkdf-tour-info-label">
					11 Days  10 Nights				</span>
                                                                                    </div>

                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="mkdf-tours-standard-item mkdf-tours-row-item mkdf-item-space post-1063 tour-item type-tour-item status-publish has-post-thumbnail hentry tour-category-historical tour-attribute-accommodation tour-attribute-guide tour-attribute-insurance tour-attribute-meals">
                                                                        <div class="mkdf-tours-standard-item-image-holder">
                                                                            <a href="https://wanderers.mikado-themes.com/tour-item/china-tour/">
                                                                                <img width="800" height="550" src="https://wanderers.mikado-themes.com/wp-content/uploads/2018/03/tour-4-featured-img-800x550.jpg" class="attachment-wanderers_mkdf_landscape size-wanderers_mkdf_landscape" alt="a" /> </a>
                                                                            <span class="mkdf-tours-standard-item-label-holder">

		<span class="mkdf-tour-item-label">
			<span class="mkdf-tour-item-label-inner">
							</span>
                                                                                </span>

                                                                                </span>
                                                                        </div>

                                                                        <div class="mkdf-tours-standard-item-content-holder">
                                                                            <div class="mkdf-tours-standard-item-content-inner">
                                                                                <div class="mkdf-tours-standard-item-title-price-holder">
                                                                                    <h4 class="mkdf-tour-title">
                                                                                        <a href="https://wanderers.mikado-themes.com/tour-item/china-tour/">China Tour</a>
                                                                                    </h4>
                                                                                    <span class="mkdf-tours-standard-item-price-holder">

		<span class="mkdf-tours-price-holder">
							<span class="mkdf-tours-item-price ">$1500</span>
                                                                                        </span>

                                                                                        <span class="mkdf-tours-item-price-additional-text">
                        / per person                    </span>
                                                                                        </span>
                                                                                </div>

                                                                                <div class="mkdf-tours-standard-item-excerpt">
                                                                                    Lorem ipsum dolor sit amet, conse tetur adipiscing elit. Mauris a rutrum&hellip; </div>
                                                                            </div>
                                                                            <div class="mkdf-tours-standard-item-bottom-content">
                                                                                <div class="mkdf-tours-standard-item-bottom-item">

                                                                                    <div class="mkdf-tour-duration-holder">
                                                                                            <span class="mkdf-tour-duration-icon mkdf-tour-info-icon">
					<span class="icon_clock"></span>
                                                                                            </span>
                                                                                        <span class="mkdf-tour-info-label">
					10 Days  9 Nights				</span>
                                                                                    </div>

                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="mkdf-tours-standard-item mkdf-tours-row-item mkdf-item-space post-1066 tour-item type-tour-item status-publish has-post-thumbnail hentry tour-category-seaside tour-attribute-accommodation tour-attribute-guide tour-attribute-insurance tour-attribute-meals">
                                                                        <div class="mkdf-tours-standard-item-image-holder">
                                                                            <a href="https://wanderers.mikado-themes.com/tour-item/safari-tour/">
                                                                                <img width="800" height="550" src="https://wanderers.mikado-themes.com/wp-content/uploads/2018/03/tour-5-featured-img-800x550.jpg" class="attachment-wanderers_mkdf_landscape size-wanderers_mkdf_landscape" alt="a" /> </a>
                                                                            <span class="mkdf-tours-standard-item-label-holder">

		<span class="mkdf-tour-item-label">
			<span class="mkdf-tour-item-label-inner">
							</span>
                                                                                </span>

                                                                                </span>
                                                                        </div>

                                                                        <div class="mkdf-tours-standard-item-content-holder">
                                                                            <div class="mkdf-tours-standard-item-content-inner">
                                                                                <div class="mkdf-tours-standard-item-title-price-holder">
                                                                                    <h4 class="mkdf-tour-title">
                                                                                        <a href="https://wanderers.mikado-themes.com/tour-item/safari-tour/">Safari Tour</a>
                                                                                    </h4>
                                                                                    <span class="mkdf-tours-standard-item-price-holder">

		<span class="mkdf-tours-price-holder">
							<span class="mkdf-tours-item-price ">$1650</span>
                                                                                        </span>

                                                                                        <span class="mkdf-tours-item-price-additional-text">
                        / per person                    </span>
                                                                                        </span>
                                                                                </div>

                                                                                <div class="mkdf-tours-standard-item-excerpt">
                                                                                    Lorem ipsum dolor sit amet, conse tetur adipiscing elit. Mauris a rutrum&hellip; </div>
                                                                            </div>
                                                                            <div class="mkdf-tours-standard-item-bottom-content">
                                                                                <div class="mkdf-tours-standard-item-bottom-item">

                                                                                    <div class="mkdf-tour-duration-holder">
                                                                                            <span class="mkdf-tour-duration-icon mkdf-tour-info-icon">
					<span class="icon_clock"></span>
                                                                                            </span>
                                                                                        <span class="mkdf-tour-info-label">
					16 Days   15 Nights				</span>
                                                                                    </div>

                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="mkdf-tours-standard-item mkdf-tours-row-item mkdf-item-space post-1067 tour-item type-tour-item status-publish has-post-thumbnail hentry tour-category-self-guided tour-attribute-accommodation tour-attribute-guide tour-attribute-insurance tour-attribute-meals">
                                                                        <div class="mkdf-tours-standard-item-image-holder">
                                                                            <a href="https://wanderers.mikado-themes.com/tour-item/canada-tour/">
                                                                                <img width="800" height="550" src="https://wanderers.mikado-themes.com/wp-content/uploads/2018/03/tour-6-featured-img-800x550.jpg" class="attachment-wanderers_mkdf_landscape size-wanderers_mkdf_landscape" alt="a" /> </a>
                                                                            <span class="mkdf-tours-standard-item-label-holder">

		<span class="mkdf-tour-item-label">
			<span class="mkdf-tour-item-label-inner">
				new			</span>
                                                                                </span>

                                                                                </span>
                                                                        </div>

                                                                        <div class="mkdf-tours-standard-item-content-holder">
                                                                            <div class="mkdf-tours-standard-item-content-inner">
                                                                                <div class="mkdf-tours-standard-item-title-price-holder">
                                                                                    <h4 class="mkdf-tour-title">
                                                                                        <a href="https://wanderers.mikado-themes.com/tour-item/canada-tour/">Canada Tour</a>
                                                                                    </h4>
                                                                                    <span class="mkdf-tours-standard-item-price-holder">

		<span class="mkdf-tours-price-holder">
							<span class="mkdf-tours-item-price ">$2000</span>
                                                                                        </span>

                                                                                        <span class="mkdf-tours-item-price-additional-text">
                        / per person                    </span>
                                                                                        </span>
                                                                                </div>

                                                                                <div class="mkdf-tours-standard-item-excerpt">
                                                                                    Lorem ipsum dolor sit amet, conse tetur adipiscing elit. Mauris a rutrum&hellip; </div>
                                                                            </div>
                                                                            <div class="mkdf-tours-standard-item-bottom-content">
                                                                                <div class="mkdf-tours-standard-item-bottom-item">

                                                                                    <div class="mkdf-tour-duration-holder">
                                                                                            <span class="mkdf-tour-duration-icon mkdf-tour-info-icon">
					<span class="icon_clock"></span>
                                                                                            </span>
                                                                                        <span class="mkdf-tour-info-label">
					12 Days  11 Nights				</span>
                                                                                    </div>

                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="mkdf-tours-standard-item mkdf-tours-row-item mkdf-item-space post-1289 tour-item type-tour-item status-publish has-post-thumbnail hentry tour-category-cultural review-criteria-accommodation review-criteria-destination review-criteria-meals review-criteria-transport review-criteria-value_for_money tour-attribute-accommodation tour-attribute-guide tour-attribute-insurance tour-attribute-meals">
                                                                        <div class="mkdf-tours-standard-item-image-holder">
                                                                            <a href="https://wanderers.mikado-themes.com/tour-item/pyramids-tour/">
                                                                                <img width="800" height="550" src="https://wanderers.mikado-themes.com/wp-content/uploads/2018/03/tour-7-featured-img-800x550.jpg" class="attachment-wanderers_mkdf_landscape size-wanderers_mkdf_landscape" alt="a" /> </a>
                                                                            <span class="mkdf-tours-standard-item-label-holder">

		<span class="mkdf-tour-item-label">
			<span class="mkdf-tour-item-label-inner">
							</span>
                                                                                </span>

                                                                                </span>
                                                                        </div>

                                                                        <div class="mkdf-tours-standard-item-content-holder">
                                                                            <div class="mkdf-tours-standard-item-content-inner">
                                                                                <div class="mkdf-tours-standard-item-title-price-holder">
                                                                                    <h4 class="mkdf-tour-title">
                                                                                        <a href="https://wanderers.mikado-themes.com/tour-item/pyramids-tour/">Pyramids Tour</a>
                                                                                    </h4>
                                                                                    <span class="mkdf-tours-standard-item-price-holder">

		<span class="mkdf-tours-price-holder">
							<span class="mkdf-tours-item-price ">$1250</span>
                                                                                        </span>

                                                                                        <span class="mkdf-tours-item-price-additional-text">
                        / per person                    </span>
                                                                                        </span>
                                                                                </div>

                                                                                <div class="mkdf-tours-standard-item-excerpt">
                                                                                    Lorem ipsum dolor sit amet, conse tetur adipiscing elit. Mauris a rutrum&hellip; </div>
                                                                            </div>
                                                                            <div class="mkdf-tours-standard-item-bottom-content">
                                                                                <div class="mkdf-tours-standard-item-bottom-item">

                                                                                    <div class="mkdf-tour-duration-holder">
                                                                                            <span class="mkdf-tour-duration-icon mkdf-tour-info-icon">
					<span class="icon_clock"></span>
                                                                                            </span>
                                                                                        <span class="mkdf-tour-info-label">
					11 Days  10 Nights				</span>
                                                                                    </div>

                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="mkdf-tours-standard-item mkdf-tours-row-item mkdf-item-space post-1290 tour-item type-tour-item status-publish has-post-thumbnail hentry tour-category-seaside tour-attribute-accommodation tour-attribute-guide tour-attribute-insurance tour-attribute-meals">
                                                                        <div class="mkdf-tours-standard-item-image-holder">
                                                                            <a href="https://wanderers.mikado-themes.com/tour-item/mexico-explore/">
                                                                                <img width="800" height="550" src="https://wanderers.mikado-themes.com/wp-content/uploads/2018/03/tour-8-featured-img-800x550.jpg" class="attachment-wanderers_mkdf_landscape size-wanderers_mkdf_landscape" alt="a" /> </a>
                                                                            <span class="mkdf-tours-standard-item-label-holder">

		<span class="mkdf-tour-item-label">
			<span class="mkdf-tour-item-label-inner">
							</span>
                                                                                </span>

                                                                                </span>
                                                                        </div>

                                                                        <div class="mkdf-tours-standard-item-content-holder">
                                                                            <div class="mkdf-tours-standard-item-content-inner">
                                                                                <div class="mkdf-tours-standard-item-title-price-holder">
                                                                                    <h4 class="mkdf-tour-title">
                                                                                        <a href="https://wanderers.mikado-themes.com/tour-item/mexico-explore/">Mexico Explore</a>
                                                                                    </h4>
                                                                                    <span class="mkdf-tours-standard-item-price-holder">

		<span class="mkdf-tours-price-holder">
							<span class="mkdf-tours-item-price ">$2950</span>
                                                                                        </span>

                                                                                        <span class="mkdf-tours-item-price-additional-text">
                        / per person                    </span>
                                                                                        </span>
                                                                                </div>

                                                                                <div class="mkdf-tours-standard-item-excerpt">
                                                                                    Lorem ipsum dolor sit amet, conse tetur adipiscing elit. Mauris a rutrum&hellip; </div>
                                                                            </div>
                                                                            <div class="mkdf-tours-standard-item-bottom-content">
                                                                                <div class="mkdf-tours-standard-item-bottom-item">

                                                                                    <div class="mkdf-tour-duration-holder">
                                                                                            <span class="mkdf-tour-duration-icon mkdf-tour-info-icon">
					<span class="icon_clock"></span>
                                                                                            </span>
                                                                                        <span class="mkdf-tour-info-label">
					12 Days  11 Nights				</span>
                                                                                    </div>

                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="mkdf-tours-standard-item mkdf-tours-row-item mkdf-item-space post-1292 tour-item type-tour-item status-publish has-post-thumbnail hentry tour-category-self-guided tour-attribute-accommodation tour-attribute-guide tour-attribute-insurance tour-attribute-meals">
                                                                        <div class="mkdf-tours-standard-item-image-holder">
                                                                            <a href="https://wanderers.mikado-themes.com/tour-item/europe-tour/">
                                                                                <img width="800" height="550" src="https://wanderers.mikado-themes.com/wp-content/uploads/2018/03/tour-9-featured-img-800x550.jpg" class="attachment-wanderers_mkdf_landscape size-wanderers_mkdf_landscape" alt="a" /> </a>
                                                                            <span class="mkdf-tours-standard-item-label-holder">

		<span class="mkdf-tour-item-label">
			<span class="mkdf-tour-item-label-inner">
							</span>
                                                                                </span>

                                                                                </span>
                                                                        </div>

                                                                        <div class="mkdf-tours-standard-item-content-holder">
                                                                            <div class="mkdf-tours-standard-item-content-inner">
                                                                                <div class="mkdf-tours-standard-item-title-price-holder">
                                                                                    <h4 class="mkdf-tour-title">
                                                                                        <a href="https://wanderers.mikado-themes.com/tour-item/europe-tour/">Europe Tour</a>
                                                                                    </h4>
                                                                                    <span class="mkdf-tours-standard-item-price-holder">

		<span class="mkdf-tours-price-holder">
							<span class="mkdf-tours-item-price ">$1850</span>
                                                                                        </span>

                                                                                        <span class="mkdf-tours-item-price-additional-text">
                        / per person                    </span>
                                                                                        </span>
                                                                                </div>

                                                                                <div class="mkdf-tours-standard-item-excerpt">
                                                                                    Lorem ipsum dolor sit amet, conse tetur adipiscing elit. Mauris a rutrum&hellip; </div>
                                                                            </div>
                                                                            <div class="mkdf-tours-standard-item-bottom-content">
                                                                                <div class="mkdf-tours-standard-item-bottom-item">

                                                                                    <div class="mkdf-tour-duration-holder">
                                                                                            <span class="mkdf-tour-duration-icon mkdf-tour-info-icon">
					<span class="icon_clock"></span>
                                                                                            </span>
                                                                                        <span class="mkdf-tour-info-label">
					12 Days  11 Nights				</span>
                                                                                    </div>

                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="mkdf-row-grid-section-wrapper " style="background-color:#fafafa">
                                    <div class="mkdf-row-grid-section">
                                        <div class="vc_row wpb_row vc_row-fluid vc_custom_1521647449096">
                                            <div class="wpb_column vc_column_container vc_col-sm-12">
                                                <div class="vc_column-inner ">
                                                    <div class="wpb_wrapper">
                                                        <div class="mkdf-tours-carousel-holder mkdf-carousel-pagination">
                                                            <div class="mkdf-tours-row mkdf-normal-space">
                                                                <div class="mkdf-tours-carousel mkdf-tours-row-inner-holder mkdf-owl-slider" data-number-of-items="3" data-enable-loop="yes" data-enable-navigation="no" data-enable-pagination="yes">

                                                                    <div class="mkdf-tours-standard-item mkdf-tours-row-item mkdf-item-space post-57 tour-item type-tour-item status-publish has-post-thumbnail hentry tour-category-seaside tour-attribute-accommodation tour-attribute-guide tour-attribute-insurance tour-attribute-meals">
                                                                        <div class="mkdf-tours-standard-item-image-holder">
                                                                            <a href="https://wanderers.mikado-themes.com/tour-item/ultimate-thailand/">
                                                                                <img width="800" height="550" src="https://wanderers.mikado-themes.com/wp-content/uploads/2018/02/tour-1-featured-img-800x550.jpg" class="attachment-wanderers_mkdf_landscape size-wanderers_mkdf_landscape" alt="a" /> </a>
                                                                            <span class="mkdf-tours-standard-item-label-holder">

		<span class="mkdf-tour-item-label">
			<span class="mkdf-tour-item-label-inner">
				fun			</span>
                                                                                </span>

                                                                                </span>
                                                                        </div>

                                                                        <div class="mkdf-tours-standard-item-content-holder">
                                                                            <div class="mkdf-tours-standard-item-content-inner">
                                                                                <div class="mkdf-tours-standard-item-title-price-holder">
                                                                                    <h4 class="mkdf-tour-title">
                                                                                        <a href="https://wanderers.mikado-themes.com/tour-item/ultimate-thailand/">Ultimate Thailand</a>
                                                                                    </h4>
                                                                                    <span class="mkdf-tours-standard-item-price-holder">

		<span class="mkdf-tours-price-holder">
							<span class="mkdf-tours-item-price ">$1350</span>
                                                                                        </span>

                                                                                        <span class="mkdf-tours-item-price-additional-text">
                        / per person                    </span>
                                                                                        </span>
                                                                                </div>

                                                                                <div class="mkdf-tours-standard-item-excerpt">
                                                                                    Lorem ipsum dolor sit amet, conse tetur adipiscing elit. Mauris a rutrum&hellip; </div>
                                                                            </div>
                                                                            <div class="mkdf-tours-standard-item-bottom-content">
                                                                                <div class="mkdf-tours-standard-item-bottom-item">

                                                                                    <div class="mkdf-tour-duration-holder">
                                                                                            <span class="mkdf-tour-duration-icon mkdf-tour-info-icon">
					<span class="icon_clock"></span>
                                                                                            </span>
                                                                                        <span class="mkdf-tour-info-label">
					12 Days  11 Nights				</span>
                                                                                    </div>

                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="mkdf-tours-standard-item mkdf-tours-row-item mkdf-item-space post-1036 tour-item type-tour-item status-publish has-post-thumbnail hentry tour-category-adventure review-criteria-accommodation review-criteria-destination review-criteria-meals review-criteria-transport review-criteria-value_for_money tour-attribute-accommodation tour-attribute-guide tour-attribute-insurance tour-attribute-meals">
                                                                        <div class="mkdf-tours-standard-item-image-holder">
                                                                            <a href="https://wanderers.mikado-themes.com/tour-item/discover-india/">
                                                                                <img width="800" height="550" src="https://wanderers.mikado-themes.com/wp-content/uploads/2018/03/tour-2-featured-img-800x550.jpg" class="attachment-wanderers_mkdf_landscape size-wanderers_mkdf_landscape" alt="a" /> </a>
                                                                            <span class="mkdf-tours-standard-item-label-holder">

		<span class="mkdf-tour-item-label">
			<span class="mkdf-tour-item-label-inner">
							</span>
                                                                                </span>

                                                                                </span>
                                                                        </div>

                                                                        <div class="mkdf-tours-standard-item-content-holder">
                                                                            <div class="mkdf-tours-standard-item-content-inner">
                                                                                <div class="mkdf-tours-standard-item-title-price-holder">
                                                                                    <h4 class="mkdf-tour-title">
                                                                                        <a href="https://wanderers.mikado-themes.com/tour-item/discover-india/">Discover India</a>
                                                                                    </h4>
                                                                                    <span class="mkdf-tours-standard-item-price-holder">

		<span class="mkdf-tours-price-holder">
							<span class="mkdf-tours-item-price ">$2650</span>
                                                                                        </span>

                                                                                        <span class="mkdf-tours-item-price-additional-text">
                        / per person                    </span>
                                                                                        </span>
                                                                                </div>

                                                                                <div class="mkdf-tours-standard-item-excerpt">
                                                                                    Lorem ipsum dolor sit amet, conse tetur adipiscing elit. Mauris a rutrum&hellip; </div>
                                                                            </div>
                                                                            <div class="mkdf-tours-standard-item-bottom-content">
                                                                                <div class="mkdf-tours-standard-item-bottom-item">

                                                                                    <div class="mkdf-tour-duration-holder">
                                                                                            <span class="mkdf-tour-duration-icon mkdf-tour-info-icon">
					<span class="icon_clock"></span>
                                                                                            </span>
                                                                                        <span class="mkdf-tour-info-label">
					10 Days  9 Nights				</span>
                                                                                    </div>

                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="mkdf-tours-standard-item mkdf-tours-row-item mkdf-item-space post-1039 tour-item type-tour-item status-publish has-post-thumbnail hentry tour-category-adventure review-criteria-accommodation review-criteria-destination review-criteria-meals review-criteria-transport review-criteria-value_for_money tour-attribute-accommodation tour-attribute-guide tour-attribute-insurance tour-attribute-meals">
                                                                        <div class="mkdf-tours-standard-item-image-holder">
                                                                            <a href="https://wanderers.mikado-themes.com/tour-item/forest-adventure/">
                                                                                <img width="800" height="550" src="https://wanderers.mikado-themes.com/wp-content/uploads/2018/03/tour-3-featured-img-800x550.jpg" class="attachment-wanderers_mkdf_landscape size-wanderers_mkdf_landscape" alt="a" /> </a>
                                                                            <span class="mkdf-tours-standard-item-label-holder">

		<span class="mkdf-tour-item-label">
			<span class="mkdf-tour-item-label-inner">
							</span>
                                                                                </span>

                                                                                </span>
                                                                        </div>

                                                                        <div class="mkdf-tours-standard-item-content-holder">
                                                                            <div class="mkdf-tours-standard-item-content-inner">
                                                                                <div class="mkdf-tours-standard-item-title-price-holder">
                                                                                    <h4 class="mkdf-tour-title">
                                                                                        <a href="https://wanderers.mikado-themes.com/tour-item/forest-adventure/">Forest Adventure</a>
                                                                                    </h4>
                                                                                    <span class="mkdf-tours-standard-item-price-holder">

		<span class="mkdf-tours-price-holder">
							<span class="mkdf-tours-item-price ">$1350</span>
                                                                                        </span>

                                                                                        <span class="mkdf-tours-item-price-additional-text">
                        / per person                    </span>
                                                                                        </span>
                                                                                </div>

                                                                                <div class="mkdf-tours-standard-item-excerpt">
                                                                                    Lorem ipsum dolor sit amet, conse tetur adipiscing elit. Mauris a rutrum&hellip; </div>
                                                                            </div>
                                                                            <div class="mkdf-tours-standard-item-bottom-content">
                                                                                <div class="mkdf-tours-standard-item-bottom-item">

                                                                                    <div class="mkdf-tour-duration-holder">
                                                                                            <span class="mkdf-tour-duration-icon mkdf-tour-info-icon">
					<span class="icon_clock"></span>
                                                                                            </span>
                                                                                        <span class="mkdf-tour-info-label">
					11 Days  10 Nights				</span>
                                                                                    </div>

                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="mkdf-tours-standard-item mkdf-tours-row-item mkdf-item-space post-1063 tour-item type-tour-item status-publish has-post-thumbnail hentry tour-category-historical tour-attribute-accommodation tour-attribute-guide tour-attribute-insurance tour-attribute-meals">
                                                                        <div class="mkdf-tours-standard-item-image-holder">
                                                                            <a href="https://wanderers.mikado-themes.com/tour-item/china-tour/">
                                                                                <img width="800" height="550" src="https://wanderers.mikado-themes.com/wp-content/uploads/2018/03/tour-4-featured-img-800x550.jpg" class="attachment-wanderers_mkdf_landscape size-wanderers_mkdf_landscape" alt="a" /> </a>
                                                                            <span class="mkdf-tours-standard-item-label-holder">

		<span class="mkdf-tour-item-label">
			<span class="mkdf-tour-item-label-inner">
							</span>
                                                                                </span>

                                                                                </span>
                                                                        </div>

                                                                        <div class="mkdf-tours-standard-item-content-holder">
                                                                            <div class="mkdf-tours-standard-item-content-inner">
                                                                                <div class="mkdf-tours-standard-item-title-price-holder">
                                                                                    <h4 class="mkdf-tour-title">
                                                                                        <a href="https://wanderers.mikado-themes.com/tour-item/china-tour/">China Tour</a>
                                                                                    </h4>
                                                                                    <span class="mkdf-tours-standard-item-price-holder">

		<span class="mkdf-tours-price-holder">
							<span class="mkdf-tours-item-price ">$1500</span>
                                                                                        </span>

                                                                                        <span class="mkdf-tours-item-price-additional-text">
                        / per person                    </span>
                                                                                        </span>
                                                                                </div>

                                                                                <div class="mkdf-tours-standard-item-excerpt">
                                                                                    Lorem ipsum dolor sit amet, conse tetur adipiscing elit. Mauris a rutrum&hellip; </div>
                                                                            </div>
                                                                            <div class="mkdf-tours-standard-item-bottom-content">
                                                                                <div class="mkdf-tours-standard-item-bottom-item">

                                                                                    <div class="mkdf-tour-duration-holder">
                                                                                            <span class="mkdf-tour-duration-icon mkdf-tour-info-icon">
					<span class="icon_clock"></span>
                                                                                            </span>
                                                                                        <span class="mkdf-tour-info-label">
					10 Days  9 Nights				</span>
                                                                                    </div>

                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="mkdf-tours-standard-item mkdf-tours-row-item mkdf-item-space post-1066 tour-item type-tour-item status-publish has-post-thumbnail hentry tour-category-seaside tour-attribute-accommodation tour-attribute-guide tour-attribute-insurance tour-attribute-meals">
                                                                        <div class="mkdf-tours-standard-item-image-holder">
                                                                            <a href="https://wanderers.mikado-themes.com/tour-item/safari-tour/">
                                                                                <img width="800" height="550" src="https://wanderers.mikado-themes.com/wp-content/uploads/2018/03/tour-5-featured-img-800x550.jpg" class="attachment-wanderers_mkdf_landscape size-wanderers_mkdf_landscape" alt="a" /> </a>
                                                                            <span class="mkdf-tours-standard-item-label-holder">

		<span class="mkdf-tour-item-label">
			<span class="mkdf-tour-item-label-inner">
							</span>
                                                                                </span>

                                                                                </span>
                                                                        </div>

                                                                        <div class="mkdf-tours-standard-item-content-holder">
                                                                            <div class="mkdf-tours-standard-item-content-inner">
                                                                                <div class="mkdf-tours-standard-item-title-price-holder">
                                                                                    <h4 class="mkdf-tour-title">
                                                                                        <a href="https://wanderers.mikado-themes.com/tour-item/safari-tour/">Safari Tour</a>
                                                                                    </h4>
                                                                                    <span class="mkdf-tours-standard-item-price-holder">

		<span class="mkdf-tours-price-holder">
							<span class="mkdf-tours-item-price ">$1650</span>
                                                                                        </span>

                                                                                        <span class="mkdf-tours-item-price-additional-text">
                        / per person                    </span>
                                                                                        </span>
                                                                                </div>

                                                                                <div class="mkdf-tours-standard-item-excerpt">
                                                                                    Lorem ipsum dolor sit amet, conse tetur adipiscing elit. Mauris a rutrum&hellip; </div>
                                                                            </div>
                                                                            <div class="mkdf-tours-standard-item-bottom-content">
                                                                                <div class="mkdf-tours-standard-item-bottom-item">

                                                                                    <div class="mkdf-tour-duration-holder">
                                                                                            <span class="mkdf-tour-duration-icon mkdf-tour-info-icon">
					<span class="icon_clock"></span>
                                                                                            </span>
                                                                                        <span class="mkdf-tour-info-label">
					16 Days   15 Nights				</span>
                                                                                    </div>

                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="mkdf-tours-standard-item mkdf-tours-row-item mkdf-item-space post-1067 tour-item type-tour-item status-publish has-post-thumbnail hentry tour-category-self-guided tour-attribute-accommodation tour-attribute-guide tour-attribute-insurance tour-attribute-meals">
                                                                        <div class="mkdf-tours-standard-item-image-holder">
                                                                            <a href="https://wanderers.mikado-themes.com/tour-item/canada-tour/">
                                                                                <img width="800" height="550" src="https://wanderers.mikado-themes.com/wp-content/uploads/2018/03/tour-6-featured-img-800x550.jpg" class="attachment-wanderers_mkdf_landscape size-wanderers_mkdf_landscape" alt="a" /> </a>
                                                                            <span class="mkdf-tours-standard-item-label-holder">

		<span class="mkdf-tour-item-label">
			<span class="mkdf-tour-item-label-inner">
				new			</span>
                                                                                </span>

                                                                                </span>
                                                                        </div>

                                                                        <div class="mkdf-tours-standard-item-content-holder">
                                                                            <div class="mkdf-tours-standard-item-content-inner">
                                                                                <div class="mkdf-tours-standard-item-title-price-holder">
                                                                                    <h4 class="mkdf-tour-title">
                                                                                        <a href="https://wanderers.mikado-themes.com/tour-item/canada-tour/">Canada Tour</a>
                                                                                    </h4>
                                                                                    <span class="mkdf-tours-standard-item-price-holder">

		<span class="mkdf-tours-price-holder">
							<span class="mkdf-tours-item-price ">$2000</span>
                                                                                        </span>

                                                                                        <span class="mkdf-tours-item-price-additional-text">
                        / per person                    </span>
                                                                                        </span>
                                                                                </div>

                                                                                <div class="mkdf-tours-standard-item-excerpt">
                                                                                    Lorem ipsum dolor sit amet, conse tetur adipiscing elit. Mauris a rutrum&hellip; </div>
                                                                            </div>
                                                                            <div class="mkdf-tours-standard-item-bottom-content">
                                                                                <div class="mkdf-tours-standard-item-bottom-item">

                                                                                    <div class="mkdf-tour-duration-holder">
                                                                                            <span class="mkdf-tour-duration-icon mkdf-tour-info-icon">
					<span class="icon_clock"></span>
                                                                                            </span>
                                                                                        <span class="mkdf-tour-info-label">
					12 Days  11 Nights				</span>
                                                                                    </div>

                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="mkdf-tours-standard-item mkdf-tours-row-item mkdf-item-space post-1289 tour-item type-tour-item status-publish has-post-thumbnail hentry tour-category-cultural review-criteria-accommodation review-criteria-destination review-criteria-meals review-criteria-transport review-criteria-value_for_money tour-attribute-accommodation tour-attribute-guide tour-attribute-insurance tour-attribute-meals">
                                                                        <div class="mkdf-tours-standard-item-image-holder">
                                                                            <a href="https://wanderers.mikado-themes.com/tour-item/pyramids-tour/">
                                                                                <img width="800" height="550" src="https://wanderers.mikado-themes.com/wp-content/uploads/2018/03/tour-7-featured-img-800x550.jpg" class="attachment-wanderers_mkdf_landscape size-wanderers_mkdf_landscape" alt="a" /> </a>
                                                                            <span class="mkdf-tours-standard-item-label-holder">

		<span class="mkdf-tour-item-label">
			<span class="mkdf-tour-item-label-inner">
							</span>
                                                                                </span>

                                                                                </span>
                                                                        </div>

                                                                        <div class="mkdf-tours-standard-item-content-holder">
                                                                            <div class="mkdf-tours-standard-item-content-inner">
                                                                                <div class="mkdf-tours-standard-item-title-price-holder">
                                                                                    <h4 class="mkdf-tour-title">
                                                                                        <a href="https://wanderers.mikado-themes.com/tour-item/pyramids-tour/">Pyramids Tour</a>
                                                                                    </h4>
                                                                                    <span class="mkdf-tours-standard-item-price-holder">

		<span class="mkdf-tours-price-holder">
							<span class="mkdf-tours-item-price ">$1250</span>
                                                                                        </span>

                                                                                        <span class="mkdf-tours-item-price-additional-text">
                        / per person                    </span>
                                                                                        </span>
                                                                                </div>

                                                                                <div class="mkdf-tours-standard-item-excerpt">
                                                                                    Lorem ipsum dolor sit amet, conse tetur adipiscing elit. Mauris a rutrum&hellip; </div>
                                                                            </div>
                                                                            <div class="mkdf-tours-standard-item-bottom-content">
                                                                                <div class="mkdf-tours-standard-item-bottom-item">

                                                                                    <div class="mkdf-tour-duration-holder">
                                                                                            <span class="mkdf-tour-duration-icon mkdf-tour-info-icon">
					<span class="icon_clock"></span>
                                                                                            </span>
                                                                                        <span class="mkdf-tour-info-label">
					11 Days  10 Nights				</span>
                                                                                    </div>

                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="mkdf-tours-standard-item mkdf-tours-row-item mkdf-item-space post-1290 tour-item type-tour-item status-publish has-post-thumbnail hentry tour-category-seaside tour-attribute-accommodation tour-attribute-guide tour-attribute-insurance tour-attribute-meals">
                                                                        <div class="mkdf-tours-standard-item-image-holder">
                                                                            <a href="https://wanderers.mikado-themes.com/tour-item/mexico-explore/">
                                                                                <img width="800" height="550" src="https://wanderers.mikado-themes.com/wp-content/uploads/2018/03/tour-8-featured-img-800x550.jpg" class="attachment-wanderers_mkdf_landscape size-wanderers_mkdf_landscape" alt="a" /> </a>
                                                                            <span class="mkdf-tours-standard-item-label-holder">

		<span class="mkdf-tour-item-label">
			<span class="mkdf-tour-item-label-inner">
							</span>
                                                                                </span>

                                                                                </span>
                                                                        </div>

                                                                        <div class="mkdf-tours-standard-item-content-holder">
                                                                            <div class="mkdf-tours-standard-item-content-inner">
                                                                                <div class="mkdf-tours-standard-item-title-price-holder">
                                                                                    <h4 class="mkdf-tour-title">
                                                                                        <a href="https://wanderers.mikado-themes.com/tour-item/mexico-explore/">Mexico Explore</a>
                                                                                    </h4>
                                                                                    <span class="mkdf-tours-standard-item-price-holder">

		<span class="mkdf-tours-price-holder">
							<span class="mkdf-tours-item-price ">$2950</span>
                                                                                        </span>

                                                                                        <span class="mkdf-tours-item-price-additional-text">
                        / per person                    </span>
                                                                                        </span>
                                                                                </div>

                                                                                <div class="mkdf-tours-standard-item-excerpt">
                                                                                    Lorem ipsum dolor sit amet, conse tetur adipiscing elit. Mauris a rutrum&hellip; </div>
                                                                            </div>
                                                                            <div class="mkdf-tours-standard-item-bottom-content">
                                                                                <div class="mkdf-tours-standard-item-bottom-item">

                                                                                    <div class="mkdf-tour-duration-holder">
                                                                                            <span class="mkdf-tour-duration-icon mkdf-tour-info-icon">
					<span class="icon_clock"></span>
                                                                                            </span>
                                                                                        <span class="mkdf-tour-info-label">
					12 Days  11 Nights				</span>
                                                                                    </div>

                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="mkdf-tours-standard-item mkdf-tours-row-item mkdf-item-space post-1292 tour-item type-tour-item status-publish has-post-thumbnail hentry tour-category-self-guided tour-attribute-accommodation tour-attribute-guide tour-attribute-insurance tour-attribute-meals">
                                                                        <div class="mkdf-tours-standard-item-image-holder">
                                                                            <a href="https://wanderers.mikado-themes.com/tour-item/europe-tour/">
                                                                                <img width="800" height="550" src="https://wanderers.mikado-themes.com/wp-content/uploads/2018/03/tour-9-featured-img-800x550.jpg" class="attachment-wanderers_mkdf_landscape size-wanderers_mkdf_landscape" alt="a" /> </a>
                                                                            <span class="mkdf-tours-standard-item-label-holder">

		<span class="mkdf-tour-item-label">
			<span class="mkdf-tour-item-label-inner">
							</span>
                                                                                </span>

                                                                                </span>
                                                                        </div>

                                                                        <div class="mkdf-tours-standard-item-content-holder">
                                                                            <div class="mkdf-tours-standard-item-content-inner">
                                                                                <div class="mkdf-tours-standard-item-title-price-holder">
                                                                                    <h4 class="mkdf-tour-title">
                                                                                        <a href="https://wanderers.mikado-themes.com/tour-item/europe-tour/">Europe Tour</a>
                                                                                    </h4>
                                                                                    <span class="mkdf-tours-standard-item-price-holder">

		<span class="mkdf-tours-price-holder">
							<span class="mkdf-tours-item-price ">$1850</span>
                                                                                        </span>

                                                                                        <span class="mkdf-tours-item-price-additional-text">
                        / per person                    </span>
                                                                                        </span>
                                                                                </div>

                                                                                <div class="mkdf-tours-standard-item-excerpt">
                                                                                    Lorem ipsum dolor sit amet, conse tetur adipiscing elit. Mauris a rutrum&hellip; </div>
                                                                            </div>
                                                                            <div class="mkdf-tours-standard-item-bottom-content">
                                                                                <div class="mkdf-tours-standard-item-bottom-item">

                                                                                    <div class="mkdf-tour-duration-holder">
                                                                                            <span class="mkdf-tour-duration-icon mkdf-tour-info-icon">
					<span class="icon_clock"></span>
                                                                                            </span>
                                                                                        <span class="mkdf-tour-info-label">
					12 Days  11 Nights				</span>
                                                                                    </div>

                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="mkdf-row-grid-section-wrapper ">
                                    <div class="mkdf-row-grid-section">
                                        <div class="vc_row wpb_row vc_row-fluid vc_custom_1521216884465">
                                            <div class="wpb_column vc_column_container vc_col-sm-12">
                                                <div class="vc_column-inner ">
                                                    <div class="wpb_wrapper">
                                                        <div class="mkdf-tours-carousel-holder mkdf-carousel-pagination">
                                                            <div class="mkdf-tours-row mkdf-normal-space">
                                                                <div class="mkdf-tours-carousel mkdf-tours-row-inner-holder mkdf-owl-slider" data-number-of-items="3" data-enable-loop="yes" data-enable-navigation="no" data-enable-pagination="yes">

                                                                    <div class="mkdf-tours-gallery-item mkdf-tours-row-item mkdf-item-space mkdf-tour-item-has-rating post-57 tour-item type-tour-item status-publish has-post-thumbnail hentry tour-category-seaside tour-attribute-accommodation tour-attribute-guide tour-attribute-insurance tour-attribute-meals">
                                                                        <div class="mkdf-tours-gallery-item-image-holder">
                                                                            <div class="mkdf-tours-gallery-item-image">
                                                                                <img width="800" height="932" src="https://wanderers.mikado-themes.com/wp-content/uploads/2018/02/tour-1-featured-img.jpg" class="attachment-full size-full" alt="a" srcset="https://wanderers.mikado-themes.com/wp-content/uploads/2018/02/tour-1-featured-img.jpg 800w, https://wanderers.mikado-themes.com/wp-content/uploads/2018/02/tour-1-featured-img-258x300.jpg 258w, https://wanderers.mikado-themes.com/wp-content/uploads/2018/02/tour-1-featured-img-768x895.jpg 768w, https://wanderers.mikado-themes.com/wp-content/uploads/2018/02/tour-1-featured-img-450x524.jpg 450w" sizes="(max-width: 800px) 100vw, 800px" />
                                                                                <div class="mkdf-tours-gallery-item-content-holder">
                                                                                    <div class="mkdf-tours-gallery-item-content-inner">
                                                                                        <div class="mkdf-tours-gallery-main-info">
                                                                                                <span class="mkdf-tours-gallery-item-label-holder">

		<span class="mkdf-tour-item-label">
			<span class="mkdf-tour-item-label-inner">
				fun			</span>
                                                                                                </span>

                                                                                                </span>
                                                                                            <div class="mkdf-tours-gallery-title-holder">
                                                                                                <h4 class="mkdf-tour-title">
                                                                                                    Ultimate Thailand                            </h4>
                                                                                            </div>
                                                                                            <div class="mkdf-tours-gallery-item-excerpt">
                                                                                                <div class="mkdf-tours-gallery-item-excerpt-inner">
                                                                                                    Lorem ipsum dolor sit amet, conse tetur adipiscing elit. Mauris a rutrum&hellip; </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <span class="mkdf-tours-gallery-item-price-holder">

		<span class="mkdf-tours-price-holder">
							<span class="mkdf-tours-item-price ">$1350</span>
                                                                                            </span>

                                                                                            </span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <a class="mkdf-tours-gallery-item-link" href="https://wanderers.mikado-themes.com/tour-item/ultimate-thailand/"></a>
                                                                        </div>
                                                                    </div>
                                                                    <div class="mkdf-tours-gallery-item mkdf-tours-row-item mkdf-item-space mkdf-tour-item-has-rating post-1036 tour-item type-tour-item status-publish has-post-thumbnail hentry tour-category-adventure review-criteria-accommodation review-criteria-destination review-criteria-meals review-criteria-transport review-criteria-value_for_money tour-attribute-accommodation tour-attribute-guide tour-attribute-insurance tour-attribute-meals">
                                                                        <div class="mkdf-tours-gallery-item-image-holder">
                                                                            <div class="mkdf-tours-gallery-item-image">
                                                                                <img width="800" height="932" src="https://wanderers.mikado-themes.com/wp-content/uploads/2018/03/tour-2-featured-img.jpg" class="attachment-full size-full" alt="a" srcset="https://wanderers.mikado-themes.com/wp-content/uploads/2018/03/tour-2-featured-img.jpg 800w, https://wanderers.mikado-themes.com/wp-content/uploads/2018/03/tour-2-featured-img-258x300.jpg 258w, https://wanderers.mikado-themes.com/wp-content/uploads/2018/03/tour-2-featured-img-768x895.jpg 768w, https://wanderers.mikado-themes.com/wp-content/uploads/2018/03/tour-2-featured-img-450x524.jpg 450w" sizes="(max-width: 800px) 100vw, 800px" />
                                                                                <div class="mkdf-tours-gallery-item-content-holder">
                                                                                    <div class="mkdf-tours-gallery-item-content-inner">
                                                                                        <div class="mkdf-tours-gallery-main-info">
                                                                                                <span class="mkdf-tours-gallery-item-label-holder">

		<span class="mkdf-tour-item-label">
			<span class="mkdf-tour-item-label-inner">
							</span>
                                                                                                </span>

                                                                                                </span>
                                                                                            <div class="mkdf-tours-gallery-title-holder">
                                                                                                <h4 class="mkdf-tour-title">
                                                                                                    Discover India                            </h4>
                                                                                            </div>
                                                                                            <div class="mkdf-tours-gallery-item-excerpt">
                                                                                                <div class="mkdf-tours-gallery-item-excerpt-inner">
                                                                                                    Lorem ipsum dolor sit amet, conse tetur adipiscing elit. Mauris a rutrum&hellip; </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <span class="mkdf-tours-gallery-item-price-holder">

		<span class="mkdf-tours-price-holder">
							<span class="mkdf-tours-item-price ">$2650</span>
                                                                                            </span>

                                                                                            </span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <a class="mkdf-tours-gallery-item-link" href="https://wanderers.mikado-themes.com/tour-item/discover-india/"></a>
                                                                        </div>
                                                                    </div>
                                                                    <div class="mkdf-tours-gallery-item mkdf-tours-row-item mkdf-item-space mkdf-tour-item-has-rating post-1039 tour-item type-tour-item status-publish has-post-thumbnail hentry tour-category-adventure review-criteria-accommodation review-criteria-destination review-criteria-meals review-criteria-transport review-criteria-value_for_money tour-attribute-accommodation tour-attribute-guide tour-attribute-insurance tour-attribute-meals">
                                                                        <div class="mkdf-tours-gallery-item-image-holder">
                                                                            <div class="mkdf-tours-gallery-item-image">
                                                                                <img width="800" height="932" src="https://wanderers.mikado-themes.com/wp-content/uploads/2018/03/tour-3-featured-img.jpg" class="attachment-full size-full" alt="a" srcset="https://wanderers.mikado-themes.com/wp-content/uploads/2018/03/tour-3-featured-img.jpg 800w, https://wanderers.mikado-themes.com/wp-content/uploads/2018/03/tour-3-featured-img-258x300.jpg 258w, https://wanderers.mikado-themes.com/wp-content/uploads/2018/03/tour-3-featured-img-768x895.jpg 768w, https://wanderers.mikado-themes.com/wp-content/uploads/2018/03/tour-3-featured-img-450x524.jpg 450w" sizes="(max-width: 800px) 100vw, 800px" />
                                                                                <div class="mkdf-tours-gallery-item-content-holder">
                                                                                    <div class="mkdf-tours-gallery-item-content-inner">
                                                                                        <div class="mkdf-tours-gallery-main-info">
                                                                                                <span class="mkdf-tours-gallery-item-label-holder">

		<span class="mkdf-tour-item-label">
			<span class="mkdf-tour-item-label-inner">
							</span>
                                                                                                </span>

                                                                                                </span>
                                                                                            <div class="mkdf-tours-gallery-title-holder">
                                                                                                <h4 class="mkdf-tour-title">
                                                                                                    Forest Adventure                            </h4>
                                                                                            </div>
                                                                                            <div class="mkdf-tours-gallery-item-excerpt">
                                                                                                <div class="mkdf-tours-gallery-item-excerpt-inner">
                                                                                                    Lorem ipsum dolor sit amet, conse tetur adipiscing elit. Mauris a rutrum&hellip; </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <span class="mkdf-tours-gallery-item-price-holder">

		<span class="mkdf-tours-price-holder">
							<span class="mkdf-tours-item-price ">$1350</span>
                                                                                            </span>

                                                                                            </span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <a class="mkdf-tours-gallery-item-link" href="https://wanderers.mikado-themes.com/tour-item/forest-adventure/"></a>
                                                                        </div>
                                                                    </div>
                                                                    <div class="mkdf-tours-gallery-item mkdf-tours-row-item mkdf-item-space mkdf-tour-item-has-rating post-1063 tour-item type-tour-item status-publish has-post-thumbnail hentry tour-category-historical tour-attribute-accommodation tour-attribute-guide tour-attribute-insurance tour-attribute-meals">
                                                                        <div class="mkdf-tours-gallery-item-image-holder">
                                                                            <div class="mkdf-tours-gallery-item-image">
                                                                                <img width="800" height="932" src="https://wanderers.mikado-themes.com/wp-content/uploads/2018/03/tour-4-featured-img.jpg" class="attachment-full size-full" alt="a" srcset="https://wanderers.mikado-themes.com/wp-content/uploads/2018/03/tour-4-featured-img.jpg 800w, https://wanderers.mikado-themes.com/wp-content/uploads/2018/03/tour-4-featured-img-258x300.jpg 258w, https://wanderers.mikado-themes.com/wp-content/uploads/2018/03/tour-4-featured-img-768x895.jpg 768w, https://wanderers.mikado-themes.com/wp-content/uploads/2018/03/tour-4-featured-img-450x524.jpg 450w" sizes="(max-width: 800px) 100vw, 800px" />
                                                                                <div class="mkdf-tours-gallery-item-content-holder">
                                                                                    <div class="mkdf-tours-gallery-item-content-inner">
                                                                                        <div class="mkdf-tours-gallery-main-info">
                                                                                                <span class="mkdf-tours-gallery-item-label-holder">

		<span class="mkdf-tour-item-label">
			<span class="mkdf-tour-item-label-inner">
							</span>
                                                                                                </span>

                                                                                                </span>
                                                                                            <div class="mkdf-tours-gallery-title-holder">
                                                                                                <h4 class="mkdf-tour-title">
                                                                                                    China Tour                            </h4>
                                                                                            </div>
                                                                                            <div class="mkdf-tours-gallery-item-excerpt">
                                                                                                <div class="mkdf-tours-gallery-item-excerpt-inner">
                                                                                                    Lorem ipsum dolor sit amet, conse tetur adipiscing elit. Mauris a rutrum&hellip; </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <span class="mkdf-tours-gallery-item-price-holder">

		<span class="mkdf-tours-price-holder">
							<span class="mkdf-tours-item-price ">$1500</span>
                                                                                            </span>

                                                                                            </span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <a class="mkdf-tours-gallery-item-link" href="https://wanderers.mikado-themes.com/tour-item/china-tour/"></a>
                                                                        </div>
                                                                    </div>
                                                                    <div class="mkdf-tours-gallery-item mkdf-tours-row-item mkdf-item-space mkdf-tour-item-has-rating post-1066 tour-item type-tour-item status-publish has-post-thumbnail hentry tour-category-seaside tour-attribute-accommodation tour-attribute-guide tour-attribute-insurance tour-attribute-meals">
                                                                        <div class="mkdf-tours-gallery-item-image-holder">
                                                                            <div class="mkdf-tours-gallery-item-image">
                                                                                <img width="800" height="932" src="https://wanderers.mikado-themes.com/wp-content/uploads/2018/03/tour-5-featured-img.jpg" class="attachment-full size-full" alt="a" srcset="https://wanderers.mikado-themes.com/wp-content/uploads/2018/03/tour-5-featured-img.jpg 800w, https://wanderers.mikado-themes.com/wp-content/uploads/2018/03/tour-5-featured-img-258x300.jpg 258w, https://wanderers.mikado-themes.com/wp-content/uploads/2018/03/tour-5-featured-img-768x895.jpg 768w, https://wanderers.mikado-themes.com/wp-content/uploads/2018/03/tour-5-featured-img-450x524.jpg 450w" sizes="(max-width: 800px) 100vw, 800px" />
                                                                                <div class="mkdf-tours-gallery-item-content-holder">
                                                                                    <div class="mkdf-tours-gallery-item-content-inner">
                                                                                        <div class="mkdf-tours-gallery-main-info">
                                                                                                <span class="mkdf-tours-gallery-item-label-holder">

		<span class="mkdf-tour-item-label">
			<span class="mkdf-tour-item-label-inner">
							</span>
                                                                                                </span>

                                                                                                </span>
                                                                                            <div class="mkdf-tours-gallery-title-holder">
                                                                                                <h4 class="mkdf-tour-title">
                                                                                                    Safari Tour                            </h4>
                                                                                            </div>
                                                                                            <div class="mkdf-tours-gallery-item-excerpt">
                                                                                                <div class="mkdf-tours-gallery-item-excerpt-inner">
                                                                                                    Lorem ipsum dolor sit amet, conse tetur adipiscing elit. Mauris a rutrum&hellip; </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <span class="mkdf-tours-gallery-item-price-holder">

		<span class="mkdf-tours-price-holder">
							<span class="mkdf-tours-item-price ">$1650</span>
                                                                                            </span>

                                                                                            </span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <a class="mkdf-tours-gallery-item-link" href="https://wanderers.mikado-themes.com/tour-item/safari-tour/"></a>
                                                                        </div>
                                                                    </div>
                                                                    <div class="mkdf-tours-gallery-item mkdf-tours-row-item mkdf-item-space mkdf-tour-item-has-rating post-1067 tour-item type-tour-item status-publish has-post-thumbnail hentry tour-category-self-guided tour-attribute-accommodation tour-attribute-guide tour-attribute-insurance tour-attribute-meals">
                                                                        <div class="mkdf-tours-gallery-item-image-holder">
                                                                            <div class="mkdf-tours-gallery-item-image">
                                                                                <img width="800" height="932" src="https://wanderers.mikado-themes.com/wp-content/uploads/2018/03/tour-6-featured-img.jpg" class="attachment-full size-full" alt="a" srcset="https://wanderers.mikado-themes.com/wp-content/uploads/2018/03/tour-6-featured-img.jpg 800w, https://wanderers.mikado-themes.com/wp-content/uploads/2018/03/tour-6-featured-img-258x300.jpg 258w, https://wanderers.mikado-themes.com/wp-content/uploads/2018/03/tour-6-featured-img-768x895.jpg 768w, https://wanderers.mikado-themes.com/wp-content/uploads/2018/03/tour-6-featured-img-450x524.jpg 450w" sizes="(max-width: 800px) 100vw, 800px" />
                                                                                <div class="mkdf-tours-gallery-item-content-holder">
                                                                                    <div class="mkdf-tours-gallery-item-content-inner">
                                                                                        <div class="mkdf-tours-gallery-main-info">
                                                                                                <span class="mkdf-tours-gallery-item-label-holder">

		<span class="mkdf-tour-item-label">
			<span class="mkdf-tour-item-label-inner">
				new			</span>
                                                                                                </span>

                                                                                                </span>
                                                                                            <div class="mkdf-tours-gallery-title-holder">
                                                                                                <h4 class="mkdf-tour-title">
                                                                                                    Canada Tour                            </h4>
                                                                                            </div>
                                                                                            <div class="mkdf-tours-gallery-item-excerpt">
                                                                                                <div class="mkdf-tours-gallery-item-excerpt-inner">
                                                                                                    Lorem ipsum dolor sit amet, conse tetur adipiscing elit. Mauris a rutrum&hellip; </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <span class="mkdf-tours-gallery-item-price-holder">

		<span class="mkdf-tours-price-holder">
							<span class="mkdf-tours-item-price ">$2000</span>
                                                                                            </span>

                                                                                            </span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <a class="mkdf-tours-gallery-item-link" href="https://wanderers.mikado-themes.com/tour-item/canada-tour/"></a>
                                                                        </div>
                                                                    </div>
                                                                    <div class="mkdf-tours-gallery-item mkdf-tours-row-item mkdf-item-space mkdf-tour-item-has-rating post-1289 tour-item type-tour-item status-publish has-post-thumbnail hentry tour-category-cultural review-criteria-accommodation review-criteria-destination review-criteria-meals review-criteria-transport review-criteria-value_for_money tour-attribute-accommodation tour-attribute-guide tour-attribute-insurance tour-attribute-meals">
                                                                        <div class="mkdf-tours-gallery-item-image-holder">
                                                                            <div class="mkdf-tours-gallery-item-image">
                                                                                <img width="800" height="932" src="https://wanderers.mikado-themes.com/wp-content/uploads/2018/03/tour-7-featured-img.jpg" class="attachment-full size-full" alt="a" srcset="https://wanderers.mikado-themes.com/wp-content/uploads/2018/03/tour-7-featured-img.jpg 800w, https://wanderers.mikado-themes.com/wp-content/uploads/2018/03/tour-7-featured-img-258x300.jpg 258w, https://wanderers.mikado-themes.com/wp-content/uploads/2018/03/tour-7-featured-img-768x895.jpg 768w, https://wanderers.mikado-themes.com/wp-content/uploads/2018/03/tour-7-featured-img-450x524.jpg 450w" sizes="(max-width: 800px) 100vw, 800px" />
                                                                                <div class="mkdf-tours-gallery-item-content-holder">
                                                                                    <div class="mkdf-tours-gallery-item-content-inner">
                                                                                        <div class="mkdf-tours-gallery-main-info">
                                                                                                <span class="mkdf-tours-gallery-item-label-holder">

		<span class="mkdf-tour-item-label">
			<span class="mkdf-tour-item-label-inner">
							</span>
                                                                                                </span>

                                                                                                </span>
                                                                                            <div class="mkdf-tours-gallery-title-holder">
                                                                                                <h4 class="mkdf-tour-title">
                                                                                                    Pyramids Tour                            </h4>
                                                                                            </div>
                                                                                            <div class="mkdf-tours-gallery-item-excerpt">
                                                                                                <div class="mkdf-tours-gallery-item-excerpt-inner">
                                                                                                    Lorem ipsum dolor sit amet, conse tetur adipiscing elit. Mauris a rutrum&hellip; </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <span class="mkdf-tours-gallery-item-price-holder">

		<span class="mkdf-tours-price-holder">
							<span class="mkdf-tours-item-price ">$1250</span>
                                                                                            </span>

                                                                                            </span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <a class="mkdf-tours-gallery-item-link" href="https://wanderers.mikado-themes.com/tour-item/pyramids-tour/"></a>
                                                                        </div>
                                                                    </div>
                                                                    <div class="mkdf-tours-gallery-item mkdf-tours-row-item mkdf-item-space mkdf-tour-item-has-rating post-1290 tour-item type-tour-item status-publish has-post-thumbnail hentry tour-category-seaside tour-attribute-accommodation tour-attribute-guide tour-attribute-insurance tour-attribute-meals">
                                                                        <div class="mkdf-tours-gallery-item-image-holder">
                                                                            <div class="mkdf-tours-gallery-item-image">
                                                                                <img width="800" height="932" src="https://wanderers.mikado-themes.com/wp-content/uploads/2018/03/tour-8-featured-img.jpg" class="attachment-full size-full" alt="a" srcset="https://wanderers.mikado-themes.com/wp-content/uploads/2018/03/tour-8-featured-img.jpg 800w, https://wanderers.mikado-themes.com/wp-content/uploads/2018/03/tour-8-featured-img-258x300.jpg 258w, https://wanderers.mikado-themes.com/wp-content/uploads/2018/03/tour-8-featured-img-768x895.jpg 768w, https://wanderers.mikado-themes.com/wp-content/uploads/2018/03/tour-8-featured-img-450x524.jpg 450w" sizes="(max-width: 800px) 100vw, 800px" />
                                                                                <div class="mkdf-tours-gallery-item-content-holder">
                                                                                    <div class="mkdf-tours-gallery-item-content-inner">
                                                                                        <div class="mkdf-tours-gallery-main-info">
                                                                                                <span class="mkdf-tours-gallery-item-label-holder">

		<span class="mkdf-tour-item-label">
			<span class="mkdf-tour-item-label-inner">
							</span>
                                                                                                </span>

                                                                                                </span>
                                                                                            <div class="mkdf-tours-gallery-title-holder">
                                                                                                <h4 class="mkdf-tour-title">
                                                                                                    Mexico Explore                            </h4>
                                                                                            </div>
                                                                                            <div class="mkdf-tours-gallery-item-excerpt">
                                                                                                <div class="mkdf-tours-gallery-item-excerpt-inner">
                                                                                                    Lorem ipsum dolor sit amet, conse tetur adipiscing elit. Mauris a rutrum&hellip; </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <span class="mkdf-tours-gallery-item-price-holder">

		<span class="mkdf-tours-price-holder">
							<span class="mkdf-tours-item-price ">$2950</span>
                                                                                            </span>

                                                                                            </span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <a class="mkdf-tours-gallery-item-link" href="https://wanderers.mikado-themes.com/tour-item/mexico-explore/"></a>
                                                                        </div>
                                                                    </div>
                                                                    <div class="mkdf-tours-gallery-item mkdf-tours-row-item mkdf-item-space mkdf-tour-item-has-rating post-1292 tour-item type-tour-item status-publish has-post-thumbnail hentry tour-category-self-guided tour-attribute-accommodation tour-attribute-guide tour-attribute-insurance tour-attribute-meals">
                                                                        <div class="mkdf-tours-gallery-item-image-holder">
                                                                            <div class="mkdf-tours-gallery-item-image">
                                                                                <img width="800" height="932" src="https://wanderers.mikado-themes.com/wp-content/uploads/2018/03/tour-9-featured-img.jpg" class="attachment-full size-full" alt="a" srcset="https://wanderers.mikado-themes.com/wp-content/uploads/2018/03/tour-9-featured-img.jpg 800w, https://wanderers.mikado-themes.com/wp-content/uploads/2018/03/tour-9-featured-img-258x300.jpg 258w, https://wanderers.mikado-themes.com/wp-content/uploads/2018/03/tour-9-featured-img-768x895.jpg 768w, https://wanderers.mikado-themes.com/wp-content/uploads/2018/03/tour-9-featured-img-450x524.jpg 450w" sizes="(max-width: 800px) 100vw, 800px" />
                                                                                <div class="mkdf-tours-gallery-item-content-holder">
                                                                                    <div class="mkdf-tours-gallery-item-content-inner">
                                                                                        <div class="mkdf-tours-gallery-main-info">
                                                                                                <span class="mkdf-tours-gallery-item-label-holder">

		<span class="mkdf-tour-item-label">
			<span class="mkdf-tour-item-label-inner">
							</span>
                                                                                                </span>

                                                                                                </span>
                                                                                            <div class="mkdf-tours-gallery-title-holder">
                                                                                                <h4 class="mkdf-tour-title">
                                                                                                    Europe Tour                            </h4>
                                                                                            </div>
                                                                                            <div class="mkdf-tours-gallery-item-excerpt">
                                                                                                <div class="mkdf-tours-gallery-item-excerpt-inner">
                                                                                                    Lorem ipsum dolor sit amet, conse tetur adipiscing elit. Mauris a rutrum&hellip; </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <span class="mkdf-tours-gallery-item-price-holder">

		<span class="mkdf-tours-price-holder">
							<span class="mkdf-tours-item-price ">$1850</span>
                                                                                            </span>

                                                                                            </span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <a class="mkdf-tours-gallery-item-link" href="https://wanderers.mikado-themes.com/tour-item/europe-tour/"></a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="mkdf-row-grid-section-wrapper " style="background-color:#fafafa">
                                    <div class="mkdf-row-grid-section">
                                        <div class="vc_row wpb_row vc_row-fluid vc_custom_1521647458816">
                                            <div class="wpb_column vc_column_container vc_col-sm-12">
                                                <div class="vc_column-inner ">
                                                    <div class="wpb_wrapper">
                                                        <div class="mkdf-tours-carousel-holder mkdf-carousel-pagination">
                                                            <div class="mkdf-tours-row mkdf-normal-space">
                                                                <div class="mkdf-tours-carousel mkdf-tours-row-inner-holder mkdf-owl-slider" data-number-of-items="3" data-enable-loop="yes" data-enable-navigation="no" data-enable-pagination="yes">

                                                                    <div class="mkdf-tours-gallery-item mkdf-tours-row-item mkdf-item-space mkdf-tour-item-has-rating post-57 tour-item type-tour-item status-publish has-post-thumbnail hentry tour-category-seaside tour-attribute-accommodation tour-attribute-guide tour-attribute-insurance tour-attribute-meals">
                                                                        <div class="mkdf-tours-gallery-item-image-holder">
                                                                            <div class="mkdf-tours-gallery-item-image">
                                                                                <img width="800" height="932" src="https://wanderers.mikado-themes.com/wp-content/uploads/2018/02/tour-1-featured-img.jpg" class="attachment-full size-full" alt="a" srcset="https://wanderers.mikado-themes.com/wp-content/uploads/2018/02/tour-1-featured-img.jpg 800w, https://wanderers.mikado-themes.com/wp-content/uploads/2018/02/tour-1-featured-img-258x300.jpg 258w, https://wanderers.mikado-themes.com/wp-content/uploads/2018/02/tour-1-featured-img-768x895.jpg 768w, https://wanderers.mikado-themes.com/wp-content/uploads/2018/02/tour-1-featured-img-450x524.jpg 450w" sizes="(max-width: 800px) 100vw, 800px" />
                                                                                <div class="mkdf-tours-gallery-item-content-holder">
                                                                                    <div class="mkdf-tours-gallery-item-content-inner">
                                                                                        <div class="mkdf-tours-gallery-main-info">
                                                                                                <span class="mkdf-tours-gallery-item-label-holder">

		<span class="mkdf-tour-item-label">
			<span class="mkdf-tour-item-label-inner">
				fun			</span>
                                                                                                </span>

                                                                                                </span>
                                                                                            <div class="mkdf-tours-gallery-title-holder">
                                                                                                <h4 class="mkdf-tour-title">
                                                                                                    Ultimate Thailand                            </h4>
                                                                                            </div>
                                                                                            <div class="mkdf-tours-gallery-item-excerpt">
                                                                                                <div class="mkdf-tours-gallery-item-excerpt-inner">
                                                                                                    Lorem ipsum dolor sit amet, conse tetur adipiscing elit. Mauris a rutrum&hellip; </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <span class="mkdf-tours-gallery-item-price-holder">

		<span class="mkdf-tours-price-holder">
							<span class="mkdf-tours-item-price ">$1350</span>
                                                                                            </span>

                                                                                            </span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <a class="mkdf-tours-gallery-item-link" href="https://wanderers.mikado-themes.com/tour-item/ultimate-thailand/"></a>
                                                                        </div>
                                                                    </div>
                                                                    <div class="mkdf-tours-gallery-item mkdf-tours-row-item mkdf-item-space mkdf-tour-item-has-rating post-1036 tour-item type-tour-item status-publish has-post-thumbnail hentry tour-category-adventure review-criteria-accommodation review-criteria-destination review-criteria-meals review-criteria-transport review-criteria-value_for_money tour-attribute-accommodation tour-attribute-guide tour-attribute-insurance tour-attribute-meals">
                                                                        <div class="mkdf-tours-gallery-item-image-holder">
                                                                            <div class="mkdf-tours-gallery-item-image">
                                                                                <img width="800" height="932" src="https://wanderers.mikado-themes.com/wp-content/uploads/2018/03/tour-2-featured-img.jpg" class="attachment-full size-full" alt="a" srcset="https://wanderers.mikado-themes.com/wp-content/uploads/2018/03/tour-2-featured-img.jpg 800w, https://wanderers.mikado-themes.com/wp-content/uploads/2018/03/tour-2-featured-img-258x300.jpg 258w, https://wanderers.mikado-themes.com/wp-content/uploads/2018/03/tour-2-featured-img-768x895.jpg 768w, https://wanderers.mikado-themes.com/wp-content/uploads/2018/03/tour-2-featured-img-450x524.jpg 450w" sizes="(max-width: 800px) 100vw, 800px" />
                                                                                <div class="mkdf-tours-gallery-item-content-holder">
                                                                                    <div class="mkdf-tours-gallery-item-content-inner">
                                                                                        <div class="mkdf-tours-gallery-main-info">
                                                                                                <span class="mkdf-tours-gallery-item-label-holder">

		<span class="mkdf-tour-item-label">
			<span class="mkdf-tour-item-label-inner">
							</span>
                                                                                                </span>

                                                                                                </span>
                                                                                            <div class="mkdf-tours-gallery-title-holder">
                                                                                                <h4 class="mkdf-tour-title">
                                                                                                    Discover India                            </h4>
                                                                                            </div>
                                                                                            <div class="mkdf-tours-gallery-item-excerpt">
                                                                                                <div class="mkdf-tours-gallery-item-excerpt-inner">
                                                                                                    Lorem ipsum dolor sit amet, conse tetur adipiscing elit. Mauris a rutrum&hellip; </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <span class="mkdf-tours-gallery-item-price-holder">

		<span class="mkdf-tours-price-holder">
							<span class="mkdf-tours-item-price ">$2650</span>
                                                                                            </span>

                                                                                            </span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <a class="mkdf-tours-gallery-item-link" href="https://wanderers.mikado-themes.com/tour-item/discover-india/"></a>
                                                                        </div>
                                                                    </div>
                                                                    <div class="mkdf-tours-gallery-item mkdf-tours-row-item mkdf-item-space mkdf-tour-item-has-rating post-1039 tour-item type-tour-item status-publish has-post-thumbnail hentry tour-category-adventure review-criteria-accommodation review-criteria-destination review-criteria-meals review-criteria-transport review-criteria-value_for_money tour-attribute-accommodation tour-attribute-guide tour-attribute-insurance tour-attribute-meals">
                                                                        <div class="mkdf-tours-gallery-item-image-holder">
                                                                            <div class="mkdf-tours-gallery-item-image">
                                                                                <img width="800" height="932" src="https://wanderers.mikado-themes.com/wp-content/uploads/2018/03/tour-3-featured-img.jpg" class="attachment-full size-full" alt="a" srcset="https://wanderers.mikado-themes.com/wp-content/uploads/2018/03/tour-3-featured-img.jpg 800w, https://wanderers.mikado-themes.com/wp-content/uploads/2018/03/tour-3-featured-img-258x300.jpg 258w, https://wanderers.mikado-themes.com/wp-content/uploads/2018/03/tour-3-featured-img-768x895.jpg 768w, https://wanderers.mikado-themes.com/wp-content/uploads/2018/03/tour-3-featured-img-450x524.jpg 450w" sizes="(max-width: 800px) 100vw, 800px" />
                                                                                <div class="mkdf-tours-gallery-item-content-holder">
                                                                                    <div class="mkdf-tours-gallery-item-content-inner">
                                                                                        <div class="mkdf-tours-gallery-main-info">
                                                                                                <span class="mkdf-tours-gallery-item-label-holder">

		<span class="mkdf-tour-item-label">
			<span class="mkdf-tour-item-label-inner">
							</span>
                                                                                                </span>

                                                                                                </span>
                                                                                            <div class="mkdf-tours-gallery-title-holder">
                                                                                                <h4 class="mkdf-tour-title">
                                                                                                    Forest Adventure                            </h4>
                                                                                            </div>
                                                                                            <div class="mkdf-tours-gallery-item-excerpt">
                                                                                                <div class="mkdf-tours-gallery-item-excerpt-inner">
                                                                                                    Lorem ipsum dolor sit amet, conse tetur adipiscing elit. Mauris a rutrum&hellip; </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <span class="mkdf-tours-gallery-item-price-holder">

		<span class="mkdf-tours-price-holder">
							<span class="mkdf-tours-item-price ">$1350</span>
                                                                                            </span>

                                                                                            </span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <a class="mkdf-tours-gallery-item-link" href="https://wanderers.mikado-themes.com/tour-item/forest-adventure/"></a>
                                                                        </div>
                                                                    </div>
                                                                    <div class="mkdf-tours-gallery-item mkdf-tours-row-item mkdf-item-space mkdf-tour-item-has-rating post-1063 tour-item type-tour-item status-publish has-post-thumbnail hentry tour-category-historical tour-attribute-accommodation tour-attribute-guide tour-attribute-insurance tour-attribute-meals">
                                                                        <div class="mkdf-tours-gallery-item-image-holder">
                                                                            <div class="mkdf-tours-gallery-item-image">
                                                                                <img width="800" height="932" src="https://wanderers.mikado-themes.com/wp-content/uploads/2018/03/tour-4-featured-img.jpg" class="attachment-full size-full" alt="a" srcset="https://wanderers.mikado-themes.com/wp-content/uploads/2018/03/tour-4-featured-img.jpg 800w, https://wanderers.mikado-themes.com/wp-content/uploads/2018/03/tour-4-featured-img-258x300.jpg 258w, https://wanderers.mikado-themes.com/wp-content/uploads/2018/03/tour-4-featured-img-768x895.jpg 768w, https://wanderers.mikado-themes.com/wp-content/uploads/2018/03/tour-4-featured-img-450x524.jpg 450w" sizes="(max-width: 800px) 100vw, 800px" />
                                                                                <div class="mkdf-tours-gallery-item-content-holder">
                                                                                    <div class="mkdf-tours-gallery-item-content-inner">
                                                                                        <div class="mkdf-tours-gallery-main-info">
                                                                                                <span class="mkdf-tours-gallery-item-label-holder">

		<span class="mkdf-tour-item-label">
			<span class="mkdf-tour-item-label-inner">
							</span>
                                                                                                </span>

                                                                                                </span>
                                                                                            <div class="mkdf-tours-gallery-title-holder">
                                                                                                <h4 class="mkdf-tour-title">
                                                                                                    China Tour                            </h4>
                                                                                            </div>
                                                                                            <div class="mkdf-tours-gallery-item-excerpt">
                                                                                                <div class="mkdf-tours-gallery-item-excerpt-inner">
                                                                                                    Lorem ipsum dolor sit amet, conse tetur adipiscing elit. Mauris a rutrum&hellip; </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <span class="mkdf-tours-gallery-item-price-holder">

		<span class="mkdf-tours-price-holder">
							<span class="mkdf-tours-item-price ">$1500</span>
                                                                                            </span>

                                                                                            </span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <a class="mkdf-tours-gallery-item-link" href="https://wanderers.mikado-themes.com/tour-item/china-tour/"></a>
                                                                        </div>
                                                                    </div>
                                                                    <div class="mkdf-tours-gallery-item mkdf-tours-row-item mkdf-item-space mkdf-tour-item-has-rating post-1066 tour-item type-tour-item status-publish has-post-thumbnail hentry tour-category-seaside tour-attribute-accommodation tour-attribute-guide tour-attribute-insurance tour-attribute-meals">
                                                                        <div class="mkdf-tours-gallery-item-image-holder">
                                                                            <div class="mkdf-tours-gallery-item-image">
                                                                                <img width="800" height="932" src="https://wanderers.mikado-themes.com/wp-content/uploads/2018/03/tour-5-featured-img.jpg" class="attachment-full size-full" alt="a" srcset="https://wanderers.mikado-themes.com/wp-content/uploads/2018/03/tour-5-featured-img.jpg 800w, https://wanderers.mikado-themes.com/wp-content/uploads/2018/03/tour-5-featured-img-258x300.jpg 258w, https://wanderers.mikado-themes.com/wp-content/uploads/2018/03/tour-5-featured-img-768x895.jpg 768w, https://wanderers.mikado-themes.com/wp-content/uploads/2018/03/tour-5-featured-img-450x524.jpg 450w" sizes="(max-width: 800px) 100vw, 800px" />
                                                                                <div class="mkdf-tours-gallery-item-content-holder">
                                                                                    <div class="mkdf-tours-gallery-item-content-inner">
                                                                                        <div class="mkdf-tours-gallery-main-info">
                                                                                                <span class="mkdf-tours-gallery-item-label-holder">

		<span class="mkdf-tour-item-label">
			<span class="mkdf-tour-item-label-inner">
							</span>
                                                                                                </span>

                                                                                                </span>
                                                                                            <div class="mkdf-tours-gallery-title-holder">
                                                                                                <h4 class="mkdf-tour-title">
                                                                                                    Safari Tour                            </h4>
                                                                                            </div>
                                                                                            <div class="mkdf-tours-gallery-item-excerpt">
                                                                                                <div class="mkdf-tours-gallery-item-excerpt-inner">
                                                                                                    Lorem ipsum dolor sit amet, conse tetur adipiscing elit. Mauris a rutrum&hellip; </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <span class="mkdf-tours-gallery-item-price-holder">

		<span class="mkdf-tours-price-holder">
							<span class="mkdf-tours-item-price ">$1650</span>
                                                                                            </span>

                                                                                            </span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <a class="mkdf-tours-gallery-item-link" href="https://wanderers.mikado-themes.com/tour-item/safari-tour/"></a>
                                                                        </div>
                                                                    </div>
                                                                    <div class="mkdf-tours-gallery-item mkdf-tours-row-item mkdf-item-space mkdf-tour-item-has-rating post-1067 tour-item type-tour-item status-publish has-post-thumbnail hentry tour-category-self-guided tour-attribute-accommodation tour-attribute-guide tour-attribute-insurance tour-attribute-meals">
                                                                        <div class="mkdf-tours-gallery-item-image-holder">
                                                                            <div class="mkdf-tours-gallery-item-image">
                                                                                <img width="800" height="932" src="https://wanderers.mikado-themes.com/wp-content/uploads/2018/03/tour-6-featured-img.jpg" class="attachment-full size-full" alt="a" srcset="https://wanderers.mikado-themes.com/wp-content/uploads/2018/03/tour-6-featured-img.jpg 800w, https://wanderers.mikado-themes.com/wp-content/uploads/2018/03/tour-6-featured-img-258x300.jpg 258w, https://wanderers.mikado-themes.com/wp-content/uploads/2018/03/tour-6-featured-img-768x895.jpg 768w, https://wanderers.mikado-themes.com/wp-content/uploads/2018/03/tour-6-featured-img-450x524.jpg 450w" sizes="(max-width: 800px) 100vw, 800px" />
                                                                                <div class="mkdf-tours-gallery-item-content-holder">
                                                                                    <div class="mkdf-tours-gallery-item-content-inner">
                                                                                        <div class="mkdf-tours-gallery-main-info">
                                                                                                <span class="mkdf-tours-gallery-item-label-holder">

		<span class="mkdf-tour-item-label">
			<span class="mkdf-tour-item-label-inner">
				new			</span>
                                                                                                </span>

                                                                                                </span>
                                                                                            <div class="mkdf-tours-gallery-title-holder">
                                                                                                <h4 class="mkdf-tour-title">
                                                                                                    Canada Tour                            </h4>
                                                                                            </div>
                                                                                            <div class="mkdf-tours-gallery-item-excerpt">
                                                                                                <div class="mkdf-tours-gallery-item-excerpt-inner">
                                                                                                    Lorem ipsum dolor sit amet, conse tetur adipiscing elit. Mauris a rutrum&hellip; </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <span class="mkdf-tours-gallery-item-price-holder">

		<span class="mkdf-tours-price-holder">
							<span class="mkdf-tours-item-price ">$2000</span>
                                                                                            </span>

                                                                                            </span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <a class="mkdf-tours-gallery-item-link" href="https://wanderers.mikado-themes.com/tour-item/canada-tour/"></a>
                                                                        </div>
                                                                    </div>
                                                                    <div class="mkdf-tours-gallery-item mkdf-tours-row-item mkdf-item-space mkdf-tour-item-has-rating post-1289 tour-item type-tour-item status-publish has-post-thumbnail hentry tour-category-cultural review-criteria-accommodation review-criteria-destination review-criteria-meals review-criteria-transport review-criteria-value_for_money tour-attribute-accommodation tour-attribute-guide tour-attribute-insurance tour-attribute-meals">
                                                                        <div class="mkdf-tours-gallery-item-image-holder">
                                                                            <div class="mkdf-tours-gallery-item-image">
                                                                                <img width="800" height="932" src="https://wanderers.mikado-themes.com/wp-content/uploads/2018/03/tour-7-featured-img.jpg" class="attachment-full size-full" alt="a" srcset="https://wanderers.mikado-themes.com/wp-content/uploads/2018/03/tour-7-featured-img.jpg 800w, https://wanderers.mikado-themes.com/wp-content/uploads/2018/03/tour-7-featured-img-258x300.jpg 258w, https://wanderers.mikado-themes.com/wp-content/uploads/2018/03/tour-7-featured-img-768x895.jpg 768w, https://wanderers.mikado-themes.com/wp-content/uploads/2018/03/tour-7-featured-img-450x524.jpg 450w" sizes="(max-width: 800px) 100vw, 800px" />
                                                                                <div class="mkdf-tours-gallery-item-content-holder">
                                                                                    <div class="mkdf-tours-gallery-item-content-inner">
                                                                                        <div class="mkdf-tours-gallery-main-info">
                                                                                                <span class="mkdf-tours-gallery-item-label-holder">

		<span class="mkdf-tour-item-label">
			<span class="mkdf-tour-item-label-inner">
							</span>
                                                                                                </span>

                                                                                                </span>
                                                                                            <div class="mkdf-tours-gallery-title-holder">
                                                                                                <h4 class="mkdf-tour-title">
                                                                                                    Pyramids Tour                            </h4>
                                                                                            </div>
                                                                                            <div class="mkdf-tours-gallery-item-excerpt">
                                                                                                <div class="mkdf-tours-gallery-item-excerpt-inner">
                                                                                                    Lorem ipsum dolor sit amet, conse tetur adipiscing elit. Mauris a rutrum&hellip; </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <span class="mkdf-tours-gallery-item-price-holder">

		<span class="mkdf-tours-price-holder">
							<span class="mkdf-tours-item-price ">$1250</span>
                                                                                            </span>

                                                                                            </span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <a class="mkdf-tours-gallery-item-link" href="https://wanderers.mikado-themes.com/tour-item/pyramids-tour/"></a>
                                                                        </div>
                                                                    </div>
                                                                    <div class="mkdf-tours-gallery-item mkdf-tours-row-item mkdf-item-space mkdf-tour-item-has-rating post-1290 tour-item type-tour-item status-publish has-post-thumbnail hentry tour-category-seaside tour-attribute-accommodation tour-attribute-guide tour-attribute-insurance tour-attribute-meals">
                                                                        <div class="mkdf-tours-gallery-item-image-holder">
                                                                            <div class="mkdf-tours-gallery-item-image">
                                                                                <img width="800" height="932" src="https://wanderers.mikado-themes.com/wp-content/uploads/2018/03/tour-8-featured-img.jpg" class="attachment-full size-full" alt="a" srcset="https://wanderers.mikado-themes.com/wp-content/uploads/2018/03/tour-8-featured-img.jpg 800w, https://wanderers.mikado-themes.com/wp-content/uploads/2018/03/tour-8-featured-img-258x300.jpg 258w, https://wanderers.mikado-themes.com/wp-content/uploads/2018/03/tour-8-featured-img-768x895.jpg 768w, https://wanderers.mikado-themes.com/wp-content/uploads/2018/03/tour-8-featured-img-450x524.jpg 450w" sizes="(max-width: 800px) 100vw, 800px" />
                                                                                <div class="mkdf-tours-gallery-item-content-holder">
                                                                                    <div class="mkdf-tours-gallery-item-content-inner">
                                                                                        <div class="mkdf-tours-gallery-main-info">
                                                                                                <span class="mkdf-tours-gallery-item-label-holder">

		<span class="mkdf-tour-item-label">
			<span class="mkdf-tour-item-label-inner">
							</span>
                                                                                                </span>

                                                                                                </span>
                                                                                            <div class="mkdf-tours-gallery-title-holder">
                                                                                                <h4 class="mkdf-tour-title">
                                                                                                    Mexico Explore                            </h4>
                                                                                            </div>
                                                                                            <div class="mkdf-tours-gallery-item-excerpt">
                                                                                                <div class="mkdf-tours-gallery-item-excerpt-inner">
                                                                                                    Lorem ipsum dolor sit amet, conse tetur adipiscing elit. Mauris a rutrum&hellip; </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <span class="mkdf-tours-gallery-item-price-holder">

		<span class="mkdf-tours-price-holder">
							<span class="mkdf-tours-item-price ">$2950</span>
                                                                                            </span>

                                                                                            </span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <a class="mkdf-tours-gallery-item-link" href="https://wanderers.mikado-themes.com/tour-item/mexico-explore/"></a>
                                                                        </div>
                                                                    </div>
                                                                    <div class="mkdf-tours-gallery-item mkdf-tours-row-item mkdf-item-space mkdf-tour-item-has-rating post-1292 tour-item type-tour-item status-publish has-post-thumbnail hentry tour-category-self-guided tour-attribute-accommodation tour-attribute-guide tour-attribute-insurance tour-attribute-meals">
                                                                        <div class="mkdf-tours-gallery-item-image-holder">
                                                                            <div class="mkdf-tours-gallery-item-image">
                                                                                <img width="800" height="932" src="https://wanderers.mikado-themes.com/wp-content/uploads/2018/03/tour-9-featured-img.jpg" class="attachment-full size-full" alt="a" srcset="https://wanderers.mikado-themes.com/wp-content/uploads/2018/03/tour-9-featured-img.jpg 800w, https://wanderers.mikado-themes.com/wp-content/uploads/2018/03/tour-9-featured-img-258x300.jpg 258w, https://wanderers.mikado-themes.com/wp-content/uploads/2018/03/tour-9-featured-img-768x895.jpg 768w, https://wanderers.mikado-themes.com/wp-content/uploads/2018/03/tour-9-featured-img-450x524.jpg 450w" sizes="(max-width: 800px) 100vw, 800px" />
                                                                                <div class="mkdf-tours-gallery-item-content-holder">
                                                                                    <div class="mkdf-tours-gallery-item-content-inner">
                                                                                        <div class="mkdf-tours-gallery-main-info">
                                                                                                <span class="mkdf-tours-gallery-item-label-holder">

		<span class="mkdf-tour-item-label">
			<span class="mkdf-tour-item-label-inner">
							</span>
                                                                                                </span>

                                                                                                </span>
                                                                                            <div class="mkdf-tours-gallery-title-holder">
                                                                                                <h4 class="mkdf-tour-title">
                                                                                                    Europe Tour                            </h4>
                                                                                            </div>
                                                                                            <div class="mkdf-tours-gallery-item-excerpt">
                                                                                                <div class="mkdf-tours-gallery-item-excerpt-inner">
                                                                                                    Lorem ipsum dolor sit amet, conse tetur adipiscing elit. Mauris a rutrum&hellip; </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <span class="mkdf-tours-gallery-item-price-holder">

		<span class="mkdf-tours-price-holder">
							<span class="mkdf-tours-item-price ">$1850</span>
                                                                                            </span>

                                                                                            </span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <a class="mkdf-tours-gallery-item-link" href="https://wanderers.mikado-themes.com/tour-item/europe-tour/"></a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
            <!-- close div.content_inner -->
        </div>
        <!-- close div.content -->
        <footer class="mkdf-page-footer">
            <div class="mkdf-footer-top-holder">
                <div class="mkdf-footer-top-inner mkdf-grid">
                    <div class="mkdf-grid-row mkdf-footer-top-alignment-left">
                        <div class="mkdf-column-content mkdf-grid-col-3">
                            <div id="media_image-3" class="widget mkdf-footer-column-1 widget_media_image">
                                <a href="https://wanderers.mikado-themes.com"><img width="128" height="41" src="https://wanderers.mikado-themes.com/wp-content/uploads/2018/02/logo-footer.png" class="image wp-image-1468  attachment-full size-full" alt="logo" style="max-width: 100%; height: auto;" /></a>
                            </div>
                            <div class="widget mkdf-separator-widget">
                                <div class="mkdf-separator-holder clearfix  mkdf-separator-center mkdf-separator-normal">
                                    <div class="mkdf-separator" style="border-style: solid;margin-top: 0px;margin-bottom: 11px"></div>
                                </div>
                            </div>
                            <div id="text-2" class="widget mkdf-footer-column-1 widget_text">
                                <div class="textwidget">
                                    <h6 style="color: #bdbdbd;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam urna lacus porta, augue eget sagittis.</h6>
                                </div>
                            </div>
                            <div id="text-3" class="widget mkdf-footer-column-1 widget_text">
                                <div class="textwidget">
                                    <div class="mkdf-iwt clearfix  mkdf-iwt-icon-left mkdf-iwt-icon-tiny">
                                        <a itemprop="url" href="tel:167712444227" target="_blank">
                                            <div class="mkdf-iwt-icon">

                                                    <span class="mkdf-icon-shortcode mkdf-normal   mkdf-icon-tiny" data-color="#ffcc05">
                    <span aria-hidden="true" class="mkdf-icon-font-elegant icon_phone mkdf-icon-element" style="color: #ffcc05;font-size:14px" ></span> </span>
                                            </div>
                                            <div class="mkdf-iwt-content" style="padding-left: 14px">
                                                <h6 class="mkdf-iwt-title" style="color: #bdbdbd;margin-top: 10px">
                                                    <span class="mkdf-iwt-title-text">1-677-124-44227</span>
                                                </h6>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="vc_empty_space" style="height: 5px"><span class="vc_empty_space_inner"></span></div>

                                    <div class="mkdf-iwt clearfix  mkdf-iwt-icon-left mkdf-iwt-icon-tiny">
                                        <a itemprop="url" href="https://www.google.com/maps/place/487+8th+Ave,+New+York,+NY+10001,+USA/@40.7526442,-73.9953178,17z/data=!3m1!4b1!4m5!3m4!1s0x89c259adc21c3015:0x2600e29999851411!8m2!3d40.7526402!4d-73.9931291" target="_self">
                                            <div class="mkdf-iwt-icon">

                                                    <span class="mkdf-icon-shortcode mkdf-normal   mkdf-icon-tiny" data-color="#ffcc05">
                    <span aria-hidden="true" class="mkdf-icon-font-elegant icon_pin mkdf-icon-element" style="color: #ffcc05;font-size:14px" ></span> </span>
                                            </div>
                                            <div class="mkdf-iwt-content" style="padding-left: 14px">
                                                <h6 class="mkdf-iwt-title" style="color: #bdbdbd;margin-top: 10px">
                                                    <span class="mkdf-iwt-title-text">Eighth Avenue 487, New York</span>
                                                </h6>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="vc_empty_space" style="height: 5px"><span class="vc_empty_space_inner"></span></div>

                                    <div class="mkdf-iwt clearfix  mkdf-iwt-icon-left mkdf-iwt-icon-tiny">
                                        <a itemprop="url" href="mailto:wanderers.wp.theme@gmail.com" target="_self">
                                            <div class="mkdf-iwt-icon">

                                                    <span class="mkdf-icon-shortcode mkdf-normal   mkdf-icon-tiny" data-color="#ffcc05">
                    <span aria-hidden="true" class="mkdf-icon-font-elegant icon_mail mkdf-icon-element" style="color: #ffcc05;font-size:14px" ></span> </span>
                                            </div>
                                            <div class="mkdf-iwt-content" style="padding-left: 14px">
                                                <h6 class="mkdf-iwt-title" style="color: #bdbdbd;margin-top: 10px">
                                                    <span class="mkdf-iwt-title-text">wanderers@mikado-themes.com</span>
                                                </h6>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="mkdf-column-content mkdf-grid-col-3">
                            <div id="text-9" class="widget mkdf-footer-column-2 widget_text">
                                <div class="mkdf-widget-title-holder">
                                    <h5 class="mkdf-widget-title">Latest Posts</h5></div>
                                <div class="textwidget"></div>
                            </div>
                            <div class="widget mkdf-separator-widget">
                                <div class="mkdf-separator-holder clearfix  mkdf-separator-center mkdf-separator-normal">
                                    <div class="mkdf-separator" style="border-style: solid;margin-top: 0px;margin-bottom: 13px"></div>
                                </div>
                            </div>
                            <div class="widget mkdf-blog-list-widget">
                                <div class="mkdf-blog-list-holder mkdf-bl-minimal mkdf-bl-one-column mkdf-small-space mkdf-bl-pag-no-pagination" data-type=minimal data-number-of-posts=2 data-number-of-columns=1 data-space-between-items=small data-category=travel data-orderby=date data-order=DESC data-image-size=thumbnail data-title-tag=h6 data-excerpt-length=40 data-post-info-section=yes data-post-info-image=yes data-post-info-author=yes data-post-info-date=yes data-post-info-category=yes data-post-info-comments=no data-post-info-like=no data-post-info-share=no data-pagination-type=no-pagination data-max-num-pages=10 data-next-page=2>
                                    <div class="mkdf-bl-wrapper mkdf-outer-space">
                                        <ul class="mkdf-blog-list">
                                            <li class="mkdf-bl-item mkdf-item-space clearfix">
                                                <div class="mkdf-bli-inner">
                                                    <div class="mkdf-bli-content">

                                                        <h6 itemprop="name" class="entry-title mkdf-post-title">
                                                            <a itemprop="url" href="https://wanderers.mikado-themes.com/traveling-it-leaves-you-speechless-then-turns-you-into-a-storyteller/" title="Traveling – It Leaves You Speechless, Then Turns You Into A Storyteller">
                                                                Traveling – It Leaves You Speechless, Then Turns You Into A Storyteller            </a>
                                                        </h6>
                                                        <div itemprop="dateCreated" class="mkdf-post-info-date entry-date published updated">
                                                            <a itemprop="url" href="https://wanderers.mikado-themes.com/2018/02/">

                                                                <span class="icon_calendar"></span> February 21, 2018 </a>
                                                            <meta itemprop="interactionCount" content="UserComments: 0" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="mkdf-bl-item mkdf-item-space clearfix">
                                                <div class="mkdf-bli-inner">
                                                    <div class="mkdf-bli-content">

                                                        <h6 itemprop="name" class="entry-title mkdf-post-title">
                                                            <a itemprop="url" href="https://wanderers.mikado-themes.com/a-journey-is-always-best-measured-in-new-friends-rather-than-miles/" title="A Journey Is Always Best Measured In New Friends, Rather Than Miles">
                                                                A Journey Is Always Best Measured In New Friends, Rather Than Miles            </a>
                                                        </h6>
                                                        <div itemprop="dateCreated" class="mkdf-post-info-date entry-date published updated">
                                                            <a itemprop="url" href="https://wanderers.mikado-themes.com/2018/02/">

                                                                <span class="icon_calendar"></span> February 21, 2018 </a>
                                                            <meta itemprop="interactionCount" content="UserComments: 0" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="mkdf-column-content mkdf-grid-col-3">
                            <div id="text-10" class="widget mkdf-footer-column-3 widget_text">
                                <div class="mkdf-widget-title-holder">
                                    <h5 class="mkdf-widget-title">Twitter Feed</h5></div>
                                <div class="textwidget"></div>
                            </div>
                            <div class="widget mkdf-separator-widget">
                                <div class="mkdf-separator-holder clearfix  mkdf-separator-center mkdf-separator-normal">
                                    <div class="mkdf-separator" style="border-style: solid;margin-top: 0px;margin-bottom: 8px"></div>
                                </div>
                            </div>
                            <div id="mkdf_twitter_widget-2" class="widget mkdf-footer-column-3 widget_mkdf_twitter_widget">
                                <ul class="mkdf-twitter-widget mkdf-twitter-standard">

                                    <li class="mkdf-tweet-holder">
                                        <div class="mkdf-tweet-text" style="padding: 0;">
                                            Wilmër was chosen by <a target="_blank" href="https://twitter.com/WPSelected">@WPSelected</a> as one of the best corporate <span>#WordPress</span> theme in May! Whether you wish to build a… <a target="_blank" href="https://t.co/kdRNsn9mHC">https://t.co/kdRNsn9mHC</a> </div>
                                    </li>

                                    <li class="mkdf-tweet-holder">
                                        <div class="mkdf-tweet-text" style="padding: 0;">
                                            Latest news, Buzzy is trending on ThemeForest! <a target="_blank" href="https://t.co/AJH1rXPn9g">https://t.co/AJH1rXPn9g</a> Whether you are starting a modern #blog, a… <a target="_blank" href="https://t.co/pYrPGPtpv2">https://t.co/pYrPGPtpv2</a> </div>
                                    </li>

                                    <li class="mkdf-tweet-holder">
                                        <div class="mkdf-tweet-text" style="padding: 0;">
                                            Welcome to Depot! <a target="_blank" href="https://t.co/bd6sd5FL5w">https://t.co/bd6sd5FL5w</a> This contemporary <span>#WordPress</span> theme is created for any type of #furniture… <a target="_blank" href="https://t.co/RexBsGgk74">https://t.co/RexBsGgk74</a> </div>
                                    </li>

                                </ul>
                            </div>
                        </div>
                        <div class="mkdf-column-content mkdf-grid-col-3">
                            <div id="text-11" class="widget mkdf-footer-column-4 widget_text">
                                <div class="mkdf-widget-title-holder">
                                    <h5 class="mkdf-widget-title">Instagram Feed</h5></div>
                                <div class="textwidget"></div>
                            </div>
                            <div class="widget mkdf-separator-widget">
                                <div class="mkdf-separator-holder clearfix  mkdf-separator-center mkdf-separator-normal">
                                    <div class="mkdf-separator" style="border-style: solid;margin-top: 0px;margin-bottom: 18px"></div>
                                </div>
                            </div>
                            <div id="mkdf_instagram_widget-2" class="widget mkdf-footer-column-4 widget_mkdf_instagram_widget">
                                <ul class="mkdf-instagram-feed clearfix mkdf-col-3 mkdf-instagram-gallery mkdf-tiny-space">
                                    <li>
                                            <span class="mkdf-instagram-image">
							<a href="https://www.instagram.com/p/Bg2-hJ4BfGP/" target="_blank">
								<img class="thumbnail" src="https://scontent.cdninstagram.com/vp/aa410c0e0fc9947a5920d79076495b8a/5D7990B7/t51.2885-15/e35/s150x150/29090247_581253678903140_1982942420098088960_n.jpg?_nc_ht=scontent.cdninstagram.com" alt="" width="150" height="150" />							</a>
						</span>
                                    </li>
                                    <li>
                                            <span class="mkdf-instagram-image">
							<a href="https://www.instagram.com/p/Bg2-eTwBsFv/" target="_blank">
								<img class="thumbnail" src="https://scontent.cdninstagram.com/vp/0d1954301125706c7590050673c066ee/5D9F458A/t51.2885-15/e35/s150x150/29401867_2176532782373693_3886205036417515520_n.jpg?_nc_ht=scontent.cdninstagram.com" alt="" width="150" height="150" />							</a>
						</span>
                                    </li>
                                    <li>
                                            <span class="mkdf-instagram-image">
							<a href="https://www.instagram.com/p/Bg2-Z1XB96p/" target="_blank">
								<img class="thumbnail" src="https://scontent.cdninstagram.com/vp/6c209852f2d50dbc8ddad4984481d50f/5D5D00BA/t51.2885-15/e35/s150x150/29092667_243519266218653_6451999218939723776_n.jpg?_nc_ht=scontent.cdninstagram.com" alt="" width="150" height="150" />							</a>
						</span>
                                    </li>
                                    <li>
                                            <span class="mkdf-instagram-image">
							<a href="https://www.instagram.com/p/Bg2-XTHB5PD/" target="_blank">
								<img class="thumbnail" src="https://scontent.cdninstagram.com/vp/c033aee71f0e64f3caf3834f1b58ab42/5D66EF9E/t51.2885-15/e35/s150x150/29092778_1482724358503982_3208307073662058496_n.jpg?_nc_ht=scontent.cdninstagram.com" alt="" width="150" height="150" />							</a>
						</span>
                                    </li>
                                    <li>
                                            <span class="mkdf-instagram-image">
							<a href="https://www.instagram.com/p/Bg2-S8WBR5y/" target="_blank">
								<img class="thumbnail" src="https://scontent.cdninstagram.com/vp/6fd66e18b8a5e4ba92177b44f67e9f28/5D9624AE/t51.2885-15/e35/s150x150/29090942_584261535287684_6588805231671246848_n.jpg?_nc_ht=scontent.cdninstagram.com" alt="" width="150" height="150" />							</a>
						</span>
                                    </li>
                                    <li>
                                            <span class="mkdf-instagram-image">
							<a href="https://www.instagram.com/p/Bg2-Qe5h_Dl/" target="_blank">
								<img class="thumbnail" src="https://scontent.cdninstagram.com/vp/78f484090928e24a5753b5b48d250330/5D7F5DF5/t51.2885-15/e35/s150x150/29416225_336642600190998_5925846750014210048_n.jpg?_nc_ht=scontent.cdninstagram.com" alt="" width="150" height="150" />							</a>
						</span>
                                    </li>
                                </ul>
                            </div>
                            <div class="widget mkdf-social-icons-group-widget text-align-center">
                                <a class="mkdf-social-icon-widget-holder mkdf-icon-has-hover" data-hover-color="#ffcc05" style="color: #ffffff;;font-size: 14px;margin: 0px 9px;" href="https://twitter.com/MikadoThemes?lang=en" target="_blank">
                                    <span class="mkdf-social-icon-widget social_twitter"></span> </a>
                                <a class="mkdf-social-icon-widget-holder mkdf-icon-has-hover" data-hover-color="#ffcc05" style="color: #ffffff;;font-size: 14px;margin: 0px 9px;" href="https://www.facebook.com/Mikado-Themes-884182241781117" target="_blank">
                                    <span class="mkdf-social-icon-widget social_facebook"></span> </a>
                                <a class="mkdf-social-icon-widget-holder mkdf-icon-has-hover" data-hover-color="#ffcc05" style="color: #ffffff;;font-size: 14px;margin: 0px 9px;" href="https://www.instagram.com/mikadothemes/" target="_blank">
                                    <span class="mkdf-social-icon-widget social_instagram"></span> </a>
                                <a class="mkdf-social-icon-widget-holder mkdf-icon-has-hover" data-hover-color="#ffcc05" style="color: #ffffff;;font-size: 14px;margin: 0px 9px;" href="http://www.tumblr.com" target="_blank">
                                    <span class="mkdf-social-icon-widget social_tumblr"></span> </a>
                                <a class="mkdf-social-icon-widget-holder mkdf-icon-has-hover" data-hover-color="#ffcc05" style="color: #ffffff;;font-size: 14px;margin: 0px 9px;" href="http://www.linkedin.com" target="_blank">
                                    <span class="mkdf-social-icon-widget social_linkedin"></span> </a>
                                <a class="mkdf-social-icon-widget-holder mkdf-icon-has-hover" data-hover-color="#ffcc05" style="color: #ffffff;;font-size: 14px;margin: 0px 9px;" href="https://www.pinterest.com/mikadothemes/" target="_blank">
                                    <span class="mkdf-social-icon-widget social_pinterest"></span> </a>
                                <a class="mkdf-social-icon-widget-holder mkdf-icon-has-hover" data-hover-color="#ffcc05" style="color: #ffffff;;font-size: 14px;margin: 0px 9px;" href="http://www.vimeo.com" target="_blank">
                                    <span class="mkdf-social-icon-widget social_vimeo"></span> </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="mkdf-footer-bottom-holder">
                <div class="mkdf-footer-bottom-inner mkdf-grid">
                    <div class="mkdf-grid-row ">
                        <div class="mkdf-grid-col-12">
                            <div class="widget mkdf-separator-widget">
                                <div class="mkdf-separator-holder clearfix  mkdf-separator-center mkdf-separator-normal">
                                    <div class="mkdf-separator" style="border-style: solid;margin-top: 0px;margin-bottom: 15px"></div>
                                </div>
                            </div>
                            <div id="text-6" class="widget mkdf-footer-bottom-column-1 widget_text">
                                <div class="textwidget">
                                    <h6>© Copyright <a href="https://qodeinteractive.com/theme-author/mikado-wordpress-themes" target="_blank" rel="noopener">Mikado Themes</a> 2018 All Rights Reserved</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>
    <!-- close div.mkdf-wrapper-inner  -->
</div>
<!-- close div.mkdf-wrapper -->
<div class="mkdf-login-register-holder">
    <div class="mkdf-login-register-content">
        <ul>
            <li><a href="#mkdf-login-content">Login</a></li>
            <li><a href="#mkdf-register-content">Register</a></li>
        </ul>
        <div class="mkdf-login-content-inner" id="mkdf-login-content">
            <div class="mkdf-wp-login-holder">
                <div class="mkdf-social-login-holder">
                    <div class="mkdf-social-login-holder-inner">
                        <form method="post" class="mkdf-login-form">
                            <p class="mkdf-membership-popup-title">Wanderers</p>
                            <fieldset>
                                <div>
                                    <i class="mkdf-popup-icon ion-person"></i>
                                    <input type="text" name="user_login_name" id="user_login_name" placeholder="User Name" value="" required pattern=".{3,}" title="Three or more characters" />
                                </div>
                                <div class="mkdf-login-pw">
                                    <i class="mkdf-popup-icon ion-unlocked"></i>
                                    <input type="password" name="user_login_password" id="user_login_password" placeholder="Password" value="" required/>
                                </div>
                                <div class="mkdf-lost-pass-remember-holder clearfix">
                                        <span class="mkdf-login-remember">
                        <input name="rememberme" value="forever" id="rememberme" type="checkbox"/>
                        <label for="rememberme" class="mkdf-checbox-label">Remember me</label>
                    </span>
                                </div>
                                <a href="https://wanderers.mikado-themes.com/my-account/lost-password/" class="mkdf-login-action-btn" data-el="#mkdf-reset-pass-content" data-title="Lost Password?">Lost Your password?</a>
                                <input type="hidden" name="redirect" id="redirect" value="">
                                <div class="mkdf-login-button-holder">
                                    <button type="submit" class="mkdf-btn mkdf-btn-small mkdf-btn-solid"> <span class="mkdf-btn-text">Login</span> </button>
                                    <input type="hidden" id="mkdf-login-security" name="mkdf-login-security" value="855c2ee7a4" />
                                    <input type="hidden" name="_wp_http_referer" value="/elements/tour-carousel/" /> </div>
                            </fieldset>
                        </form>
                    </div>
                    <div class="mkdf-membership-response-holder clearfix"></div>
                    <script type="text/template" class="mkdf-membership-response-template">
                        <div class="mkdf-membership-response <%= messageClass %> ">
                                <div class="mkdf-membership-response-message">
                                    <p>
                                        <%= message %>
                                    </p>
                                </div>
                            </div>
                        </script>
                    </div>
                </div>
            </div>
            <div class="mkdf-register-content-inner" id="mkdf-register-content">
                <div class="mkdf-wp-register-holder">
                    <div class="mkdf-social-register-holder">
                        <form method="post" class="mkdf-register-form">
                            <p class="mkdf-membership-popup-title">Wanderers</p>
                            <fieldset>
                                <div>
                                    <i class="mkdf-popup-icon ion-person"></i>
                                    <input type="text" name="user_register_name" id="user_register_name" placeholder="User Name" value="" required pattern=".{3,}" title="Three or more characters" />
                                </div>
                                <div>
                                    <i class="mkdf-popup-icon ion-email"></i>
                                    <input type="email" name="user_register_email" id="user_register_email" placeholder="Email" value="" required />
                                </div>
                                <div>
                                    <i class="mkdf-popup-icon ion-unlocked"></i>
                                    <input type="password" name="user_register_password" id="user_register_password" placeholder="Password" value="" required />
                                </div>
                                <div>
                                    <i class="mkdf-popup-icon ion-unlocked"></i>
                                    <input type="password" name="user_register_confirm_password" id="user_register_confirm_password" placeholder="Repeat Password" value="" required />
                                </div>
                                <div class="mkdf-register-button-holder">
                                    <button type="submit" class="mkdf-btn mkdf-btn-small mkdf-btn-solid"> <span class="mkdf-btn-text">Register</span> </button>
                                    <input type="hidden" id="mkdf-register-security" name="mkdf-register-security" value="ca9c407477" />
                                    <input type="hidden" name="_wp_http_referer" value="/elements/tour-carousel/" /> </div>
                            </fieldset>
                        </form>
                        <div class="mkdf-membership-response-holder clearfix"></div>
                        <script type="text/template" class="mkdf-membership-response-template">
                            <div class="mkdf-membership-response <%= messageClass %> ">
                                <div class="mkdf-membership-response-message">
                                    <p>
                                        <%= message %>
                                    </p>
                                </div>
                            </div>
                        </script>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="rbt-toolbar" data-theme="Wanderers" data-featured="" data-button-position="62%" data-button-horizontal="right" data-button-alt="no"></div>
    <!-- Google Tag Manager (noscript) -->
    <noscript>
        <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KTQ2BTD" height="0" width="0" style="display:none;visibility:hidden"></iframe>
    </noscript>
    <!-- End Google Tag Manager (noscript) -->
    <script type='text/javascript' src='https://wanderers.mikado-themes.com/wp-content/plugins/js_composer/assets/js/dist/js_composer_front.min.js'></script>
    <script type='text/javascript'>
        /* <![CDATA[ */
        var wpcf7 = {
            "apiSettings": {
                "root": "https:\/\/wanderers.mikado-themes.com\/wp-json\/contact-form-7\/v1",
                "namespace": "contact-form-7\/v1"
            },
            "recaptcha": {
                "messages": {
                    "empty": "Please verify that you are not a robot."
                }
            },
            "cached": "1"
        };
        /* ]]> */
    </script>
    <script type='text/javascript' src='https://wanderers.mikado-themes.com/wp-content/plugins/contact-form-7/includes/js/scripts.js'></script>
    <script type='text/javascript' src='https://wanderers.mikado-themes.com/wp-includes/js/underscore.min.js'></script>
    <script type='text/javascript' src='https://wanderers.mikado-themes.com/wp-includes/js/jquery/ui/core.min.js'></script>
    <script type='text/javascript' src='https://wanderers.mikado-themes.com/wp-includes/js/jquery/ui/widget.min.js'></script>
    <script type='text/javascript' src='https://wanderers.mikado-themes.com/wp-includes/js/jquery/ui/tabs.min.js'></script>
    <script type='text/javascript'>
        /* <![CDATA[ */
        var mkdfGlobalVars = {
            "vars": {
                "mkdfAddForAdminBar": 0,
                "mkdfElementAppearAmount": -100,
                "mkdfAjaxUrl": "https:\/\/wanderers.mikado-themes.com\/wp-admin\/admin-ajax.php",
                "mkdfStickyHeaderHeight": 70,
                "mkdfStickyHeaderTransparencyHeight": 70,
                "mkdfTopBarHeight": 46,
                "mkdfLogoAreaHeight": 0,
                "mkdfMenuAreaHeight": 126,
                "mkdfMobileHeaderHeight": 70
            }
        };
        var mkdfPerPageVars = {
            "vars": {
                "mkdfStickyScrollAmount": 0,
                "mkdfHeaderTransparencyHeight": 0,
                "mkdfHeaderVerticalWidth": 0
            }
        };
        /* ]]> */
    </script>
    <script type='text/javascript' src='https://wanderers.mikado-themes.com/wp-content/themes/wanderers/assets/js/modules.min.js'></script>
    <script type='text/javascript' src='https://wanderers.mikado-themes.com/wp-content/plugins/mkdf-membership/assets/js/membership.min.js'></script>
    <script type='text/javascript' src='https://wanderers.mikado-themes.com/wp-content/plugins/mkdf-tours/assets/js/modules/plugins/nouislider.min.js'></script>
    <script type='text/javascript' src='https://wanderers.mikado-themes.com/wp-content/plugins/mkdf-tours/assets/js/modules/plugins/typeahead.bundle.min.js'></script>
    <script type='text/javascript' src='https://wanderers.mikado-themes.com/wp-content/plugins/mkdf-tours/assets/js/modules/plugins/bloodhound.min.js'></script>
    <script type='text/javascript' src='https://wanderers.mikado-themes.com/wp-content/plugins/mkdf-tours/assets/js/modules/plugins/select2.min.js'></script>
    <script type='text/javascript' src='https://wanderers.mikado-themes.com/wp-includes/js/jquery/ui/datepicker.min.js'></script>
    <script type='text/javascript'>
        jQuery(document).ready(function(jQuery) {
            jQuery.datepicker.setDefaults({
                "closeText": "Close",
                "currentText": "Today",
                "monthNames": ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
                "monthNamesShort": ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
                "nextText": "Next",
                "prevText": "Previous",
                "dayNames": ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
                "dayNamesShort": ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
                "dayNamesMin": ["S", "M", "T", "W", "T", "F", "S"],
                "dateFormat": "MM d, yy",
                "firstDay": 1,
                "isRTL": false
            });
        });
    </script>
    <script type='text/javascript'>
        /* <![CDATA[ */
        var mkdfToursSearchData = {
            "tours": ["Ultimate Thailand", "Discover India", "Forest Adventure", "China Tour", "Safari Tour", "Canada Tour", "Pyramids Tour", "Mexico Explore", "Europe Tour", "Berlin Tour", "Budapest Tour", "Highlights Rome", "Diving Adventure", "Reindeer Lake", "Slovenia Wildlife", "European History", "Wild Lakes", "Seaside Adventure", "Discover Rome", "Hungary Tour", "Germany Tour", "City Tour", "Explore Mexico", "Egypt Tour", "Discover Thailand", "Exotic Adventure", "India Tour", "Eastern Tour", "Desert Tour", "Wildlife Tour", "Antique Europe", "Awesome Mexico", "Ancient Pyramids", "Adventure Alaska", "Action Safari", "Asian Dicovery"],
            "destinations": ["Canada", "Mexico", "Europe", "Japan", "China", "Thailand", "India", "Africa"]
        };
        /* ]]> */
    </script>
    <script type='text/javascript' src='https://wanderers.mikado-themes.com/wp-content/plugins/mkdf-tours/assets/js/tours.min.js'></script>
    <script type='text/javascript' src='https://toolbar.qodeinteractive.com/_toolbar/assets/js/jquery.lazy.min.js?ver=4.9.10'></script>
    <script type='text/javascript' src='https://toolbar.qodeinteractive.com/_toolbar/assets/js/rbt-modules.js?ver=4.9.10'></script>
    <script type='text/javascript' src='https://wanderers.mikado-themes.com/wp-content/plugins/woocommerce/assets/js/jquery-blockui/jquery.blockUI.min.js'></script>
    <script type='text/javascript' src='https://wanderers.mikado-themes.com/wp-content/plugins/woocommerce/assets/js/js-cookie/js.cookie.min.js'></script>
    <script type='text/javascript'>
        /* <![CDATA[ */
        var woocommerce_params = {
            "ajax_url": "\/wp-admin\/admin-ajax.php",
            "wc_ajax_url": "\/?wc-ajax=%%endpoint%%"
        };
        /* ]]> */
    </script>
    <script type='text/javascript' src='https://wanderers.mikado-themes.com/wp-content/plugins/woocommerce/assets/js/frontend/woocommerce.min.js'></script>
    <script type='text/javascript'>
        /* <![CDATA[ */
        var wc_cart_fragments_params = {
            "ajax_url": "\/wp-admin\/admin-ajax.php",
            "wc_ajax_url": "\/?wc-ajax=%%endpoint%%",
            "cart_hash_key": "wc_cart_hash_a2e1041dbd20ba843985c2c0042c04ae",
            "fragment_name": "wc_fragments_a2e1041dbd20ba843985c2c0042c04ae"
        };
        /* ]]> */
    </script>
    <script type='text/javascript' src='https://wanderers.mikado-themes.com/wp-content/plugins/woocommerce/assets/js/frontend/cart-fragments.min.js'></script>
    <script type='text/javascript' src='https://wanderers.mikado-themes.com/wp-includes/js/jquery/ui/accordion.min.js'></script>
    <script type='text/javascript' src='https://wanderers.mikado-themes.com/wp-includes/js/mediaelement/wp-mediaelement.min.js'></script>
    <script type='text/javascript' src='https://wanderers.mikado-themes.com/wp-content/themes/wanderers/assets/js/modules/plugins/jquery.appear.js'></script>
    <script type='text/javascript' src='https://wanderers.mikado-themes.com/wp-content/themes/wanderers/assets/js/modules/plugins/modernizr.min.js'></script>
    <script type='text/javascript' src='https://wanderers.mikado-themes.com/wp-includes/js/hoverIntent.min.js'></script>
    <script type='text/javascript' src='https://wanderers.mikado-themes.com/wp-content/themes/wanderers/assets/js/modules/plugins/jquery.plugin.js'></script>
    <script type='text/javascript' src='https://wanderers.mikado-themes.com/wp-content/themes/wanderers/assets/js/modules/plugins/owl.carousel.min.js'></script>
    <script type='text/javascript' src='https://wanderers.mikado-themes.com/wp-content/plugins/js_composer/assets/lib/waypoints/waypoints.min.js'></script>
    <script type='text/javascript' src='https://wanderers.mikado-themes.com/wp-content/themes/wanderers/assets/js/modules/plugins/fluidvids.min.js'></script>
    <script type='text/javascript' src='https://wanderers.mikado-themes.com/wp-content/plugins/js_composer/assets/lib/prettyphoto/js/jquery.prettyPhoto.min.js'></script>
    <script type='text/javascript' src='https://wanderers.mikado-themes.com/wp-content/themes/wanderers/assets/js/modules/plugins/perfect-scrollbar.jquery.min.js'></script>
    <script type='text/javascript' src='https://wanderers.mikado-themes.com/wp-content/themes/wanderers/assets/js/modules/plugins/ScrollToPlugin.min.js'></script>
    <script type='text/javascript' src='https://wanderers.mikado-themes.com/wp-content/themes/wanderers/assets/js/modules/plugins/parallax.min.js'></script>
    <script type='text/javascript' src='https://wanderers.mikado-themes.com/wp-content/themes/wanderers/assets/js/modules/plugins/jquery.waitforimages.js'></script>
    <script type='text/javascript' src='https://wanderers.mikado-themes.com/wp-content/themes/wanderers/assets/js/modules/plugins/jquery.easing.1.3.js'></script>
    <script type='text/javascript' src='https://wanderers.mikado-themes.com/wp-content/plugins/js_composer/assets/lib/bower/isotope/dist/isotope.pkgd.min.js'></script>
    <script type='text/javascript' src='https://wanderers.mikado-themes.com/wp-content/themes/wanderers/assets/js/modules/plugins/packery-mode.pkgd.min.js'></script>
    <script type='text/javascript' src='https://wanderers.mikado-themes.com/wp-content/plugins/mkdf-core/shortcodes/countdown/assets/js/plugins/jquery.countdown.min.js'></script>
    <script type='text/javascript' src='https://wanderers.mikado-themes.com/wp-content/plugins/mkdf-core/shortcodes/counter/assets/js/plugins/counter.js'></script>
    <script type='text/javascript' src='https://wanderers.mikado-themes.com/wp-content/plugins/mkdf-core/shortcodes/counter/assets/js/plugins/absoluteCounter.min.js'></script>
    <script type='text/javascript' src='https://wanderers.mikado-themes.com/wp-content/plugins/mkdf-core/shortcodes/custom-font/assets/js/plugins/typed.js'></script>
    <script type='text/javascript' src='//maps.googleapis.com/maps/api/js?key=AIzaSyADhFz3a11s8_leQorrsxUXHTQ3IYo-sYM&#038;ver=4.9.10'></script>
    <script type='text/javascript' src='https://wanderers.mikado-themes.com/wp-includes/js/wp-embed.min.js'></script>
</body>

</html>
<!--
Performance optimized by W3 Total Cache. Learn more: https://www.w3-edge.com/products/

Object Caching 193/482 objects using disk
Page Caching using disk: enhanced 
Minified using disk

Served from: wanderers.mikado-themes.com @ 2019-05-22 13:29:40 by W3 Total Cache
-->