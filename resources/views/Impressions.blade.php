@extends('layouts.newmaster')

@section('title', 'Kivu Belt')

@section('content')
    <style>

    </style>
    <div class="mkdf-wrapper">
        <div class="mkdf-wrapper-inner">
            @include('layouts.newtopmenu')
            <div class="mkdf-content" style="margin-top: -80px">
                <div class="mkdf-content-inner">
                    <div class="mkdf-full-width">
                        <div class="mkdf-full-width-inner">
                            <div class="mkdf-grid-row">
                                <div class="mkdf-page-content-holder mkdf-grid-col-12">
                                    <div class="mkdf-title-holder mkdf-centered-type mkdf-title-va-window-top mkdf-preload-background mkdf-has-bg-image mkdf-bg-parallax" style="height: 515px;background-color: #303030;background-image:url(frontend/assets/images/lakekivu2.jpg); background-size: cover;" data-height="435">
                                        <div class="mkdf-title-image">
                                            <img itemprop="image" src="frontend/assets/images/5.jpg" alt="a" />
                                        </div>
                                        <div class="mkdf-title-wrapper" >
                                            <div class="mkdf-title-inner">
                                                <div class="mkdf-grid">
                                                    <h1 class="mkdf-page-title entry-title" >Impressions</h1>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="mkdf-full-width">
                        <div class="mkdf-full-width-inner">
                            <div class="mkdf-grid-row">
                                <div class="mkdf-grid-row-medium-gutter">
                                    <div class="mkdf-grid-col-12">
                                        <article class="mkdf-tour-item-wrapper">
                                            <div class="mkdf-tour-item-section mkdf-tab-container" id="tour-item-info-id">
                                                <div class="mkdf-tour-gallery-item-holder">



                                                    <div class="mkdf-tour-gallery clearfix">
                                                        <?php
                                                        $insta_source = file_get_contents('https://www.instagram.com/kivubelt_/'); // instagrame tag url
                                                        $shards = explode('window._sharedData = ', $insta_source);
                                                        $insta_json = explode(';</script>', $shards[1]);
                                                        $insta_array = json_decode($insta_json[0]);
                                                        $instagramdata = $insta_array->entry_data->ProfilePage[0]->graphql->user->edge_owner_to_timeline_media->edges;
                                                        $secig = json_encode($instagramdata);
                                                        $limit = array_slice($instagramdata,0,5);

                                                        ?>
                                                        @foreach($instagramdata as $instagramdatas)
                                                            <div class="mkdf-tour-gallery-item">
                                                                <a href="{{$instagramdatas->node->thumbnail_src}}" data-rel="prettyPhoto[gallery_excerpt_pretty_photo]">
                                                                    <img width="550" height="550" src="{{$instagramdatas->node->thumbnail_src}}" class="attachment-wanderers_mkdf_square size-wanderers_mkdf_square" alt="a" srcset="{{$instagramdatas->node->thumbnail_src}}" sizes="(max-width: 550px) 100vw, 550px" />                            </a>
                                                            </div>
                                                        @endforeach


                                                    </div>

                                                    {{--<a itemprop="url" href="#" target="_self" class="mkdf-btn mkdf-btn-medium mkdf-btn-solid mkdf-st-title">--}}
                                                    {{--<span class="mkdf-btn-text">See More</span>--}}
                                                    {{--</a>--}}


                                                </div>
                                            </div>
                                        </article>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    @include('layouts.newfooter')
@endsection
