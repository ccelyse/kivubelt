<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AgentCompanyWebsite extends Model
{
    protected $table = "agentcompanywebsite";
    protected $fillable = ['id','companyprofileid','companywebsite'];
}
