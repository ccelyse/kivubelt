<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AboutRegionRow2 extends Model
{
    protected $table = "aboutregionrow2";
    protected $fillable = ['row_title','row_cover_pic','row_text'];
}
