<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TourOperator extends Model
{
    protected $table = "touroperator";
    protected $fillable = ['id','companyname','email','phonenumber','password'];
}
