<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AgentTourism extends Model
{
    protected $table = "agenttourism";
    protected $fillable = ['id','title','firstname','lastname','email','phonenumber','password','agentcompanyname'];

}
