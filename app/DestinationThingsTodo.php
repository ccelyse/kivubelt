<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DestinationThingsTodo extends Model
{
    protected $table = "destinationthingstodo";
    protected $fillable = ['id','destinationid','thingstodo'];
}
