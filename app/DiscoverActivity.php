<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DiscoverActivity extends Model
{
    protected $table = "discoveractivity";
    protected $fillable = ['id','discoveractivity_title','discoveractivity_coverimage','discoveractivity_generalpicture','discoveractivity_category','discoveractivity_text'];
}
