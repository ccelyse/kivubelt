<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AboutRegionRow1 extends Model
{
    protected $table = "aboutregionrow1";
    protected $fillable = ['row_title','row_cover_pic','row_text'];
}
