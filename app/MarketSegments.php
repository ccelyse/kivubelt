<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MarketSegments extends Model
{
    protected $table = "marketsegments";
    protected $fillable = ['id','marketsegments'];
}
