<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AgentProfilePhoto extends Model
{
    protected $table = "agentprofile_photo";
    protected $fillable = ['id','agent_profile_id','agent_profile_photoname'];

}
