<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PressRelease extends Model
{
    protected $table = "pressrelease";
    protected $fillable = ['id','news_title','news_details','newsdescription','userid'];
}
