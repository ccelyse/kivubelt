<?php

namespace App\Http\Controllers;

use App\AboutRegionRow1;
use App\AboutRegionRow2;
use App\AboutRegionRow3;
use App\AboutRwandaRow1;
use App\AboutRwandaRow2;
use App\ActivityCategory;
use App\AddDestination;
use App\AddPlaces;
use App\CompanyCategory;
use App\CountryProfile;
use App\DestinationThingsTodo;
use App\DiscoverActivity;
use App\EatAndDrink;
use App\Event;
use App\EventAttending;
use App\ExperienceDetails;
use App\ExperienceDetailsImage;
use App\Experiences;
use App\Homerow1;
use App\Homerow2;
use App\Homerow3;
use App\IgGallery;
use App\JoinMember;
use App\MemberPhotoLibrary;
use App\PhotoCategories;
use App\PlacesPictures;
use App\Post;
use App\PressRelease;
use App\RegisterProcessCatName;
use App\RegisterProcessQuestions;
use App\RegisterProcessSubCatName;
use App\Stay;
use App\SubCompany;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class FrontendController extends Controller
{
    public function Home()
    {
        return view('welcome');
    }
    public function NewHome()
    {
        $listthings = AddPlaces::all();
        $listhomerow1 = Homerow1::all();
        $listhomerow2 = Homerow2::all();
        $listhomerow3 = Homerow3::all();
        $insta_source = file_get_contents('https://www.instagram.com/kivubelt_/'); // instagrame tag url
        $shards = explode('window._sharedData = ', $insta_source);
        $insta_json = explode(';</script>', $shards[1]);
        $insta_array = json_decode($insta_json[0]);
        $instagramdata = $insta_array->entry_data->ProfilePage[0]->graphql->user->edge_owner_to_timeline_media->edges;
        $secig = json_encode($instagramdata);
//        foreach ($instagramdata as $instagramdatas){
//            dd($instagramdatas->node->thumbnail_src);
//        }
//        dd($instagramdata);
        return view('new.welcome')->with(['listthings'=>$listthings,'instagramdata'=>$instagramdata,'listhomerow1'=>$listhomerow1,'listhomerow2'=>$listhomerow2,'listhomerow3'=>$listhomerow3]);
    }
    public function AboutRwanda(){
        $listrwandarow1 = AboutRwandaRow1::all();
        $listrwandarow2 = AboutRwandaRow2::all();
        return view('AboutRwanda')->with(['listrwandarow1'=>$listrwandarow1,'listrwandarow2'=>$listrwandarow2]);
    }
    public function AboutRegion(){
        $listregionrow1 = AboutRegionRow1::all();
        $listregionrow2 = AboutRegionRow2::all();
        $listregionrow3 = AboutRegionRow3::all();
        return view('AboutRegion')->with(['listregionrow1'=>$listregionrow1,'listregionrow2'=>$listregionrow2,'listregionrow3'=>$listregionrow3]);
    }
    public function ThingToDos(){
        $listthings = AddPlaces::all();
        return view('ThingToDo')->with(['listthings'=>$listthings]);
    }
    public function MoreThingsDo(Request $request){
        $id = $request['id'];
        $getmore = AddPlaces::where('id',$id)->get();
        $listthings = DiscoverActivity::where('discoveractivity_category',$id)->get();
        return view('MoreThingsDo')->with(['getmore'=>$getmore,'listthings'=>$listthings]);
    }
    public function MoreDiscoverActivity(Request $request){
        $id = $request['id'];
        $listthings = DiscoverActivity::where('id',$id)->get();
        return view('MoreDiscoverActivity')->with(['listthings'=>$listthings]);
    }

    public function Impressions(){
        return view('Impressions');
    }

    public function FStay(){
        $listthings = Stay::all();

        return view('FStay')->with(['listthings'=>$listthings]);
    }
    public function MoreStay(Request $request){
        $id = $request['id'];
        $getmore = Stay::where('id',$id)->get();
        return view('MoreStay')->with(['getmore'=>$getmore]);
    }
    public function ContactUs(){
        return view('ContactUs');
    }
    public function EatAndDrink(){
        $listeatdrinks = EatAndDrink::all();
        return view('EatAndDrink')->with(['listeatdrinks'=>$listeatdrinks]);
    }
    public function MoreEatAndDrink(Request $request){
        $id = $request['id'];
        $getmore = EatAndDrink::where('id',$id)->get();
        return view('MoreEatAndDrink')->with(['getmore'=>$getmore]);
    }

    public function Login(){
        return view('auth.login');
    }
//    public function ViewDestinations(){
////        $listdestination = AddDestination::select('AddDestination.*');
//        $listdestination = DB::table('AddDestination')->select('id','destinationtitle','coverimage')->where('topdestination','top')->orderBy('destinationtitle')->limit('8')->get();
//        return response()->json($listdestination);
//    }
//    public function ViewPlaces(){
////        $listplaces =AddPlaces::join('PlacesPictures', 'AddPlaces.id', '=', 'PlacesPictures.places_id')
////            ->select('AddPlaces.*', 'PlacesPictures.places_picture_name','PlacesPictures.places_id')
////            ->groupBy('PlacesPictures.places_id')
////            ->get();
//
//        $listplaces =AddPlaces::all();
//        return response()->json($listplaces);
//
//    }
//    public function ViewDestinationsI(Request $request){
//        $id = $request['id'];
//        $destinations = AddDestination::where('id',$id)->get();
//        return response()->json($destinations);
//
//    }
//    public function ViewDestinationPlace(Request $request){
//        $id = $request['id'];
//        $destiplace = AddPlaces::where('placedestination',$id)->get();
////        $listdestination = DB::table('AddDestination')->select('id','destinationtitle','coverimage')->get();
//        return response()->json($destiplace);
//    }
//    public function ViewMorePlace(Request $request){
//        //        $listplaces =AddPlaces::join('PlacesPictures', 'AddPlaces.id', '=', 'PlacesPictures.places_id')
////            ->select('AddPlaces.*', 'PlacesPictures.places_picture_name','PlacesPictures.places_id')
////            ->groupBy('PlacesPictures.places_id')
////            ->get();
//        $id = $request['id'];
//        $listplaces =AddPlaces::where('id',$id)->get();
//        return response()->json($listplaces);
//    }
//    public function ViewMorePlaceGallery(Request $request){
//        $id = $request['id'];
//        $listplacespictures =PlacesPictures::where('places_id',$id)->get();
//        return response()->json($listplacespictures);
//    }
//    public function ViewMorePlaceThings(Request $request){
//        $id = $request['id'];
//        $thingstodo= Experiences::where('activity_place',$id)->select('id','activity_title','activity_coverimage')->get();
//        return response()->json($thingstodo);
//    }
//    public function ViewMoreThings(Request $request){
//        $id = $request['id'];
//        $thingstodo= Experiences::where('id',$id)->get();
//        return response()->json($thingstodo);
//    }
//    public function Experiencedetails(Request $request){
//        $id = $request['id'];
//        $experiencedetails= ExperienceDetails::where('activity_id',$id)->get();
//        return response()->json($experiencedetails);
//    }
//    public function ExperiencedetailsImage(Request $request){
//        $activity_id = $request['activity_id'];
//        $activity_details_id = $request['activity_details_id'];
//        $images = ExperienceDetailsImage::where('activity_id',$activity_id)->where('activity_details_id',$activity_details_id)->get();
//        return response()->json($images);
//    }
//    public function HomeExperience(){
//        $listactivity = Experiences::select('id','activity_title','activity_coverimage')->limit(6)->get();
//        return response()->json($listactivity);
//    }
//    public function ExperienceAll(){
//        $listallexp =Experiences::all();
//        return response()->json($listallexp);
//
//    }
//    public function ExperienceMore(Request $request){
//        $id = $request['id'];
//        $listmoreexp =Experiences::where('id',$id)->get();
//        return response()->json($listmoreexp);
//    }
//
//    public function AllActivity(){
//        $listallact = ActivityCategory::all();
//        return response()->json($listallact);
//    }
//    public function GetRegisterQuestionCat(){
//        $allcat = RegisterProcessCatName::all();
//        return response()->json($allcat);
//    }
//    public function GetRegisterQuestionSubCat(Request $request){
//        $id = $request['id'];
//        $allsubcat = RegisterProcessSubCatName::where('category_name_id',$id)->get();
//        return response()->json($allsubcat);
//    }
//    public function GetRegisterQuestionQuestion(Request $request){
//        $id = $request['id'];
//        $allques = RegisterProcessQuestions::where('category_name_id',$id)->get();
//        return response()->json($allques);
//    }
//
//    public function DestinationPackages(Request $request){
//        $id = $request['id'];
//        $thingstodo= Experiences::all();
//        return response()->json($thingstodo);
//    }
//    public function DestinationThings(Request $request){
//        $id = $request['id'];
//        $destthingstodo= DestinationThingsTodo::where('destinationid',$id);
//        return response()->json($destthingstodo);
//    }
//    public function Events(){
//        $listevent = Event::select('id','eventname','eventenddate','eventimagename','eventlocation','eventstarttime')->get();
//
//        foreach ($listevent as $eventdatas){
//            $eventdatas->id;
//            $going = EventAttending::select(DB::raw('count(id) as eventgoing'))->where('event_status','Going')->where('event_id',$eventdatas->id)->value('eventgoing');
//            $Interested = EventAttending::select(DB::raw('count(id) as eventinterested'))->where('event_status','Interested')->where('event_id',$eventdatas->id)->value('eventinterested');
//            $listevents = Event::select('id','eventname','eventenddate','eventimagename','eventlocation','eventstarttime')->get();
//
//        }
//
//        $response = array(
//            'eventinfo' => array(
//                'eventlist' => [ 'list' => $listevent,
//                    'going' => $going,
//                    'Interested' => $Interested ]
//            ),
//
//        );
//
//        return response()->json($listevent);
//    }
//    public function EventsMore(Request $request){
//        $id = $request['id'];
//        $listevent = Event::where('id',$id)->get();
//        return response()->json($listevent);
//    }
//    public function CountryCategory(){
//        $listcountry = CountryProfile::select('id','countryname','coverimage')->get();
//        return response()->json($listcountry);
//    }
//    public function CountryDestination(Request $request){
//        $id = $request['id'];
//        $listdestination = AddDestination::where('destinationcountry',$id)->get();
//        return response()->json($listdestination);
//    }
//    public function CountryMore(Request $request){
//        $id = $request['id'];
//        $listcountry = CountryProfile::where('id',$id)->get();
//        return response()->json($listcountry);
//    }
//    public function GetIgImages(){
//        $tag = 'visiteastafrica';
//        $insta_source = file_get_contents('https://www.instagram.com/explore/tags/'.$tag.'/'); // instagrame tag url
//        $shards = explode('window._sharedData = ', $insta_source);
//        $insta_json = explode(';</script>', $shards[1]);
//        $insta_array = json_decode($insta_json[0]);
//        $instagramdata = $insta_array->entry_data->TagPage[0]->graphql->hashtag->edge_hashtag_to_media->edges;
////        $instagramdata = $insta_array->entry_data->TagPage[0]->graphql->hashtag;
////        $data = $insta_array->country_code;
//        dd($instagramdata);
//        return response()->json($instagramdata);
////        return view('backend.GetIgImages')->with(['instagramdata'=>$instagramdata]);
////
//    }
//    public function AddAttendant(Request $request){
//
//        $addattendance = new EventAttending();
//
//        $checkemail = EventAttending::where('event_company_email',$request['email'])->where('event_id',$request['event_id'])->value('event_company_email');
//
//        $addattendance->event_company_name = $request['companyname'];
//        $addattendance->event_company_email = $request['email'];
//        $addattendance->event_company_website = $request['web_url'];
//        $addattendance->event_status = $request['status'];
//        $addattendance->event_id = $request['event_id'];
//        $addattendance->event_attenting_country = $request['selectedCountry'];
//        $addattendance->save();
//
//        return response()->json(array(
//            'status' => 200, // success or not?
//            'message' => 'success'
//        ));
//
////        if(empty($checkemail)){
////            $addattendance->event_company_name = $request['companyname'];
////            $addattendance->event_company_email = $request['email'];
////            $addattendance->event_company_website = $request['web_url'];
////            $addattendance->event_status = $request['status'];
////            $addattendance->event_id = $request['event_id'];
////            $addattendance->event_attenting_country = $request['selectedCountry'];
////            $addattendance->save();
////
////            return response()->json(array(
////                'status' => 200, // success or not?
////                'message' => 'success'
////            ));
////        }else{
////            return response()->json(array(
////                'status' => 401, // success or not?
////                'message' => 'email is already registered'
////            ));
////        }
//
//
//    }
//    public function CountEventAttendantGoing(Request $request){
//        $going = EventAttending::select(DB::raw('count(id) as eventgoing'))->where('event_status','Going')->where('event_id',$request['event_id'])->value('eventgoing');
//        return response()->json(array(
//            'event_id' => $request['event_id'], // success or not?
//            'eventgoingnumber' => $going
//        ));
//
//    }
//    public function CountEventAttendantInterested(Request $request){
//
//        $Interested = EventAttending::select(DB::raw('count(id) as eventinterested'))->where('event_status','Interested')->where('event_id',$request['event_id'])->value('eventinterested');
//        return response()->json(array(
//            'event_id' => $request['event_id'], // success or not?
//            'eventinterestednumber' => $Interested
//        ));
//    }
//    public function ListEventGoing(Request $request){
//        $listeventgoing = EventAttending::where('event_status','Going')->where('event_id',$request['event_id'])->get();
//        $listeventinterested = EventAttending::where('event_status','Interested')->where('event_id',$request['event_id'])->get();
//
//        $response = array(
//            'listgoing' => $listeventgoing,
//            'eventinterestednumber' => $listeventinterested
//        );
//
//        return response()->json($response);
//    }
//    public function  IgChoosedPictures(){
//        $listimages = IgGallery::all();
//        return response()->json($listimages);
//
//    }
//    public function PressRelease(){
//        $press = PressRelease::all();
//        return response()->json($press);
//    }
//    public function MorePressRelease(Request $request){
//        $id = $request['id'];
//        $release = PressRelease::where('id',$id)->get();
//        return response()->json($release);
//    }
}
