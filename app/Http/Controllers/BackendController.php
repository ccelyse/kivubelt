<?php

namespace App\Http\Controllers;

use AfricasTalking\SDK\AfricasTalking;
use App\AboutRegionRow1;
use App\AboutRegionRow2;
use App\AboutRwandaRow1;
use App\AboutRwandaRow2;
use App\ActivityCategory;
use App\AddDestination;
use App\AddPlaces;
use App\AgentCompanyAddress;
use App\AgentCompanyPhone;
use App\AgentCompanyType;
use App\AgentCompanyWebsite;
use App\AgentMarketSegment;
use App\AgentProfile;
use App\AgentProfileCover;
use App\AgentProfilePhoto;
use App\AgentProfilePicture;
use App\AgentProfileServices;
use App\AgentProfileVideo;
use App\AgentTourism;
use App\Attractions;
use App\CompanyCategory;
use App\CompanyTypes;
use App\Countries;
use App\CountryProfile;
use App\DestinationThingsTodo;
use App\DiscoverActivity;
use App\EatAndDrink;
use App\EatDrinkGallery;
use App\Event;
use App\ExperienceDetails;
use App\ExperienceDetailsImage;
use App\Experiences;
use App\Gallery;
use App\Hireaguide;
use App\Homerow1;
use App\Homerow2;
use App\Homerow3;
use App\IgGallery;
use App\JoinMember;
use App\MarketSegments;
use App\MemberPhotoLibrary;
use App\MemberPhotoLibraryCategories;
use App\Notifications\WebAppNotification;
use App\OperatorProfile;
use App\PhotoCategories;
use App\PlacesPictures;
use App\Post;
use App\PressRelease;
use App\RegisterProcessCatName;
use App\RegisterProcessQuestions;
use App\RegisterProcessSubCatName;
use App\Stay;
use App\StayGallery;
use App\SubCompany;
use App\ThingToDoGallery;
use App\TourOperator;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
//use Illuminate\Notifications\Notification;
use Notification;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
require_once('./Class_MR_SMS_API.php');
class BackendController extends Controller
{

    public function CreateAccount(){
        return view('backend.CreateAccount');
    }
    public function CreateAccount_(Request $request){
        $all  = $request->all();

        $request->validate([
            'name' => 'required|string:posts|max:255',
            'email' => 'required|string|email|max:255|unique:users','exists:connection.user,email',
            'password' => 'required|string|min:6|confirmed',
        ]);

        $register = new User();
        $register->name = $request['name'];
        $register->email = $request['email'];
        $register->password = bcrypt($request['password']);
        $register->role = '0';
        $register->save();

        return redirect()->route('backend.AccountList')->with('Success','You have successfully Created Your Account');
    }
    public function UpdateAccountInfo(Request $request){
        $all = $request->all();
        $password = $request['password'];
        $id = $request['id'];
        if($password == null){
           $update = User::find($id);
            $update->name = $request['name'];
            $update->email = $request['email'];
            $update->save();
            return back()->with('success','you have successfully updated account information');
        }else{
            $update = User::find($id);
            $update->name = $request['name'];
            $update->email = $request['email'];
            $update->password = bcrypt($request['password']);;
            $update->save();
            return back()->with('success','you have successfully updated account information');
        }
    }
    public function AccountList(){
        $listaccount = User::where('role','0')->get();
        return view('backend.AccountList')->with(['listaccount'=>$listaccount]);
    }
    public function DeleteAccount(Request $request){
        $id = $request['id'];
        $deleteaccount = User::find($id);
        $deleteaccount->delete();
        return back()->with('success','you have successfully deleted account');
    }
    public function SignIn(Request $request){

        if (Auth::attempt(['email' => $request['email'], 'password' => $request['password']])) {

            $user_get_role = User::where('email', $request['email'])->value('role');

            switch ($user_get_role) {
                case "0":
                    return redirect()->intended('/AccountList');
                    break;
                case "1":
                    return redirect()->intended('/AccountList');
                    break;
                case "agent":
                    return redirect()->intended('/AgentAccount');
                    break;
                case "operator":
                    return redirect()->intended('/AgentTourOperators');
                    break;
                default:
                    return back();
                    break;
            }

        }
        else{
            return back()->with('success','your email or your password is not matching');
        }
    }
    public function SignIn_(Request $request){

        if (Auth::attempt(['email' => $request['email'], 'password' => $request['password']])) {

            $user_get_role = User::where('email', $request['email'])->value('role');

            switch ($user_get_role) {
                case "0":
                    return redirect()->intended('/AccountList');
                    break;
                case "1":
                    return redirect()->intended('/AccountList');
                    break;
                case "agent":
                    return redirect()->intended('/AgentAccount');
                    break;
                case "operator":
                    return redirect()->intended('/AgentTourOperators');
                    break;
                default:
                    return back();
                    break;
            }

        }
        else{
            return back()->with('success','your email or your password is not matching');
        }
    }
    public function getLogout(){
        Auth::logout();
        return redirect()->route('welcome');
    }
//    public function MarkAsRead(){
//        Auth::user()->unreadNotifications->markAsRead();
//        return back();
//    }
    public function Dashboard(){
        return view('backend.Dashboard');
    }

    public function AgentAccount(){
        $user_email= \Auth::user()->email;
        $listagent = AgentTourism::where('email',$user_email)->get();

        return view('backend.AgentAccount')->with(['listagent'=>$listagent]);
    }
    public function EditAgentAccount(Request $request){
        $all = $request->all();
        $id = $request['id'];

        if($request['agentcompanyname'] == null){
            $editagent = AgentTourism::find($id);
            $editagent ->title = $request['title'];
            $editagent ->firstname = $request['firstname'];
            $editagent ->lastname = $request['lastname'];
            $editagent ->email = $request['email'];
            $editagent ->phonenumber = $request['phonenumber'];
            $editagent ->agentcompanyname = $request['agentcompanyname'];
            $editagent->save();

            $updatuser = User::where('email',$request['email'])
                ->update(['email' => $request['email'],'name' =>  $request['title'].' '.$request['firstname'].' '.$request['lastname']]);

            return back()->with('success','you have successfully updated Account Information');

        }else{
            $editagent = AgentTourism::find($id);
            $editagent ->title = $request['title'];
            $editagent ->firstname = $request['firstname'];
            $editagent ->lastname = $request['lastname'];
            $editagent ->email = $request['email'];
            $editagent ->phonenumber = $request['phonenumber'];
            $editagent ->agentcompanyname = $request['agentcompanyname'];
            $editagent->save();

            $updatuser = User::where('email',$request['email'])
                ->update(['email' => $request['email'],'name' =>  $request['title'].' '.$request['firstname'].' '.$request['lastname'],'password' => bcrypt($request['password'])]);

            return back()->with('success','you have successfully updated Account Information');
        }

    }
    public function AgentProfile(){
        $user_id= \Auth::user()->id;
        $listagentinfo = AgentProfile::where('user_id',$user_id);
        $listcountry = Countries::all();
        $listcompanyaddress = AgentCompanyAddress::where('companyprofileid',$user_id) ->join('country', 'country.id', '=', 'agentcompanyaddress.companycountry')
            ->select('country.name', 'agentcompanyaddress.*')->get();
//        dd($listcompanyaddress);
        $listweb = AgentCompanyWebsite::all();
        $listphone = AgentCompanyPhone::all();
        $listcompanytype = CompanyTypes::all();
//        $listagentmarketsegment = AgentMarketSegment::all();
        $listagentcompanytype = AgentCompanyType::where('agent_profile_id',$user_id)
            ->join('companytypes', 'agentcompanytype.agentcompanytype', '=', 'companytypes.id')
            ->select('companytypes.companytypes','agentcompanytype.*')
            ->get();
        $listagentmarketsegment = AgentMarketSegment::where('agent_profile_id',$user_id)
            ->join('marketsegments', 'agentmarketsegment.agentmarketsegment', '=', 'marketsegments.id')
            ->select('marketsegments.marketsegments','agentmarketsegment.*')
            ->get();

        $listabout = AgentProfile::all();
        $listmarketsegment = MarketSegments::all();
        $agentprofile = AgentProfilePicture::where('agent_profile_id',$user_id)->value('agent_profile_photoname');
        $agentcover = AgentProfileCover::where('agent_profile_id',$user_id)->value('agent_profile_photoname');

        return view('backend.AgentProfile')->with(['listagentmarketsegment'=>$listagentmarketsegment,'listmarketsegment'=>$listmarketsegment,'listagentcompanytype'=>$listagentcompanytype,'listcompanytype'=>$listcompanytype,'user_id'=>$user_id,'agentcover'=>$agentcover,'agentprofile'=>$agentprofile,'listabout'=>$listabout,'listphone'=>$listphone,'listweb'=>$listweb,'listcompanyaddress'=>$listcompanyaddress,'listagentinfo'=>$listagentinfo,'listcountry'=>$listcountry]);
    }
    public function UploadAgentMarketSegment(Request $request){
        $all = $request->all();
        dd($all);
    }
    public function AddAgentCompanyAddress(Request $request){
        $all = $request->all();
        $user_id= \Auth::user()->id;
        $addcompadd = new AgentCompanyAddress();
        $addcompadd->companystreetaddress = $request['companystreetaddress'];
        $addcompadd->companyofficebuiling = $request['companyofficebuiling'];
        $addcompadd->companycity = $request['companycity'];
        $addcompadd->companystateprovince = $request['companystateprovince'];
        $addcompadd->companycountry = $request['companycountry'];
        $addcompadd->companyzipcode = $request['companyzipcode'];
        $addcompadd->companyprofileid = $user_id;
        $addcompadd->save();
        return back()->with('success','you have successfully added company address information');
    }
    public function AddAgentCompanyWebsite(Request $request){
        $addweb = new AgentCompanyWebsite();
        $user_id= \Auth::user()->id;
        $addweb->companywebsite = $request['companywebsite'];
        $addweb->companyprofileid = $user_id;
        $addweb->save();
        return back()->with('success','you have successfully added company website');
    }
    public function UpdateAgentCompanyWebsite(Request $request){
        $id = $request['id'];
        $user_id= \Auth::user()->id;
        $updateweb = AgentCompanyWebsite::find($id);
        $updateweb->companywebsite = $request['companywebsite'];
        $updateweb->companyprofileid = $user_id;
        $updateweb->save();

        return back()->with('success','you have successfully update company website');

    }
    public function AddAgentCompanyPhone(Request $request){
        $addphone = new AgentCompanyPhone();
        $user_id= \Auth::user()->id;
        $addphone->companyphone = $request['companyphone'];
        $addphone->companyprofileid = $user_id;
        $addphone->save();
        return back()->with('success','you have successfully added company phone');
    }
    public function UpdateAgentCompanyPhone(Request $request){
        $id = $request['id'];
        $user_id= \Auth::user()->id;
        $updatephone = AgentCompanyPhone::find($id);
        $updatephone->companyphone = $request['companyphone'];
        $updatephone->companyprofileid = $user_id;
        $updatephone->save();

        return back()->with('success','you have successfully update company phone');

    }
    public function AddAgentCompanyAbout(Request $request){
        $all = $request->all();
        $user_id= \Auth::user()->id;
        $addabout = new AgentProfile();
        $addabout->agent_about = $request['agent_about'];
        $addabout->companyprofileid = $user_id;
        $addabout->save();
        return back()->with('success','you have successfully added company About information');
    }
    public function AddAgentProfilePhoto(Request $request){
        $data = $request->image;
        list($type, $data) = explode(';', $data);
        list(, $data)      = explode(',', $data);

        $data = base64_decode($data);
        $image_name= time().'.png';
        $path = public_path() . "/AgentPhoto/" . $image_name;
        $user_id= \Auth::user()->id;
        $addprofile = new AgentProfilePicture();
        $addprofile->agent_profile_id = $user_id;
        $addprofile->agent_profile_photoname = $image_name;

        file_put_contents($path, $data);
        $addprofile->save();
        return response()->json(['success'=>'done']);

    }
    public function UpdateAgentProfilePhoto(Request $request){

        $user_id= \Auth::user()->id;
        $data = $request->image;
        list($type, $data) = explode(';', $data);
        list(, $data)      = explode(',', $data);

        $data = base64_decode($data);
        $image_name= time().'.png';
        $path = public_path() . "/AgentPhoto/" . $image_name;
        $updatecover = AgentProfilePicture::where('agent_profile_id',$user_id)->update(['agent_profile_photoname'=>$image_name]);

        file_put_contents($path, $data);


        return response()->json(['success'=>'done']);

    }
    public function AddAgentCoverPhoto(Request $request){
        $user_id= \Auth::user()->id;
        $image = $request->file('agent_profile_cover');
        $addcover = new AgentProfileCover();
        $addcover->agent_profile_id = $user_id;
        $addcover->agent_profile_photoname = $image->getClientOriginalName();
        $imagename = $image->getClientOriginalName();
        $destinationPath = public_path('/AgentPhoto');
        $image->move($destinationPath, $imagename);
        $addcover->save();
        return back()->with('success','you have successfully uploaded cover picture');

    }
    public function UpdateAgentCoverPhoto(Request $request){

        $image = $request->file('agent_profile_cover');
        $user_id = \Auth::user()->id;
        $updatecover = AgentProfileCover::where('agent_profile_id',$user_id)->update(['agent_profile_photoname'=>$image->getClientOriginalName()]);
        $imagename = $image->getClientOriginalName();
        $destinationPath = public_path('/AgentPhoto');
        $image->move($destinationPath, $imagename);

        return back()->with('success','you have successfully updated cover picture');

    }

    public function UpdateAgentCompanyAbout(Request $request){
        $all = $request->all();
        $user_id= \Auth::user()->id;
        $id = $request['id'];
        $updateabout = AgentProfile::find($id);
        $updateabout->agent_about = $request['agent_about'];
        $updateabout->companyprofileid = $user_id;
        $updateabout->save();
        return back()->with('success','you have successfully added company About information');
    }
    public function UpdateAgentCompanyAddress(Request $request){
        $all = $request->all();
//        dd($all);
        $id = $request['id'];
        $user_id= \Auth::user()->id;
        $updatecompadd = AgentCompanyAddress::find($id);
        $updatecompadd->companystreetaddress = $request['companystreetaddress'];
        $updatecompadd->companyofficebuiling = $request['companyofficebuiling'];
        $updatecompadd->companycity = $request['companycity'];
        $updatecompadd->companystateprovince = $request['companystateprovince'];
        $updatecompadd->companycountry = $request['companycountry'];
        $updatecompadd->companyzipcode = $request['companyzipcode'];
        $updatecompadd->companyprofileid = $user_id;
        $updatecompadd->save();

        return back()->with('success','you have successfully updated company address information');
    }
    public function AddAgentProfile(Request $request){
        $all = $request->all();
//        dd($all);
        $addnew = new AgentProfile();
        $addnew->agent_about = $request['agent_about'];
        $addnew->agent_emailaddress = $request['agent_emailaddress'];
        $addnew->agent_phoneaddress = $request['agent_phoneaddress'];
        $addnew->agent_mapddress = $request['agent_mapddress'];
        $addnew->save();

        $lastid = $addnew->id;

        $agent_profile_services = $request['agent_profile_services'];
        $agent_profile_videolink = $request['agent_profile_videolink'];
        $agent_profile_photoname = $request['agent_profile_photoname'];

        foreach ($agent_profile_services as $key => $agent_profile_service){
            $addgentservice = new AgentProfileServices();
            $addgentservice->agent_profile_id = $lastid;
            $addgentservice->agent_profile_services = $request['agent_profile_services'][$key];
            $addgentservice->save();
        }

        foreach ($agent_profile_videolink as $key => $agent_profile_videolinks){
            $addgentvideo = new AgentProfileVideo();
            $addgentvideo->agent_profile_id = $lastid;
            $addgentvideo->agent_profile_videolink = $request['agent_profile_videolink'][$key];
            $addgentvideo->save();
        }

        foreach ($agent_profile_photoname as $images){
            $image = $request->file('agent_profile_photoname');
            $addgentphoto = new AgentProfilePhoto();

            $addgentphoto->agent_profile_id = $lastid;
            $addgentphoto->agent_profile_photoname = $images->getClientOriginalName();
            $imagename = $images->getClientOriginalName();
            $destinationPath = public_path('/AgentPhoto');
            $images->move($destinationPath, $imagename);
            $addgentphoto->save();
        }

        return back()->with('success','you have successfully added a new profile information');

    }
    public function EditAgentOther(Request $request){
        $id = $request['id'];
        $addnew = AgentProfile::find($id);
        $addnew->agent_about = $request['agent_about'];
        $addnew->agent_emailaddress = $request['agent_emailaddress'];
        $addnew->agent_phoneaddress = $request['agent_phoneaddress'];
        $addnew->agent_mapddress = $request['agent_mapddress'];
        $addnew->save();
        return back()->with('success','You have successfully edit  agent information');
    }
    public function DeleteAgentOther(Request $request){
        $id = $request['id'];
        $deleteimage = AgentProfile::find($id);
        $deleteimage->delete();

        $deleteagentservice = AgentProfileServices::where('agent_profile_id', $id)->delete();
        $deleteagentvideo = AgentProfileVideo::where('agent_profile_id', $id)->delete();
        $deleteagentphoto = AgentProfilePhoto::where('agent_profile_id', $id)->delete();

        return back()->with('success','You have deleted agent Information');
    }
    public function AddMoreAgentService(Request $request){
        $id = $request['id'];
        $agent_profile_services = $request['agent_profile_services'];
        foreach ($agent_profile_services as $key => $agent_profile_service){
            $addgentservice = new AgentProfileServices();
            $addgentservice->agent_profile_id = $id;
            $addgentservice->agent_profile_services = $request['agent_profile_services'][$key];
            $addgentservice->save();
        }
        return back()->with('success','You have successfully added  new service');
    }
    public function DeleteAgentServices(Request $request){
        $id = $request['id'];
        $deleteimage = AgentProfileServices::find($id);
        $deleteimage->delete();
        return back()->with('success','You have deleted agent services');
    }
    public function AddMoreVideosAgentProfile(Request $request){
        $id = $request['id'];
        $agent_profile_videolink = $request['agent_profile_videolink'];
        foreach ($agent_profile_videolink as $key => $agent_profile_videolinks){
            $addgentvideo = new AgentProfileVideo();
            $addgentvideo->agent_profile_id = $id;
            $addgentvideo->agent_profile_videolink = $request['agent_profile_videolink'][$key];
            $addgentvideo->save();
        }
        return back()->with('success','You have successfully added a new video');
    }
    public function DeleteAgentVideos(Request $request){
        $id = $request['id'];
        $deleteimage = AgentProfileVideo::find($id);
        $deleteimage->delete();
        return back()->with('success','You have deleted agent video');
    }
    public function AddMoreImagesAgentProfile(Request $request){
        $id = $request['id'];
        $agent_profile_photoname = $request['agent_profile_photoname'];
        foreach ($agent_profile_photoname as $images){
            $image = $request->file('agent_profile_photoname');
            $addgentphoto = new AgentProfilePhoto();

            $addgentphoto->agent_profile_id = $id;
            $addgentphoto->agent_profile_photoname = $images->getClientOriginalName();
            $imagename = $images->getClientOriginalName();
            $destinationPath = public_path('/AgentPhoto');
            $images->move($destinationPath, $imagename);
            $addgentphoto->save();
        }
        return back()->with('success','You have successfully added a new image');
    }
    public function DeleteAgentImages(Request $request){
        $id = $request['id'];
        $deleteimage = AgentProfilePhoto::find($id);
        $deleteimage->delete();
        return back()->with('success','You have deleted agent image');

    }

    public function Event(){
        $user_id= \Auth::user()->id;
        $listevent = Event::where('eventuserid',$user_id)
            ->join('users', 'event.eventuserid', '=', 'users.id')
            ->select('users.name','event.*')
            ->get();
        return view('backend.Event')->with(['listevent'=>$listevent]);

    }
    public function AddEvent(Request $request){
        $all = $request->all();
        $user_id= \Auth::user()->id;
        $addevent = new Event();
        $eventimagename = $request->file('eventimagename');
        $addevent->eventname = $request['eventname'];
        $addevent->eventdescription = $request['eventdescription'];
        $addevent->eventstartdate = $request['eventstartdate'];
        $addevent->eventstarttime = $request['eventstarttime'];
        $addevent->eventenddate = $request['eventenddate'];
        $addevent->eventendtime = $request['eventendtime'];
        $addevent->eventlocation = $request['eventlocation'];
        $addevent->eventuserid = $user_id;
        $addevent->eventimagename = $eventimagename->getClientOriginalName();
        $imagename = $eventimagename->getClientOriginalName();
        $destinationPath = public_path('/Eventimages');
        $eventimagename->move($destinationPath, $imagename);

        $addevent->save();
        $lastid = $addevent->id;
        $eventby = \Auth::user()->name;
        $eventtitle = $request['eventname'];
        $eventid = $lastid;
        $eventpurpose = 'new event';

        Notification::send(User::all(), new WebAppNotification($eventtitle,$eventid,$eventby,$eventpurpose));
//        User::find($user_id)->notify(new WebAppNotification($eventtitle,$eventid,$eventby,$eventpurpose));

        return back()->with('success','You have successfully added a new event');
    }
    public function EditEvent(Request $request){
        $id = $request['id'];
        $eventimagename = $request->file('eventimagename');

        if(empty($eventimagename)){
            $editevent = Event::find($id);
            $user_id= \Auth::user()->id;
            $editevent->eventname = $request['eventname'];
            $editevent->eventdescription = $request['eventdescription'];
            $editevent->eventstartdate = $request['eventstartdate'];
            $editevent->eventstarttime = $request['eventstarttime'];
            $editevent->eventenddate = $request['eventenddate'];
            $editevent->eventendtime = $request['eventendtime'];
            $editevent->eventlocation = $request['eventlocation'];
            $editevent->eventuserid = $user_id;
            $editevent->save();
            return back()->with('success','You have successfully edited an event');

        }else{
            $editevent = Event::find($id);
            $user_id= \Auth::user()->id;
            $editevent->eventname = $request['eventname'];
            $editevent->eventdescription = $request['eventdescription'];
            $editevent->eventstartdate = $request['eventstartdate'];
            $editevent->eventstarttime = $request['eventstarttime'];
            $editevent->eventenddate = $request['eventenddate'];
            $editevent->eventendtime = $request['eventendtime'];
            $editevent->eventlocation = $request['eventlocation'];
            $editevent->eventuserid = $user_id;
            $editevent->eventimagename = $eventimagename->getClientOriginalName();
            $imagename = $eventimagename->getClientOriginalName();
            $destinationPath = public_path('/Eventimages');
            $eventimagename->move($destinationPath, $imagename);
            $editevent->save();
            return back()->with('success','You have successfully edited an event');
        }
    }

    public function DeleteEvent(Request $request){
        $id = $request['id'];
        $deleteevent = Event::find($id);
        $deleteevent->delete();
        return back()->with('success','You have successfully deleted an event');
    }

    public function ListOfTourismAgents(){
        return view('backend.ListOfTourismAgents');
    }
    public function AddTourismAgent(Request $request){
        $all = $request->all();
//        return response()->json($all);
        $checkemail = User::where('email',$request['email'])->value('email');

        if($checkemail == null) {
//            $addagent = new AgentTourism();
//            $addagent ->title = $request['title'];
//            $addagent ->firstname = $request['firstname'];
//            $addagent ->lastname = $request['lastname'];
//            $addagent ->email = $request['email'];
//            $addagent ->phonenumber = $request['phonenumber'];
//            $addagent ->agentcompanyname = $request['agentcompanyname'];
//            $addagent->save();

            $request->validate([
                'email' => 'required|string|email|max:255|unique:users','exists:connection.user,email',
            ]);

            $register = new User();
            $register->name = $request['title'].' '.$request['firstname'].' '.$request['lastname'];
            $register ->email = $request['email'];
            $register ->phonenumber = $request['phonenumber'];
            $register ->agentcompanyname = $request['agentcompanyname'];
            $register->password = bcrypt($request['password']);
            $register->role = 'agent';
            $register->save();
            return response()->json(['response' => 'success']);

        }else{
            return response()->json(['response' => 'email is already in our database']);
        }


//        return response()->json(['success' => 'success'],200);
//        return back()->with('success','you have created an agent account');
    }
    public function AgentTourOperators(){
        $user_email= \Auth::user()->email;
        $listagentinfo = TourOperator::where('email',$user_email)->get();
        return view('backend.AgentTourOperators')->with(['listagentinfo'=>$listagentinfo]);
    }
    public function EditAgentTourOperators(Request $request){
        $all = $request->all();
//        dd($all);
        $id = $request['id'];
        if($request['password'] == null){

            $editoperator = TourOperator::find($id);
            $editoperator ->companyname = $request['companyname'];
            $editoperator ->email = $request['email'];
            $editoperator ->phonenumber = $request['phonenumber'];
            $editoperator->save();

            $updatuser = User::where('email',$request['email'])
                ->update(['email' => $request['email'],'name' => $request['companyname']]);

            return back()->with('success','you have successfully updated Account Information');

        }else{
            $editoperator = AgentTourism::find($id);
            $editoperator ->companyname = $request['companyname'];
            $editoperator ->email = $request['email'];
            $editoperator ->phonenumber = $request['phonenumber'];
            $editoperator->save();

            $updatuser = User::where('email',$request['email'])
                ->update(['email' => $request['email'],'name' =>  $request['companyname'],'password' => bcrypt($request['password'])]);

            return back()->with('success','you have successfully updated Account Information');
        }

    }
    public function TourOperatorProfile(){
        $user_id= \Auth::user()->id;
        $listoperatorinfo = OperatorProfile::where('user_id',$user_id)->get();
        return view('backend.TourOperatorProfile')->with(['listoperatorinfo'=>$listoperatorinfo]);
    }
    public function AddTourOperatorProfile(Request $request){
        $all = $request->all();
//        dd($all);
        $user_id= \Auth::user()->id;

        $addnew = new OperatorProfile();
        $addnew->agent_about = $request['agent_about'];
        $addnew->agent_emailaddress = $request['agent_emailaddress'];
        $addnew->agent_phoneaddress = $request['agent_phoneaddress'];
        $addnew->agent_mapddress = $request['agent_mapddress'];
        $addnew->user_id = $user_id;
        $addnew->save();

        $lastid = $addnew->id;

        $agent_profile_services = $request['agent_profile_services'];
        $agent_profile_videolink = $request['agent_profile_videolink'];
        $agent_profile_photoname = $request['agent_profile_photoname'];

        foreach ($agent_profile_services as $key => $agent_profile_service){
            $addgentservice = new AgentProfileServices();
            $addgentservice->agent_profile_id = $lastid;
            $addgentservice->agent_profile_services = $request['agent_profile_services'][$key];
            $addgentservice->save();
        }

        foreach ($agent_profile_videolink as $key => $agent_profile_videolinks){
            $addgentvideo = new AgentProfileVideo();
            $addgentvideo->agent_profile_id = $lastid;
            $addgentvideo->agent_profile_videolink = $request['agent_profile_videolink'][$key];
            $addgentvideo->save();
        }

        foreach ($agent_profile_photoname as $images){
            $image = $request->file('agent_profile_photoname');
            $addgentphoto = new AgentProfilePhoto();

            $addgentphoto->agent_profile_id = $lastid;
            $addgentphoto->agent_profile_photoname = $images->getClientOriginalName();
            $imagename = $images->getClientOriginalName();
            $destinationPath = public_path('/TourOperator');
            $images->move($destinationPath, $imagename);
            $addgentphoto->save();
        }

        return back()->with('success','you have successfully added a new profile information');

    }
    public function EditTourOperatorProfileOther(Request $request){
        $id = $request['id'];
        $addnew = OperatorProfile::find($id);
        $addnew->agent_about = $request['agent_about'];
        $addnew->agent_emailaddress = $request['agent_emailaddress'];
        $addnew->agent_phoneaddress = $request['agent_phoneaddress'];
        $addnew->agent_mapddress = $request['agent_mapddress'];
        $addnew->save();
        return back()->with('success','You have successfully edit  agent information');
    }
    public function DeleteTourOperatorProfileOther(Request $request){
        $id = $request['id'];
        $deleteimage = OperatorProfile::find($id);
        $deleteimage->delete();
        $deleteagentservice = AgentProfileServices::where('agent_profile_id', $id)->delete();
        $deleteagentvideo = AgentProfileVideo::where('agent_profile_id', $id)->delete();
        $deleteagentphoto = AgentProfilePhoto::where('agent_profile_id', $id)->delete();

        return back()->with('success','You have deleted agent Information');
    }
    public function AddMoreTourOperatorProfileService(Request $request){
        $id = $request['id'];
        $agent_profile_services = $request['agent_profile_services'];
        foreach ($agent_profile_services as $key => $agent_profile_service){
            $addgentservice = new AgentProfileServices();
            $addgentservice->agent_profile_id = $id;
            $addgentservice->agent_profile_services = $request['agent_profile_services'][$key];
            $addgentservice->save();
        }
        return back()->with('success','You have successfully added  new service');
    }
    public function DeleteTourOperatorProfileServices(Request $request){
        $id = $request['id'];
        $deleteimage = AgentProfileServices::find($id);
        $deleteimage->delete();
        return back()->with('success','You have deleted agent services');
    }
    public function AddMoreVideosTourOperatorProfile(Request $request){
        $id = $request['id'];
        $agent_profile_videolink = $request['agent_profile_videolink'];
        foreach ($agent_profile_videolink as $key => $agent_profile_videolinks){
            $addgentvideo = new AgentProfileVideo();
            $addgentvideo->agent_profile_id = $id;
            $addgentvideo->agent_profile_videolink = $request['agent_profile_videolink'][$key];
            $addgentvideo->save();
        }
        return back()->with('success','You have successfully added a new video');
    }
    public function DeleteTourOperatorProfileVideos(Request $request){
        $id = $request['id'];
        $deleteimage = AgentProfileVideo::find($id);
        $deleteimage->delete();
        return back()->with('success','You have deleted agent video');
    }
    public function AddMoreImagesTourOperatorProfile(Request $request){
        $id = $request['id'];
        $agent_profile_photoname = $request['agent_profile_photoname'];
        foreach ($agent_profile_photoname as $images){
            $image = $request->file('agent_profile_photoname');
            $addgentphoto = new AgentProfilePhoto();

            $addgentphoto->agent_profile_id = $id;
            $addgentphoto->agent_profile_photoname = $images->getClientOriginalName();
            $imagename = $images->getClientOriginalName();
            $destinationPath = public_path('/TourOperator');
            $images->move($destinationPath, $imagename);
            $addgentphoto->save();
        }
        return back()->with('success','You have successfully added a new image');
    }
    public function DeleteTourOperatorProfileImages(Request $request){
        $id = $request['id'];
        $deleteimage = AgentProfilePhoto::find($id);
        $deleteimage->delete();
        return back()->with('success','You have deleted agent image');

    }
    public function Experience(){
        $user_id= \Auth::user()->id;
        $listdestination = AddDestination::all();
        $listplaces = AddPlaces::all();
        $listactivity = Experiences::where('user_id',$user_id)
            ->join('experience_details', 'experience_details.activity_id', '=', 'experiences.id')
            ->join('AddDestination', 'AddDestination.id', '=', 'experiences.activity_destination')
            ->join('AddPlaces', 'AddPlaces.id', '=', 'experiences.activity_place')
            ->select('AddPlaces.placetitle','AddDestination.destinationtitle','experiences.*','experience_details.activity_day','experience_details.activity_day_title','experience_details.activity_details')
            ->orderBy('experience_details.activity_id')
            ->get();
        return view('backend.Experience')->with(['listactivity'=>$listactivity,'listplaces'=>$listplaces,'listdestination'=>$listdestination]);

    }


    public function AddActivity(Request $request){

        $all = $request->all();
//        dd($all);
        $this->validate($request, [
            'activity_coverimage' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048|dimensions:width=1000,height=750',
        ]);

        $user_id= \Auth::user()->id;
        $addactivitysum = new Experiences();
        $addactivitysum->activity_title = $request['activity_title'];
        $addactivitysum->activity_summary = $request['activity_summary'];
        $addactivitysum->activity_price = $request['activity_price'];
        $addactivitysum->activity_numberofguest = $request['activity_numberofguest'];
        $addactivitysum->activity_destination = $request['activity_destination'];
        $addactivitysum->activity_place = $request['activity_place'];
        $addactivitysum->user_id = $user_id;
        $lastid = $addactivitysum->id;

        $image = $request->file('activity_coverimage');
        $addactivitysum->activity_coverimage = $image->getClientOriginalName();
        $imagename = $image->getClientOriginalName();
        $destinationPath = public_path('/ActivityCoverImage');
        $image->move($destinationPath, $imagename);
        $addactivitysum->save();

        $activity_day = $request['activity_day'];
        $activity_id = $addactivitysum->id;

        foreach ($activity_day  as $key => $activity_days){
            $addactivitydet = new ExperienceDetails();
            $addactivitydet->activity_id = $activity_id;
            $addactivitydet->activity_day = $request['activity_day'][$key];
            $addactivitydet->activity_day_title = $request['activity_day_title'][$key];
            $addactivitydet->activity_details = $request['activity_details'];
            $addactivitydet->save();
        }

//        if($request->file('activity_information_pdf')){
//            $information_pdf = $request->file('activity_information_pdf');
//            $addactivitysum->activity_information_pdf = $image->getClientOriginalName();
//            $imagename = $information_pdf->getClientOriginalName();
//            $destinationPath = public_path('/ActivityPdf');
//            $information_pdf->move($destinationPath, $imagename);
//            $addactivitysum->save();
//        }
//        $activity_details_id = $addactivitydet->id;
//        $activity_details_image = $request['activity_details_image'];

//        if($request->file('activity_details_image')){
//
//            foreach ($activity_details_image as $activity_details_images) {
//                $image = $request->file('activity_details_image');
//                $addactivitydetailsimage = new ExperienceDetailsImage();
//                $addactivitydetailsimage->activity_id = $activity_id;
//                $addactivitydetailsimage->activity_details_id = $activity_details_id;
//                $addactivitydetailsimage->activity_details_image = $activity_details_images->getClientOriginalName();
//                $imagename = $activity_details_images->getClientOriginalName();
//                $destinationPath = public_path('/ActivityImages');
//                $activity_details_images->move($destinationPath, $imagename);
//                $addactivitydetailsimage->save();
//            }
//
//        }


        $eventby = \Auth::user()->name;
        $eventtitle = $request['activity_title'];
        $eventid = $lastid;
        $eventpurpose = 'new package';

//        User::find($user_id)->notify(new WebAppNotification($eventtitle,$eventid,$eventby,$eventpurpose));
        Notification::send(User::all(), new WebAppNotification($eventtitle,$eventid,$eventby,$eventpurpose));
        return back()->with('success','You have successfully added an activity');
    }
    public function ActivityDetails(Request $request){
        $id = $request['id'];
//        dd($id);
        $listexperience = ExperienceDetails::where('activity_id',$id)->get();
        return view('backend.ActivityDetails')->with(['listexperience'=>$listexperience]);
    }

    public function ListOfTourOperators(){
        return view('backend.ListOfTourOperators');
    }
    public function OperatorAccount(){
        $user_email= \Auth::user()->email;
        $listagent = AgentTourism::where('email',$user_email)->get();
        return view('backend.OperatorAccount')->with(['listagent'=>$listagent]);
    }
    public function AddTourOperators(Request $request){
        $all = $request->all();
//        return response()->json($all);

        $checkemail = User::where('email',$request['email'])->value('email');

        if($checkemail == null){

            $addtouroperator = new TourOperator();
            $addtouroperator ->companyname = $request['companyname'];
            $addtouroperator ->email = $request['email'];
            $addtouroperator ->phonenumber = $request['phonenumber'];
            $addtouroperator->save();

            $register = new User();
            $register->name = $request['companyname'];
            $register->email = $request['email'];
            $register->password = bcrypt($request['password']);
            $register->role = 'agent';
            $register->save();

            return response()->json(['success' => 'success'],200);
        }else{
            return response()->json(['fail' => 'email is already taken']);
        }

//        return back()->with('success','you have created an tour operator account');
    }
    public function CountryProfile(){
        $CountryProfile = CountryProfile::all();
        return view('backend.CountryProfile')->with(['CountryProfile'=>$CountryProfile]);
    }

    public function AddCountryProfile(Request $request){
        $user_id = \Auth::user()->id;

        $all = $request->all();
//        dd($all);
        $this->validate($request, [
            'coverimage' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048|dimensions:width=1000,height=750',
        ]);
        $addcountryprofile = new CountryProfile();
        $addcountryprofile->countryname = $request['countryname'];
        $addcountryprofile->countrygeneralinformation = $request['countrygeneralinformation'];
        $addcountryprofile->countrymap = $request['countrymap'];
        $addcountryprofile->countrylanguage = $request['countrylanguage'];
        $addcountryprofile->countrycurrency = $request['countrycurrency'];
        $addcountryprofile->destinationcapital = $request['destinationcapital'];
        $addcountryprofile->destinationpopulation = $request['destinationpopulation'];
        $addcountryprofile->destinationlang = $request['destinationlang'];
        $addcountryprofile->destinationcurrency = $request['destinationcurrency'];
        $addcountryprofile->destinationsurfacearea = $request['destinationsurfacearea'];
        $addcountryprofile->countrytimezone = $request['countrytimezone'];

        $imageflag = $request->file('destinationflag');
        $addcountryprofile->destinationflag = $imageflag->getClientOriginalName();
        $imagename = $imageflag->getClientOriginalName();
        $destinationPath = public_path('/DestinationFlag');
        $imageflag->move($destinationPath, $imagename);

        $image = $request->file('coverimage');
        $addcountryprofile->coverimage = $image->getClientOriginalName();
        $imagename = $image->getClientOriginalName();
        $destinationPath = public_path('/CountryProfile');
        $image->move($destinationPath, $imagename);
        $addcountryprofile->save();
        $lastid = $addcountryprofile->id;

        $eventby = \Auth::user()->name;
        $eventtitle = $request['countryname'];
        $eventid = $lastid;
        $eventpurpose = 'new country profile';

//        User::find($user_id)->notify(new WebAppNotification($eventtitle,$eventid,$eventby,$eventpurpose));
        Notification::send(User::all(), new WebAppNotification($eventtitle,$eventid,$eventby,$eventpurpose));
        return back()->with('success','you have added a new country profile');
    }
    public function EditCountryProfile(Request $request){
        $all = $request->all();
        $id = $request['id'];
//        $this->validate($request, [
//            'coverimage' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048|dimensions:width=1000,height=750',
//        ]);
        if(empty($request->file('coverimage')) AND empty($request->file('destinationflag'))){
            $editcountryprofile = CountryProfile::find($id);
            $editcountryprofile->countryname = $request['countryname'];
            $editcountryprofile->countrygeneralinformation = $request['countrygeneralinformation'];
            $editcountryprofile->countrymap = $request['countrymap'];
            $editcountryprofile->countrylanguage = $request['countrylanguage'];
            $editcountryprofile->countrycurrency = $request['countrycurrency'];
            $editcountryprofile->countrylanguage = $request['countrylanguage'];
            $editcountryprofile->countrycurrency = $request['countrycurrency'];
            $editcountryprofile->destinationcapital = $request['destinationcapital'];
            $editcountryprofile->destinationpopulation = $request['destinationpopulation'];
            $editcountryprofile->destinationsurfacearea = $request['destinationsurfacearea'];
            $editcountryprofile->countrytimezone = $request['countrytimezone'];

            $editcountryprofile->save();

        }elseif ($request->file('destinationflag') AND empty($request->file('coverimage'))){

            $editcountryprofile = CountryProfile::find($id);
            $all = $request->all();
//            dd($all);
            $editcountryprofile->countryname = $request['countryname'];
            $editcountryprofile->countrygeneralinformation = $request['countrygeneralinformation'];
            $editcountryprofile->countrymap = $request['countrymap'];
            $editcountryprofile->countrylanguage = $request['countrylanguage'];
            $editcountryprofile->countrycurrency = $request['countrycurrency'];
            $editcountryprofile->countrylanguage = $request['countrylanguage'];
            $editcountryprofile->countrycurrency = $request['countrycurrency'];
            $editcountryprofile->destinationcapital = $request['destinationcapital'];
            $editcountryprofile->destinationpopulation = $request['destinationpopulation'];
            $editcountryprofile->destinationsurfacearea = $request['destinationsurfacearea'];
            $editcountryprofile->countrytimezone = $request['countrytimezone'];

            $image = $request->file('destinationflag');
            $editcountryprofile->destinationflag = $image->getClientOriginalName();
            $imagename = $image->getClientOriginalName();
            $destinationPath = public_path('/DestinationFlag');
            $image->move($destinationPath, $imagename);
            $editcountryprofile->save();

        }
        elseif (empty($request->file('destinationflag')) AND ($request->file('coverimage'))){

            $editcountryprofile = CountryProfile::find($id);
            $editcountryprofile->countryname = $request['countryname'];
            $editcountryprofile->countrygeneralinformation = $request['countrygeneralinformation'];
            $editcountryprofile->countrymap = $request['countrymap'];
            $editcountryprofile->countrylanguage = $request['countrylanguage'];
            $editcountryprofile->countrycurrency = $request['countrycurrency'];
            $editcountryprofile->countrylanguage = $request['countrylanguage'];
            $editcountryprofile->countrycurrency = $request['countrycurrency'];
            $editcountryprofile->destinationcapital = $request['destinationcapital'];
            $editcountryprofile->destinationpopulation = $request['destinationpopulation'];
            $editcountryprofile->destinationsurfacearea = $request['destinationsurfacearea'];
            $editcountryprofile->countrytimezone = $request['countrytimezone'];

            $image = $request->file('coverimage');
            $editcountryprofile->coverimage = $image->getClientOriginalName();
            $imagename = $image->getClientOriginalName();
            $destinationPath = public_path('/CountryProfile');
            $image->move($destinationPath, $imagename);
            $editcountryprofile->save();


        }else{
            $editcountryprofile = CountryProfile::find($id);
            $editcountryprofile->countryname = $request['countryname'];
            $editcountryprofile->countrygeneralinformation = $request['countrygeneralinformation'];
            $editcountryprofile->countrymap = $request['countrymap'];
            $editcountryprofile->countrylanguage = $request['countrylanguage'];
            $editcountryprofile->countrycurrency = $request['countrycurrency'];
            $editcountryprofile->countrylanguage = $request['countrylanguage'];
            $editcountryprofile->countrycurrency = $request['countrycurrency'];
            $editcountryprofile->destinationcapital = $request['destinationcapital'];
            $editcountryprofile->destinationpopulation = $request['destinationpopulation'];
            $editcountryprofile->destinationsurfacearea = $request['destinationsurfacearea'];
            $editcountryprofile->countrytimezone = $request['countrytimezone'];

            $image = $request->file('destinationflag');
            $editcountryprofile->destinationflag = $image->getClientOriginalName();
            $imagename = $image->getClientOriginalName();
            $destinationPath = public_path('/DestinationFlag');
            $image->move($destinationPath, $imagename);

            $image = $request->file('coverimage');
            $editcountryprofile->coverimage = $image->getClientOriginalName();
            $imagename = $image->getClientOriginalName();
            $destinationPath = public_path('/CountryProfile');
            $image->move($destinationPath, $imagename);
            $editcountryprofile->save();
        }

        return back()->with('success','you have edited country profile');
    }

    public function Destinations(){
//        $listdestination = AddDestination::all();
        $listcountryprofile = CountryProfile::all();
        $listdestination = AddDestination::join('countryprofile', 'AddDestination.destinationcountry', '=', 'countryprofile.id')
            ->select('AddDestination.*', 'countryprofile.countryname')
        ->get();
        return view('backend.Destinations')->with(['listcountryprofile'=>$listcountryprofile,'listdestination'=>$listdestination]);
    }
    public function AddDestination(Request $request){
        $user_id = \Auth::user()->id;

        $all = $request->all();
//        dd($all);
        $this->validate($request, [
            'coverimage' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048|dimensions:width=1000,height=750',
        ]);
        $adddestination = new AddDestination();
        $adddestination->destinationtitle = $request['destinationtitle'];
        $adddestination->destinationgeneralinformation = $request['destinationgeneralinformation'];
        $adddestination->destinationmap = $request['destinationmap'];
        $adddestination->destinationcountry = $request['destinationcountry'];
        $adddestination->topdestination = $request['topdestination'];
//        $adddestination->destinationphotocredit = $request['destinationphotocredit'];

        $image = $request->file('destinationcoverimage');
        $adddestination->destinationcoverimage = $image->getClientOriginalName();
        $imagename = $image->getClientOriginalName();
        $destinationPath = public_path('/Destination');
        $image->move($destinationPath, $imagename);

        $image = $request->file('coverimage');
        $adddestination->coverimage = $image->getClientOriginalName();
        $imagename = $image->getClientOriginalName();
        $destinationPath = public_path('/Destination');
        $image->move($destinationPath, $imagename);


        $adddestination->save();
        $lastid = $adddestination->id;

//        $thingstodo = $request['thingstodo'];

//        foreach ($thingstodo  as $key => $thingstodos){
//            $addthings = new DestinationThingsTodo();
//            $addthings->thingstodo = $thingstodo[$key];
//            $addthings->destinationid = $lastid;
//            $addthings->save();
//        }
        $eventby = \Auth::user()->name;
        $eventtitle = $request['destinationtitle'];
        $eventid = $lastid;
        $eventpurpose = 'new destination';

//        User::find($user_id)->notify(new WebAppNotification($eventtitle,$eventid,$eventby,$eventpurpose));
        Notification::send(User::all(), new WebAppNotification($eventtitle,$eventid,$eventby,$eventpurpose));
        return back()->with('success','you have added destination');
    }
    public function EditDestination(Request $request){
        $all = $request->all();
        $id = $request['id'];
//        $this->validate($request, [
//            'coverimage' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048|dimensions:width=1000,height=750',
//        ]);

        if(empty($request->file('coverimage')) AND empty($request->file('destinationcoverimage'))){

            $editdestination = AddDestination::find($id);
            $editdestination->destinationtitle = $request['destinationtitle'];
            $editdestination->destinationgeneralinformation = $request['destinationgeneralinformation'];
            $editdestination->destinationmap = $request['destinationmap'];
            $editdestination->destinationcountry = $request['destinationcountry'];
            $editdestination->topdestination = $request['topdestination'];
//            $editdestination->destinationphotocredit = $request['destinationphotocredit'];
            $editdestination->save();

        }elseif(empty($request->file('coverimage')) AND $request->file('destinationcoverimage')){
            $all = $request->all();
//            dd($all);
            $editdestination = AddDestination::find($id);
            $editdestination->destinationtitle = $request['destinationtitle'];
            $editdestination->destinationgeneralinformation = $request['destinationgeneralinformation'];
            $editdestination->destinationmap = $request['destinationmap'];
            $editdestination->destinationcountry = $request['destinationcountry'];
            $editdestination->topdestination = $request['topdestination'];
//            $editdestination->destinationphotocredit = $request['destinationphotocredit'];

            $image = $request->file('destinationcoverimage');
            $editdestination->destinationcoverimage = $image->getClientOriginalName();
            $imagename = $image->getClientOriginalName();
            $destinationPath = public_path('/Destination');
            $image->move($destinationPath, $imagename);

            $editdestination->save();

        }elseif($request->file('coverimage') AND empty($request->file('destinationcoverimage'))){
            $editdestination = AddDestination::find($id);
            $editdestination->destinationtitle = $request['destinationtitle'];
            $editdestination->destinationgeneralinformation = $request['destinationgeneralinformation'];
            $editdestination->destinationmap = $request['destinationmap'];
            $editdestination->destinationcountry = $request['destinationcountry'];
            $editdestination->topdestination = $request['topdestination'];
//            $editdestination->destinationphotocredit = $request['destinationphotocredit'];

            $image = $request->file('coverimage');
            $editdestination->coverimage = $image->getClientOriginalName();
            $imagename = $image->getClientOriginalName();
            $destinationPath = public_path('/Destination');
            $image->move($destinationPath, $imagename);
            $editdestination->save();
        }
        else{

            $editdestination = AddDestination::find($id);
            $editdestination->destinationtitle = $request['destinationtitle'];
            $editdestination->destinationgeneralinformation = $request['destinationgeneralinformation'];
            $editdestination->destinationmap = $request['destinationmap'];
            $editdestination->destinationcountry = $request['destinationcountry'];
            $editdestination->topdestination = $request['topdestination'];
//            $editdestination->destinationphotocredit = $request['destinationphotocredit'];

            $image = $request->file('destinationcoverimage');
            $editdestination->destinationcoverimage = $image->getClientOriginalName();
            $imagename = $image->getClientOriginalName();
            $destinationPath = public_path('/Destination');
            $image->move($destinationPath, $imagename);

            $image = $request->file('coverimage');
            $editdestination->coverimage = $image->getClientOriginalName();
            $imagename = $image->getClientOriginalName();
            $destinationPath = public_path('/Destination');
            $image->move($destinationPath, $imagename);
            $editdestination->save();

        }


        return back()->with('success','you have edit destination');
    }
    public function DeleteDestination(Request $request){
        $id = $request['id'];
        $deletedestination = AddDestination::find($id);
        $deletedestination->delete();
        return back()->with('success','you have deleted destination');
    }
//    public function AddDestinationthingstodo(Request $request){
//        $id = $request['id'];
//        $thingstodo = $request['thingstodo'];
//
//        foreach ($thingstodo  as $key => $thingstodos){
//            $addthings = new DestinationThingsTodo();
//            $addthings->thingstodo = $thingstodo[$key];
//            $addthings->destinationid = $id;
//            $addthings->save();
//        }
//
//        return back()->with('success','you have added destination things to do');
//    }

    public function Places(){
        $listplaces = AddPlaces::all();
        $listdestination = AddDestination::all();
        return view('backend.Places')->with(['listdestination'=>$listdestination,'listplaces'=>$listplaces]);

    }
    public function AddPlaces(Request $request){
        $all = $request->all();
        $user_id = \Auth::user()->id;
//        dd($all);
        $this->validate($request, [
            'coverimage' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048|dimensions:width=1000,height=750',
        ]);
        $placespicture = $request['placespicture'];

        $addplaces = new AddPlaces();
        $addplaces ->placetitle = $request['placetitle'];
        $addplaces ->discovercaption = $request['discovercaption'];
        $addplaces ->placegeneralinformation = $request['placegeneralinformation'];

        $image = $request->file('coverimage');
        $addplaces->coverimage = $image->getClientOriginalName();
        $imagename = $image->getClientOriginalName();
        $destinationPath = public_path('/Placecoverimage');
        $image->move($destinationPath, $imagename);

        $image = $request->file('generalimage');
        $addplaces->generalimage = $image->getClientOriginalName();
        $imagename = $image->getClientOriginalName();
        $destinationPath = public_path('/Placecoverimage');
        $image->move($destinationPath, $imagename);

        $addplaces->save();
        $lastid = $addplaces->id;

        $thingstodogallery = $request['thingstodogallery'];

        foreach ($thingstodogallery as $thingstodogallerys) {

            $image = $request->file('thingstodogallery');
            $addpicture = new ThingToDoGallery();

            $addpicture->thingstodoid = $lastid;
            $addpicture->thingstodogallery = $thingstodogallerys->getClientOriginalName();
            $imagename = $thingstodogallerys->getClientOriginalName();
            $destinationPath = public_path('/PlacesPictures');
            $thingstodogallerys->move($destinationPath, $imagename);
            $addpicture->save();

        }

        $previous_id = $addplaces->id;

        $eventby = \Auth::user()->name;
        $eventtitle = $request['placetitle'];
        $eventid = $lastid;
        $eventpurpose = 'new things to do';

//        User::find($user_id)->notify(new WebAppNotification($eventtitle,$eventid,$eventby,$eventpurpose));
        Notification::send(User::all(), new WebAppNotification($eventtitle,$eventid,$eventby,$eventpurpose));
        return back()->with('success','you have successfully added a new things to do');
    }
    public function EditPlaces(Request $request){
        $id = $request['id'];
        $editplaces = AddPlaces::find($id);
//        $this->validate($request, [
//            'coverimage' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048|dimensions:width=1000,height=750',
//        ]);
        if($request->file('coverimage') AND empty($request->file('generalimage'))){

            $editplaces ->placetitle = $request['placetitle'];
            $editplaces ->discovercaption = $request['discovercaption'];
            $editplaces ->placegeneralinformation = $request['placegeneralinformation'];

            $image = $request->file('coverimage');
            $editplaces->coverimage = $image->getClientOriginalName();
            $imagename = $image->getClientOriginalName();
            $destinationPath = public_path('/Placecoverimage');
            $image->move($destinationPath, $imagename);

            $editplaces->save();
            return back()->with('success','you have successfully edited things to do');

        }elseif($request->file('generalimage') AND empty($request->file('coverimage'))){

            $editplaces ->placetitle = $request['placetitle'];
            $editplaces ->discovercaption = $request['discovercaption'];
            $editplaces ->placegeneralinformation = $request['placegeneralinformation'];

            $image = $request->file('generalimage');
            $editplaces->generalimage = $image->getClientOriginalName();
            $imagename = $image->getClientOriginalName();
            $destinationPath = public_path('/Placecoverimage');
            $image->move($destinationPath, $imagename);
            $editplaces->save();
            return back()->with('success','you have successfully edited things to do');

        }else{
            $editplaces ->placetitle = $request['placetitle'];
            $editplaces ->discovercaption = $request['discovercaption'];
            $editplaces ->placegeneralinformation = $request['placegeneralinformation'];
            $editplaces->save();
            return back()->with('success','you have successfully edited things to do');
        }


    }
    public function DiscoverActivity(){
//        $discovers = DiscoverActivity::all();
        $listplaces = AddPlaces::all();
        $listdestination = AddDestination::all();
        $discovers = DiscoverActivity::join('AddPlaces', 'discoveractivity.discoveractivity_category', '=', 'AddPlaces.id')
         ->select('AddPlaces.placetitle','discoveractivity.*')
         ->get();
        return view('backend.DiscoverActivity')->with(['discovers'=>$discovers,'listdestination'=>$listdestination,'listplaces'=>$listplaces]);

    }
    public function AddDiscoverActivity(Request $request){
        $all = $request->all();
        $this->validate($request, [
            'discoveractivity_coverimage' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048|dimensions:width=1000,height=750',
        ]);

        $addplacesactivity = new DiscoverActivity();
        $addplacesactivity ->discoveractivity_title = $request['discoveractivity_title'];
        $addplacesactivity ->discoveractivity_text = $request['discoveractivity_text'];
        $addplacesactivity ->discoveractivity_category = $request['discoveractivity_category'];

        $image = $request->file('discoveractivity_coverimage');
        $addplacesactivity->discoveractivity_coverimage = $image->getClientOriginalName();
        $imagename = $image->getClientOriginalName();
        $destinationPath = public_path('/discovercover');
        $image->move($destinationPath, $imagename);

        $image = $request->file('discoveractivity_generalpicture');
        $addplacesactivity->discoveractivity_generalpicture = $image->getClientOriginalName();
        $imagename = $image->getClientOriginalName();
        $destinationPath = public_path('/discover');
        $image->move($destinationPath, $imagename);

        $addplacesactivity->save();
        $lastid = $addplacesactivity->id;

        $thingstodogallery = $request['thingstodogallery'];

        foreach ($thingstodogallery as $thingstodogallerys) {

            $image = $request->file('thingstodogallery');
            $addpicture = new ThingToDoGallery();

            $addpicture->thingstodoid = $lastid;
            $addpicture->thingstodogallery = $thingstodogallerys->getClientOriginalName();
            $imagename = $thingstodogallerys->getClientOriginalName();
            $destinationPath = public_path('/PlacesPictures');
            $thingstodogallerys->move($destinationPath, $imagename);
            $addpicture->save();

        }

        return back()->with('success','you have successfully added a new discover activity');
    }
    public function EditDiscoverActivity(Request $request){
        $all = $request->all();
        $id = $request['id'];
        $editplaces = DiscoverActivity::find($id);
//        $this->validate($request, [
//            'coverimage' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048|dimensions:width=1000,height=750',
//        ]);
        if($request->file('discoveractivity_coverimage') AND empty($request->file('discoveractivity_generalpicture'))){

            $editplaces ->discoveractivity_title = $request['discoveractivity_title'];
            $editplaces ->discoveractivity_text = $request['discoveractivity_text'];
            $editplaces ->discoveractivity_category = $request['discoveractivity_category'];

            $image = $request->file('discoveractivity_coverimage');
            $editplaces->discoveractivity_coverimage = $image->getClientOriginalName();
            $imagename = $image->getClientOriginalName();
            $destinationPath = public_path('/discovercover');
            $image->move($destinationPath, $imagename);
            $editplaces->save();
            return back()->with('success','you have successfully updated discover activity');

        }elseif($request->file('discoveractivity_generalpicture') AND empty($request->file('discoveractivity_coverimage'))){

            $editplaces ->discoveractivity_title = $request['discoveractivity_title'];
            $editplaces ->discoveractivity_text = $request['discoveractivity_text'];
            $editplaces ->discoveractivity_category = $request['discoveractivity_category'];

            $image = $request->file('discoveractivity_generalpicture');
            $editplaces->discoveractivity_generalpicture = $image->getClientOriginalName();
            $imagename = $image->getClientOriginalName();
            $destinationPath = public_path('/discover');
            $image->move($destinationPath, $imagename);

            $editplaces->save();
            return back()->with('success','you have successfully updated discover activity');

        }else{
            $editplaces ->discoveractivity_title = $request['discoveractivity_title'];
            $editplaces ->discoveractivity_text = $request['discoveractivity_text'];
            $editplaces ->discoveractivity_category = $request['discoveractivity_category'];
            $editplaces->save();
            return back()->with('success','you have successfully updated discover activity');
        }
    }
    public function DeleteDiscoverActivity(Request $request){
        $id = $request['id'];
        $deletedisco = DiscoverActivity::find($id);
        $deletedisco->delete();
        $deletediscogalert = ThingToDoGallery::where('thingstodoid',$id)->delete();
        return back()->with('success','you have successfully deleted discover activity');
    }

    public function Stay(){
        $listplaces = Stay::all();
        $listdestination = AddDestination::all();
        return view('backend.Stay')->with(['listdestination'=>$listdestination,'listplaces'=>$listplaces]);
    }
    public function AddStay(Request $request){
        $all = $request->all();
        $user_id = \Auth::user()->id;
//
        $this->validate($request, [
            'coverimage' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048|dimensions:width=1000,height=750',
        ]);

        $addplaces = new Stay();
        $addplaces ->thingstitle = $request['thingstitle'];
        $addplaces ->thingsgeneralinformation = $request['thingsgeneralinformation'];

        $image = $request->file('coverimage');
        $addplaces->coverimage = $image->getClientOriginalName();
        $imagename = $image->getClientOriginalName();
        $destinationPath = public_path('/Placecoverimage');
        $image->move($destinationPath, $imagename);

        $image = $request->file('generalimage');
        $addplaces->generalimage = $image->getClientOriginalName();
        $imagename = $image->getClientOriginalName();
        $destinationPath = public_path('/Placecoverimage');
        $image->move($destinationPath, $imagename);

        $addplaces->save();


        $lastid = $addplaces->id;

        $staygallery = $request['staygallery'];
        foreach ($staygallery as $staygallerys) {

            $image = $request->file('staygallery');
            $addpicture = new StayGallery();

            $addpicture->stayid = $lastid;
            $addpicture->staygallery = $staygallerys->getClientOriginalName();
            $imagename = $staygallerys->getClientOriginalName();
            $destinationPath = public_path('/PlacesPictures');
            $staygallerys->move($destinationPath, $imagename);
            $addpicture->save();

        }

        $eventby = \Auth::user()->name;
        $eventtitle = $request['thingstitle'];
        $eventid = $lastid;
        $eventpurpose = 'new place to stay';

//        User::find($user_id)->notify(new WebAppNotification($eventtitle,$eventid,$eventby,$eventpurpose));
        Notification::send(User::all(), new WebAppNotification($eventtitle,$eventid,$eventby,$eventpurpose));
        return back()->with('success','you have successfully added a new place');
    }
    public function EditStay(Request $request){
        $id = $request['id'];
        $editplaces = Stay::find($id);
//        $this->validate($request, [
//            'coverimage' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048|dimensions:width=1000,height=750',
//        ]);
        if($request->file('coverimage') AND empty($request->file('generalimage'))){

            $editplaces ->placetitle = $request['placetitle'];
            $editplaces ->placegeneralinformation = $request['placegeneralinformation'];

            $image = $request->file('coverimage');
            $editplaces->coverimage = $image->getClientOriginalName();
            $imagename = $image->getClientOriginalName();
            $destinationPath = public_path('/Placecoverimage');
            $image->move($destinationPath, $imagename);

            $editplaces->save();
            return back()->with('success','you have successfully edited place to stay');

        }elseif($request->file('generalimage') AND empty($request->file('coverimage'))){

            $editplaces ->placetitle = $request['placetitle'];
            $editplaces ->placegeneralinformation = $request['placegeneralinformation'];

            $image = $request->file('generalimage');
            $editplaces->generalimage = $image->getClientOriginalName();
            $imagename = $image->getClientOriginalName();
            $destinationPath = public_path('/Placecoverimage');
            $image->move($destinationPath, $imagename);
            $editplaces->save();
            return back()->with('success','you have successfully edited place to stay');

        }else{
            $editplaces ->placetitle = $request['placetitle'];
            $editplaces ->placegeneralinformation = $request['placegeneralinformation'];
            $editplaces->save();
            return back()->with('success','you have successfully edited place to stay');
        }


    }

    public function DeleteStay(Request $request){
        $id = $request['id'];
        $deleteplaces = AddPlaces::find($id);
        $deleteimageplaces = PlacesPictures::where('places_id', $id)->delete();
        $deleteplaces->delete();

        return back()->with('success','you have successfully deleted place information');
    }

    public function HomeRow1(){
        $homerow = Homerow1::all();
        return view('backend.HomeRow1')->with(['homerow'=>$homerow]);
    }
    public function AddHomeRow1(Request $request){
        $all = $request->all();
        $addhome = new Homerow1();
        $addhome->row_title = $request['row_title'];
        $addhome->row_text = $request['row_text'];

        $image = $request->file('row_cover_pic');
        $addhome->row_cover_pic = $image->getClientOriginalName();
        $imagename = $image->getClientOriginalName();
        $destinationPath = public_path('/Homerow1');
        $image->move($destinationPath, $imagename);

        $addhome->save();
        return back()->with('success','you have successfully added home row one content');

    }
    public function EditHomeRow1(Request $request){
        if($request->file('row_cover_pic')){
            $all = $request->all();
            $id =$request['id'];
            $addhome = Homerow1::find($id);
            $addhome->row_title = $request['row_title'];
            $addhome->row_text = $request['row_text'];
            $image = $request->file('row_cover_pic');
            $addhome->row_cover_pic = $image->getClientOriginalName();
            $imagename = $image->getClientOriginalName();
            $destinationPath = public_path('/Homerow1');
            $image->move($destinationPath, $imagename);
            $addhome->save();
            return back()->with('success','you have successfully updated home row one content');
        }else{
            $id =$request['id'];
            $addhome = Homerow1::find($id);
            $addhome->row_title = $request['row_title'];
            $addhome->row_text = $request['row_text'];
            $addhome->save();
            return back()->with('success','you have successfully updated home row one content');
        }
    }
    public function HomeRow2(){
        $homerow = Homerow2::all();
        return view('backend.HomeRow2')->with(['homerow'=>$homerow]);
    }
    public function AddHomeRow2(Request $request){
        $all = $request->all();
        $addhome = new Homerow2();
        $addhome->row_title = $request['row_title'];
        $addhome->row_text = $request['row_text'];
        $image = $request->file('row_cover_pic');
        $addhome->row_cover_pic = $image->getClientOriginalName();
        $imagename = $image->getClientOriginalName();
        $destinationPath = public_path('/Homerow2');
        $image->move($destinationPath, $imagename);
        $addhome->save();
        return back()->with('success','you have successfully added home row two content');

    }
    public function EditHomeRow2(Request $request){
        if($request->file('row_cover_pic')){
            $all = $request->all();
            $id =$request['id'];
            $addhome = Homerow2::find($id);
            $addhome->row_title = $request['row_title'];
            $addhome->row_text = $request['row_text'];
            $image = $request->file('row_cover_pic');
            $addhome->row_cover_pic = $image->getClientOriginalName();
            $imagename = $image->getClientOriginalName();
            $destinationPath = public_path('/Homerow2');
            $image->move($destinationPath, $imagename);
            $addhome->save();
            return back()->with('success','you have successfully updated home row two content');
        }else{
            $id =$request['id'];
            $addhome = Homerow2::find($id);
            $addhome->row_title = $request['row_title'];
            $addhome->row_text = $request['row_text'];
            $addhome->save();
            return back()->with('success','you have successfully updated home row two content');
        }
    }
    public function HomeRow3(){
        $homerow = Homerow3::all();
        return view('backend.HomeRow3')->with(['homerow'=>$homerow]);
    }
    public function AddHomeRow3(Request $request){
        $all = $request->all();
        $addhome = new Homerow3();
        $addhome->row_title = $request['row_title'];
        $addhome->row_text = $request['row_text'];
        $image = $request->file('row_cover_pic');
        $addhome->row_cover_pic = $image->getClientOriginalName();
        $imagename = $image->getClientOriginalName();
        $destinationPath = public_path('/Homerow3');
        $image->move($destinationPath, $imagename);
        $addhome->save();
        return back()->with('success','you have successfully added home row Three content');

    }
    public function EditHomeRow3(Request $request){
        if($request->file('row_cover_pic')){
            $all = $request->all();
            $id =$request['id'];
            $addhome = Homerow3::find($id);
            $addhome->row_title = $request['row_title'];
            $addhome->row_text = $request['row_text'];
            $image = $request->file('row_cover_pic');
            $addhome->row_cover_pic = $image->getClientOriginalName();
            $imagename = $image->getClientOriginalName();
            $destinationPath = public_path('/Homerow3');
            $image->move($destinationPath, $imagename);
            $addhome->save();
            return back()->with('success','you have successfully updated home row Three content');
        }else{
            $id =$request['id'];
            $addhome = Homerow3::find($id);
            $addhome->row_title = $request['row_title'];
            $addhome->row_text = $request['row_text'];
            $addhome->save();
            return back()->with('success','you have successfully updated home row Three content');
        }
    }
    public function AboutRwandaRow1(){
        $homerow = AboutRwandaRow1::all();
        return view('backend.AboutRwandaRow1')->with(['homerow'=>$homerow]);
    }
    public function AddAboutRwandaRowOne(Request $request){
        $all = $request->all();
        $addhome = new AboutRwandaRow1();
        $addhome->row_title = $request['row_title'];
        $addhome->row_text = $request['row_text'];
        $image = $request->file('row_cover_pic');
        $addhome->row_cover_pic = $image->getClientOriginalName();
        $imagename = $image->getClientOriginalName();
        $destinationPath = public_path('/AboutRwandaRow1');
        $image->move($destinationPath, $imagename);
        $addhome->save();
        return back()->with('success','you have successfully added About Rwanda Row one content');

    }
    public function EditAboutRwandaRowOne(Request $request){
        if($request->file('row_cover_pic')){
            $all = $request->all();
            $id =$request['id'];
            $addhome = AboutRwandaRow1::find($id);
            $addhome->row_title = $request['row_title'];
            $addhome->row_text = $request['row_text'];
            $image = $request->file('row_cover_pic');
            $addhome->row_cover_pic = $image->getClientOriginalName();
            $imagename = $image->getClientOriginalName();
            $destinationPath = public_path('/AboutRwandaRow1');
            $image->move($destinationPath, $imagename);
            $addhome->save();
            return back()->with('success','you have successfully updated About Rwanda Row one content');
        }else{
            $all= $request->all();
            $id =$request['id'];
            $addhome = AboutRwandaRow1::find($id);
            $addhome->row_title = $request['row_title'];
            $addhome->row_text = $request['row_text'];
            $addhome->save();

            return back()->with('success','you have successfully updated About Rwanda Row one content');
        }
    }
    public function AboutRwandaRow2(){
        $homerow = AboutRwandaRow2::all();
        return view('backend.AboutRwandaRow2')->with(['homerow'=>$homerow]);
    }
    public function AddAboutRwandaRow2(Request $request){
        $all = $request->all();
        $addhome = new AboutRwandaRow2();
        $addhome->row_title = $request['row_title'];
        $addhome->row_text = $request['row_text'];
        $image = $request->file('row_cover_pic');
        $addhome->row_cover_pic = $image->getClientOriginalName();
        $imagename = $image->getClientOriginalName();
        $destinationPath = public_path('/AboutRwandaRow2');
        $image->move($destinationPath, $imagename);
        $addhome->save();
        return back()->with('success','you have successfully added About Rwanda Row Two content');

    }
    public function EditAboutRwandaRow2(Request $request){
        if($request->file('row_cover_pic')){
            $all = $request->all();
            $id =$request['id'];
            $addhome = AboutRwandaRow2::find($id);
            $addhome->row_title = $request['row_title'];
            $addhome->row_text = $request['row_text'];
            $image = $request->file('row_cover_pic');
            $addhome->row_cover_pic = $image->getClientOriginalName();
            $imagename = $image->getClientOriginalName();
            $destinationPath = public_path('/AboutRwandaRow2');
            $image->move($destinationPath, $imagename);
            $addhome->save();
            return back()->with('success','you have successfully updated About Rwanda Row Two content');
        }else{
            $all= $request->all();
            $id =$request['id'];
            $addhome = AboutRwandaRow2::find($id);
            $addhome->row_title = $request['row_title'];
            $addhome->row_text = $request['row_text'];
            $addhome->save();

            return back()->with('success','you have successfully updated About Rwanda Row Two content');
        }
    }
    public function AboutRegionRowOne(){
        $homerow = AboutRegionRow1::all();
        return view('backend.AboutRegionRow1')->with(['homerow'=>$homerow]);
    }
    public function AddAboutRegionRow1(Request $request){
        $all = $request->all();
        $addhome = new AboutRegionRow1();
        $addhome->row_title = $request['row_title'];
        $addhome->row_text = $request['row_text'];
        $image = $request->file('row_cover_pic');
        $addhome->row_cover_pic = $image->getClientOriginalName();
        $imagename = $image->getClientOriginalName();
        $destinationPath = public_path('/AboutRegionRow1');
        $image->move($destinationPath, $imagename);
        $addhome->save();
        return back()->with('success','you have successfully added About Region Row One content');

    }
    public function EditAboutRegionRow1(Request $request){
        if($request->file('row_cover_pic')){
            $all = $request->all();
            $id =$request['id'];
            $addhome = AboutRegionRow1::find($id);
            $addhome->row_title = $request['row_title'];
            $addhome->row_text = $request['row_text'];
            $image = $request->file('row_cover_pic');
            $addhome->row_cover_pic = $image->getClientOriginalName();
            $imagename = $image->getClientOriginalName();
            $destinationPath = public_path('/AboutRegionRow1');
            $image->move($destinationPath, $imagename);
            $addhome->save();
            return back()->with('success','you have successfully updated About Region Row One content');
        }else{
            $all= $request->all();
            $id =$request['id'];
            $addhome = AboutRegionRow1::find($id);
            $addhome->row_title = $request['row_title'];
            $addhome->row_text = $request['row_text'];
            $addhome->save();

            return back()->with('success','you have successfully updated About Region Row One content');
        }
    }

    public function AboutRegionRowTwo(){
        $homerow = AboutRegionRow2::all();
        return view('backend.AboutRegionRow2')->with(['homerow'=>$homerow]);
    }
    public function AddAboutRegionRowTwo(Request $request){
        $all = $request->all();
        $addhome = new AboutRegionRow2();
        $addhome->row_title = $request['row_title'];
        $addhome->row_text = $request['row_text'];
        $image = $request->file('row_cover_pic');
        $addhome->row_cover_pic = $image->getClientOriginalName();
        $imagename = $image->getClientOriginalName();
        $destinationPath = public_path('/AboutRegionRow2');
        $image->move($destinationPath, $imagename);
        $addhome->save();
        return back()->with('success','you have successfully added About Region Row Two content');

    }
    public function EditAboutRegionRowTwo(Request $request){
        if($request->file('row_cover_pic')){
            $all = $request->all();
            $id =$request['id'];
            $addhome = AboutRegionRow2::find($id);
            $addhome->row_title = $request['row_title'];
            $addhome->row_text = $request['row_text'];
            $image = $request->file('row_cover_pic');
            $addhome->row_cover_pic = $image->getClientOriginalName();
            $imagename = $image->getClientOriginalName();
            $destinationPath = public_path('/AboutRegionRow2');
            $image->move($destinationPath, $imagename);
            $addhome->save();
            return back()->with('success','you have successfully updated About Region Row Two content');
        }else{
            $all= $request->all();
            $id =$request['id'];
            $addhome = AboutRegionRow2::find($id);
            $addhome->row_title = $request['row_title'];
            $addhome->row_text = $request['row_text'];
            $addhome->save();

            return back()->with('success','you have successfully updated About Region Row Two content');
        }
    }
    public function AddEatAndDrink(){
        $listplaces = EatAndDrink::all();
        $listdestination = AddDestination::all();
        return view('backend.AddEatAndDrink')->with(['listdestination'=>$listdestination,'listplaces'=>$listplaces]);
    }
    public function UploadEatAndDrink(Request $request){
        $all = $request->all();
        $user_id = \Auth::user()->id;
//
        $this->validate($request, [
            'coverimage' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048|dimensions:width=1000,height=750',
        ]);

        $addplaces = new EatAndDrink();
        $addplaces ->placetitle = $request['placetitle'];
        $addplaces ->placegeneralinformation = $request['placegeneralinformation'];
        $addplaces ->placelocation = $request['placelocation'];
        $addplaces ->placecategory = $request['placecategory'];

        $image = $request->file('coverimage');
        $addplaces->coverimage = $image->getClientOriginalName();
        $imagename = $image->getClientOriginalName();
        $destinationPath = public_path('/Placecoverimage');
        $image->move($destinationPath, $imagename);

        $image = $request->file('generalimage');
        $addplaces->generalimage = $image->getClientOriginalName();
        $imagename = $image->getClientOriginalName();
        $destinationPath = public_path('/Placecoverimage');
        $image->move($destinationPath, $imagename);


        $addplaces->save();
        $lastid = $addplaces->id;
        $eventby = \Auth::user()->name;
        $eventtitle = $request['placetitle'];
        $eventid = $lastid;
        $eventpurpose = 'new eat and drink place';

        $eatanddrinkgallery = $request['eatanddrinkgallery'];

        foreach ($eatanddrinkgallery as $eatanddrinkgallerys) {

            $image = $request->file('eatdrinkpicturename');
            $addpicture = new EatDrinkGallery();

            $addpicture->eatdrinkid = $lastid;
            $addpicture->eatdrinkpicturename = $eatanddrinkgallerys->getClientOriginalName();
            $imagename = $eatanddrinkgallerys->getClientOriginalName();
            $destinationPath = public_path('/PlacesPictures');
            $eatanddrinkgallerys->move($destinationPath, $imagename);
            $addpicture->save();

        }

//        User::find($user_id)->notify(new WebAppNotification($eventtitle,$eventid,$eventby,$eventpurpose));
        Notification::send(User::all(), new WebAppNotification($eventtitle,$eventid,$eventby,$eventpurpose));
        return back()->with('success','you have successfully added a new eat and drink place');
    }
    public function EditEatAndDrink(Request $request){
        $id = $request['id'];
        $editplaces = EatAndDrink::find($id);
//        $this->validate($request, [
//            'coverimage' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048|dimensions:width=1000,height=750',
//        ]);
        if($request->file('coverimage') AND empty($request->file('generalimage'))){

            $editplaces ->placetitle = $request['placetitle'];
            $editplaces ->placegeneralinformation = $request['placegeneralinformation'];
            $editplaces ->placelocation = $request['placelocation'];
            $editplaces ->placecategory = $request['placecategory'];

            $image = $request->file('coverimage');
            $editplaces->coverimage = $image->getClientOriginalName();
            $imagename = $image->getClientOriginalName();
            $destinationPath = public_path('/Placecoverimage');
            $image->move($destinationPath, $imagename);

            $editplaces->save();
            return back()->with('success','you have successfully edited eat and drink section');

        }elseif($request->file('generalimage') AND empty($request->file('coverimage'))){

            $editplaces ->placetitle = $request['placetitle'];
            $editplaces ->placegeneralinformation = $request['placegeneralinformation'];
            $editplaces ->placelocation = $request['placelocation'];
            $editplaces ->placecategory = $request['placecategory'];

            $image = $request->file('generalimage');
            $editplaces->generalimage = $image->getClientOriginalName();
            $imagename = $image->getClientOriginalName();
            $destinationPath = public_path('/Placecoverimage');
            $image->move($destinationPath, $imagename);
            $editplaces->save();
            return back()->with('success','you have successfully edited eat and drink section');

        }else{
            $editplaces ->placetitle = $request['placetitle'];
            $editplaces ->placegeneralinformation = $request['placegeneralinformation'];
            $editplaces ->placelocation = $request['placelocation'];
            $editplaces ->placecategory = $request['placecategory'];
            $editplaces->save();
            return back()->with('success','you have successfully edited eat and drink section');
        }


    }
    public function DeleteEatAndDrink(Request $request){
        $id = $request['id'];
        $deleteplaces = EatAndDrink::find($id);
        $deleteplaces->delete();
        $deleteimageplaces = EatDrinkGallery::where('eatdrinkid', $id)->delete();

        return back()->with('success','you have successfully deleted eat and drink section');
    }
    public function PlacesImages(Request $request){
        $id = $request['id'];
        $getimages = PlacesPictures::where('places_id',$id)->get();
        return view('backend.PlacesImages')->with(['getimages'=>$getimages,'id'=>$id]);
    }
    public function DeletePlaces(Request $request){
        $id = $request['id'];
        $deleteplaces = AddPlaces::find($id);
        $deleteimageplaces = PlacesPictures::where('places_id', $id)->delete();
        $deleteplaces->delete();

        return back()->with('success','you have successfully deleted place information');
    }
    public function PlacesMoreImages(Request $request){
        $placespicture = $request['placespicture'];
        $id = $request['id'];
        $placeimages = $request->file('placespicture');

        foreach ($placespicture as $placespictures) {
            $image = $request->file('placespicture');
            $editpictures = new PlacesPictures;
            $editpictures->places_id = $id;
            $editpictures->places_picture_name = $placespictures->getClientOriginalName();
            $imagename = $placespictures->getClientOriginalName();
            $destinationPath = public_path('/PlacesPictures');
            $placespictures->move($destinationPath, $imagename);
            $editpictures->save();

        }
        return back()->with('success','you have successfully added more images');
    }
    public function DeletePlacesImages(Request $request){
        $id = $request['id'];
        $deleteplaces = PlacesPictures::find($id);
        $deleteplaces->delete();
        return back()->with('success','you have successfully deleted Place Image');
    }
    public function ActivityCategory(){
        $listcat = ActivityCategory::all();
        return view('backend.ActivityCategory')->with(['listcat'=>$listcat]);
    }

    public function AddActivityCategory(Request $request){
        $all = $request->all();
//        dd($all);
        $activityname = $request['activityname'];

        foreach ($activityname as $key => $activitynames){
            $addact = new ActivityCategory();
            $addact->activityname = $request['activityname'][$key];
            $addact->save();
        }
        return back()->with('success','you have successfully added an activity');

    }
    public function EditActivityCategory(Request $request){
        $id = $request['id'];
        $editact = ActivityCategory::find($id);
        $editact->activityname = $request['activityname'];
        $editact->save();
        return back()->with('success','you have successfully added an activity');
    }
    public function RegisteringProcess(){
        $listcat = RegisterProcessCatName::all();
        return view('backend.RegisteringProcess')->with(['listcat'=>$listcat]);
    }
    public function AddRegisteringProcess(Request $request){
        $all = $request->all();

        $addcategory = new RegisterProcessCatName();
        $addcategory ->category_name = $request['category_name'];
        $addcategory ->save();

        $category_name_id = $addcategory->id;
        $subcategory_name = $request['subcategory_name'];
        $questions = $request['questions'];

        foreach ($subcategory_name as $key => $subcategory_names){
            $addsubact = new RegisterProcessSubCatName();
            $addsubact->subcategory_name = $request['subcategory_name'][$key];
            $addsubact->category_name_id = $category_name_id;
            $addsubact->save();

        }

        foreach ($questions as $key => $questionss){
            $addquest = new RegisterProcessQuestions();
            $addquest->questions = $request['questions'][$key];
            $addquest->questions_title = $request['questions_title'][$key];
            $addquest->category_name_id = $category_name_id;
            $addquest->save();
        }

        return back()->with('success','you have successfully added a register process');
    }
    public function DeleteRegisteringProcess(Request $request){
        $id = $request['id'];
        $deletecat = RegisterProcessCatName::find($id);
        $deletecat->delete();
        $deletesubcat = RegisterProcessSubCatName::where('category_name_id', $id)->delete();
        $deletequest = RegisterProcessQuestions::where('category_name_id', $id)->delete();

        return back()->with('success','you have successfully deleted a register  process');
    }
    public function Gallery(){
        $listgallery = Gallery::all();
        return view('backend.Gallery')->with(['listgallery'=>$listgallery]);
    }

    public function AddGallery(Request $request){
        $all = $request->all();
        $placeimages = $request->file('gallery');

        foreach ($placeimages as $placespictures) {
            $image = $request->file('gallery');
            $editpictures = new Gallery();
            $editpictures->galleryname = $placespictures->getClientOriginalName();
            $imagename = $placespictures->getClientOriginalName();
            $destinationPath = public_path('/GalleryPictures');
            $placespictures->move($destinationPath, $imagename);
            $editpictures->save();
        }
        return back()->with('success','you have successfully uploaded an image');
    }
    public function DeleteGallery(Request $request){
        $id = $request['id'];
        $delete = Gallery::find($id);
        $delete->delete();
        return back()->with('success','you have successfully deleted an image');
    }
    public function IgApi(){
        $listgallery = IgGallery::all();
        return view('backend.IgApi')->with(['listgallery'=>$listgallery]);
    }
    public function AddIGGallery(Request $request){
        $addgallery = new IgGallery();
        $addgallery->igpicturetext = $request['igpicturetext'];
        $image = $request->file('igpicturename');
        $addgallery->igpicturename = $image->getClientOriginalName();
        $imagename = $image->getClientOriginalName();
        $destinationPath = public_path('/IGGallery');
        $image->move($destinationPath, $imagename);
        $addgallery->save();
        return back()->with('success','you have successfully added an image');
    }
    public function DeleteIGGallery(Request $request){
        $id = $request['id'];
        $delete = IgGallery::find($id);
        $delete->delete();
        return back()->with('success','you have successfully deleted an image');
    }
    public function GetIgImages(){
        $tag = 'visiteastafrica';
        $insta_source = file_get_contents('https://www.instagram.com/kivubelt_/'); // instagrame tag url
        $shards = explode('window._sharedData = ', $insta_source);
        $insta_json = explode(';</script>', $shards[1]);
        $insta_array = json_decode($insta_json[0]);
        $instagramdata = $insta_array->entry_data->ProfilePage[0]->graphql->user->edge_owner_to_timeline_media->edges;
        $secig = json_encode($instagramdata);
        $limit = array_slice($instagramdata,0,5);
        dd($limit);

//        return view('backend.GetIgImages')->with(['instagramdata'=>$instagramdata]);
//
    }

//    public function CompanyTypes(){
//        $listcompanytypes = CompanyTypes::all();
//        return view('backend.CompanyTypes')->with(['listcompanytypes'=>$listcompanytypes]);
//    }
//    public function AddCompanyTypes(Request $request){
//
//        $companytypes = $request['companytypes'];
//
//        foreach ($companytypes as $key => $types){
//            $addcomptype = new CompanyTypes();
//            $addcomptype->companytypes = $request['companytypes'][$key];
//            $addcomptype->save();
//        }
//
//        return back()->with('success','you have successfully added company type');
//    }
//    public function EditCompanyTypes(Request $request){
//        $id = $request['id'];
//        $addcomptype = CompanyTypes::find($id);
//        $addcomptype->companytypes = $request['companytypes'];
//        $addcomptype->save();
//
//        return back()->with('success','you have successfully edited company type');
//    }
//    public function DeleteCompanyTypes(Request $request){
//        $id = $request['id'];
//        $delete = CompanyTypes::find($id);
//        $delete->delete();
//        return back()->with('success','you have successfully deleted company type');
//    }
//    public function MarketSegments(){
//        $listsegments = MarketSegments::all();
//        return view('backend.MarketSegments')->with(['listsegments'=>$listsegments]);
//    }
//    public function AddMarketSegments(Request $request){
//
//        $marketsegments = $request['marketsegments'];
//
//        foreach ($marketsegments as $key => $types){
//            $addseg = new MarketSegments();
//            $addseg->marketsegments = $request['marketsegments'][$key];
//            $addseg->save();
//        }
//
//        return back()->with('success','you have successfully added market segment');
//    }
//    public function DeleteMarketSegments(Request $request){
//        $id = $request['id'];
//        $delete = MarketSegments::find($id);
//        $delete->delete();
//        return back()->with('success','you have successfully deleted market segment');
//    }
//    public function EditMarketSegments(Request $request){
//        $id = $request['id'];
//        $addseg = MarketSegments::find($id);
//        $addseg->marketsegments = $request['marketsegments'];
//        $addseg->save();
//
//        return back()->with('success','you have successfully edited market segment');
//    }
//    public function AddAgentCompanyType(Request $request){
//        $user_id= \Auth::user()->id;
//        $agentcompanytype = $request['agentcompanytype'];
//        $all = $request->all();
////        dd($all);
//
//        foreach ($agentcompanytype as $key => $types){
//            $addseg = new AgentCompanyType();
//            $addseg->agent_profile_id = $user_id;
//            $addseg->agentcompanytype = $request['agentcompanytype'][$key];
//            $addseg->save();
//        }
//        return back()->with('success','you have successfully added company type');
//    }
//    public function EditAgentCompanyType(Request $request){
//        $id = $request['id'];
//        $user_id= \Auth::user()->id;
//        $deletect = AgentCompanyType::where('agent_profile_id',$user_id)->delete();
//        $agentcompanytype = $request['agentcompanytype'];
//        $all = $request->all();
////        dd($all);
//
//        foreach ($agentcompanytype as $key => $types){
//            $addseg = new AgentCompanyType();
//            $addseg->agent_profile_id = $user_id;
//            $addseg->agentcompanytype = $request['agentcompanytype'][$key];
//            $addseg->save();
//        }
//        return back()->with('success','you have successfully added company type');
//    }
//
//    public function AddAgentMarketSegment(Request $request){
//        $user_id= \Auth::user()->id;
//        $agentmarketsegment = $request['agentmarketsegment'];
//        $all = $request->all();
////        dd($all);
//
//        foreach ($agentmarketsegment as $key => $types){
//            $addseg = new AgentMarketSegment();
//            $addseg->agent_profile_id = $user_id;
//            $addseg->agentmarketsegment = $request['agentmarketsegment'][$key];
//            $addseg->save();
//        }
//        return back()->with('success','you have successfully added market segment');
//    }
//
//    public function EditAgentMarketSegment(Request $request){
//        $id = $request['id'];
//        $user_id= \Auth::user()->id;
//        $deletect = AgentMarketSegment::where('agent_profile_id',$user_id)->delete();
//        $agentmarketsegment = $request['agentmarketsegment'];
//        $all = $request->all();
////        dd($all);
//
//        foreach ($agentmarketsegment as $key => $types){
//            $addseg = new AgentMarketSegment();
//            $addseg->agent_profile_id = $user_id;
//            $addseg->agentmarketsegment = $request['agentmarketsegment'][$key];
//            $addseg->save();
//        }
//        return back()->with('success','you have successfully added market segment');
//    }
//    public function PartnerFinder(){
//        $listcompantype = CompanyTypes::all();
//        $listmarketsegment = MarketSegments::all();
//        $listagentdetails = AgentTourism::all();
//        $listusers = User::where('role','agent')->get();
//        return view('backend.PartnerFinder')->with(['listusers'=>$listusers,'listagentdetails'=>$listagentdetails,'listmarketsegment'=>$listmarketsegment,'listcompantype'=>$listcompantype]);
//    }
//    public function SearchUserData(Request $request){
//        $companyname = $request['companyname'];
//        $listusers = User::where('agentcompanyname', 'like', '%' . $companyname . '%')->where('role','agent')->get();
//
//        foreach($listusers as $userslist){
//
//            $companyemail = $userslist->email;
//            $companyuserid = \App\User::where('email',$companyemail)->value('id');
//            $id = $userslist->id;
//            $getcompanywebsite = \App\AgentCompanyWebsite::where('companyprofileid',$id)->value('companywebsite');
//            $getcompanyphoto = \App\AgentProfilePhoto::where('agent_profile_id',$id)->value('agent_profile_photoname');
//            $getcompanytypes = \App\AgentCompanyType::where('agent_profile_id',$id)
//                ->join('companytypes', 'agentcompanytype.agentcompanytype', '=', 'companytypes.id')
//                ->select('companytypes.companytypes','agentcompanytype.*')
//                ->get();
//            $listagentmarketsegment = \App\AgentMarketSegment::where('agent_profile_id',$id)
//                ->join('marketsegments', 'agentmarketsegment.agentmarketsegment', '=', 'marketsegments.id')
//                ->select('marketsegments.marketsegments','agentmarketsegment.*')
//                ->get();
//            $getcompanyaddress = \App\AgentCompanyAddress::where('companyprofileid',$id) ->join('country', 'country.id', '=', 'agentcompanyaddress.companycountry')
//                ->select('country.name', 'agentcompanyaddress.*')->get();
//
//            foreach($getcompanytypes as $types){
//                $types = $types->companytypes;
//            }
//            foreach($listagentmarketsegment as $segment) {
//                $segments = $segment->marketsegments;
//            }
//            foreach($getcompanyaddress as $address){
//                $country = $address->name;
//                $city = $address->companycity;
//            }
//            echo "<li class='media' style='padding: 5px'><div class='media-left'><a href='#'><img class='media-object width-150' src='AgentPhoto/$getcompanyphoto' alt='Generic placeholder image'></a></div><div class='media-body media-search' style='padding: 10px;'><p class='lead mb-0'><span class='text-bold-600'>$userslist->agentcompanyname</span></p><p class='mb-0'><a href='#' class='teal darken-1'>Website: $getcompanywebsite<i class='la la-angle-down' aria-hidden='true'></i></a></p><p><span class='text-muted'><strong>Market Segment: $segments</strong>  </span></p><p><span class='text-muted'><strong>Company Type: $types</strong></span></p><p><span class=\"text-muted\"><strong>Country:</strong> $country</span> <span class=\"text-muted\"><strong>City:</strong> $city </span></p></div></li>";
//        }
//    }
//    public function  PressRelease(){
//        $listpress = PressRelease::all();
//        return view('backend.PressReleases')->with(['listpress'=>$listpress]);
//    }
//
//    public function AddPressRelease(Request $request){
//        $all = $request->all();
//        $addnews = new PressRelease();
//        $addnews->news_title = $request['news_title'];
//        $addnews->news_details = $request['news_details'];
//        $addnews->newsdescription = $request['newsdescription'];
//        $image = $request->file('presscoverpicture');
//        $addnews->presscoverpicture = $image->getClientOriginalName();
//        $imagename = $image->getClientOriginalName();
//        $destinationPath = public_path('/PressRelease');
//        $image->move($destinationPath, $imagename);
//        $addnews->save();
//        return back()->with('success','you have created a new press release');
//    }
//    public function EditPressRelease(Request $request){
//        $id = $request['id'];
//        $addnews = PressRelease::find($id);
//        $addnews->news_title = $request['news_title'];
//        $addnews->news_details = $request['news_details'];
//        $addnews->newsdescription = $request['newsdescription'];
//        $image = $request->file('presscoverpicture');
//        $addnews->presscoverpicture = $image->getClientOriginalName();
//        $imagename = $image->getClientOriginalName();
//        $destinationPath = public_path('/PressRelease');
//        $image->move($destinationPath, $imagename);
//        $addnews->save();
//        return back()->with('success','you have created press release');
//    }
//    public function DeletePressRelease(Request $request){
//        $id =$request['id'];
//        $delete = PressRelease::find($id);
//        $delete->delete();
//        return back()->with('success','you have deleted press release');
//    }

}
