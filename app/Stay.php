<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stay extends Model
{
    protected $table = "stay";
    protected $fillable = ['id','thingstitle','thingsgeneralinformation','coverimage','generalimage'];
}
