<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AgentProfileServices extends Model
{
    protected $table = "agentprofile_services";
    protected $fillable = ['id','agent_profile_id','agent_profile_services'];
}
