<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EatAndDrink extends Model
{
    protected $table = "eatanddrink";
    protected $fillable = ['id','placetitle','placegeneralinformation','coverimage','generalimage','placelocation','placecategory'];
}
