<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AddPlaces extends Model
{
    protected $table = "AddPlaces";
    protected $fillable = ['id','placetitle','discovercaption','placegeneralinformation','coverimage','generalimage'];
}
