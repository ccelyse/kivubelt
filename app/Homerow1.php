<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Homerow1 extends Model
{
    protected $table = "homerow1";
    protected $fillable = ['row_title','row_cover_pic','row_text'];
}
