<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EatDrinkGallery extends Model
{
    protected $table = "eatdrinkgallery";
    protected $fillable = ['id','eatdrinkid','eatdrinkpicturename'];
}
