<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExperienceDetailsImage extends Model
{
    protected $table = "experience_details_image";
    protected $fillable = ['id','activity_id','activity_details_id','activity_details_image'];
}
