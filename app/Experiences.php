<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Experiences extends Model
{
    protected $table = "experiences";
    protected $fillable = ['id','activity_title','activity_summary','activity_price','activity_numberofguest','activity_information_pdf',
        'activity_coverimage','activity_destination','activity_place'];
}
