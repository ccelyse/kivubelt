<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $table = "event";
    protected $fillable = ['id','eventname','eventdescription','eventstartdate','eventstarttime','eventenddate','eventendtime','eventimagename','eventlocation','eventuserid'];
}
