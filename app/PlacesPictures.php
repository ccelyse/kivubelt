<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlacesPictures extends Model
{
    protected $table = "PlacesPictures";
    protected $fillable = ['id','places_id','places_picture_name'];
}
