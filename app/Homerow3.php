<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Homerow3 extends Model
{
    protected $table = "homerow3";
    protected $fillable = ['row_title','row_cover_pic','row_text'];
}
