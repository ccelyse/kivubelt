<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AboutRegionRow3 extends Model
{
    protected $table = "aboutregionrow3";
    protected $fillable = ['row_title','row_cover_pic','row_text'];
}
