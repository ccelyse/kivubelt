<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RegisterProcessSubCatName extends Model
{
    protected $table = "registerprocess_subcat_name";
    protected $fillable = ['id','subcategory_name','category_name_id'];
}
