<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OperatorProfile extends Model
{
    protected $table = "OperatorProfile";
    protected $fillable = ['id','agent_about','agent_emailaddress','agent_phoneaddress','agent_mapddress'];
}
