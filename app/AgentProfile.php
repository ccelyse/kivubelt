<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AgentProfile extends Model
{
    protected $table = "agentprofile";
    protected $fillable = ['id','agent_about','companyprofileid','agent_profilepicture','agent_coverpicture'];
}
