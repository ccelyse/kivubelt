<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AgentCompanyType extends Model
{
    protected $table = "agentcompanytype";
    protected $fillable = ['id','agentcompanytype','agent_profile_id'];
}
