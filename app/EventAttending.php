<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventAttending extends Model
{
    protected $table = "event_attending";
    protected $fillable = ['id','event_attenting_country','event_company_name','event_company_email','event_company_website','event_status','event_id'];
}
