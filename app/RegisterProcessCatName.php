<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RegisterProcessCatName extends Model
{
    protected $table = "registerprocess_cat_name";
    protected $fillable = ['id','category_name'];
}
