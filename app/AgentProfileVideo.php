<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AgentProfileVideo extends Model
{
    protected $table = "agentprofile_video";
    protected $fillable = ['id','agent_profile_id','agent_profile_videolink'];

}
