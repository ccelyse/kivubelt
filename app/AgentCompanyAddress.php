<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AgentCompanyAddress extends Model
{
    protected $table = "agentcompanyaddress";
    protected $fillable = ['id','companyprofileid','companystreetaddress','companyofficebuiling','companycity','companystateprovince','companyzipcode','companycountry'];
}
