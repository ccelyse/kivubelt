<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AddDestination extends Model
{
    protected $table = "AddDestination";
    protected $fillable = ['id','topdestination','destinationphotocredit','destinationcoverimage','destinationtitle','destinationcurrency','destinationlanguage','destinationgeneralinformation','destinationmap','coverimage'];

}
