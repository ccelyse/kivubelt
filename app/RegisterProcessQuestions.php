<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RegisterProcessQuestions extends Model
{
    protected $table = "registerprocess_questions";
    protected $fillable = ['id','category_name_id','questions_title','questions'];
}
