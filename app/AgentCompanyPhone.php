<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AgentCompanyPhone extends Model
{
    protected $table = "agentcompanyphone";
    protected $fillable = ['id','companyprofileid','companyphone'];
}
