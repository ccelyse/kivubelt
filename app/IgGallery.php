<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IgGallery extends Model
{
    protected $table = "iggallery";
    protected $fillable = ['id','galleryname'];
}
