<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AboutRwandaRow2 extends Model
{
    protected $table = "aboutrwandarow2";
    protected $fillable = ['row_title','row_cover_pic','row_text'];
}
