<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExperienceDetails extends Model
{
    protected $table = "experience_details";
    protected $fillable = ['id','activity_day','activity_day_title','activity_details','activity_id'];
}
