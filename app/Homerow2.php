<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Homerow2 extends Model
{
    protected $table = "homerow2";
    protected $fillable = ['row_title','row_cover_pic','row_text'];
}
