<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AboutRwandaRow1 extends Model
{
    protected $table = "aboutrwandarow1";
    protected $fillable = ['row_title','row_cover_pic','row_text'];
}
