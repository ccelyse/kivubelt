<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AgentProfilePicture extends Model
{
    protected $table = "agentprofilepicture";
    protected $fillable = ['id','agent_profilepicture','agent_coverpicture'];
}
