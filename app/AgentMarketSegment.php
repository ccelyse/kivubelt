<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AgentMarketSegment extends Model
{
    protected $table = "agentmarketsegment";
    protected $fillable = ['id','agent_profile_id','agentmarketsegment'];
}
