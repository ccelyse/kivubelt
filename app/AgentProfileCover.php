<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AgentProfileCover extends Model
{
    protected $table = "agentprofilecover";
    protected $fillable = ['id','agent_profilepicture','agent_coverpicture'];
}
