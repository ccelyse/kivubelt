<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ThingToDoGallery extends Model
{
    protected $table = "thingstodogallery";
    protected $fillable = ['id','thingstodoid','thingstodogallery'];
}
