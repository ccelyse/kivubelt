<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CountryProfile extends Model
{
    protected $table = "countryprofile";
    protected $fillable = ['id','countrytimezone','destinationsurfacearea','destinationflag','destinationcapital','destinationpopulation','countryname','countrygeneralinformation','countrymap','countrylanguage','countrycurrency','coverimage'];

}
