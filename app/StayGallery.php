<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StayGallery extends Model
{
    protected $table = "staygallery";
    protected $fillable = ['id','stayid','staygallery'];
}
