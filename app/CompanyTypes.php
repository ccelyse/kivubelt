<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyTypes extends Model
{
    protected $table = "companytypes";
    protected $fillable = ['id','companytypes'];
}
