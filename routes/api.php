<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::group(['middleware' => 'api_login'], function () {
    Route::get('/MembersList/', 'ListMembers@index');

});

Route::post('/AddTourismAgent', ['as' => 'backend.AddTourismAgent', 'uses' => 'BackendController@AddTourismAgent']);
Route::post('/AddTourOperators', ['as' => 'backend.AddTourOperators', 'uses' => 'BackendController@AddTourOperators']);
Route::get('/MembersList/{id}', 'ListMembers@show');
Route::get('/ViewDestinations', ['as'=>'ViewDestinations','uses'=>'FrontendController@ViewDestinations']);
Route::get('/ViewPlaces', ['as'=>'ViewPlaces','uses'=>'FrontendController@ViewPlaces']);
Route::post('/ViewDestinationsI', ['as'=>'ViewDestinationsI','uses'=>'FrontendController@ViewDestinationsI']);
Route::post('/ViewDestinationPlace', ['as'=>'ViewDestinationPlace','uses'=>'FrontendController@ViewDestinationPlace']);
Route::post('/ViewMorePlace', ['as'=>'ViewMorePlace','uses'=>'FrontendController@ViewMorePlace']);
Route::post('/ViewMorePlaceGallery', ['as'=>'ViewMorePlaceGallery','uses'=>'FrontendController@ViewMorePlaceGallery']);
Route::post('/ViewMorePlaceThings', ['as'=>'ViewMorePlaceThings','uses'=>'FrontendController@ViewMorePlaceThings']);
Route::post('/ViewMoreThings', ['as'=>'ViewMoreThings','uses'=>'FrontendController@ViewMoreThings']);
Route::post('/Experiencedetails', ['as'=>'Experiencedetails','uses'=>'FrontendController@Experiencedetails']);
Route::post('/ExperiencedetailsImage', ['as'=>'ExperiencedetailsImage','uses'=>'FrontendController@ExperiencedetailsImage']);

Route::get('/ExperienceAll', ['as'=>'ExperienceAll','uses'=>'FrontendController@ExperienceAll']);
Route::post('/ExperienceMore', ['as'=>'ExperienceMore','uses'=>'FrontendController@ExperienceMore']);
Route::get('/AllActivity', ['as'=>'AllActivity','uses'=>'FrontendController@AllActivity']);

Route::get('/GetRegisterQuestionCat', ['as'=>'GetRegisterQuestionCat','uses'=>'FrontendController@GetRegisterQuestionCat']);
Route::post('/GetRegisterQuestionSubCat', ['as'=>'GetRegisterQuestionSubCat','uses'=>'FrontendController@GetRegisterQuestionSubCat']);
Route::post('/GetRegisterQuestionQuestion', ['as'=>'GetRegisterQuestionQuestion','uses'=>'FrontendController@GetRegisterQuestionQuestion']);
Route::post('/DestinationPackages', ['as'=>'DestinationPackages','uses'=>'FrontendController@DestinationPackages']);
Route::post('/DestinationThings', ['as'=>'DestinationThings','uses'=>'FrontendController@DestinationThings']);

Route::get('/Events', ['as'=>'Events','uses'=>'FrontendController@Events']);
Route::post('/EventsMore', ['as'=>'EventsMore','uses'=>'FrontendController@EventsMore']);

Route::get('/CountryCategory', ['as'=>'CountryCategory','uses'=>'FrontendController@CountryCategory']);
Route::post('/CountryDestination', ['as'=>'CountryDestination','uses'=>'FrontendController@CountryDestination']);
Route::post('/CountryMore', ['as'=>'CountryMore','uses'=>'FrontendController@CountryMore']);
Route::post('/AddAttendant', ['as'=>'AddAttendant','uses'=>'FrontendController@AddAttendant']);
Route::post('/CountEventAttendantGoing', ['as'=>'CountEventAttendantGoing','uses'=>'FrontendController@CountEventAttendantGoing']);
Route::post('/CountEventAttendantInterested', ['as'=>'CountEventAttendantInterested','uses'=>'FrontendController@CountEventAttendantInterested']);
Route::post('/ListEventGoing', ['as'=>'ListEventGoing','uses'=>'FrontendController@ListEventGoing']);
Route::get('/GetIgImages', ['as' => 'GetIgImages', 'uses' => 'FrontendController@GetIgImages']);
Route::post('/GetIgImages', ['as' => 'GetIgImages', 'uses' => 'FrontendController@GetIgImages']);
Route::get('/IgChoosedPic', ['as' => 'IgChoosedPic', 'uses' => 'FrontendController@IgChoosedPic']);
Route::get('/IgChoosedPictures', ['as' => 'IgChoosedPictures', 'uses' => 'FrontendController@IgChoosedPictures']);
Route::get('/PressRelease', ['as' => 'PressRelease', 'uses' => 'FrontendController@PressRelease']);
Route::post('/MorePressRelease', ['as' => 'MorePressRelease', 'uses' => 'FrontendController@MorePressRelease']);





