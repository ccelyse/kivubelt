<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//
//Route::get('/', function () {
//    return view('welcome');
//});

Route::get('/',['as'=>'welcome','uses'=>'FrontendController@NewHome']);
Route::get('/login',['as'=>'welcome','uses'=>'FrontendController@Login']);
Route::get('/AboutRwanda',['as'=>'AboutRwanda','uses'=>'FrontendController@AboutRwanda']);
Route::get('/AboutRegion',['as'=>'AboutRegion','uses'=>'FrontendController@AboutRegion']);

Route::get('/DiscoverK',['as'=>'ThingToDo','uses'=>'FrontendController@ThingToDos']);
Route::get('/Impressions',['as'=>'Impressions','uses'=>'FrontendController@Impressions']);
Route::get('/MoreDiscoverActivity',['as'=>'MoreDiscoverActivity','uses'=>'FrontendController@MoreDiscoverActivity']);
Route::get('/MoreDiscover',['as'=>'MoreDiscover','uses'=>'FrontendController@MoreThingsDo']);
Route::get('/FStay',['as'=>'Stay','uses'=>'FrontendController@FStay']);
Route::get('/MoreStay',['as'=>'MoreStay','uses'=>'FrontendController@MoreStay']);
Route::get('/ContactUs',['as'=>'ContactUs','uses'=>'FrontendController@ContactUs']);
Route::get('/EatAndDrink',['as'=>'EatAndDrink','uses'=>'FrontendController@EatAndDrink']);
Route::get('/MoreEatAndDrink',['as'=>'MoreEatAndDrink','uses'=>'FrontendController@MoreEatAndDrink']);
Route::post('/SignIn_', ['as' => 'backend.SignIn_', 'uses' => 'BackendController@SignIn_']);
Route::post('/SignIn', ['as' => 'backend.SignIn', 'uses' => 'BackendController@SignIn']);
Route::get('/LoginAT', ['as' => 'backend.LoginAT', 'uses' => 'BackendController@LoginAT']);
Route::get('/Test', ['as'=>'backend.Login','uses'=>'FrontendController@Login']);
Route::get('/ViewDestinations', ['as'=>'ViewDestinations','uses'=>'FrontendController@ViewDestinations']);
Route::get('/GetIgImages', ['as'=>'GetIgImages','uses'=>'BackendController@GetIgImages']);


Route::get('/ViewPlaces', ['as'=>'ViewPlaces','uses'=>'FrontendController@ViewPlaces']);



Auth::routes();

Route::group(['middleware' => ['auth']], function () {
    Route::group(['middleware' => 'disablepreventback'],function(){
        Route::get('/home', 'HomeController@index')->name('home');


        Route::get('/Dashboard', ['as' => 'backend.Dashboard', 'uses' => 'BackendController@Dashboard']);
        Route::get('/MarkAsRead', ['as' => 'backend.MarkAsRead', 'uses' => 'BackendController@MarkAsRead']);
        Route::get('/AgentAccount', ['as' => 'backend.AgentAccount', 'uses' => 'BackendController@AgentAccount']);
        Route::post('/EditAgentAccount', ['as' => 'backend.EditAgentAccount', 'uses' => 'BackendController@EditAgentAccount']);
        Route::get('/AgentProfile', ['as' => 'backend.AgentProfile', 'uses' => 'BackendController@AgentProfile']);
        Route::post('/AddAgentProfilePhoto', ['as' => 'backend.AddAgentProfilePhoto', 'uses' => 'BackendController@AddAgentProfilePhoto']);
        Route::post('/UpdateAgentProfilePhoto', ['as' => 'backend.UpdateAgentProfilePhoto', 'uses' => 'BackendController@UpdateAgentProfilePhoto']);
        Route::post('/AddAgentCoverPhoto', ['as' => 'backend.AddAgentCoverPhoto', 'uses' => 'BackendController@AddAgentCoverPhoto']);
        Route::post('/UpdateAgentCoverPhoto', ['as' => 'backend.UpdateAgentCoverPhoto', 'uses' => 'BackendController@UpdateAgentCoverPhoto']);
        Route::post('/AddAgentProfile', ['as' => 'backend.AddAgentProfile', 'uses' => 'BackendController@AddAgentProfile']);
        Route::post('/AddMoreImagesAgentProfile', ['as' => 'backend.AddMoreImagesAgentProfile', 'uses' => 'BackendController@AddMoreImagesAgentProfile']);
        Route::get('/DeleteAgentImages', ['as' => 'backend.DeleteAgentImages', 'uses' => 'BackendController@DeleteAgentImages']);
        Route::post('/AddMoreVideosAgentProfile', ['as' => 'backend.AddMoreVideosAgentProfile', 'uses' => 'BackendController@AddMoreVideosAgentProfile']);
        Route::get('/DeleteAgentVideos', ['as' => 'backend.DeleteAgentVideos', 'uses' => 'BackendController@DeleteAgentVideos']);
        Route::post('/AddMoreAgentService', ['as' => 'backend.AddMoreAgentService', 'uses' => 'BackendController@AddMoreAgentService']);
        Route::get('/DeleteAgentServices', ['as' => 'backend.DeleteAgentServices', 'uses' => 'BackendController@DeleteAgentServices']);
        Route::post('/EditAgentOther', ['as' => 'backend.EditAgentOther', 'uses' => 'BackendController@EditAgentOther']);
        Route::get('/DeleteAgentOther', ['as' => 'backend.DeleteAgentOther', 'uses' => 'BackendController@DeleteAgentOther']);

        Route::post('/AddAgentCompanyAddress', ['as' => 'backend.AddAgentCompanyAddress', 'uses' => 'BackendController@AddAgentCompanyAddress']);
        Route::post('/UpdateAgentCompanyAddress', ['as' => 'backend.UpdateAgentCompanyAddress', 'uses' => 'BackendController@UpdateAgentCompanyAddress']);
        Route::post('/AddAgentCompanyWebsite', ['as' => 'backend.AddAgentCompanyWebsite', 'uses' => 'BackendController@AddAgentCompanyWebsite']);
        Route::post('/UpdateAgentCompanyWebsite', ['as' => 'backend.UpdateAgentCompanyWebsite', 'uses' => 'BackendController@UpdateAgentCompanyWebsite']);
        Route::post('/AddAgentCompanyPhone', ['as' => 'backend.AddAgentCompanyPhone', 'uses' => 'BackendController@AddAgentCompanyPhone']);
        Route::post('/UpdateAgentCompanyPhone', ['as' => 'backend.UpdateAgentCompanyPhone', 'uses' => 'BackendController@UpdateAgentCompanyPhone']);
        Route::post('/AddAgentCompanyAbout', ['as' => 'backend.AddAgentCompanyAbout', 'uses' => 'BackendController@AddAgentCompanyAbout']);
        Route::post('/UpdateAgentCompanyAbout', ['as' => 'backend.UpdateAgentCompanyAbout', 'uses' => 'BackendController@UpdateAgentCompanyAbout']);
        Route::get('/Event', ['as' => 'backend.Event', 'uses' => 'BackendController@Event']);
        Route::post('/AddEvent', ['as' => 'backend.AddEvent', 'uses' => 'BackendController@AddEvent']);
        Route::post('/EditEvent', ['as' => 'backend.EditEvent', 'uses' => 'BackendController@EditEvent']);
        Route::get('/DeleteEvent', ['as' => 'backend.DeleteEvent', 'uses' => 'BackendController@DeleteEvent']);

        Route::get('/OperatorAccount', ['as' => 'backend.OperatorAccount', 'uses' => 'BackendController@OperatorAccount']);
        Route::get('/AgentTourOperators', ['as' => 'backend.AgentTourOperators', 'uses' => 'BackendController@AgentTourOperators']);
        Route::post('/EditAgentTourOperators', ['as' => 'backend.EditAgentTourOperators', 'uses' => 'BackendController@EditAgentTourOperators']);
        Route::get('/TourOperatorProfile', ['as' => 'backend.TourOperatorProfile', 'uses' => 'BackendController@TourOperatorProfile']);
        Route::post('/AddTourOperatorProfile', ['as' => 'backend.AddTourOperatorProfile', 'uses' => 'BackendController@AddTourOperatorProfile']);
        Route::post('/AddMoreTourOperatorProfileService', ['as' => 'backend.AddMoreTourOperatorProfileService', 'uses' => 'BackendController@AddMoreTourOperatorProfileService']);
        Route::post('/AddMoreVideosTourOperatorProfile', ['as' => 'backend.AddMoreVideosTourOperatorProfile', 'uses' => 'BackendController@AddMoreVideosTourOperatorProfile']);
        Route::get('/DeleteTourOperatorProfileServices', ['as' => 'backend.DeleteTourOperatorProfileServices', 'uses' => 'BackendController@DeleteTourOperatorProfileServices']);
        Route::get('/DeleteTourOperatorProfileVideos', ['as' => 'backend.DeleteTourOperatorProfileVideos', 'uses' => 'BackendController@DeleteTourOperatorProfileVideos']);
        Route::post('/AddMoreImagesTourOperatorProfile', ['as' => 'backend.AddMoreImagesTourOperatorProfile', 'uses' => 'BackendController@AddMoreImagesTourOperatorProfile']);
        Route::get('/DeleteTourOperatorProfileImages', ['as' => 'backend.DeleteTourOperatorProfileImages', 'uses' => 'BackendController@DeleteTourOperatorProfileImages']);
        Route::post('/EditTourOperatorProfileOther', ['as' => 'backend.EditTourOperatorProfileOther', 'uses' => 'BackendController@EditTourOperatorProfileOther']);
        Route::get('/DeleteTourOperatorProfileOther', ['as' => 'backend.DeleteTourOperatorProfileOther', 'uses' => 'BackendController@DeleteTourOperatorProfileOther']);

        Route::get('/Experience', ['as' => 'backend.Experience', 'uses' => 'BackendController@Experience']);
        Route::post('/AddActivity', ['as' => 'backend.AddActivity', 'uses' => 'BackendController@AddActivity']);
        Route::get('/ActivityDetails', ['as' => 'backend.ActivityDetails', 'uses' => 'BackendController@ActivityDetails']);

        Route::get('/Dashboard', ['as' => 'backend.Dashboard', 'uses' => 'BackendController@Dashboard']);
        Route::get('/AccountList', ['as' => 'backend.AccountList', 'uses' => 'BackendController@AccountList']);
        Route::get('/CreateAccount', ['as' => 'backend.CreateAccount', 'uses' => 'BackendController@CreateAccount']);
        Route::post('/CreateAccount_', ['as' => 'backend.CreateAccount', 'uses' => 'BackendController@CreateAccount_']);
        Route::post('/UpdateAccountInfo', ['as' => 'backend.UpdateAccountInfo', 'uses' => 'BackendController@UpdateAccountInfo']);
        Route::get('/DeleteAccount', ['as' => 'backend.DeleteAccount', 'uses' => 'BackendController@DeleteAccount']);

        Route::get('/ListOfTourismAgents', ['as' => 'backend.ListOfTourismAgents', 'uses' => 'BackendController@ListOfTourismAgents']);
        Route::post('/AddTourismAgent', ['as' => 'backend.AddTourismAgent', 'uses' => 'BackendController@AddTourismAgent']);
        Route::get('/ListOfTourOperators', ['as' => 'backend.ListOfTourOperators', 'uses' => 'BackendController@ListOfTourOperators']);
        Route::post('/AddTourOperators', ['as' => 'backend.AddTourOperators', 'uses' => 'BackendController@AddTourOperators']);

        Route::get('/Destinations', ['as' => 'backend.Destinations', 'uses' => 'BackendController@Destinations']);
        Route::get('/CountryProfiles', ['as' => 'backend.CountryProfile', 'uses' => 'BackendController@CountryProfile']);
        Route::post('/AddCountryProfile', ['as' => 'backend.AddCountryProfile', 'uses' => 'BackendController@AddCountryProfile']);
        Route::post('/EditCountryProfile', ['as' => 'backend.EditCountryProfile', 'uses' => 'BackendController@EditCountryProfile']);
        Route::post('/AddDestination', ['as' => 'backend.AddDestination', 'uses' => 'BackendController@AddDestination']);
        Route::post('/EditDestination', ['as' => 'backend.EditDestination', 'uses' => 'BackendController@EditDestination']);
        Route::get('/DeleteDestination', ['as' => 'backend.DeleteDestination', 'uses' => 'BackendController@DeleteDestination']);
        Route::post('/AddDestinationthingstodo', ['as' => 'backend.AddDestinationthingstodo', 'uses' => 'BackendController@AddDestinationthingstodo']);

        Route::get('/ThingToDo', ['as' => 'backend.Places', 'uses' => 'BackendController@Places']);
        Route::post('/AddPlaces', ['as' => 'backend.AddPlaces', 'uses' => 'BackendController@AddPlaces']);
        Route::post('/EditPlaces', ['as' => 'backend.EditPlaces', 'uses' => 'BackendController@EditPlaces']);
        Route::get('/PlacesImages', ['as' => 'backend.PlacesImages', 'uses' => 'BackendController@PlacesImages']);
        Route::get('/DeletePlaces', ['as' => 'backend.DeletePlaces', 'uses' => 'BackendController@DeletePlaces']);
        Route::post('/PlacesMoreImages', ['as' => 'backend.PlacesMoreImages', 'uses' => 'BackendController@PlacesMoreImages']);
        Route::get('/DeletePlacesImages', ['as' => 'backend.DeletePlacesImages', 'uses' => 'BackendController@DeletePlacesImages']);

        Route::get('/DiscoverActivity', ['as' => 'backend.DiscoverActivity', 'uses' => 'BackendController@DiscoverActivity']);
        Route::post('/AddDiscoverActivity', ['as' => 'backend.AddDiscoverActivity', 'uses' => 'BackendController@AddDiscoverActivity']);
        Route::post('/EditDiscoverActivity', ['as' => 'backend.EditDiscoverActivity', 'uses' => 'BackendController@EditDiscoverActivity']);
        Route::get('/DeleteDiscoverActivity', ['as' => 'backend.DeleteDiscoverActivity', 'uses' => 'BackendController@DeleteDiscoverActivity']);

        Route::get('/HomeRowOne', ['as' => 'backend.HomeRowOne', 'uses' => 'BackendController@HomeRow1']);
        Route::post('/AddHomeRow1', ['as' => 'backend.AddHomeRow1', 'uses' => 'BackendController@AddHomeRow1']);
        Route::post('/EditHomeRow1', ['as' => 'backend.EditHomeRow1', 'uses' => 'BackendController@EditHomeRow1']);

        Route::get('/HomeRowTwo', ['as' => 'backend.HomeRowTwo', 'uses' => 'BackendController@HomeRow2']);
        Route::post('/AddHomeRow2', ['as' => 'backend.AddHomeRow2', 'uses' => 'BackendController@AddHomeRow2']);
        Route::post('/EditHomeRow2', ['as' => 'backend.EditHomeRow2', 'uses' => 'BackendController@EditHomeRow2']);


        Route::get('/HomeRowThree', ['as' => 'backend.HomeRowThree', 'uses' => 'BackendController@HomeRow3']);
        Route::post('/AddHomeRow3', ['as' => 'backend.AddHomeRow3', 'uses' => 'BackendController@AddHomeRow3']);
        Route::post('/EditHomeRow3', ['as' => 'backend.EditHomeRow3', 'uses' => 'BackendController@EditHomeRow3']);

        Route::get('/AboutRwandaRowOne', ['as' => 'backend.AboutRwandaRowOne', 'uses' => 'BackendController@AboutRwandaRow1']);
        Route::post('/AddAboutRwandaRowOne', ['as' => 'backend.AddAboutRwandaRowOne', 'uses' => 'BackendController@AddAboutRwandaRowOne']);
        Route::post('/EditAboutRwandaRowOne', ['as' => 'backend.EditAboutRwandaRowOne', 'uses' => 'BackendController@EditAboutRwandaRowOne']);

        Route::get('/AboutRwandaRowTwo', ['as' => 'backend.AboutRwandaRowTwo', 'uses' => 'BackendController@AboutRwandaRow2']);
        Route::post('/AddAboutRwandaRow2', ['as' => 'backend.AddAboutRwandaRow2', 'uses' => 'BackendController@AddAboutRwandaRow2']);
        Route::post('/EditAboutRwandaRow2', ['as' => 'backend.EditAboutRwandaRow2', 'uses' => 'BackendController@EditAboutRwandaRow2']);

        Route::get('/AboutRegionRowOne', ['as' => 'backend.AboutRegionRowOne', 'uses' => 'BackendController@AboutRegionRowOne']);
        Route::post('/AddAboutRegionRow1', ['as' => 'backend.AddAboutRegionRow1', 'uses' => 'BackendController@AddAboutRegionRow1']);
        Route::post('/EditAboutRegionRow1', ['as' => 'backend.EditAboutRegionRow1', 'uses' => 'BackendController@EditAboutRegionRow1']);

        Route::get('/AboutRegionRowTwo', ['as' => 'backend.AboutRegionRowTwo', 'uses' => 'BackendController@AboutRegionRowTwo']);
        Route::post('/AddAboutRegionRowTwo', ['as' => 'backend.AddAboutRegionRowTwo', 'uses' => 'BackendController@AddAboutRegionRowTwo']);
        Route::post('/EditAboutRegionRowTwo', ['as' => 'backend.EditAboutRegionRowTwo', 'uses' => 'BackendController@EditAboutRegionRowTwo']);

        Route::get('/Stay', ['as' => 'backend.Stay', 'uses' => 'BackendController@Stay']);
        Route::post('/AddStay', ['as' => 'backend.AddStay', 'uses' => 'BackendController@AddStay']);
        Route::post('/EditStay', ['as' => 'backend.EditStay', 'uses' => 'BackendController@EditStay']);
        Route::get('/DeleteStay', ['as' => 'backend.DeleteStay', 'uses' => 'BackendController@DeleteStay']);

        Route::get('/AddEatAndDrink', ['as' => 'backend.AddEatAndDrink', 'uses' => 'BackendController@AddEatAndDrink']);
        Route::post('/UploadEatAndDrink', ['as' => 'backend.UploadEatAndDrink', 'uses' => 'BackendController@UploadEatAndDrink']);
        Route::post('/EditEatAndDrink', ['as' => 'backend.EditEatAndDrink', 'uses' => 'BackendController@EditEatAndDrink']);
        Route::get('/DeleteEatAndDrink', ['as' => 'backend.DeleteEatAndDrink', 'uses' => 'BackendController@DeleteEatAndDrink']);

        Route::get('/ActivityCategory', ['as' => 'backend.ActivityCategory', 'uses' => 'BackendController@ActivityCategory']);
        Route::post('/AddActivityCategory', ['as' => 'backend.AddActivityCategory', 'uses' => 'BackendController@AddActivityCategory']);
        Route::post('/EditActivityCategory', ['as' => 'backend.EditActivityCategory', 'uses' => 'BackendController@EditActivityCategory']);
        Route::get('/AllActivity', ['as' => 'backend.AllActivity', 'uses' => 'BackendController@AllActivity']);

        Route::get('/RegisteringProcess', ['as' => 'backend.RegisteringProcess', 'uses' => 'BackendController@RegisteringProcess']);
        Route::post('/AddRegisteringProcess', ['as' => 'backend.AddRegisteringProcess', 'uses' => 'BackendController@AddRegisteringProcess']);
        Route::get('/DeleteRegisteringProcess', ['as' => 'backend.DeleteRegisteringProcess', 'uses' => 'BackendController@DeleteRegisteringProcess']);

        Route::get('/Gallery', ['as' => 'backend.Gallery', 'uses' => 'BackendController@Gallery']);
        Route::get('/IgApi', ['as' => 'backend.IgApi', 'uses' => 'BackendController@IgApi']);
        Route::post('/AddGallery', ['as' => 'backend.AddGallery', 'uses' => 'BackendController@AddGallery']);
        Route::post('/AddIGGallery', ['as' => 'backend.AddIGGallery', 'uses' => 'BackendController@AddIGGallery']);
        Route::get('/DeleteGallery', ['as' => 'backend.DeleteGallery', 'uses' => 'BackendController@DeleteGallery']);
        Route::get('/DeleteIGGallery', ['as' => 'backend.DeleteIGGallery', 'uses' => 'BackendController@DeleteIGGallery']);

        Route::get('/CompanyTypes', ['as' => 'backend.CompanyTypes', 'uses' => 'BackendController@CompanyTypes']);
        Route::post('/AddCompanyTypes', ['as' => 'backend.AddCompanyTypes', 'uses' => 'BackendController@AddCompanyTypes']);
        Route::get('/DeleteCompanyTypes', ['as' => 'backend.DeleteCompanyTypes', 'uses' => 'BackendController@DeleteCompanyTypes']);
        Route::post('/EditCompanyTypes', ['as' => 'backend.EditCompanyTypes', 'uses' => 'BackendController@EditCompanyTypes']);

        Route::get('/MarketSegments', ['as' => 'backend.MarketSegments', 'uses' => 'BackendController@MarketSegments']);
        Route::post('/AddMarketSegments', ['as' => 'backend.AddMarketSegments', 'uses' => 'BackendController@AddMarketSegments']);
        Route::post('/EditMarketSegments', ['as' => 'backend.EditMarketSegments', 'uses' => 'BackendController@EditMarketSegments']);
        Route::get('/DeleteMarketSegments', ['as' => 'backend.DeleteMarketSegments', 'uses' => 'BackendController@DeleteMarketSegments']);

        Route::post('/AddAgentMarketSegment', ['as' => 'backend.AddAgentMarketSegment', 'uses' => 'BackendController@AddAgentMarketSegment']);
        Route::post('/UploadAgentMarketSegment', ['as' => 'backend.UploadAgentMarketSegment', 'uses' => 'BackendController@UploadAgentMarketSegment']);
        Route::post('/AddAgentCompanyType', ['as' => 'backend.AddAgentCompanyType', 'uses' => 'BackendController@AddAgentCompanyType']);
        Route::post('/EditAgentCompanyType', ['as' => 'backend.EditAgentCompanyType', 'uses' => 'BackendController@EditAgentCompanyType']);
        Route::post('/EditAgentMarketSegment', ['as' => 'backend.EditAgentMarketSegment', 'uses' => 'BackendController@EditAgentMarketSegment']);

        Route::get('/PartnerFinder', ['as' => 'backend.PartnerFinder', 'uses' => 'BackendController@PartnerFinder']);
        Route::post('/SearchUserData', ['as' => 'backend.SearchUserData', 'uses' => 'BackendController@SearchUserData']);
        Route::get('/PressReleases', ['as' => 'backend.PressReleases', 'uses' => 'BackendController@PressRelease']);
        Route::post('/AddPressRelease', ['as' => 'backend.AddPressRelease', 'uses' => 'BackendController@AddPressRelease']);
        Route::post('/EditPressRelease', ['as' => 'backend.EditPressRelease', 'uses' => 'BackendController@EditPressRelease']);
        Route::get('/DeletePressRelease', ['as' => 'backend.DeletePressRelease', 'uses' => 'BackendController@DeletePressRelease']);

    });
});





